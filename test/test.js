let tests = [
	{src:"test/_.js"},
	{src:"test/_.get_set.js"},
	{src:"test/_.on.js"},
	{src:"test/_.rule.js"}
]

/*
 * var t = new test()
 * 
 * t.add("testing _.flow",  function(next, error){
 *   // error("something went wrong")
 *   next()
 * })
 * 
 * t.run( alert )
 */
function test(opts){
  this.node   = typeof process != undefined && typeof process != "undefined"
  this.tests  = this.tests || []
  this.errors = 0
  this.error  = (msg) => { this.errors += 1; console.error("> error: "+msg) }
  this.add    = (description, cb) => this.tests.push({ description, cb })
  this.done   = (ready) => { console.log("\n> tests : "+this.tests.length+"\n> errors: "+this.errors); if( this.node ) process.exit( this.errors == 0 ? 0 : 1); ready(this) }
  this.run    = (ready) => {
	  var p = Promise.resolve()
	  var runTest = (i) => {
		  return new Promise( (resolve, reject) => {
			  var test = this.tests[i]
			  if( !test ) return this.done(ready)
			  var printf  = this.node ? process.stdout.write.bind(process.stdout) : console.log
			  if( this.node ) printf("[ ] "+test.description+"\r")
			  var onError = (err) => { this.error(err); this.done(ready) }
			  var _next   = () => { printf("[✓] "+test.description+"\n"); p.then(runTest(i+1)) }
			  try { test.cb(_next, onError ) } catch (e) { onError(e) }
		  })
	  }
	  p.then( runTest(0) )
  }
  return this
}
 
window.test = new test()
_.load( tests, function(){
  setTimeout( () => {
	  test.run( () => {

	  })
  }, 1000)
})()

