test.add("trying _.set _.get",  function(next, error){
  var a = _({a:1})
  a.set('a',2)
  if( a.a != 2  ) error('a not 2')
  next()
})

test.add("trying _.set _.get ({a:{}})",  function(next, error){
  var a = _({a:{}})
  a.on('set',(set,k,v)=> {
	set(k,v)
  })
  a.set('a.b.c',2)
  if( a.get('a.b.c') != 2  ) error('a not 2')
  next()
})

test.add("trying _.set _.get ({})",  function(next, error){
  var a = _({})
  a.on('set',(set,k,v)=> {
	set(k,v)
  })
  a.set('a.b.c',2)
  if( a.get('a.b.c') != 2  ) error('a not 2')
  next()
})

test.add("trying _.set _.get (nested+default)",  function(next, error){
  var a = _({a:1})
  if( a.get('a.b.c',2) != 2  ) error('a not 2')
  next()
})

