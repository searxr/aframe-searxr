test.add("listen to 'foo'-calls",  function(next, error){
  var a = _({a:1})
  a.foo = (k, v) => a[k] = v 
  a.on('foo', function(foo, k, v){
	a.called = true
	foo(k, v)
  })
  a.foo('a', 2) 
  if( !a.called ) error('called not set')
  if( a.a != 2  ) error('a not 2')
  next()
})

test.add("listen to '_.set'-calls",  function(next, error){
  var a = _({a:1})
  a.on('set', function(foo, k, v){
	a.called = true
	foo(k, v)
  })
  a.set('a', 2) 
  if( !a.called ) error('called not set')
  if( a.a != 2  ) error('a not 2')
  next()
})
