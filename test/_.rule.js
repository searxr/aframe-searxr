test.add("test _.rule() engine",  function(next, error){
  var x = {a:1}
  x.foo  = (f) => true 
  
  // add composable conditions & actions
  x.is_date  = (date) => new Date() // fictional date check
  x.do_thing = (info) => x.thing_happened = true
  
  // optional: add schemas for gui composability
  x.is_date.schema  = [{type:"string",format:"date",title:"Date"}]
  x.do_thing.schema = [{type:"string",title:"Info"}]
  
  // add json rule
  _.rule( x, {
    title: 'at specific date do_thing',
    at:   'foo',
    if:   ['is_date','2017-21-01'],
    then: ['do_thing','thing happened!']
  })
 
  x.log = () => true // suppress output
  if( !x.triggers().length   ) error('empty triggers')
  if( !x.conditions().length ) error('empty conditions')
  if( !x.actions().length    ) error('empty actions')
  x.foo() // foo! thing happened! 
  if( !x.thing_happened ) error('thing did not happen')
  next()
})
