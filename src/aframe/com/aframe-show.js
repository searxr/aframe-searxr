AFRAME.utils.objs = {}
AFRAME.registerComponent('show', {
  schema:{
    "init": {type:'boolean',  default:true }, 
    "VR": {type:'boolean',  default:true }, 
    "AR": {type:'boolean',  default:true }, 
  }, 
  // Set this object invisible while in VR mode.
  init: function () {
    this.el.sceneEl.addEventListener('enter-vr', (ev) => {
      if (this.el.sceneEl.is('vr-mode')) this.el.setAttribute('visible',this.data.VR);
      if (this.el.sceneEl.is('ar-mode')) this.el.setAttribute('visible',this.data.AR);
    });
    this.el.sceneEl.addEventListener('exit-vr', (ev) => {
      this.el.setAttribute('visible', this.data.init )
    });
    this.el.setAttribute('visible', this.data.init )
  }
});
