AFRAME.registerComponent('thumbscroller', {

    init: function () {
      this.el.addEventListener('thumbstickmoved',  this.logThumbstick);
      this.data.xyscroll = document.querySelector("a-xywindow")
      console.log("scroller inited")
    }, 
    logThumbstick: function (evt) {
      if (evt.detail.y > 0.95) { 
        console.log("DOWN"); 
        let y = this.data.xyscroll.components.xyscroll._scrollY - 5;
        this.data.xyscroll.components.xyscroll.setScroll(0, y)
      }
      if (evt.detail.y < -0.95) { 
        console.log("UP"); 
        let y = this.data.xyscroll.components.xyscroll._scrollY + 5;
        this.data.xyscroll.components.xyscroll.setScroll(0, y)
      }
      if (evt.detail.x < -0.95) { console.log("LEFT"); }
      if (evt.detail.x > 0.95) { console.log("RIGHT"); }
    } 

});
