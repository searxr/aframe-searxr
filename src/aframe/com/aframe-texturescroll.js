AFRAME.registerComponent('texturescroll', {
  schema:{
    names: {type:'array'},
    amplitude: {type:'float'},
    speed: {type:'float'}
  },
  init: function(){
    this.SCROLL_X = 1
    this.SCROLL_Y = 2
    this.SCROLL_X_Y = 3
    this.SCROLL_X_SIN= 4
    this.SCROLL_Y_SIN = 5
    this.SCROLL_X_Y_SIN = 6
    this.update()
    this.el.addEventListener('model-loaded', () => this.update())
  },
  update: function(){
    this.objs = []
    let mesh = this.el.getObject3D("mesh") || this.el.object3D
    this.data.names.map( (str) => {
      let name = str.replace(/:.*/,'')
      let type = str.replace(/.*:/,'')
      let el = mesh.getObjectByName(name) 
      if( el && !this.objs[el.name] ){
        el.scrolltype = parseInt(type) || 6
        this.objs[el.name] = el 
      }
    })
  },
  tick: function(time,timeDelta){
    for( let i in this.objs ){
      let obj = this.objs[i]
      if( !obj.material ){
        console.warn('texturescroll: could not find object '+i)
        continue
      }
      let mats = obj.material.length ? obj.material : [obj.material] // support multilayered textures
      mats.map( (mat) => {
        if( obj.scrolltype == this.SCROLL_X_SIN || obj.scrolltype == this.SCROLL_X_Y_SIN ) 
          mat.map.offset.x = 1.0 + ( Math.sin( this.data.speed * (time /10000) ) * this.data.amplitude )
        if( obj.scrolltype == this.SCROLL_Y_SIN || obj.scrolltype == this.SCROLL_X_Y_SIN ) 
          mat.map.offset.y = 1.0 + ( Math.cos( this.data.speed * (time /10000) ) * this.data.amplitude )
        if( obj.scrolltype == this.SCROLL_X || obj.scrolltype == this.SCROLL_X_Y ) 
          mat.map.offset.x = ( this.data.speed * (time /10000) ) * this.data.amplitude
        if( obj.scrolltype == this.SCROLL_Y || obj.scrolltype == this.SCROLL_X_Y ) 
          mat.map.offset.y = ( this.data.speed * (time /10000) ) * this.data.amplitude
      })
    }
  }
});
