// alternative to .emit() to emit promise to turn listeners into controllers 
//
//   $('a-scene').addEventListener('abc', (e) => {
//     // let promise = e.detail.promise()
//     console.log("hello")
//     // promise.resolve()
//     // promise.error('no')
//   })
//
//   AFRAME.registerComponent('mycomponent', {
//
//     init: function(){
//       AFRAME.utils.emitPromise('abc', {title:'foo'}, this )
//       .then( ()  => console.log("abc fired") )
//       .then( (e) => console.error(e)         )  // 'no' 
//     }
//   }
AFRAME.utils.emitPromise = function(event, opts, el){ 
  return new Promise( (resolve, reject) => { 
    opts.promise = () => { 
      opts.promise.halted = true      
      return { resolve, reject }      
    }
    el.emit(e, opts)     
    if( !opts.promise.halted ) resolve()
  })
}    
