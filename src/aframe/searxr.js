/* SPDX-License-Identifier: CPAL-1.0                    */
/* Copyright Contributors to the aframe-searxr project. */

AFRAME.registerComponent('searxr', {
    init: function () {

      let scene = $('a-scene')

      const myscene = (enable) => () => {
        if( enable ){
          let music = document.querySelector("#bg")
          music.volume = 0.8
          music.play()
        }else document.querySelector("#bg").pause()
      }
      scene.addEventListener("enter-vr", myscene(true) )
      scene.addEventListener("enter-ar", myscene(true) )
      scene.addEventListener("exit-vr",  myscene(false) )
      scene.addEventListener("exit-ar",  myscene(false) )

      const initMenus = () => {
        let menus = [...document.querySelectorAll('.sidemenu') ]
        menus.map( (menu) => {
          let btn = menu.querySelector(".sidemenu__btn")
          menu.toggle = function(){
            btn.active    = btn.active ? false : true
            btn.className = btn.active ? 'sidemenu__btn active' : 'sidemenu__btn'
            let drawer = menu.querySelector("nav")
            drawer.className     = btn.active ? '' : 'hide'
          }
          btn.addEventListener('click', menu.toggle )
        })
        notie.setOptions({alertTime:0.6})
      }

      const initResults = () => {
        let listEl = this.listEl = document.getElementById("results");
        listEl.addEventListener('clickitem', (ev) => {
          let item = this.store.items[ ev.detail.index ]
          if( !item ) return
          this.visit(item)
        })

        let list = listEl.components.xylist;
        list.setAdapter({
          create(parent) {
            return document.createElement("a-entity")
          },
          bind(position, el, items) {
            let data = items[position]
            let rect = el.components.xyrect;
            el.setAttribute("geometry", {width: rect.width * 0.9, height: rect.height * 0.9, depth: 0.1});
            //el.setAttribute("color", ["#CCC", "#DDD", "#EEE"][position % 5]);
            let scale      = 0.9;
            let scaleInner = 0.75;
            el.data =       data
            el.setAttribute("scale", "-1 1 1") // flip normal
            let image = document.createElement("a-image")
            image.setAttribute("position", "0 0 -0.1")
            image.setAttribute("src", "#tile")
            image.setAttribute("width", rect.width*scale )
            image.setAttribute("height", rect.height*scale )
            if( data.img_src ){
              //if( !session || !session.renderState || !session.renderState.layers ){
              let imgEl = document.createElement("a-image")
              imgEl.setAttribute("src", data.img_src )
              imgEl.setAttribute("width", rect.width*scaleInner )
              imgEl.setAttribute("height", rect.height*scaleInner )
              imgEl.setAttribute("position", "0 0 -0.15")
              el.appendChild(imgEl)
console.dir(el)
              //}else{
              //  console.log(`type: quad; src: ${data.img_src}; width: ${itemWidth*scale}; height: ${itemHeight*scale}`)
              //  let img = document.createElement("img")
              //  img.crossOrigin = "anonymous"
              //  img.id = "img"+position
              //  img.onload = function(){
              //    console.log("creating layer")
              //    let el2 = document.createElement("a-entity")
              //    el2.setAttribute("layer", `type: quad; src: #img${position}; width: ${itemWidth*scale}; height: ${itemHeight*scale}`)
              //    el2.setAttribute("position","0 0 0.1")
              //    el.appendChild(el2)
              //  }
              //  $('a-assets').appendChild(img)
              //  img.src = data.img_src
            }
            el.appendChild(image)
            // hide xyrect
            el.object3D.children[0].material.opacity = 0
            el.object3D.children[0].material.transparent = true
          }
        });
      }

      const initURL = () => {
        if( document.location.search ){  
          if( document.location.search.match('q=') ){
            let iframe = document.querySelector('iframe')
            iframe.src = iframe.src + document.location.search
          }else if( document.location.search.match('://') ){ // assume 3D file
            this.openFile({href: document.location.search.substr(1) })
          }
        }
      }

      this.store  = {

        maxItems: 10000,

        items: [],

        load: (cb) => {
          if( localStorage.getItem("store") ){
            let store = JSON.parse( localStorage.getItem("store") )
            if( !store.version || store.version != "0.1" ){
              localStorage.removeItem("store")
              return this.store.load(cb)
            }
            for ( let i in store ) this.store[i] = store[i]
            cb()
          }else{
            fetch('./searxr.json')
            .then( (res) => res.json()       )
            .then( (cfg) => { for(i in cfg) this.store[i] = cfg[i] })
            .then( cb )
          }
        },

        save: () => {
          localStorage.setItem("store", JSON.stringify( this.store ))
        },

        addItem: (data) => {
          this.store.items.unshift(data)
          this.alert({text:"saved to bookmarks", time:0.6})
          this.update()
        },

        removeItem: (i) => {
          this.store.items.splice( i, 1) // removes item without de-referencing alpinejs store
          this.alert({text:"removed from bookmarks", time:0.6})
          this.update()
        },

        alpinejsify: (storekey) => { // turns stores into alpinejs stores to allow dom reactivity
          Alpine.store(storekey, this.store[storekey] ) // update alpine store
          this.store[storekey] = Alpine.store(storekey)
        }
      }

      this.store.load( () => {
        this.store.alpinejsify('items')
        this.enableImmersiveNavigation()
        this.enableIframeCom()
        initMenus();
        initResults()
        initURL()
        this.update()
      })

      //scene.addEventListener("loaded", () => {
      // sadly webxr cannot handle so much layers
      // "Failed to execute 'updateRenderState' on 'XRSession': Too many layers provided in layers array"
      // this.el.sceneEl.renderer.xr.addEventListener( 'sessionstart', () => this.onXR() )
      //})

    },

    enableImmersiveNavigation: function(){
      if ( 'xr' in navigator ) {
        navigator.xr.addEventListener( 'sessiongranted',  () => {
          var scene = document.querySelector('a-scene');
          if (scene.hasLoaded) {
            scene.enterVR();
          } else {
            scene.addEventListener('loaded',  function () {
              scene.enterVR();
            });
          }
        });
      }
    },

    onXR: function(){
      this.session = this.el.sceneEl.renderer.xr.getSession()
      this.session.requestReferenceSpace( 'local' ).then( ( refSpace ) => {
        this.update()
      })
    },

    update: function(){
      if( this.updateId ){ // throttle calls
        clearTimeout(this.updateId)
        this.updateId = setTimeout(this.update, 200)
      }
      this.store.save()                        // save to localstorage
      if( !this.listEl ) return console.warn("this.listEl not exist")
      let items = this.store.items.map( (i) => i )  // clone
      if( items.length < this.store.maxItems ){
        for ( i = 0; items.length < this.store.maxItems; i++  )
          items.push({})
      }
      this.listEl.components.xylist.setContents([])
      this.listEl.components.xylist.setContents(items)
      // hide scrollbarstuff (the laserpointer can do that for now)
      // *TODO* use wearable for cardboard?
      $('a-xywindow').children[0].children[1].object3D.visible = false
      $('a-xywindow').children[1].object3D.visible = false
    },

    search: function(q){
      let url = `https://searxr.me/search?q=${encodeURI(q)}&category_XR=on&language=en-US&time_range=&safesearch=0&theme=searxr&format=json`
      fetch(url)
      .then( (res) => res.json() )
      .then( (data) => {
        data.results.map( (r) => {
          let {title, url, img_src,  engine} = r
          img_src = "https://corsproxy.io/?" + img_src
          this.store.items.push({title, url, img_src, engine})
        })
        this.update()
      })
    },

    visit: function(data){
      if( !data.immersive ){
        if( this.el.sceneEl.renderer.xr.isPresenting )
          this.el.sceneEl.exitVR()
        window.open(data.url, "_blank")
      }else{
        console.log("todo immersive implement")
      }
    },

    addItem: function(data){
      if( data.img_src ) data.img_src = "https://corsproxy.io/?"+data.img_src // proxy
      this.store.addItem(data)
    },

    openFile: function(data){
      let $scene = document.querySelector('[searxr]')
      let $iframe = document.querySelector('iframe')
      if( !AFRAME.XRF.navigator.notfound ){
        AFRAME.XRF.navigator.notfound = () => {
          $iframe.style.display = ''
          alert("no asset found at "+data.href)
        }
        AFRAME.XRF.addEventListener('reset', () => {
          if( $iframe  ) $iframe.style.display = 'none'
          $scene.innerHTML = ''
        })
        AFRAME.XRF.addEventListener('noasset', () => AFRAME.XRF.navigator.notfound() )
      }
      try {
        AFRAME.XRF.navigator.to( data.href )
      } catch (e) { notfound() }
    },

    enableIframeCom: function(){
      let iframe = document.querySelector('iframe')
      let first = () => {
        iframe.removeEventListener('load',first)
        iframe.addEventListener('load', () => {
          let head = document.querySelector('#head')
          if( head ) head.remove()
        })
      }
      iframe.addEventListener('load', first)

      window.addEventListener("message", (e) => {
        if( !e || String(e.data)[0] != '{' ) return; // ignore non json messages
        let data = JSON.parse(e.data)
        console.dir(data)
        if( data.type == "visit" ){
          console.log("visit")
          console.dir(data)
          this.visit(data)
        }
        if( data.type == "xrbookmark" ){
          console.log("bookmark")
          console.dir(data)
          this.addItem(data)
        }
        if( data.type == "file" ){
          console.log("file")
          this.openFile(data)
        }
      })
    },

    alert(i){ notie.alert(i) }
});
