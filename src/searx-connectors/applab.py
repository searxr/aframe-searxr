# SPDX-License-Identifier: AGPL-3.0-or-later
"""
 ExtendedCollection (WebXR)
"""
# pylint: disable=missing-function-docstring

from urllib.parse import urlencode, urlparse
from lxml import html
import re

# about
about = {
    "website": 'https://applabdb.com',
    "use_official_api": False,
    "require_api_key": False,
    "results": 'HTML',
}

# engine dependent config
categories = ['Applab']
paging = False

# search-url
base_url = 'https://applabdb.com/search'
count_url = 'https://applabdb.com/new'

def request(query, params):

    q = urlparse( str(query) ).query
    count = re.findall("count:[0-9]+",q)
    if count:
        count = re.sub(r'.*:','', count[0])
        count = int(count)

    if count:
        params['url'] = count_url + '?' + str(query) # no query
    else:
        params['method'] = 'POST'
        params['data'] = { 'searchterm': 'castle' }
        params['url'] = base_url + '?s=' + str(query) # we'll search the html

    return params

def response(resp):

    results = []

    dom = html.fromstring(resp.text)

    q = urlparse(resp.url).query
    count = re.findall("count:[0-9]+",q)
    if count:
        count = re.sub(r'.*:','', count[0])
        count = int(count)

    for row in dom.xpath('//div[@class="row"]/div'):
        a_tag     = row.xpath('.//div/a')
        img_tag   = row.xpath('.//div/a/img')
        title     = row.xpath('.//h5[@class="card-title"]/text()')[0]
        href      = a_tag[0].attrib.get('href')
        img_src   = img_tag[0].attrib.get('src')
        if count and len(results) >= count :
            continue
        results.append({
            'template': 'webxr.html',
            'url': href, 
            'img_src': img_src, 
            'title': title 
        })

    return results

### test with 'wget https://constructarcade.com -O constructarcade.html'
#with open('extendedcollection.html','r') as file:
#    mock = type('obj', (object,), {
#        'text' : file.read(),
#        'url' : 'https://extendedcollection.com/?s=third'
#    })
#    res = response(mock)
