# SPDX-License-Identifier: AGPL-3.0-or-later
"""
 ExtendedCollection (WebXR)
"""
# pylint: disable=missing-function-docstring

from urllib.parse import urlencode, urlparse
from lxml import html
import re

# about
about = {
    "website": 'https://extendedcollection.com/',
    "use_official_api": False,
    "require_api_key": False,
    "results": 'HTML',
}

# engine dependent config
categories = ['XR','WebXR']
paging = False

# search-url
base_url = 'https://extendedcollection.com'

def request(query, params):

    count = re.findall("count:[0-9]+",query)
    if count:
        count = re.sub(r'.*:','', count[0])
        count = int(count)

    if count:
        params['url'] = base_url + '?' + str(query) # no query
    else:
        params['url'] = base_url + '?s=' + str(query) # we'll search the html
    query =  {
        'q'    : query,
    }

    return params

def response(resp):

    results = []

    dom = html.fromstring(resp.text)

    q = urlparse(resp.url).query
    count = re.findall("count:[0-9]+",q)
    if count:
        count = re.sub(r'.*:','', count[0])
        count = int(count)

    for row in dom.xpath('//article'):
        url_tag = row.xpath('./a')
        cover = row.xpath('.//img')
        title_tag = row.xpath('.//h2[@class="entry-title"]/a/text()')
        if count and len(results) >= count :
            continue
        url   = url_tag[0].attrib.get('href')
        imgUrl= cover[0].attrib.get('src')
        results.append({
            'template': 'webxr.html',
            'url': url, 
            'img_src': imgUrl, 
            'title': title_tag[0]
        })

    return results

#with open('extendedcollection.html','r') as file:
#    mock = type('obj', (object,), {
#        'text' : file.read(),
#        'url' : 'https://extendedcollection.com/?s=third'
#    })
#    res = response(mock)
#    print(res)
