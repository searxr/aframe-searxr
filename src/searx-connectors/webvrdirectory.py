# SPDX-License-Identifier: AGPL-3.0-or-later
"""
 Webvr directory (WebXR)         NOTE: work in progress
"""
# pylint: disable=missing-function-docstring

from urllib.parse import urlencode
from json import loads
from lxml import html
import re

# about
about = {
    "website": 'https://webvr.directory/',
    "use_official_api": False,
    "require_api_key": False,
    "results": 'HTML',
}

# engine dependent config
categories = ['WebXR']
paging = False

# search-url
base_url = 'https://webvr.directory/js/app.js'

def extract_JSON(text):
  match = re.search(r"JSON\.parse\('(.*?)'\)", text)
  if match:
    print(match.group(1))
    return match.group(1)
  else:
    return None

def request(query, params):
    params['url'] = base_url
    query =  {
        'q'    : query,
    }
    return params

def response(resp):

    results = []
    json = extract_JSON(resp.text)
    if( json == None ):
        return results

    print(json)
    items = loads(json)

    #print(library)

    #for row in dom.xpath('//li[@class="site"]'):
    #    a_tag = row.xpath('.//a')
    #    cover = row.xpath('.//a/img[@class="siteImage"]')
    #    title = row.xpath('.//div[@class="siteInfo"]/h3')
    #    if len(a_tag) != 0 and len(cover) != 0:
    #        title = title[0].text.strip()
    #        url   = a_tag[0].attrib.get('href')
    #        imgUrl= cover[0].attrib.get('src')
    #        results.append({
    #            'template': 'webxr.html',
    #            'url': url, 
    #            'img_src': imgUrl, 
    #            'title': title
    #        })
    #        print(url) # + " " + imgUrl + " " + title)


    return results

### test with 'wget https://constructarcade.com -O constructarcade.html'
with open('./webvrdirectory.js','r') as file:
    mock = type('obj', (object,), {
        'text' : file.read(),
        'url' : baseurl 
    })
    res = response(mock)
