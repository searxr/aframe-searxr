# SPDX-License-Identifier: AGPL-3.0-or-later
"""
 Sketchfab (WebXR)
"""

import re
from urllib.parse import urlparse,urlencode
from json import loads
from urllib.parse import urlencode

# about
about = {
    "website": 'https://sketchfab.com',
    "use_official_api": False,
    "require_api_key": False,
    "results": 'JSON',
}

categories = ['Sketchfab']
paging = False

base_url = 'https://sketchfab.com/i/search?q={text}&type=models&sort_by=-relevance'
count_url = 'https://sketchfab.com/i/search?q=PBR&type=models&sort_by=-relevance'

def request(query, params):
    if "count:" in query:
        params['url'] = count_url + '?' + str(query)
    else:
        params['url'] = base_url.format(text=urlencode({'text': query}))
    return params

def response(resp):
    results = []
    search_results = loads(resp.text)
    query = urlparse(resp.url).query

    count = re.findall("count:[0-9]+",query)
    if count:
        count = re.sub(r'.*:','', count[0])
        count = int(count)

    # return empty array if there are no results
    if 'results' not in search_results:
        return []
    for result in search_results.get("results",[]):
        thumbs = result["thumbnails"]["images"]
        if count and len(results) > count:
            continue
        results.append(
            {
                "url": result["viewerUrl"],
                "title": result["name"],
                "img_src": thumbs[int(len(thumbs)/2)]["url"],
                "template": "webxr.html"
            }
        )

    return results[0:5]

## test with 'wget 'https://sketchfab.com/i/search?q=foo&sort_by=-relevance&type=models' -O sketchfab.json
#with open('./sketchfab.json','r') as file:
#    mock = type('obj', (object,), {
#        'text' : file.read(),
#        'url' : 'https://sketchfab.com/i/search?q=foo&sort_by=-relevance&type=models'
#    })
#    res = response(mock)
