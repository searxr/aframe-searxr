# SPDX-License-Identifier: AGPL-3.0-or-later
"""
 Sidequest (WebXR)
"""

import re
from urllib.parse import urlparse,urlencode,quote_plus
from json import loads

# about
about = {
    "website": 'https://sidequestvr.com',
    "use_official_api": False,
    "require_api_key": False,
    "results": 'JSON',
}

categories = ['XR','SideQuest']
paging = False

base_url = 'https://api.sidequestvr.com/v2/search?skip=0&limit=36&sortOn=query_rank&descending=true&type=3&query={text}'

def request(query, params):
    params['url'] = base_url.format(text=quote_plus(query))
    return params

def response(resp):
    results = []
    search_results = loads(resp.text)
    query = urlparse(resp.url).query

    count = search_results['total_result_count']

    # return empty array if there are no results
    if 'results' not in search_results:
        return []
    for result in search_results.get("results",[]):
        thumb = result["content"]["app_banner"]
        if count and len(results) > 20:
            continue
        results.append(
            {
                "url": "https://sidequestvr.com/app/" + result["apps_id"],
                "title": result["content"]["name"],
                "img_src": thumb,
                "template": "webxr.html"
            }
        )

    return results[0:5]

#with open('./sidequestvr.json','r') as file:
#    mock = type('obj', (object,), {
#        'text' : file.read(),
#        'url' : 'https://api.sidequestvr.com/v2/search?skip=0&limit=36&sortOn=query_rank&descending=true&type=3&query=foo'
#    })
#    res = response(mock)
#    print(res)
