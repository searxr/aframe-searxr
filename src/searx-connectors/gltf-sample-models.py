# SPDX-License-Identifier: AGPL-3.0-or-later
"""
 ExtendedCollection (WebXR)
"""
# pylint: disable=missing-function-docstring

from urllib.parse import urlparse
from lxml import html
import re
from pathlib import Path
import pdb

# about
about = {
    "website": 'https://github.com/KhronosGroup/glTF-Sample-Models',
    "use_official_api": False,
    "require_api_key": False,
    "results": 'HTML',
}

# engine dependent config
categories = ['WebXR']
paging = False

# search-url
base_url = 'https://github.com/KhronosGroup/glTF-Sample-Assets/tree/main/Models' 
cdn_url  = 'https://raw.GithubUserContent.com/DRx3D/glTF-Sample-Models/main/Models'

def request(query, params):

    params['url'] = base_url + '?' + str(query) 
    return params

def response(resp):

    results = []

    dom = html.fromstring(resp.text)
    q = urlparse(resp.url).query
    count = re.findall("count:[0-9]+",q)
    if count:
        count = re.sub(r'.*:','', count[0])
        count = int(count)

    for row in dom.xpath('//table[aria-labelledby=folders-and-files]//div[@class=react-directory-truncate]'):
        a_tag = row.xpath('.//td[1]/a')
        id = row.text.rsplit('/', 1)[-1]
        title = a_tag[0].text.strip()
        id = Path( a_tag[0].attrib.get('href') ).name 
        url   = cdn_url + id + "/glTF/" + id + ".gltf"
        imgUrl= cdn_url + id + "/glTF/screenshot/screenshot.jpg"

        if re.search( q, title, re.IGNORECASE ) or (count and len(results) < count):
            results.append({
               'template': 'webxr.html',
               'url': url, 
               'img_src': imgUrl, 
               'title': title
            })

    return results

#### test with 'wget (base_url here) -O gltf-sample-models.html'
#with open('gltf-sample-models.html','r') as file:
#    mock = type('obj', (object,), {
#        'text' : file.read(),
#        'url' : base_url
#    })
#    res = response(mock)
#    print(res)
