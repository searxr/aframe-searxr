# SPDX-License-Identifier: AGPL-3.0-or-later
"""
 HeyVR (WebXR)
"""

import re
from urllib.parse import urlparse,urlencode,quote_plus
from json import loads

# about
about = {
    "website": 'https://heyvr.io',
    "use_official_api": False,
    "require_api_key": False,
    "results": 'JSON',
}

categories = ['XR','WebXR']
paging = False

base_url = 'https://api.heyvr.io/v1/public/arcade/game/search?query={text}'

def request(query, params):
    params['url'] = base_url.format(text=quote_plus(query))
    return params

def response(resp):
    results = []
    search_results = loads(resp.text)
    query = urlparse(resp.url).query

    count = search_results['data']['pagination']['total']

    # return empty array if there are no results
    if 'data' not in search_results:
        return []

    for result in search_results.get("data",[]).get('pagination',{}).get('items',[]):
        if count and len(results) > 20:
            continue
        results.append(
            {
                "url": "https://heyvr.io/arcade/games/" + result["slug"],
                "title": result["title"],
                "img_src": "https://media.heyvr.io"+ result["hero"].replace("\\/", "/"),
                "template": "webxr.html"
            }
        )

    return results[0:5]

#with open('./heyvr.json','r') as file:
#    mock = type('obj', (object,), {
#        'text' : file.read(),
#        'url' : 'https://api.heyvr.io/v1/public/arcade/game/search?query=hover'
#    })
#    res = response(mock)
#    print(res)
#
#
