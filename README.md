## Public instance

> go to [https://searxr.me](https://searxr.me)

see [blog here](https://blog.searxr.me)

## SearXR Usage

SearXR can be installed, used as an API, and selfhosted:

1. Selfhost (see `Selfhosting: easy` below)
1. Write your own WebXR client (use the API: let your app search for `avocado` by doing a HTTP GET `https://api.searxr.me/search?category_WebXR=on&format=json&q=avocado`)
1. APK-file (VR-headset users only): click [here to download the latest `.apk` for android-based VR-devices](https://cdn.sidequestvr.com/file/293362/searxr.apk)
1. Nix/OCI container (see selfhosting: Expert)

> NOTE: SearXR is not a turnkey-solution, it's in beta/demo stage.

## Selfhosting: easy

* clone or download this repository 
* buy a domain somewhere 
* make `public/index.html` the starting point

> Profit! Please make sure to report issues/bugfix in the issuetracker 

## Selfhosting: expert

* Install [SearX](https://github.com/searx/searx)

* [download latest SearXR theme ZIP-file here](https://gitlab.com/searxr/aframe-searxr/-/jobs/artifacts/master/raw/searx-theme.zip?job=searx_template)

For example configuration see `settings.yml`

* Now install the webxr-client as described above (Selfhosting: easy) and change the iframe `src` to your private searx instance-url

##### Nix, OCI-container / Docker 

```shell
alias podman=$(which podman || which docker)
cat example/standalone/index.html | sed 's|<head>|<head><base href="static/themes/searxr/release/"/>|g' > $(pwd)/src/searx-theme/templates/searxr/index-3d.html
podman rm -f searx
podman run -d --name searx -p '8081:8080' \
--volume $(pwd)/src/searx-theme/settings.yml:/usr/local/searx/searx/settings.yml \
--volume $(pwd)/src/searx-theme/static/themes/searxr:/usr/local/searx/searx/static/themes/searxr \
--volume $(pwd)/src/searx-theme/templates/searxr:/usr/local/searx/searx/templates/searxr \
--volume $(pwd)/example/standalone:/usr/local/searx/searx/static/themes/searxr/release \
--volume $(pwd)/dist/searxr.standalone.js:/usr/local/searx/searx/static/themes/searxr/release/js/searxr.standalone.js \
docker.io/searx/searx 
echo "surf to http://localhost:8081/static/themes/searxr/release/index.html"
```

> *TODO* add `docker-compose.yml` and nix-file

## Javascript bundling 


```
-rw-rw-r-- 1 leon leon 102K dec  9 12:04 example/standalone/js/searxr.aframe.js
-rw-rw-r-- 1 leon leon 2,7K dec  9 12:04 example/standalone/style.css

real	0m0,015s
user	0m0,004s
sys	  0m0,011s
```

> this will bundle multiple files below 1 second (without exploding your RPi, unlike webpack)

## Licensing

SearX templates are [AGPL](LICENSE_SEARX) and the WebXR-client [CPAL](LICENSE)
