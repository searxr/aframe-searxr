/* SPDX-License-Identifier: CPAL-1.0                    */
/* Copyright Contributors to the aframe-searxr project. */
// alternative to .emit() to emit promise to turn listeners into controllers 
//
//   $('a-scene').addEventListener('abc', (e) => {
//     // let promise = e.detail.promise()
//     console.log("hello")
//     // promise.resolve()
//     // promise.error('no')
//   })
//
//   AFRAME.registerComponent('mycomponent', {
//
//     init: function(){
//       AFRAME.utils.emitPromise('abc', {title:'foo'}, this )
//       .then( ()  => console.log("abc fired") )
//       .then( (e) => console.error(e)         )  // 'no' 
//     }
//   }
AFRAME.utils.emitPromise = function(event, opts, el){ 
  return new Promise( (resolve, reject) => { 
    opts.promise = () => { 
      opts.promise.halted = true      
      return { resolve, reject }      
    }
    el.emit(e, opts)     
    if( !opts.promise.halted ) resolve()
  })
}    
!function(e,t){"object"==typeof exports&&"object"==typeof module?module.exports=t():"function"==typeof define&&define.amd?define([],t):"object"==typeof exports?exports.notie=t():e.notie=t()}(this,function(){return function(e){function t(s){if(n[s])return n[s].exports;var a=n[s]={i:s,l:!1,exports:{}};return e[s].call(a.exports,a,a.exports,t),a.l=!0,a.exports}var n={};return t.m=e,t.c=n,t.i=function(e){return e},t.d=function(e,n,s){t.o(e,n)||Object.defineProperty(e,n,{configurable:!1,enumerable:!0,get:s})},t.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(n,"a",n),n},t.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},t.p="",t(t.s=1)}([function(e,t){e.exports=function(e){return e.webpackPolyfill||(e.deprecate=function(){},e.paths=[],e.children||(e.children=[]),Object.defineProperty(e,"loaded",{enumerable:!0,get:function(){return e.l}}),Object.defineProperty(e,"id",{enumerable:!0,get:function(){return e.i}}),e.webpackPolyfill=1),e}},function(e,t,n){"use strict";(function(e){var n,s,a,i="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e};!function(c,o){"object"===i(t)&&"object"===i(e)?e.exports=o():(s=[],n=o,a="function"==typeof n?n.apply(t,s):n,!(void 0!==a&&(e.exports=a)))}(void 0,function(){return function(e){function t(s){if(n[s])return n[s].exports;var a=n[s]={i:s,l:!1,exports:{}};return e[s].call(a.exports,a,a.exports,t),a.l=!0,a.exports}var n={};return t.m=e,t.c=n,t.i=function(e){return e},t.d=function(e,n,s){t.o(e,n)||Object.defineProperty(e,n,{configurable:!1,enumerable:!0,get:s})},t.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(n,"a",n),n},t.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},t.p="",t(t.s=0)}([function(e,t,n){function s(e,t){var n={};for(var s in e)t.indexOf(s)>=0||Object.prototype.hasOwnProperty.call(e,s)&&(n[s]=e[s]);return n}Object.defineProperty(t,"__esModule",{value:!0});var a="function"==typeof Symbol&&"symbol"===i(Symbol.iterator)?function(e){return"undefined"==typeof e?"undefined":i(e)}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":"undefined"==typeof e?"undefined":i(e)},c=Object.assign||function(e){for(var t=1;t<arguments.length;t++){var n=arguments[t];for(var s in n)Object.prototype.hasOwnProperty.call(n,s)&&(e[s]=n[s])}return e},o={top:"top",bottom:"bottom"},r={alertTime:3,dateMonths:["January","February","March","April","May","June","July","August","September","October","November","December"],overlayClickDismiss:!0,overlayOpacity:.75,transitionCurve:"ease",transitionDuration:.3,transitionSelector:"all",classes:{container:"notie-container",textbox:"notie-textbox",textboxInner:"notie-textbox-inner",button:"notie-button",element:"notie-element",elementHalf:"notie-element-half",elementThird:"notie-element-third",overlay:"notie-overlay",backgroundSuccess:"notie-background-success",backgroundWarning:"notie-background-warning",backgroundError:"notie-background-error",backgroundInfo:"notie-background-info",backgroundNeutral:"notie-background-neutral",backgroundOverlay:"notie-background-overlay",alert:"notie-alert",inputField:"notie-input-field",selectChoiceRepeated:"notie-select-choice-repeated",dateSelectorInner:"notie-date-selector-inner",dateSelectorUp:"notie-date-selector-up"},ids:{overlay:"notie-overlay"},positions:{alert:o.top,force:o.top,confirm:o.top,input:o.top,select:o.bottom,date:o.top}},l=t.setOptions=function(e){r=c({},r,e,{classes:c({},r.classes,e.classes),ids:c({},r.ids,e.ids),positions:c({},r.positions,e.positions)})},d=function(){return new Promise(function(e){return setTimeout(e,0)})},u=function(e){return new Promise(function(t){return setTimeout(t,1e3*e)})},p=function(){document.activeElement&&document.activeElement.blur()},f=function(){var e="xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g,function(e){var t=16*Math.random()|0,n="x"===e?t:3&t|8;return n.toString(16)});return"notie-"+e},m={1:r.classes.backgroundSuccess,success:r.classes.backgroundSuccess,2:r.classes.backgroundWarning,warning:r.classes.backgroundWarning,3:r.classes.backgroundError,error:r.classes.backgroundError,4:r.classes.backgroundInfo,info:r.classes.backgroundInfo,5:r.classes.backgroundNeutral,neutral:r.classes.backgroundNeutral},v=function(){return r.transitionSelector+" "+r.transitionDuration+"s "+r.transitionCurve},b=function(e){return 13===e.keyCode},x=function(e){return 27===e.keyCode},y=function(e,t){e.classList.add(r.classes.container),e.style[t]="-10000px",document.body.appendChild(e),e.style[t]="-"+e.offsetHeight+"px",e.listener&&window.addEventListener("keydown",e.listener),d().then(function(){e.style.transition=v(),e.style[t]=0})},L=function(e,t){var n=document.getElementById(e);n&&(n.style[t]="-"+n.offsetHeight+"px",n.listener&&window.removeEventListener("keydown",n.listener),u(r.transitionDuration).then(function(){n.parentNode&&n.parentNode.removeChild(n)}))},g=function(e,t){var n=document.createElement("div");n.id=r.ids.overlay,n.classList.add(r.classes.overlay),n.classList.add(r.classes.backgroundOverlay),n.style.opacity=0,e&&r.overlayClickDismiss&&(n.onclick=function(){L(e.id,t),h()}),document.body.appendChild(n),d().then(function(){n.style.transition=v(),n.style.opacity=r.overlayOpacity})},h=function(){var e=document.getElementById(r.ids.overlay);e.style.opacity=0,u(r.transitionDuration).then(function(){e.parentNode&&e.parentNode.removeChild(e)})},k=t.hideAlerts=function(e){var t=document.getElementsByClassName(r.classes.alert);if(t.length){for(var n=0;n<t.length;n++){var s=t[n];L(s.id,s.position)}e&&u(r.transitionDuration).then(function(){return e()})}},C=t.alert=function(e){var t=e.type,n=void 0===t?4:t,s=e.text,a=e.time,i=void 0===a?r.alertTime:a,c=e.stay,o=void 0!==c&&c,l=e.position,d=void 0===l?r.positions.alert||d.top:l;p(),k();var v=document.createElement("div"),g=f();v.id=g,v.position=d,v.classList.add(r.classes.textbox),v.classList.add(m[n]),v.classList.add(r.classes.alert),v.innerHTML='<div class="'+r.classes.textboxInner+'">'+s+"</div>",v.onclick=function(){return L(g,d)},v.listener=function(e){(b(e)||x(e))&&k()},y(v,d),i&&i<1&&(i=1),!o&&i&&u(i).then(function(){return L(g,d)})},E=t.force=function(e,t){var n=e.type,s=void 0===n?5:n,a=e.text,i=e.buttonText,c=void 0===i?"OK":i,o=e.callback,l=e.position,d=void 0===l?r.positions.force||d.top:l;p(),k();var u=document.createElement("div"),v=f();u.id=v;var x=document.createElement("div");x.classList.add(r.classes.textbox),x.classList.add(r.classes.backgroundInfo),x.innerHTML='<div class="'+r.classes.textboxInner+'">'+a+"</div>";var C=document.createElement("div");C.classList.add(r.classes.button),C.classList.add(m[s]),C.innerHTML=c,C.onclick=function(){L(v,d),h(),o?o():t&&t()},u.appendChild(x),u.appendChild(C),u.listener=function(e){b(e)&&C.click()},y(u,d),g()},T=t.confirm=function(e,t,n){var s=e.text,a=e.submitText,i=void 0===a?"Yes":a,c=e.cancelText,o=void 0===c?"Cancel":c,l=e.submitCallback,d=e.cancelCallback,u=e.position,m=void 0===u?r.positions.confirm||m.top:u;p(),k();var v=document.createElement("div"),C=f();v.id=C;var E=document.createElement("div");E.classList.add(r.classes.textbox),E.classList.add(r.classes.backgroundInfo),E.innerHTML='<div class="'+r.classes.textboxInner+'">'+s+"</div>";var T=document.createElement("div");T.classList.add(r.classes.button),T.classList.add(r.classes.elementHalf),T.classList.add(r.classes.backgroundSuccess),T.innerHTML=i,T.onclick=function(){L(C,m),h(),l?l():t&&t()};var M=document.createElement("div");M.classList.add(r.classes.button),M.classList.add(r.classes.elementHalf),M.classList.add(r.classes.backgroundError),M.innerHTML=o,M.onclick=function(){L(C,m),h(),d?d():n&&n()},v.appendChild(E),v.appendChild(T),v.appendChild(M),v.listener=function(e){b(e)?T.click():x(e)&&M.click()},y(v,m),g(v,m)},M=function(e,t,n){var i=e.text,c=e.submitText,o=void 0===c?"Submit":c,l=e.cancelText,d=void 0===l?"Cancel":l,u=e.submitCallback,m=e.cancelCallback,v=e.position,C=void 0===v?r.positions.input||C.top:v,E=s(e,["text","submitText","cancelText","submitCallback","cancelCallback","position"]);p(),k();var T=document.createElement("div"),M=f();T.id=M;var H=document.createElement("div");H.classList.add(r.classes.textbox),H.classList.add(r.classes.backgroundInfo),H.innerHTML='<div class="'+r.classes.textboxInner+'">'+i+"</div>";var S=document.createElement("input");S.classList.add(r.classes.inputField),S.setAttribute("autocapitalize",E.autocapitalize||"none"),S.setAttribute("autocomplete",E.autocomplete||"off"),S.setAttribute("autocorrect",E.autocorrect||"off"),S.setAttribute("autofocus",E.autofocus||"true"),S.setAttribute("inputmode",E.inputmode||"verbatim"),S.setAttribute("max",E.max||""),S.setAttribute("maxlength",E.maxlength||""),S.setAttribute("min",E.min||""),S.setAttribute("minlength",E.minlength||""),S.setAttribute("placeholder",E.placeholder||""),S.setAttribute("spellcheck",E.spellcheck||"default"),S.setAttribute("step",E.step||"any"),S.setAttribute("type",E.type||"text"),S.value=E.value||"",E.allowed&&(S.oninput=function(){var e=void 0;if(Array.isArray(E.allowed)){for(var t="",n=E.allowed,s=0;s<n.length;s++)"an"===n[s]?t+="0-9a-zA-Z":"a"===n[s]?t+="a-zA-Z":"n"===n[s]&&(t+="0-9"),"s"===n[s]&&(t+=" ");e=new RegExp("[^"+t+"]","g")}else"object"===a(E.allowed)&&(e=E.allowed);S.value=S.value.replace(e,"")});var w=document.createElement("div");w.classList.add(r.classes.button),w.classList.add(r.classes.elementHalf),w.classList.add(r.classes.backgroundSuccess),w.innerHTML=o,w.onclick=function(){L(M,C),h(),u?u(S.value):t&&t(S.value)};var O=document.createElement("div");O.classList.add(r.classes.button),O.classList.add(r.classes.elementHalf),O.classList.add(r.classes.backgroundError),O.innerHTML=d,O.onclick=function(){L(M,C),h(),m?m(S.value):n&&n(S.value)},T.appendChild(H),T.appendChild(S),T.appendChild(w),T.appendChild(O),T.listener=function(e){b(e)?w.click():x(e)&&O.click()},y(T,C),S.focus(),g(T,C)};t.input=M;var H=t.select=function(e,t){var n=e.text,s=e.cancelText,a=void 0===s?"Cancel":s,i=e.cancelCallback,c=e.choices,o=e.position,l=void 0===o?r.positions.select||l.top:o;p(),k();var d=document.createElement("div"),u=f();d.id=u;var v=document.createElement("div");v.classList.add(r.classes.textbox),v.classList.add(r.classes.backgroundInfo),v.innerHTML='<div class="'+r.classes.textboxInner+'">'+n+"</div>",d.appendChild(v),c.forEach(function(e,t){var n=e.type,s=void 0===n?1:n,a=e.text,i=e.handler,o=document.createElement("div");o.classList.add(m[s]),o.classList.add(r.classes.button),o.classList.add(r.classes.selectChoice);var p=c[t+1];p&&!p.type&&(p.type=1),p&&p.type===s&&o.classList.add(r.classes.selectChoiceRepeated),o.innerHTML=a,o.onclick=function(){L(u,l),h(),i()},d.appendChild(o)});var b=document.createElement("div");b.classList.add(r.classes.backgroundNeutral),b.classList.add(r.classes.button),b.innerHTML=a,b.onclick=function(){L(u,l),h(),i?i():t&&t()},d.appendChild(b),d.listener=function(e){x(e)&&b.click()},y(d,l),g(d,l)},S=t.date=function(e,t,n){var s=e.value,a=void 0===s?new Date:s,i=e.submitText,c=void 0===i?"OK":i,o=e.cancelText,l=void 0===o?"Cancel":o,d=e.submitCallback,u=e.cancelCallback,m=e.position,v=void 0===m?r.positions.date||v.top:m;p(),k();var C="&#9662",E=document.createElement("div"),T=document.createElement("div"),M=document.createElement("div"),H=function(e){E.innerHTML=r.dateMonths[e.getMonth()],T.innerHTML=e.getDate(),M.innerHTML=e.getFullYear()},S=function(e){var t=new Date(a.getFullYear(),a.getMonth()+1,0).getDate(),n=e.target.textContent.replace(/^0+/,"").replace(/[^\d]/g,"").slice(0,2);Number(n)>t&&(n=t.toString()),e.target.textContent=n,Number(n)<1&&(n="1"),a.setDate(Number(n))},w=function(e){var t=e.target.textContent.replace(/^0+/,"").replace(/[^\d]/g,"").slice(0,4);e.target.textContent=t,a.setFullYear(Number(t))},O=function(e){H(a)},A=function(e){var t=new Date(a.getFullYear(),a.getMonth()+e+1,0).getDate();a.getDate()>t&&a.setDate(t),a.setMonth(a.getMonth()+e),H(a)},D=function(e){a.setDate(a.getDate()+e),H(a)},I=function(e){var t=a.getFullYear()+e;t<0?a.setFullYear(0):a.setFullYear(a.getFullYear()+e),H(a)},j=document.createElement("div"),N=f();j.id=N;var P=document.createElement("div");P.classList.add(r.classes.backgroundInfo);var F=document.createElement("div");F.classList.add(r.classes.dateSelectorInner);var Y=document.createElement("div");Y.classList.add(r.classes.button),Y.classList.add(r.classes.elementThird),Y.classList.add(r.classes.dateSelectorUp),Y.innerHTML=C;var _=document.createElement("div");_.classList.add(r.classes.button),_.classList.add(r.classes.elementThird),_.classList.add(r.classes.dateSelectorUp),_.innerHTML=C;var z=document.createElement("div");z.classList.add(r.classes.button),z.classList.add(r.classes.elementThird),z.classList.add(r.classes.dateSelectorUp),z.innerHTML=C,E.classList.add(r.classes.element),E.classList.add(r.classes.elementThird),E.innerHTML=r.dateMonths[a.getMonth()],T.classList.add(r.classes.element),T.classList.add(r.classes.elementThird),T.setAttribute("contentEditable",!0),T.addEventListener("input",S),T.addEventListener("blur",O),T.innerHTML=a.getDate(),M.classList.add(r.classes.element),M.classList.add(r.classes.elementThird),M.setAttribute("contentEditable",!0),M.addEventListener("input",w),M.addEventListener("blur",O),M.innerHTML=a.getFullYear();var U=document.createElement("div");U.classList.add(r.classes.button),U.classList.add(r.classes.elementThird),U.innerHTML=C;var B=document.createElement("div");B.classList.add(r.classes.button),B.classList.add(r.classes.elementThird),B.innerHTML=C;var J=document.createElement("div");J.classList.add(r.classes.button),J.classList.add(r.classes.elementThird),J.innerHTML=C,Y.onclick=function(){return A(1)},_.onclick=function(){return D(1)},z.onclick=function(){return I(1)},U.onclick=function(){return A(-1)},B.onclick=function(){return D(-1)},J.onclick=function(){return I(-1)};var R=document.createElement("div");R.classList.add(r.classes.button),R.classList.add(r.classes.elementHalf),R.classList.add(r.classes.backgroundSuccess),R.innerHTML=c,R.onclick=function(){L(N,v),h(),d?d(a):t&&t(a)};var W=document.createElement("div");W.classList.add(r.classes.button),W.classList.add(r.classes.elementHalf),W.classList.add(r.classes.backgroundError),W.innerHTML=l,W.onclick=function(){L(N,v),h(),u?u(a):n&&n(a)},F.appendChild(Y),F.appendChild(_),F.appendChild(z),F.appendChild(E),F.appendChild(T),F.appendChild(M),F.appendChild(U),F.appendChild(B),F.appendChild(J),P.appendChild(F),j.appendChild(P),j.appendChild(R),j.appendChild(W),j.listener=function(e){b(e)?R.click():x(e)&&W.click()},y(j,v),g(j,v)};t.default={alert:C,force:E,confirm:T,input:M,select:H,date:S,setOptions:l,hideAlerts:k}}])})}).call(t,n(0)(e))}])});/* SPDX-License-Identifier: CPAL-1.0                    */
/* Copyright Contributors to the aframe-searxr project. */

AFRAME.registerComponent('searxr', {
    init: function () {

      let scene = $('a-scene')

      const myscene = (enable) => () => {
        if( enable ){
          let music = document.querySelector("#bg")
          music.volume = 0.8
          music.play()
        }else document.querySelector("#bg").pause()
      }
      scene.addEventListener("enter-vr", myscene(true) )
      scene.addEventListener("enter-ar", myscene(true) )
      scene.addEventListener("exit-vr",  myscene(false) )
      scene.addEventListener("exit-ar",  myscene(false) )

      const initMenus = () => {
        let menus = [...document.querySelectorAll('.sidemenu') ]
        menus.map( (menu) => {
          let btn = menu.querySelector(".sidemenu__btn")
          menu.toggle = function(){
            btn.active    = btn.active ? false : true
            btn.className = btn.active ? 'sidemenu__btn active' : 'sidemenu__btn'
            let drawer = menu.querySelector("nav")
            drawer.className     = btn.active ? '' : 'hide'
          }
          btn.addEventListener('click', menu.toggle )
        })
        notie.setOptions({alertTime:0.6})
      }

      const initResults = () => {
        let listEl = this.listEl = document.getElementById("results");
        listEl.addEventListener('clickitem', (ev) => {
          let item = this.store.items[ ev.detail.index ]
          if( !item ) return
          this.visit(item)
        })

        let list = listEl.components.xylist;
        list.setAdapter({
          create(parent) {
            return document.createElement("a-entity")
          },
          bind(position, el, items) {
            let data = items[position]
            let rect = el.components.xyrect;
            el.setAttribute("geometry", {width: rect.width * 0.9, height: rect.height * 0.9, depth: 0.1});
            //el.setAttribute("color", ["#CCC", "#DDD", "#EEE"][position % 5]);
            let scale      = 0.9;
            let scaleInner = 0.75;
            el.data =       data
            el.setAttribute("scale", "-1 1 1") // flip normal
            let image = document.createElement("a-image")
            image.setAttribute("position", "0 0 -0.1")
            image.setAttribute("src", "#tile")
            image.setAttribute("width", rect.width*scale )
            image.setAttribute("height", rect.height*scale )
            if( data.img_src ){
              //if( !session || !session.renderState || !session.renderState.layers ){
              let imgEl = document.createElement("a-image")
              imgEl.setAttribute("src", data.img_src )
              imgEl.setAttribute("width", rect.width*scaleInner )
              imgEl.setAttribute("height", rect.height*scaleInner )
              imgEl.setAttribute("position", "0 0 -0.15")
              el.appendChild(imgEl)
console.dir(el)
              //}else{
              //  console.log(`type: quad; src: ${data.img_src}; width: ${itemWidth*scale}; height: ${itemHeight*scale}`)
              //  let img = document.createElement("img")
              //  img.crossOrigin = "anonymous"
              //  img.id = "img"+position
              //  img.onload = function(){
              //    console.log("creating layer")
              //    let el2 = document.createElement("a-entity")
              //    el2.setAttribute("layer", `type: quad; src: #img${position}; width: ${itemWidth*scale}; height: ${itemHeight*scale}`)
              //    el2.setAttribute("position","0 0 0.1")
              //    el.appendChild(el2)
              //  }
              //  $('a-assets').appendChild(img)
              //  img.src = data.img_src
            }
            el.appendChild(image)
            // hide xyrect
            el.object3D.children[0].material.opacity = 0
            el.object3D.children[0].material.transparent = true
          }
        });
      }

      const initURL = () => {
        if( document.location.search ){  
          if( document.location.search.match('q=') ){
            let iframe = document.querySelector('iframe')
            iframe.src = iframe.src + document.location.search
          }else if( document.location.search.match('://') ){ // assume 3D file
            this.openFile({href: document.location.search.substr(1) })
          }
        }
      }

      this.store  = {

        maxItems: 10000,

        items: [],

        load: (cb) => {
          if( localStorage.getItem("store") ){
            let store = JSON.parse( localStorage.getItem("store") )
            if( !store.version || store.version != "0.1" ){
              localStorage.removeItem("store")
              return this.store.load(cb)
            }
            for ( let i in store ) this.store[i] = store[i]
            cb()
          }else{
            fetch('./searxr.json')
            .then( (res) => res.json()       )
            .then( (cfg) => { for(i in cfg) this.store[i] = cfg[i] })
            .then( cb )
          }
        },

        save: () => {
          localStorage.setItem("store", JSON.stringify( this.store ))
        },

        addItem: (data) => {
          this.store.items.unshift(data)
          this.alert({text:"saved to bookmarks", time:0.6})
          this.update()
        },

        removeItem: (i) => {
          this.store.items.splice( i, 1) // removes item without de-referencing alpinejs store
          this.alert({text:"removed from bookmarks", time:0.6})
          this.update()
        },

        alpinejsify: (storekey) => { // turns stores into alpinejs stores to allow dom reactivity
          Alpine.store(storekey, this.store[storekey] ) // update alpine store
          this.store[storekey] = Alpine.store(storekey)
        }
      }

      this.store.load( () => {
        this.store.alpinejsify('items')
        this.enableImmersiveNavigation()
        this.enableIframeCom()
        initMenus();
        initResults()
        initURL()
        this.update()
      })

      //scene.addEventListener("loaded", () => {
      // sadly webxr cannot handle so much layers
      // "Failed to execute 'updateRenderState' on 'XRSession': Too many layers provided in layers array"
      // this.el.sceneEl.renderer.xr.addEventListener( 'sessionstart', () => this.onXR() )
      //})

    },

    enableImmersiveNavigation: function(){
      if ( 'xr' in navigator ) {
        navigator.xr.addEventListener( 'sessiongranted',  () => {
          var scene = document.querySelector('a-scene');
          if (scene.hasLoaded) {
            scene.enterVR();
          } else {
            scene.addEventListener('loaded',  function () {
              scene.enterVR();
            });
          }
        });
      }
    },

    onXR: function(){
      this.session = this.el.sceneEl.renderer.xr.getSession()
      this.session.requestReferenceSpace( 'local' ).then( ( refSpace ) => {
        this.update()
      })
    },

    update: function(){
      if( this.updateId ){ // throttle calls
        clearTimeout(this.updateId)
        this.updateId = setTimeout(this.update, 200)
      }
      this.store.save()                        // save to localstorage
      if( !this.listEl ) return console.warn("this.listEl not exist")
      let items = this.store.items.map( (i) => i )  // clone
      if( items.length < this.store.maxItems ){
        for ( i = 0; items.length < this.store.maxItems; i++  )
          items.push({})
      }
      this.listEl.components.xylist.setContents([])
      this.listEl.components.xylist.setContents(items)
      // hide scrollbarstuff (the laserpointer can do that for now)
      // *TODO* use wearable for cardboard?
      $('a-xywindow').children[0].children[1].object3D.visible = false
      $('a-xywindow').children[1].object3D.visible = false
    },

    search: function(q){
      let url = `https://searxr.me/search?q=${encodeURI(q)}&category_XR=on&language=en-US&time_range=&safesearch=0&theme=searxr&format=json`
      fetch(url)
      .then( (res) => res.json() )
      .then( (data) => {
        data.results.map( (r) => {
          let {title, url, img_src,  engine} = r
          img_src = "https://corsproxy.io/?" + img_src
          this.store.items.push({title, url, img_src, engine})
        })
        this.update()
      })
    },

    visit: function(data){
      if( !data.immersive ){
        if( this.el.sceneEl.renderer.xr.isPresenting )
          this.el.sceneEl.exitVR()
        window.open(data.url, "_blank")
      }else{
        console.log("todo immersive implement")
      }
    },

    addItem: function(data){
      if( data.img_src ) data.img_src = "https://corsproxy.io/?"+data.img_src // proxy
      this.store.addItem(data)
    },

    openFile: function(data){
      let $scene = document.querySelector('[searxr]')
      let $iframe = document.querySelector('iframe')
      if( !AFRAME.XRF.navigator.notfound ){
        AFRAME.XRF.navigator.notfound = () => {
          $iframe.style.display = ''
          alert("no asset found at "+data.href)
        }
        AFRAME.XRF.addEventListener('reset', () => {
          if( $iframe  ) $iframe.style.display = 'none'
          $scene.innerHTML = ''
        })
        AFRAME.XRF.addEventListener('noasset', () => AFRAME.XRF.navigator.notfound() )
      }
      try {
        AFRAME.XRF.navigator.to( data.href )
      } catch (e) { notfound() }
    },

    enableIframeCom: function(){
      let iframe = document.querySelector('iframe')
      let first = () => {
        iframe.removeEventListener('load',first)
        iframe.addEventListener('load', () => {
          let head = document.querySelector('#head')
          if( head ) head.remove()
        })
      }
      iframe.addEventListener('load', first)

      window.addEventListener("message", (e) => {
        if( !e || String(e.data)[0] != '{' ) return; // ignore non json messages
        let data = JSON.parse(e.data)
        console.dir(data)
        if( data.type == "visit" ){
          console.log("visit")
          console.dir(data)
          this.visit(data)
        }
        if( data.type == "xrbookmark" ){
          console.log("bookmark")
          console.dir(data)
          this.addItem(data)
        }
        if( data.type == "file" ){
          console.log("file")
          this.openFile(data)
        }
      })
    },

    alert(i){ notie.alert(i) }
});
AFRAME.registerComponent('gltf-to-entity', {
  schema: {
    from: {default: '', type: 'selector'},
    name: {default: ''},
    duplicate: {type:'boolean'}
  },

  init: function () {
    var el = this.el;
    var data = this.data;

    data.from.addEventListener('model-loaded', evt => {
      var model;
      var subset;
      model = evt.detail.model;
      subset = model.getObjectByName(data.name);
      if (!subset){
        console.error("Sub-object", data.name, "not found in #"+data.from.id);
        return;
      }
      if( !this.data.duplicate ) subset.parent.remove(subset)
      el.setObject3D('mesh', subset.clone());
      el.emit('model-loaded', el.getObject3D('mesh'));
    });
  }
});
AFRAME.utils.objs = {}
AFRAME.registerComponent('show', {
  schema:{
    "init": {type:'boolean',  default:true }, 
    "VR": {type:'boolean',  default:true }, 
    "AR": {type:'boolean',  default:true }, 
  }, 
  // Set this object invisible while in VR mode.
  init: function () {
    this.el.sceneEl.addEventListener('enter-vr', (ev) => {
      if (this.el.sceneEl.is('vr-mode')) this.el.setAttribute('visible',this.data.VR);
      if (this.el.sceneEl.is('ar-mode')) this.el.setAttribute('visible',this.data.AR);
    });
    this.el.sceneEl.addEventListener('exit-vr', (ev) => {
      this.el.setAttribute('visible', this.data.init )
    });
    this.el.setAttribute('visible', this.data.init )
  }
});
AFRAME.registerComponent('texturescroll', {
  schema:{
    names: {type:'array'},
    amplitude: {type:'float'},
    speed: {type:'float'}
  },
  init: function(){
    this.SCROLL_X = 1
    this.SCROLL_Y = 2
    this.SCROLL_X_Y = 3
    this.SCROLL_X_SIN= 4
    this.SCROLL_Y_SIN = 5
    this.SCROLL_X_Y_SIN = 6
    this.update()
    this.el.addEventListener('model-loaded', () => this.update())
  },
  update: function(){
    this.objs = []
    let mesh = this.el.getObject3D("mesh") || this.el.object3D
    this.data.names.map( (str) => {
      let name = str.replace(/:.*/,'')
      let type = str.replace(/.*:/,'')
      let el = mesh.getObjectByName(name) 
      if( el && !this.objs[el.name] ){
        el.scrolltype = parseInt(type) || 6
        this.objs[el.name] = el 
      }
    })
  },
  tick: function(time,timeDelta){
    for( let i in this.objs ){
      let obj = this.objs[i]
      if( !obj.material ){
        console.warn('texturescroll: could not find object '+i)
        continue
      }
      let mats = obj.material.length ? obj.material : [obj.material] // support multilayered textures
      mats.map( (mat) => {
        if( obj.scrolltype == this.SCROLL_X_SIN || obj.scrolltype == this.SCROLL_X_Y_SIN ) 
          mat.map.offset.x = 1.0 + ( Math.sin( this.data.speed * (time /10000) ) * this.data.amplitude )
        if( obj.scrolltype == this.SCROLL_Y_SIN || obj.scrolltype == this.SCROLL_X_Y_SIN ) 
          mat.map.offset.y = 1.0 + ( Math.cos( this.data.speed * (time /10000) ) * this.data.amplitude )
        if( obj.scrolltype == this.SCROLL_X || obj.scrolltype == this.SCROLL_X_Y ) 
          mat.map.offset.x = ( this.data.speed * (time /10000) ) * this.data.amplitude
        if( obj.scrolltype == this.SCROLL_Y || obj.scrolltype == this.SCROLL_X_Y ) 
          mat.map.offset.y = ( this.data.speed * (time /10000) ) * this.data.amplitude
      })
    }
  }
});
AFRAME.registerComponent('thumbscroller', {

    init: function () {
      this.el.addEventListener('thumbstickmoved',  this.logThumbstick);
      this.data.xyscroll = document.querySelector("a-xywindow")
      console.log("scroller inited")
    }, 
    logThumbstick: function (evt) {
      if (evt.detail.y > 0.95) { 
        console.log("DOWN"); 
        let y = this.data.xyscroll.components.xyscroll._scrollY - 5;
        this.data.xyscroll.components.xyscroll.setScroll(0, y)
      }
      if (evt.detail.y < -0.95) { 
        console.log("UP"); 
        let y = this.data.xyscroll.components.xyscroll._scrollY + 5;
        this.data.xyscroll.components.xyscroll.setScroll(0, y)
      }
      if (evt.detail.x < -0.95) { console.log("LEFT"); }
      if (evt.detail.x > 0.95) { console.log("RIGHT"); }
    } 

});
AFRAME.registerComponent('unlit', {

    init: function () {

        function recursivelySetChildrenUnlit(mesh) {
            const renderer = document.querySelector('a-scene').renderer
            const maxAnisotropy = renderer.capabilities.getMaxAnisotropy();

            if (mesh.material && mesh.material.map) {
                mesh.material = new THREE.MeshBasicMaterial({ map: mesh.material.map });
                mesh.material.dithering = true
                mesh.material.map.anisotropy = maxAnisotropy;
            }

            if (mesh.children) {
                for (var i = 0; i < mesh.children.length; i++) {
                    recursivelySetChildrenUnlit(mesh.children[i]);
                }
            }
        }

        this.el.addEventListener('model-loaded', (e) => {
            const mesh = e.target.getObject3D('mesh');
            recursivelySetChildrenUnlit(mesh);
        });
    }
});
/*
 * v0.5.1 generated at Wed Mar 27 05:24:12 PM CET 2024
 * https://xrfragment.org
 * SPDX-License-Identifier: MPL-2.0
 */
var $hx_exports = typeof exports != "undefined" ? exports : typeof window != "undefined" ? window : typeof self != "undefined" ? self : this;
(function ($global) { "use strict";
$hx_exports["xrfragment"] = $hx_exports["xrfragment"] || {};
var $estr = function() { return js_Boot.__string_rec(this,''); },$hxEnums = $hxEnums || {},$_;
function $extend(from, fields) {
	var proto = Object.create(from);
	for (var name in fields) proto[name] = fields[name];
	if( fields.toString !== Object.prototype.toString ) proto.toString = fields.toString;
	return proto;
}
var EReg = function(r,opt) {
	this.r = new RegExp(r,opt.split("u").join(""));
};
EReg.__name__ = true;
EReg.prototype = {
	match: function(s) {
		if(this.r.global) {
			this.r.lastIndex = 0;
		}
		this.r.m = this.r.exec(s);
		this.r.s = s;
		return this.r.m != null;
	}
	,matched: function(n) {
		if(this.r.m != null && n >= 0 && n < this.r.m.length) {
			return this.r.m[n];
		} else {
			throw haxe_Exception.thrown("EReg::matched");
		}
	}
	,matchedRight: function() {
		if(this.r.m == null) {
			throw haxe_Exception.thrown("No string matched");
		}
		var sz = this.r.m.index + this.r.m[0].length;
		return HxOverrides.substr(this.r.s,sz,this.r.s.length - sz);
	}
	,matchedPos: function() {
		if(this.r.m == null) {
			throw haxe_Exception.thrown("No string matched");
		}
		return { pos : this.r.m.index, len : this.r.m[0].length};
	}
	,split: function(s) {
		var d = "#__delim__#";
		return s.replace(this.r,d).split(d);
	}
};
var HxOverrides = function() { };
HxOverrides.__name__ = true;
HxOverrides.cca = function(s,index) {
	var x = s.charCodeAt(index);
	if(x != x) {
		return undefined;
	}
	return x;
};
HxOverrides.substr = function(s,pos,len) {
	if(len == null) {
		len = s.length;
	} else if(len < 0) {
		if(pos == 0) {
			len = s.length + len;
		} else {
			return "";
		}
	}
	return s.substr(pos,len);
};
HxOverrides.now = function() {
	return Date.now();
};
Math.__name__ = true;
var Reflect = function() { };
Reflect.__name__ = true;
Reflect.field = function(o,field) {
	try {
		return o[field];
	} catch( _g ) {
		return null;
	}
};
Reflect.getProperty = function(o,field) {
	var tmp;
	if(o == null) {
		return null;
	} else {
		var tmp1;
		if(o.__properties__) {
			tmp = o.__properties__["get_" + field];
			tmp1 = tmp;
		} else {
			tmp1 = false;
		}
		if(tmp1) {
			return o[tmp]();
		} else {
			return o[field];
		}
	}
};
Reflect.fields = function(o) {
	var a = [];
	if(o != null) {
		var hasOwnProperty = Object.prototype.hasOwnProperty;
		for( var f in o ) {
		if(f != "__id__" && f != "hx__closures__" && hasOwnProperty.call(o,f)) {
			a.push(f);
		}
		}
	}
	return a;
};
Reflect.isObject = function(v) {
	if(v == null) {
		return false;
	}
	var t = typeof(v);
	if(!(t == "string" || t == "object" && v.__enum__ == null)) {
		if(t == "function") {
			return (v.__name__ || v.__ename__) != null;
		} else {
			return false;
		}
	} else {
		return true;
	}
};
Reflect.deleteField = function(o,field) {
	if(!Object.prototype.hasOwnProperty.call(o,field)) {
		return false;
	}
	delete(o[field]);
	return true;
};
Reflect.copy = function(o) {
	if(o == null) {
		return null;
	}
	var o2 = { };
	var _g = 0;
	var _g1 = Reflect.fields(o);
	while(_g < _g1.length) {
		var f = _g1[_g];
		++_g;
		o2[f] = Reflect.field(o,f);
	}
	return o2;
};
var Std = function() { };
Std.__name__ = true;
Std.string = function(s) {
	return js_Boot.__string_rec(s,"");
};
Std.parseInt = function(x) {
	if(x != null) {
		var _g = 0;
		var _g1 = x.length;
		while(_g < _g1) {
			var i = _g++;
			var c = x.charCodeAt(i);
			if(c <= 8 || c >= 14 && c != 32 && c != 45) {
				var nc = x.charCodeAt(i + 1);
				var v = parseInt(x,nc == 120 || nc == 88 ? 16 : 10);
				if(isNaN(v)) {
					return null;
				} else {
					return v;
				}
			}
		}
	}
	return null;
};
var StringBuf = function() {
	this.b = "";
};
StringBuf.__name__ = true;
var StringTools = function() { };
StringTools.__name__ = true;
StringTools.isSpace = function(s,pos) {
	var c = HxOverrides.cca(s,pos);
	if(!(c > 8 && c < 14)) {
		return c == 32;
	} else {
		return true;
	}
};
StringTools.ltrim = function(s) {
	var l = s.length;
	var r = 0;
	while(r < l && StringTools.isSpace(s,r)) ++r;
	if(r > 0) {
		return HxOverrides.substr(s,r,l - r);
	} else {
		return s;
	}
};
StringTools.rtrim = function(s) {
	var l = s.length;
	var r = 0;
	while(r < l && StringTools.isSpace(s,l - r - 1)) ++r;
	if(r > 0) {
		return HxOverrides.substr(s,0,l - r);
	} else {
		return s;
	}
};
StringTools.trim = function(s) {
	return StringTools.ltrim(StringTools.rtrim(s));
};
StringTools.replace = function(s,sub,by) {
	return s.split(sub).join(by);
};
var haxe_Exception = function(message,previous,native) {
	Error.call(this,message);
	this.message = message;
	this.__previousException = previous;
	this.__nativeException = native != null ? native : this;
};
haxe_Exception.__name__ = true;
haxe_Exception.caught = function(value) {
	if(((value) instanceof haxe_Exception)) {
		return value;
	} else if(((value) instanceof Error)) {
		return new haxe_Exception(value.message,null,value);
	} else {
		return new haxe_ValueException(value,null,value);
	}
};
haxe_Exception.thrown = function(value) {
	if(((value) instanceof haxe_Exception)) {
		return value.get_native();
	} else if(((value) instanceof Error)) {
		return value;
	} else {
		var e = new haxe_ValueException(value);
		return e;
	}
};
haxe_Exception.__super__ = Error;
haxe_Exception.prototype = $extend(Error.prototype,{
	unwrap: function() {
		return this.__nativeException;
	}
	,get_native: function() {
		return this.__nativeException;
	}
	,__properties__: {get_native:"get_native"}
});
var haxe__$Template_TemplateExpr = $hxEnums["haxe._Template.TemplateExpr"] = { __ename__:true,__constructs__:null
	,OpVar: ($_=function(v) { return {_hx_index:0,v:v,__enum__:"haxe._Template.TemplateExpr",toString:$estr}; },$_._hx_name="OpVar",$_.__params__ = ["v"],$_)
	,OpExpr: ($_=function(expr) { return {_hx_index:1,expr:expr,__enum__:"haxe._Template.TemplateExpr",toString:$estr}; },$_._hx_name="OpExpr",$_.__params__ = ["expr"],$_)
	,OpIf: ($_=function(expr,eif,eelse) { return {_hx_index:2,expr:expr,eif:eif,eelse:eelse,__enum__:"haxe._Template.TemplateExpr",toString:$estr}; },$_._hx_name="OpIf",$_.__params__ = ["expr","eif","eelse"],$_)
	,OpStr: ($_=function(str) { return {_hx_index:3,str:str,__enum__:"haxe._Template.TemplateExpr",toString:$estr}; },$_._hx_name="OpStr",$_.__params__ = ["str"],$_)
	,OpBlock: ($_=function(l) { return {_hx_index:4,l:l,__enum__:"haxe._Template.TemplateExpr",toString:$estr}; },$_._hx_name="OpBlock",$_.__params__ = ["l"],$_)
	,OpForeach: ($_=function(expr,loop) { return {_hx_index:5,expr:expr,loop:loop,__enum__:"haxe._Template.TemplateExpr",toString:$estr}; },$_._hx_name="OpForeach",$_.__params__ = ["expr","loop"],$_)
	,OpMacro: ($_=function(name,params) { return {_hx_index:6,name:name,params:params,__enum__:"haxe._Template.TemplateExpr",toString:$estr}; },$_._hx_name="OpMacro",$_.__params__ = ["name","params"],$_)
};
haxe__$Template_TemplateExpr.__constructs__ = [haxe__$Template_TemplateExpr.OpVar,haxe__$Template_TemplateExpr.OpExpr,haxe__$Template_TemplateExpr.OpIf,haxe__$Template_TemplateExpr.OpStr,haxe__$Template_TemplateExpr.OpBlock,haxe__$Template_TemplateExpr.OpForeach,haxe__$Template_TemplateExpr.OpMacro];
var haxe_iterators_ArrayIterator = function(array) {
	this.current = 0;
	this.array = array;
};
haxe_iterators_ArrayIterator.__name__ = true;
haxe_iterators_ArrayIterator.prototype = {
	hasNext: function() {
		return this.current < this.array.length;
	}
	,next: function() {
		return this.array[this.current++];
	}
};
var haxe_Template = function(str) {
	var tokens = this.parseTokens(str);
	this.expr = this.parseBlock(tokens);
	if(!tokens.isEmpty()) {
		throw haxe_Exception.thrown("Unexpected '" + Std.string(tokens.first().s) + "'");
	}
};
haxe_Template.__name__ = true;
haxe_Template.prototype = {
	execute: function(context,macros) {
		this.macros = macros == null ? { } : macros;
		this.context = context;
		this.stack = new haxe_ds_List();
		this.buf = new StringBuf();
		this.run(this.expr);
		return this.buf.b;
	}
	,resolve: function(v) {
		if(v == "__current__") {
			return this.context;
		}
		if(Reflect.isObject(this.context)) {
			var value = Reflect.getProperty(this.context,v);
			if(value != null || Object.prototype.hasOwnProperty.call(this.context,v)) {
				return value;
			}
		}
		var _g_head = this.stack.h;
		while(_g_head != null) {
			var val = _g_head.item;
			_g_head = _g_head.next;
			var ctx = val;
			var value = Reflect.getProperty(ctx,v);
			if(value != null || Object.prototype.hasOwnProperty.call(ctx,v)) {
				return value;
			}
		}
		return Reflect.field(haxe_Template.globals,v);
	}
	,parseTokens: function(data) {
		var tokens = new haxe_ds_List();
		while(haxe_Template.splitter.match(data)) {
			var p = haxe_Template.splitter.matchedPos();
			if(p.pos > 0) {
				tokens.add({ p : HxOverrides.substr(data,0,p.pos), s : true, l : null});
			}
			if(HxOverrides.cca(data,p.pos) == 58) {
				tokens.add({ p : HxOverrides.substr(data,p.pos + 2,p.len - 4), s : false, l : null});
				data = haxe_Template.splitter.matchedRight();
				continue;
			}
			var parp = p.pos + p.len;
			var npar = 1;
			var params = [];
			var part = "";
			while(true) {
				var c = HxOverrides.cca(data,parp);
				++parp;
				if(c == 40) {
					++npar;
				} else if(c == 41) {
					--npar;
					if(npar <= 0) {
						break;
					}
				} else if(c == null) {
					throw haxe_Exception.thrown("Unclosed macro parenthesis");
				}
				if(c == 44 && npar == 1) {
					params.push(part);
					part = "";
				} else {
					part += String.fromCodePoint(c);
				}
			}
			params.push(part);
			tokens.add({ p : haxe_Template.splitter.matched(2), s : false, l : params});
			data = HxOverrides.substr(data,parp,data.length - parp);
		}
		if(data.length > 0) {
			tokens.add({ p : data, s : true, l : null});
		}
		return tokens;
	}
	,parseBlock: function(tokens) {
		var l = new haxe_ds_List();
		while(true) {
			var t = tokens.first();
			if(t == null) {
				break;
			}
			if(!t.s && (t.p == "end" || t.p == "else" || HxOverrides.substr(t.p,0,7) == "elseif ")) {
				break;
			}
			l.add(this.parse(tokens));
		}
		if(l.length == 1) {
			return l.first();
		}
		return haxe__$Template_TemplateExpr.OpBlock(l);
	}
	,parse: function(tokens) {
		var t = tokens.pop();
		var p = t.p;
		if(t.s) {
			return haxe__$Template_TemplateExpr.OpStr(p);
		}
		if(t.l != null) {
			var pe = new haxe_ds_List();
			var _g = 0;
			var _g1 = t.l;
			while(_g < _g1.length) {
				var p1 = _g1[_g];
				++_g;
				pe.add(this.parseBlock(this.parseTokens(p1)));
			}
			return haxe__$Template_TemplateExpr.OpMacro(p,pe);
		}
		var kwdEnd = function(kwd) {
			var pos = -1;
			var length = kwd.length;
			if(HxOverrides.substr(p,0,length) == kwd) {
				pos = length;
				var _g_offset = 0;
				var _g_s = HxOverrides.substr(p,length,null);
				while(_g_offset < _g_s.length) {
					var c = _g_s.charCodeAt(_g_offset++);
					if(c == 32) {
						++pos;
					} else {
						break;
					}
				}
			}
			return pos;
		};
		var pos = kwdEnd("if");
		if(pos > 0) {
			p = HxOverrides.substr(p,pos,p.length - pos);
			var e = this.parseExpr(p);
			var eif = this.parseBlock(tokens);
			var t = tokens.first();
			var eelse;
			if(t == null) {
				throw haxe_Exception.thrown("Unclosed 'if'");
			}
			if(t.p == "end") {
				tokens.pop();
				eelse = null;
			} else if(t.p == "else") {
				tokens.pop();
				eelse = this.parseBlock(tokens);
				t = tokens.pop();
				if(t == null || t.p != "end") {
					throw haxe_Exception.thrown("Unclosed 'else'");
				}
			} else {
				t.p = HxOverrides.substr(t.p,4,t.p.length - 4);
				eelse = this.parse(tokens);
			}
			return haxe__$Template_TemplateExpr.OpIf(e,eif,eelse);
		}
		var pos = kwdEnd("foreach");
		if(pos >= 0) {
			p = HxOverrides.substr(p,pos,p.length - pos);
			var e = this.parseExpr(p);
			var efor = this.parseBlock(tokens);
			var t = tokens.pop();
			if(t == null || t.p != "end") {
				throw haxe_Exception.thrown("Unclosed 'foreach'");
			}
			return haxe__$Template_TemplateExpr.OpForeach(e,efor);
		}
		if(haxe_Template.expr_splitter.match(p)) {
			return haxe__$Template_TemplateExpr.OpExpr(this.parseExpr(p));
		}
		return haxe__$Template_TemplateExpr.OpVar(p);
	}
	,parseExpr: function(data) {
		var l = new haxe_ds_List();
		var expr = data;
		while(haxe_Template.expr_splitter.match(data)) {
			var p = haxe_Template.expr_splitter.matchedPos();
			var k = p.pos + p.len;
			if(p.pos != 0) {
				l.add({ p : HxOverrides.substr(data,0,p.pos), s : true});
			}
			var p1 = haxe_Template.expr_splitter.matched(0);
			l.add({ p : p1, s : p1.indexOf("\"") >= 0});
			data = haxe_Template.expr_splitter.matchedRight();
		}
		if(data.length != 0) {
			var _g_offset = 0;
			var _g_s = data;
			while(_g_offset < _g_s.length) {
				var _g1_key = _g_offset;
				var _g1_value = _g_s.charCodeAt(_g_offset++);
				var i = _g1_key;
				var c = _g1_value;
				if(c != 32) {
					l.add({ p : HxOverrides.substr(data,i,null), s : true});
					break;
				}
			}
		}
		var e;
		try {
			e = this.makeExpr(l);
			if(!l.isEmpty()) {
				throw haxe_Exception.thrown(l.first().p);
			}
		} catch( _g ) {
			var _g1 = haxe_Exception.caught(_g).unwrap();
			if(typeof(_g1) == "string") {
				var s = _g1;
				throw haxe_Exception.thrown("Unexpected '" + s + "' in " + expr);
			} else {
				throw _g;
			}
		}
		return function() {
			try {
				return e();
			} catch( _g ) {
				var exc = haxe_Exception.caught(_g).unwrap();
				throw haxe_Exception.thrown("Error : " + Std.string(exc) + " in " + expr);
			}
		};
	}
	,makeConst: function(v) {
		haxe_Template.expr_trim.match(v);
		v = haxe_Template.expr_trim.matched(1);
		if(HxOverrides.cca(v,0) == 34) {
			var str = HxOverrides.substr(v,1,v.length - 2);
			return function() {
				return str;
			};
		}
		if(haxe_Template.expr_int.match(v)) {
			var i = Std.parseInt(v);
			return function() {
				return i;
			};
		}
		if(haxe_Template.expr_float.match(v)) {
			var f = parseFloat(v);
			return function() {
				return f;
			};
		}
		var me = this;
		return function() {
			return me.resolve(v);
		};
	}
	,makePath: function(e,l) {
		var p = l.first();
		if(p == null || p.p != ".") {
			return e;
		}
		l.pop();
		var field = l.pop();
		if(field == null || !field.s) {
			throw haxe_Exception.thrown(field.p);
		}
		var f = field.p;
		haxe_Template.expr_trim.match(f);
		f = haxe_Template.expr_trim.matched(1);
		return this.makePath(function() {
			return Reflect.field(e(),f);
		},l);
	}
	,makeExpr: function(l) {
		return this.makePath(this.makeExpr2(l),l);
	}
	,skipSpaces: function(l) {
		var p = l.first();
		while(p != null) {
			var _g_offset = 0;
			var _g_s = p.p;
			while(_g_offset < _g_s.length) {
				var c = _g_s.charCodeAt(_g_offset++);
				if(c != 32) {
					return;
				}
			}
			l.pop();
			p = l.first();
		}
	}
	,makeExpr2: function(l) {
		this.skipSpaces(l);
		var p = l.pop();
		this.skipSpaces(l);
		if(p == null) {
			throw haxe_Exception.thrown("<eof>");
		}
		if(p.s) {
			return this.makeConst(p.p);
		}
		switch(p.p) {
		case "!":
			var e = this.makeExpr(l);
			return function() {
				var v = e();
				if(v != null) {
					return v == false;
				} else {
					return true;
				}
			};
		case "(":
			this.skipSpaces(l);
			var e1 = this.makeExpr(l);
			this.skipSpaces(l);
			var p1 = l.pop();
			if(p1 == null || p1.s) {
				throw haxe_Exception.thrown(p1);
			}
			if(p1.p == ")") {
				return e1;
			}
			this.skipSpaces(l);
			var e2 = this.makeExpr(l);
			this.skipSpaces(l);
			var p2 = l.pop();
			this.skipSpaces(l);
			if(p2 == null || p2.p != ")") {
				throw haxe_Exception.thrown(p2);
			}
			switch(p1.p) {
			case "!=":
				return function() {
					return e1() != e2();
				};
			case "&&":
				return function() {
					return e1() && e2();
				};
			case "*":
				return function() {
					return e1() * e2();
				};
			case "+":
				return function() {
					return e1() + e2();
				};
			case "-":
				return function() {
					return e1() - e2();
				};
			case "/":
				return function() {
					return e1() / e2();
				};
			case "<":
				return function() {
					return e1() < e2();
				};
			case "<=":
				return function() {
					return e1() <= e2();
				};
			case "==":
				return function() {
					return e1() == e2();
				};
			case ">":
				return function() {
					return e1() > e2();
				};
			case ">=":
				return function() {
					return e1() >= e2();
				};
			case "||":
				return function() {
					return e1() || e2();
				};
			default:
				throw haxe_Exception.thrown("Unknown operation " + p1.p);
			}
			break;
		case "-":
			var e3 = this.makeExpr(l);
			return function() {
				return -e3();
			};
		}
		throw haxe_Exception.thrown(p.p);
	}
	,run: function(e) {
		switch(e._hx_index) {
		case 0:
			var v = e.v;
			var _this = this.buf;
			var x = Std.string(this.resolve(v));
			_this.b += Std.string(x);
			break;
		case 1:
			var e1 = e.expr;
			var _this = this.buf;
			var x = Std.string(e1());
			_this.b += Std.string(x);
			break;
		case 2:
			var e1 = e.expr;
			var eif = e.eif;
			var eelse = e.eelse;
			var v = e1();
			if(v == null || v == false) {
				if(eelse != null) {
					this.run(eelse);
				}
			} else {
				this.run(eif);
			}
			break;
		case 3:
			var str = e.str;
			this.buf.b += str == null ? "null" : "" + str;
			break;
		case 4:
			var l = e.l;
			var _g_head = l.h;
			while(_g_head != null) {
				var val = _g_head.item;
				_g_head = _g_head.next;
				var e1 = val;
				this.run(e1);
			}
			break;
		case 5:
			var e1 = e.expr;
			var loop = e.loop;
			var v = e1();
			try {
				var x = $getIterator(v);
				if(x.hasNext == null) {
					throw haxe_Exception.thrown(null);
				}
				v = x;
			} catch( _g ) {
				try {
					if(v.hasNext == null) {
						throw haxe_Exception.thrown(null);
					}
				} catch( _g1 ) {
					throw haxe_Exception.thrown("Cannot iter on " + Std.string(v));
				}
			}
			this.stack.push(this.context);
			var v1 = v;
			var ctx = v1;
			while(ctx.hasNext()) {
				var ctx1 = ctx.next();
				this.context = ctx1;
				this.run(loop);
			}
			this.context = this.stack.pop();
			break;
		case 6:
			var m = e.name;
			var params = e.params;
			var v = Reflect.field(this.macros,m);
			var pl = [];
			var old = this.buf;
			pl.push($bind(this,this.resolve));
			var _g_head = params.h;
			while(_g_head != null) {
				var val = _g_head.item;
				_g_head = _g_head.next;
				var p = val;
				if(p._hx_index == 0) {
					var v1 = p.v;
					pl.push(this.resolve(v1));
				} else {
					this.buf = new StringBuf();
					this.run(p);
					pl.push(this.buf.b);
				}
			}
			this.buf = old;
			try {
				var _this = this.buf;
				var x = Std.string(v.apply(this.macros,pl));
				_this.b += Std.string(x);
			} catch( _g ) {
				var e = haxe_Exception.caught(_g).unwrap();
				var plstr;
				try {
					plstr = pl.join(",");
				} catch( _g1 ) {
					plstr = "???";
				}
				var msg = "Macro call " + m + "(" + plstr + ") failed (" + Std.string(e) + ")";
				throw haxe_Exception.thrown(msg);
			}
			break;
		}
	}
};
var haxe_ValueException = function(value,previous,native) {
	haxe_Exception.call(this,String(value),previous,native);
	this.value = value;
};
haxe_ValueException.__name__ = true;
haxe_ValueException.__super__ = haxe_Exception;
haxe_ValueException.prototype = $extend(haxe_Exception.prototype,{
	unwrap: function() {
		return this.value;
	}
});
var haxe_ds_List = function() {
	this.length = 0;
};
haxe_ds_List.__name__ = true;
haxe_ds_List.prototype = {
	add: function(item) {
		var x = new haxe_ds__$List_ListNode(item,null);
		if(this.h == null) {
			this.h = x;
		} else {
			this.q.next = x;
		}
		this.q = x;
		this.length++;
	}
	,push: function(item) {
		var x = new haxe_ds__$List_ListNode(item,this.h);
		this.h = x;
		if(this.q == null) {
			this.q = x;
		}
		this.length++;
	}
	,first: function() {
		if(this.h == null) {
			return null;
		} else {
			return this.h.item;
		}
	}
	,pop: function() {
		if(this.h == null) {
			return null;
		}
		var x = this.h.item;
		this.h = this.h.next;
		if(this.h == null) {
			this.q = null;
		}
		this.length--;
		return x;
	}
	,isEmpty: function() {
		return this.h == null;
	}
	,toString: function() {
		var s_b = "";
		var first = true;
		var l = this.h;
		s_b += "{";
		while(l != null) {
			if(first) {
				first = false;
			} else {
				s_b += ", ";
			}
			s_b += Std.string(Std.string(l.item));
			l = l.next;
		}
		s_b += "}";
		return s_b;
	}
};
var haxe_ds__$List_ListNode = function(item,next) {
	this.item = item;
	this.next = next;
};
haxe_ds__$List_ListNode.__name__ = true;
var js_Boot = function() { };
js_Boot.__name__ = true;
js_Boot.__string_rec = function(o,s) {
	if(o == null) {
		return "null";
	}
	if(s.length >= 5) {
		return "<...>";
	}
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) {
		t = "object";
	}
	switch(t) {
	case "function":
		return "<function>";
	case "object":
		if(o.__enum__) {
			var e = $hxEnums[o.__enum__];
			var con = e.__constructs__[o._hx_index];
			var n = con._hx_name;
			if(con.__params__) {
				s = s + "\t";
				return n + "(" + ((function($this) {
					var $r;
					var _g = [];
					{
						var _g1 = 0;
						var _g2 = con.__params__;
						while(true) {
							if(!(_g1 < _g2.length)) {
								break;
							}
							var p = _g2[_g1];
							_g1 = _g1 + 1;
							_g.push(js_Boot.__string_rec(o[p],s));
						}
					}
					$r = _g;
					return $r;
				}(this))).join(",") + ")";
			} else {
				return n;
			}
		}
		if(((o) instanceof Array)) {
			var str = "[";
			s += "\t";
			var _g = 0;
			var _g1 = o.length;
			while(_g < _g1) {
				var i = _g++;
				str += (i > 0 ? "," : "") + js_Boot.__string_rec(o[i],s);
			}
			str += "]";
			return str;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( _g ) {
			return "???";
		}
		if(tostr != null && tostr != Object.toString && typeof(tostr) == "function") {
			var s2 = o.toString();
			if(s2 != "[object Object]") {
				return s2;
			}
		}
		var str = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		var k = null;
		for( k in o ) {
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str.length != 2) {
			str += ", \n";
		}
		str += s + k + " : " + js_Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str += "\n" + s + "}";
		return str;
	case "string":
		return o;
	default:
		return String(o);
	}
};
var xrfragment_Filter = $hx_exports["xrfragment"]["Filter"] = function(str) {
	this.q = { };
	this.str = "";
	if(str != null) {
		this.parse(str);
	}
};
xrfragment_Filter.__name__ = true;
xrfragment_Filter.prototype = {
	toObject: function() {
		return Reflect.copy(this.q);
	}
	,get: function() {
		return Reflect.copy(this.q);
	}
	,parse: function(str) {
		var token = str.split(" ");
		var q = { };
		var process = function(str,prefix) {
			if(prefix == null) {
				prefix = "";
			}
			str = StringTools.trim(str);
			var k = str.split("=")[0];
			var v = str.split("=")[1];
			var filter = { };
			if(q[prefix + k]) {
				filter = q[prefix + k];
			}
			if(xrfragment_XRF.isProp.match(str)) {
				var oper = "";
				if(str.indexOf(">") != -1) {
					oper = ">";
				}
				if(str.indexOf("<") != -1) {
					oper = "<";
				}
				if(xrfragment_XRF.isExclude.match(k)) {
					k = HxOverrides.substr(k,1,null);
				}
				v = HxOverrides.substr(v,oper.length,null);
				if(oper.length == 0) {
					oper = "=";
				}
				var rule = { };
				if(xrfragment_XRF.isNumber.match(v)) {
					rule[oper] = parseFloat(v);
				} else {
					rule[oper] = v;
				}
				q["expr"] = rule;
			}
			var value = xrfragment_XRF.isDeep.match(str) ? k.split("*").length - 1 : 0;
			q["deep"] = value;
			var value = xrfragment_XRF.isExclude.match(str) ? false : true;
			q["show"] = value;
			var value = k.replace(xrfragment_XRF.operators.r,"");
			q["key"] = value;
			q["value"] = v;
		};
		var _g = 0;
		var _g1 = token.length;
		while(_g < _g1) {
			var i = _g++;
			process(token[i]);
		}
		return this.q = q;
	}
	,test: function(obj) {
		var qualify = false;
		var _g = 0;
		var _g1 = Reflect.fields(obj);
		while(_g < _g1.length) {
			var k = _g1[_g];
			++_g;
			var v = Std.string(Reflect.field(obj,k));
			if(this.testProperty(k,v)) {
				qualify = true;
			}
		}
		var _g = 0;
		var _g1 = Reflect.fields(obj);
		while(_g < _g1.length) {
			var k = _g1[_g];
			++_g;
			var v = Std.string(Reflect.field(obj,k));
			if(this.testProperty(k,v,true)) {
				qualify = false;
			}
		}
		return qualify;
	}
	,testProperty: function(property,value,exclude) {
		var conds = 0;
		var fails = 0;
		var qualify = 0;
		var testprop = function(expr) {
			conds += 1;
			fails += expr ? 0 : 1;
			return expr;
		};
		if(this.q[value] != null) {
			var v = this.q[value];
			if(v[property] != null) {
				return v[property];
			}
		}
		if(Reflect.field(this.q,"expr")) {
			var f = Reflect.field(this.q,"expr");
			if(!Reflect.field(this.q,"show")) {
				if(Reflect.field(f,"!=") != null && testprop((value == null ? "null" : "" + value) == Std.string(Reflect.field(f,"!="))) && exclude) {
					++qualify;
				}
			} else {
				if(Reflect.field(f,"*") != null && testprop(parseFloat(value) != null)) {
					++qualify;
				}
				if(Reflect.field(f,">") != null && testprop(parseFloat(value) >= parseFloat(Reflect.field(f,">")))) {
					++qualify;
				}
				if(Reflect.field(f,"<") != null && testprop(parseFloat(value) <= parseFloat(Reflect.field(f,"<")))) {
					++qualify;
				}
				if(Reflect.field(f,"=") != null && (testprop(value == Reflect.field(f,"=")) || testprop(parseFloat(value) == parseFloat(Reflect.field(f,"="))))) {
					++qualify;
				}
			}
		}
		return qualify > 0;
	}
};
var xrfragment_Parser = $hx_exports["xrfragment"]["Parser"] = function() { };
xrfragment_Parser.__name__ = true;
xrfragment_Parser.parse = function(key,value,store,index) {
	var Frag_h = Object.create(null);
	Frag_h["#"] = xrfragment_XRF.IMMUTABLE | xrfragment_XRF.T_PREDEFINED_VIEW | xrfragment_XRF.PV_EXECUTE;
	Frag_h["src"] = xrfragment_XRF.T_URL;
	Frag_h["href"] = xrfragment_XRF.T_URL | xrfragment_XRF.T_PREDEFINED_VIEW;
	Frag_h["tag"] = xrfragment_XRF.IMMUTABLE | xrfragment_XRF.T_STRING;
	Frag_h["pos"] = xrfragment_XRF.PV_OVERRIDE | xrfragment_XRF.T_VECTOR3 | xrfragment_XRF.T_STRING | xrfragment_XRF.METADATA | xrfragment_XRF.NAVIGATOR;
	Frag_h["rot"] = xrfragment_XRF.QUERY_OPERATOR | xrfragment_XRF.PV_OVERRIDE | xrfragment_XRF.T_VECTOR3 | xrfragment_XRF.METADATA | xrfragment_XRF.NAVIGATOR;
	Frag_h["t"] = xrfragment_XRF.PV_OVERRIDE | xrfragment_XRF.T_FLOAT | xrfragment_XRF.T_VECTOR2 | xrfragment_XRF.NAVIGATOR | xrfragment_XRF.METADATA;
	Frag_h["s"] = xrfragment_XRF.PV_OVERRIDE | xrfragment_XRF.T_MEDIAFRAG;
	Frag_h["loop"] = xrfragment_XRF.PV_OVERRIDE | xrfragment_XRF.T_PREDEFINED_VIEW;
	Frag_h["uv"] = xrfragment_XRF.T_VECTOR2 | xrfragment_XRF.T_MEDIAFRAG;
	Frag_h["namespace"] = xrfragment_XRF.IMMUTABLE | xrfragment_XRF.T_STRING;
	Frag_h["SPDX"] = xrfragment_XRF.IMMUTABLE | xrfragment_XRF.T_STRING;
	Frag_h["unit"] = xrfragment_XRF.IMMUTABLE | xrfragment_XRF.T_STRING;
	Frag_h["description"] = xrfragment_XRF.IMMUTABLE | xrfragment_XRF.T_STRING;
	var keyStripped = key.replace(xrfragment_XRF.operators.r,"");
	var isPVDynamic = key.length > 0 && !Object.prototype.hasOwnProperty.call(Frag_h,key);
	if(isPVDynamic) {
		var v = new xrfragment_XRF(key,xrfragment_XRF.PV_EXECUTE | xrfragment_XRF.NAVIGATOR,index);
		v.validate(value);
		v.flags = xrfragment_XRF.set(xrfragment_XRF.T_DYNAMICKEY,v.flags);
		if(!Object.prototype.hasOwnProperty.call(Frag_h,key)) {
			v.flags = xrfragment_XRF.set(xrfragment_XRF.CUSTOMFRAG,v.flags);
		}
		if(value.length == 0) {
			v.flags = xrfragment_XRF.set(xrfragment_XRF.T_DYNAMICKEYVALUE,v.flags);
		}
		store[keyStripped] = v;
		return true;
	}
	var v = new xrfragment_XRF(key,Frag_h[key],index);
	if(Object.prototype.hasOwnProperty.call(Frag_h,key)) {
		if(!v.validate(value)) {
			console.log("src/xrfragment/Parser.hx:67:","⚠ fragment '" + key + "' has incompatible value (" + value + ")");
			return false;
		}
		store[keyStripped] = v;
		if(xrfragment_Parser.debug) {
			console.log("src/xrfragment/Parser.hx:71:","✔ " + key + ": " + v.string);
		}
	} else {
		if(typeof(value) == "string") {
			v.guessType(v,value);
		}
		v.flags = xrfragment_XRF.set(xrfragment_XRF.CUSTOMFRAG,v.flags);
		store[keyStripped] = v;
	}
	return true;
};
xrfragment_Parser.getMetaData = function() {
	var meta = { title : ["title","og:title","dc.title"], description : ["aria-description","og:description","dc.description"], author : ["author","dc.creator"], publisher : ["publisher","dc.publisher"], website : ["og:site_name","og:url","dc.publisher"], license : ["SPDX","dc.rights"]};
	return meta;
};
var xrfragment_URI = $hx_exports["xrfragment"]["URI"] = function() { };
xrfragment_URI.__name__ = true;
xrfragment_URI.parse = function(url,filter) {
	var store = { };
	if(url == null || url.indexOf("#") == -1) {
		return store;
	}
	var fragment = url.split("#");
	var splitArray = fragment[1].split("&");
	var _g = 0;
	var _g1 = splitArray.length;
	while(_g < _g1) {
		var i = _g++;
		var splitByEqual = splitArray[i].split("=");
		var regexPlus = new EReg("\\+","g");
		var key = splitByEqual[0];
		var value = "";
		if(splitByEqual.length > 1) {
			if(xrfragment_XRF.isVector.match(splitByEqual[1])) {
				value = splitByEqual[1];
			} else {
				var s = regexPlus.split(splitByEqual[1]).join(" ");
				value = decodeURIComponent(s.split("+").join(" "));
			}
		}
		var ok = xrfragment_Parser.parse(key,value,store,i);
	}
	if(filter != null && filter != 0) {
		var _g = 0;
		var _g1 = Reflect.fields(store);
		while(_g < _g1.length) {
			var key = _g1[_g];
			++_g;
			var xrf = store[key];
			if(!xrf.is(filter)) {
				Reflect.deleteField(store,key);
			}
		}
	}
	return store;
};
xrfragment_URI.template = function(uri,vars) {
	var parts = uri.split("#");
	if(parts.length == 1) {
		return uri;
	}
	var frag = parts[1];
	frag = StringTools.replace(frag,"{","::");
	frag = StringTools.replace(frag,"}","::");
	frag = new haxe_Template(frag).execute(vars);
	frag = StringTools.replace(frag,"null","");
	parts[1] = frag;
	return parts.join("#");
};
var xrfragment_XRF = $hx_exports["xrfragment"]["XRF"] = function(_fragment,_flags,_index) {
	this.floats = [];
	this.shift = [];
	this.fragment = _fragment;
	this.flags = _flags;
	this.index = _index;
};
xrfragment_XRF.__name__ = true;
xrfragment_XRF.set = function(flag,flags) {
	return flags | flag;
};
xrfragment_XRF.unset = function(flag,flags) {
	return flags & ~flag;
};
xrfragment_XRF.prototype = {
	is: function(flag) {
		var v = this.flags;
		if(!(typeof(v) == "number" && ((v | 0) === v))) {
			this.flags = 0;
		}
		return (this.flags & flag) != 0;
	}
	,validate: function(value) {
		this.guessType(this,value);
		var ok = true;
		if(value.length == 0 && !this.is(xrfragment_XRF.T_PREDEFINED_VIEW)) {
			ok = false;
		}
		if(!this.is(xrfragment_XRF.T_FLOAT) && this.is(xrfragment_XRF.T_VECTOR2) && !(typeof(this.x) == "number" && typeof(this.y) == "number")) {
			ok = false;
		}
		if(!(this.is(xrfragment_XRF.T_VECTOR2) || this.is(xrfragment_XRF.T_STRING)) && this.is(xrfragment_XRF.T_VECTOR3) && !(typeof(this.x) == "number" && typeof(this.y) == "number" && typeof(this.z) == "number")) {
			ok = false;
		}
		return ok;
	}
	,guessType: function(v,str) {
		v.string = str;
		if(xrfragment_XRF.isReset.match(v.fragment)) {
			v.reset = true;
		}
		if(v.fragment == "loop") {
			v.loop = true;
		}
		if(typeof(str) != "string") {
			return;
		}
		if(str.length > 0) {
			if(xrfragment_XRF.isXRFScheme.match(str)) {
				v.xrfScheme = true;
				str = str.replace(xrfragment_XRF.isXRFScheme.r,"");
				v.string = str;
			}
			if(str.split(",").length > 1) {
				var xyzn = str.split(",");
				if(xyzn.length > 0) {
					v.x = parseFloat(xyzn[0]);
				}
				if(xyzn.length > 1) {
					v.y = parseFloat(xyzn[1]);
				}
				if(xyzn.length > 2) {
					v.z = parseFloat(xyzn[2]);
				}
				var _g = 0;
				var _g1 = xyzn.length;
				while(_g < _g1) {
					var i = _g++;
					v.shift.push(xrfragment_XRF.isShift.match(xyzn[i]));
					v.floats.push(parseFloat(xyzn[i].replace(xrfragment_XRF.isShift.r,"")));
				}
			}
			if(xrfragment_XRF.isColor.match(str)) {
				v.color = str;
			}
			if(xrfragment_XRF.isFloat.match(str)) {
				v.x = parseFloat(str);
				v.float = v.x;
			}
			if(xrfragment_XRF.isInt.match(str)) {
				v.int = Std.parseInt(str);
				v.x = v.int;
				v.floats.push(v.x);
			}
			v.filter = new xrfragment_Filter(v.fragment + "=" + v.string);
		} else {
			v.filter = new xrfragment_Filter(v.fragment);
		}
	}
};
function $getIterator(o) { if( o instanceof Array ) return new haxe_iterators_ArrayIterator(o); else return o.iterator(); }
function $bind(o,m) { if( m == null ) return null; if( m.__id__ == null ) m.__id__ = $global.$haxeUID++; var f; if( o.hx__closures__ == null ) o.hx__closures__ = {}; else f = o.hx__closures__[m.__id__]; if( f == null ) { f = m.bind(o); o.hx__closures__[m.__id__] = f; } return f; }
$global.$haxeUID |= 0;
if(typeof(performance) != "undefined" ? typeof(performance.now) == "function" : false) {
	HxOverrides.now = performance.now.bind(performance);
}
if( String.fromCodePoint == null ) String.fromCodePoint = function(c) { return c < 0x10000 ? String.fromCharCode(c) : String.fromCharCode((c>>10)+0xD7C0)+String.fromCharCode((c&0x3FF)+0xDC00); }
String.__name__ = true;
Array.__name__ = true;
js_Boot.__toStr = ({ }).toString;
haxe_Template.splitter = new EReg("(::[A-Za-z0-9_ ()&|!+=/><*.\"-]+::|\\$\\$([A-Za-z0-9_-]+)\\()","");
haxe_Template.expr_splitter = new EReg("(\\(|\\)|[ \r\n\t]*\"[^\"]*\"[ \r\n\t]*|[!+=/><*.&|-]+)","");
haxe_Template.expr_trim = new EReg("^[ ]*([^ ]+)[ ]*$","");
haxe_Template.expr_int = new EReg("^[0-9]+$","");
haxe_Template.expr_float = new EReg("^([+-]?)(?=\\d|,\\d)\\d*(,\\d*)?([Ee]([+-]?\\d+))?$","");
haxe_Template.globals = { };
haxe_Template.hxKeepArrayIterator = new haxe_iterators_ArrayIterator([]);
xrfragment_Parser.error = "";
xrfragment_Parser.debug = false;
xrfragment_URI.__meta__ = { statics : { template : { keep : null}}};
xrfragment_XRF.IMMUTABLE = 1;
xrfragment_XRF.PROP_BIND = 2;
xrfragment_XRF.QUERY_OPERATOR = 4;
xrfragment_XRF.PROMPT = 8;
xrfragment_XRF.CUSTOMFRAG = 16;
xrfragment_XRF.NAVIGATOR = 32;
xrfragment_XRF.METADATA = 64;
xrfragment_XRF.PV_OVERRIDE = 128;
xrfragment_XRF.PV_EXECUTE = 256;
xrfragment_XRF.T_COLOR = 8192;
xrfragment_XRF.T_INT = 16384;
xrfragment_XRF.T_FLOAT = 32768;
xrfragment_XRF.T_VECTOR2 = 65536;
xrfragment_XRF.T_VECTOR3 = 131072;
xrfragment_XRF.T_URL = 262144;
xrfragment_XRF.T_PREDEFINED_VIEW = 524288;
xrfragment_XRF.T_STRING = 1048576;
xrfragment_XRF.T_MEDIAFRAG = 2097152;
xrfragment_XRF.T_DYNAMICKEY = 4194304;
xrfragment_XRF.T_DYNAMICKEYVALUE = 8388608;
xrfragment_XRF.isColor = new EReg("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$","");
xrfragment_XRF.isInt = new EReg("^[-0-9]+$","");
xrfragment_XRF.isFloat = new EReg("^[-0-9]+\\.[0-9]+$","");
xrfragment_XRF.isVector = new EReg("([,]+|\\w)","");
xrfragment_XRF.isUrl = new EReg("(://)?\\..*","");
xrfragment_XRF.isUrlOrPretypedView = new EReg("(^#|://)?\\..*","");
xrfragment_XRF.isString = new EReg(".*","");
xrfragment_XRF.operators = new EReg("(^[-]|^[!]|[\\*]$)","g");
xrfragment_XRF.isProp = new EReg("^.*=[><=]?","");
xrfragment_XRF.isExclude = new EReg("^-","");
xrfragment_XRF.isDeep = new EReg("\\*","");
xrfragment_XRF.isNumber = new EReg("^[0-9\\.]+$","");
xrfragment_XRF.isMediaFrag = new EReg("^([0-9\\.,\\*+-]+)$","");
xrfragment_XRF.isReset = new EReg("^!","");
xrfragment_XRF.isShift = new EReg("^(\\+|--)","");
xrfragment_XRF.isXRFScheme = new EReg("^xrf://","");
})(typeof window != "undefined" ? window : typeof global != "undefined" ? global : typeof self != "undefined" ? self : this);
var xrfragment = $hx_exports["xrfragment"];
// the core project uses #vanillajs #proxies #clean #noframework
var $      = typeof $   != 'undefined' ? $  : (s) => document.querySelector(s)            // respect jquery
var $$     = typeof $$  != 'undefined' ? $$ : (s) => [...document.querySelectorAll(s)]    // zepto etc.

var $el = (html,tag) => {
  let el = document.createElement('div')
  el.innerHTML = html
  return el.children[0]
}
// SPDX-License-Identifier: MPL-2.0        
// Copyright (c) 2023 Leon van Kammen/NLNET 

var xrf = {}

xrf.init = function(opts){
  opts      = opts || {}

  xrf.debug = document.location.hostname.match(/^(localhost|[0-9])/) ? 0 : false
  if( !xrf.debug ){
    console.log("add #debug=[0-9] to URL to see XR Fragment debuglog")
    xrf.debug = parseInt( ( document.location.hash.match(/debug=([0-9])/) || [0,'0'] )[1] )
  }

  xrf.Parser.debug = xrf.debug 
  xrf.detectCameraRig(opts)
  for ( let i in opts    ) xrf[i] = opts[i]
  xrf.emit('init',opts)
  return xrf
}

xrf.query = function(){
  // framework implementations can override this function, see src/3rd/js/three/index.sj 
  alert("queries are not implemented (yet) for this particular framework")
}

xrf.detectCameraRig = function(opts){
  if( opts.camera ){ // detect rig (if any)
    let getCam  = ((cam) => () => cam)(opts.camera)
    let offsetY = 0 
    while( opts.camera.parent.type != "Scene" ){
      offsetY += opts.camera.position.y
      opts.camera = opts.camera.parent
      opts.camera.getCam = getCam
      opts.camera.updateProjectionMatrix = () => opts.camera.getCam().updateProjectionMatrix()
    }
    opts.camera.offsetY = offsetY
  }
}

xrf.stats = () => {
  // bookmarklet from https://github.com/zlgenuine/threejs_stats
  (function(){
    for( let i = 0; i < 4; i++ ){
      var script=document.createElement('script');script.onload=function(){var stats=new Stats();stats.showPanel( i ); 
        stats.dom.style.marginTop = `${i*48}px`;  document.body.appendChild(stats.dom);requestAnimationFrame(function loop(){stats.update();requestAnimationFrame(loop)});};script.src='//rawgit.com/mrdoob/stats.js/master/build/stats.min.js';document.head.appendChild(script);
    }
  })()
}

xrf.hasTag = (tag,tags) => String(tags).match( new RegExp(`(^| )${tag}( |$)`,`g`) )

// map library functions to xrf
for ( let i in xrfragment ) xrf[i] = xrfragment[i] 
/* 
 * (promise-able) EVENTS (optionally continue after listeners are finished using .then)
 *
 * example:
 *
 *  xrf.addEventListener('foo',(e) => {
 *    // let promise = e.promise()   
 *    console.log("navigating to: "+e.detail.destination.url)
 *    // promise.resolve()
 *    // promise.reject("not going to happen")
 *  })
 *
 *  xrf.emit('foo',123)
 *  xrf.emit('foo',123).then(...).catch(...).finally(...)
 */

xrf.addEventListener = function(eventName, callback, opts) {
  if( !this._listeners ) this._listeners = []
  callback.opts = opts || {weight: this._listeners.length}
  if (!this._listeners[eventName]) {
      // create a new array for this event name if it doesn't exist yet
      this._listeners[eventName] = [];
  }
  // add the callback to the listeners array for this event name
  this._listeners[eventName].push(callback);
  // sort
  this._listeners[eventName] = this._listeners[eventName].sort( (a,b) => a.opts.weight > b.opts.weight )
  callback.unlisten = () => {
    this._listeners[eventName] = this._listeners[eventName].filter( (c) => c != callback )
  }
  return callback.unlisten
};

xrf.emit = function(eventName, data){
  if( typeof data != 'object' ) throw 'emit() requires passing objects'
  if( xrf.debug && xrf.debug > 1 && ( !eventName.match(/^render/) || xrf.debug == eventName ) ){
    let label = String(`xrf.emit('${eventName}')`).padEnd(35," ");
    label +=  data.mesh && data.mesh.name ? '#'+data.mesh.name : ''
    console.groupCollapsed(label)
    console.info(data)
    console.groupEnd(label)
    if( xrf.debug > 1 ) debugger
  }
  return xrf.emit.promise(eventName,data)
}

xrf.emit.normal = function(eventName, opts) {
    if( !xrf._listeners ) xrf._listeners = []
    var callbacks = xrf._listeners[eventName]
    if (callbacks) {
        for (var i = 0; i < callbacks.length && !opts.halt; i++) {
          try{
            callbacks[i](opts);
          }catch(e){ console.error(e) }
        }
    }
};

xrf.emit.promise = function(e, opts){ 
  return new Promise( (resolve, reject) => {
    opts.promise = () => {
      opts.promises = opts.promises || []
      opts.promises.push(0)
      return { 
        resolve: ((index) => () => {
          opts.promises[index] = 1 
          let succesful = opts.promises.reduce( (a,b) => a+b )
          if( succesful == opts.promises.length ) resolve(opts)
        })(opts.promises.length-1),
        reject: (reason) => {
          opts.halt = true
          console.warn(`'${e}' event rejected: ${reason}`)
        }
      }
    }
    xrf.emit.normal(e, opts)     
    if( !opts.promises ) resolve(opts)
    delete opts.promise
  })
}

xrf.addEventListener('reset', () => {
  let events = ['renderPost']
  events.map( (e) => {
    if( xrf._listeners[e] ) xrf._listeners[e].map( (r) => r.unlisten && r.unlisten() )
  })
})
/*! rasterizeHTML.js - v1.3.1 - 2023-07-06
* http://www.github.com/cburgmer/rasterizeHTML.js
* Copyright (c) 2023 Christoph Burgmer; Licensed MIT */

!function(o,i){void 0===o&&void 0!==window&&(o=window),"function"==typeof define&&define.amd?define(["url","xmlserializer","sane-domparser-error","inlineresources"],function(e,t,n,r){return o.rasterizeHTML=i(e,t,n,r)}):"object"==typeof module&&module.exports?module.exports=i(require("url"),require("xmlserializer"),require("sane-domparser-error"),require("inlineresources")):o.rasterizeHTML=i(o.url,o.xmlserializer,o.sanedomparsererror,o.inlineresources)}(this,function(e,t,n,r){var o=function(n){"use strict";var o={},t=[];o.joinUrl=function(e,t){return e?n.resolve(e,t):t},o.getConstantUniqueIdFor=function(e){return t.indexOf(e)<0&&t.push(e),t.indexOf(e)},o.clone=function(e){var t,n={};for(t in e)e.hasOwnProperty(t)&&(n[t]=e[t]);return n};return o.parseOptionalParameters=function(e){var t,n,r={canvas:null,options:{}};return null==e[0]||(t=e[0],"object"==typeof(n=t)&&null!==n&&Object.prototype.toString.apply(t).match(/\[object (Canvas|HTMLCanvasElement)\]/i))?(r.canvas=e[0]||null,r.options=o.clone(e[1])):r.options=o.clone(e[0]),r},o}(e),i=function(i){"use strict";function u(e,t,n){var r=e[t];return e[t]=function(){var e=Array.prototype.slice.call(arguments);return n.apply(this,[e,r])},r}var e={};return e.baseUrlRespectingXhr=function(t,o){return function(){var e=new t;return u(e,"open",function(e,t){var n=e.shift(),r=e.shift(),r=i.joinUrl(o,r);return t.apply(this,[n,r].concat(e))}),e}},e.finishNotifyingXhr=function(t){function e(){var e=new t;return u(e,"send",function(e,t){return r+=1,t.apply(this,arguments)}),e.addEventListener("load",function(){o+=1,n()}),e}var n,r=0,o=0,i=!1,c=new Promise(function(e){n=function(){r-o<=0&&i&&e({totalCount:r})}});return e.waitForRequestsToFinish=function(){return i=!0,n(),c},e},e}(o),e=function(i){"use strict";function r(e){return Array.prototype.slice.call(e)}var e={},c={active:!0,hover:!0,focus:!1,target:!1};return e.fakeUserAction=function(e,t,n){var r=e.querySelector(t),o=":"+n,t="rasterizehtml"+n;r&&(c[n]?i.addClassNameRecursively(r,t):i.addClassName(r,t),i.rewriteCssSelectorWith(e,o,"."+t))},e.persistInputValues=function(e){function t(e){return"checkbox"===e.type||"radio"===e.type}var n=e.querySelectorAll("input"),e=e.querySelectorAll("textarea");r(n).filter(t).forEach(function(e){e.checked?e.setAttribute("checked",""):e.removeAttribute("checked")}),r(n).filter(function(e){return!t(e)}).forEach(function(e){e.setAttribute("value",e.value)}),r(e).forEach(function(e){e.textContent=e.value})},e.rewriteTagNameSelectorsToLowerCase=function(e){i.lowercaseCssTypeSelectors(e,i.findHtmlOnlyNodeNames(e))},e}(function(){"use strict";function c(e){return Array.prototype.slice.call(e)}var n={};n.addClassName=function(e,t){e.className+=" "+t},n.addClassNameRecursively=function(e,t){n.addClassName(e,t),e.parentNode!==e.ownerDocument&&n.addClassNameRecursively(e.parentNode,t)};function r(e,t,o){var i="((?:^|[^.#:\\w])|(?=\\W))("+t.join("|")+")(?=\\W|$)";c(e.querySelectorAll("style")).forEach(function(e){var t,n;void 0===e.sheet&&(t=e,n=document.implementation.createHTMLDocument(""),(r=document.createElement("style")).textContent=t.textContent,n.body.appendChild(r),t.sheet=r.sheet);var r=c(e.sheet.cssRules).filter(function(e){return e.selectorText&&new RegExp(i,"i").test(e.selectorText)});r.length&&(r.forEach(function(e){var t,n=e.selectorText.replace(new RegExp(i,"gi"),function(e,t,n){return t+o(n)});n!==e.selectorText&&(t=n,e=(n=e).cssText.replace(/^[^\{]+/,""),u(n,t+" "+e))}),e.textContent=a(e.sheet.cssRules))})}var u=function(e,t){var n=e.parentStyleSheet,e=c(n.cssRules).indexOf(e);n.insertRule(t,e+1),n.deleteRule(e)},a=function(e){return c(e).reduce(function(e,t){return e+t.cssText},"")};return n.rewriteCssSelectorWith=function(e,t,n){r(e,[t],function(){return n})},n.lowercaseCssTypeSelectors=function(e,t){r(e,t,function(e){return e.toLowerCase()})},n.findHtmlOnlyNodeNames=function(e){for(var t,n=e.ownerDocument.createTreeWalker(e,NodeFilter.SHOW_ELEMENT),r={},o={};t=n.currentNode.tagName.toLowerCase(),"http://www.w3.org/1999/xhtml"===n.currentNode.namespaceURI?r[t]=!0:o[t]=!0,n.nextNode(););return Object.keys(r).filter(function(e){return!o[e]})},n}()),i=function(a,f,t,m){"use strict";var e={};e.executeJavascript=function(s,l){return new Promise(function(t){function n(){m.document.getElementsByTagName("body")[0].removeChild(r)}function e(){var e=r.contentDocument;t({document:e,errors:i,cleanUp:n})}var r=function(e,t,n,r){t=e.createElement(t);return t.style.visibility="hidden",t.style.width=n+"px",t.style.height=r+"px",t.style.position="absolute",t.style.top=-1e4-r+"px",t.style.left=-1e4-n+"px",e.getElementsByTagName("body")[0].appendChild(t),t}(m.document,"iframe",l.width,l.height),o=s.outerHTML,i=[],c=l.executeJsTimeout||0,u=r.contentWindow.XMLHttpRequest,a=f.finishNotifyingXhr(u),u=f.baseUrlRespectingXhr(a,l.baseUrl);r.onload=function(){var t;(0<(t=c)?new Promise(function(e){setTimeout(e,t)}):Promise.resolve()).then(a.waitForRequestsToFinish).then(e)},r.contentDocument.open(),r.contentWindow.XMLHttpRequest=u,r.contentWindow.onerror=function(e){i.push({resourceType:"scriptExecution",msg:e})},r.contentDocument.write("<!DOCTYPE html>"),r.contentDocument.write(o),r.contentDocument.close()})};function s(e,t,n,r,o){var i,c,u,a=Math.max(e.scrollWidth,e.clientWidth),s=Math.max(e.scrollHeight,e.clientHeight),l=t?(i=(l=function(e,t){var n=e.querySelector(t);if(n)return n;if(e.ownerDocument.querySelector(t)===e)return e;throw{message:"Clipping selector not found"}}(e,t).getBoundingClientRect()).top,c=l.left,u=l.width,l.height):(c=i=0,u=a,s);return l={width:u,height:l},r=r,o=o,r={width:Math.max(l.width*o,n),height:Math.max(l.height*o,r)},e=m.getComputedStyle(e.ownerDocument.documentElement).fontSize,{left:c,top:i,width:r.width,height:r.height,viewportWidth:a,viewportHeight:s,rootFontSize:e}}e.calculateDocumentContentSize=function(c,u){return new Promise(function(n,r){var e,t,o=u.zoom||1,i=function(e,t,n){e=Math.floor(e/n),n=Math.floor(t/n);return function(e,t,n){e=e.createElement("iframe");return e.style.width=t+"px",e.style.height=n+"px",e.style.visibility="hidden",e.style.position="absolute",e.style.top=-1e4-n+"px",e.style.left=-1e4-t+"px",e.style.borderWidth=0,e.sandbox="allow-same-origin",e.scrolling="no",e}(m.document,e,n)}(u.width,u.height,o);m.document.getElementsByTagName("body")[0].appendChild(i),i.onload=function(){var e,t=i.contentDocument;try{e=s(function(e,t){e=e.tagName;return t.querySelector(e)}(c,t),u.clip,u.width,u.height,o),n(e)}catch(e){r(e)}finally{m.document.getElementsByTagName("body")[0].removeChild(i)}},i.contentDocument.open(),i.contentDocument.write("<!DOCTYPE html>"),i.contentDocument.write("html"===(t=(e=c).tagName.toLowerCase())||"body"===t?e.outerHTML:'<body style="margin: 0;">'+e.outerHTML+"</body>"),i.contentDocument.close()})},e.parseHtmlFragment=function(e){var t=m.document.implementation.createHTMLDocument("");t.documentElement.innerHTML=e;t=t.querySelector("body").firstChild;if(!t)throw"Invalid source";return t};e.parseHTML=function(e){var t=m.document.implementation.createHTMLDocument("");return t.documentElement.innerHTML=e,function(e,t){var n,r,o,i=/<html((?:\s+[^>]*)?)>/im.exec(t),t=m.document.implementation.createHTMLDocument("");if(i)for(i="<div"+i[1]+"></div>",t.documentElement.innerHTML=i,r=t.querySelector("div"),n=0;n<r.attributes.length;n++)o=r.attributes[n],e.documentElement.setAttribute(o.name,o.value)}(t,e),t};function n(e){try{return t.failOnParseError(e)}catch(e){throw{message:"Invalid source",originalError:e}}}e.validateXHTML=function(e){e=(new DOMParser).parseFromString(e,"application/xml");n(e)};function r(c,u){return new Promise(function(e,t){function n(e){t({message:"Unable to load page",originalError:e})}var r=new window.XMLHttpRequest,o=a.joinUrl(u.baseUrl,c),i=(i=o,"none"===(o=u.cache)||"repeated"===o?i+"?_="+(l=null===l||"repeated"!==o?Date.now():l):i);r.addEventListener("load",function(){200===r.status||0===r.status?e(r.responseXML):n(r.statusText)},!1),r.addEventListener("error",function(e){n(e)},!1);try{r.open("GET",i,!0),r.responseType="document",r.send(null)}catch(e){n(e)}})}var l=null;return e.loadDocument=function(e,t){return r(e,t).then(n)},e}(o,i,n,window),n=function(r){"use strict";function o(e,t){return t?URL.createObjectURL(new Blob([e],{type:"image/svg+xml"})):"data:image/svg+xml;charset=utf-8,"+encodeURIComponent(e)}function c(e){e instanceof Blob&&URL.revokeObjectURL(e)}function i(o){return new Promise(function(t,e){var n=document.createElement("canvas"),r=new Image;r.onload=function(){var e=n.getContext("2d");try{e.drawImage(r,0,0),n.toDataURL("image/png"),t(!0)}catch(e){t(!1)}},r.onerror=e,r.src=o})}function u(t){return(e=void 0===e?n():e).then(function(e){return o(t,e)})}var e,t={},a='<svg xmlns="http://www.w3.org/2000/svg" width="1" height="1"><foreignObject></foreignObject></svg>',n=function(){return new Promise(function(t,e){var n;(function(){if(r.Blob)try{return new Blob(["<b></b>"],{type:"text/xml"}),!0}catch(e){}return!1})()&&r.URL?(n=o(a,!0),i(n).then(function(e){return c(n),!e&&i(o(a,!1)).then(function(e){return e})},function(){return!1}).then(function(e){t(!e)},function(){e()})):t(!1)})};return t.renderSvg=function(i){return new Promise(function(e,t){function n(){r&&c(r)}var r,o=new Image;o.onload=function(){o.onload=null,o.onerror=null,n(),e(o)},o.onerror=function(){n(),t()},u(i).then(function(e){r=e,o.src=r},t)})},t}(window);return function(o,i,c){"use strict";var u={};u.drawDocument=function(){var e=arguments[0],t=Array.prototype.slice.call(arguments,1),n=o.parseOptionalParameters(t),r=e.documentElement||e;return c.rasterize(r,n.canvas,(e=(t=n).canvas,r=t.options,n=e?e.width:300,e=e?e.height:200,e={width:void 0!==r.width?r.width:n,height:void 0!==r.height?r.height:e},(t=o.clone(t.options)).width=e.width,t.height=e.height,t))};u.drawHTML=function(){var e=arguments[0],t=Array.prototype.slice.call(arguments,1),t=o.parseOptionalParameters(t);return function(e,t,n){e=i.parseHTML(e);return u.drawDocument(e,t,n)}(e,t.canvas,t.options)};function n(t,n,r){return i.loadDocument(t,r).then(function(e){e=function(e,t,n){var r=document.implementation.createHTMLDocument("");r.replaceChild(e.documentElement,r.documentElement);e=n?o.clone(n):{};return n.baseUrl||(e.baseUrl=t),{document:r,options:e}}(e,t,r);return u.drawDocument(e.document,n,e.options)})}return u.drawURL=function(){var e=arguments[0],t=Array.prototype.slice.call(arguments,1),t=o.parseOptionalParameters(t);return n(e,t.canvas,t.options)},u}(o,i,function(o,i,c,r,e,u){"use strict";function a(t){return e.renderSvg(t).then(function(e){return{image:e,svg:t}},function(e){throw l(e)})}function s(e,t,n){return r.drawDocumentAsSvg(e,n).then(a).then(function(e){return t&&function(e,t){try{t.getContext("2d").drawImage(e,0,0)}catch(e){throw l(e)}}(e.image,t),e})}var t={},l=function(e){return{message:"Error rendering page",originalError:e}};return t.rasterize=function(e,n,r){var t=o.clone(r);return t.inlineScripts=!0===r.executeJs,u.inlineReferences(e,t).then(function(t){return r.executeJs?i.executeJavascript(e,r).then(function(e){var t=e.document;return c.persistInputValues(t),{document:t,errors:e.errors,cleanUp:e.cleanUp}}).then(function(e){return{element:e.document.documentElement,errors:t.concat(e.errors),cleanUp:e.cleanUp}}):{element:e,errors:t,cleanUp:function(){}}}).then(function(t){return s(t.element,n,r).then(function(e){return t.cleanUp(),{image:e.image,svg:e.svg,errors:t.errors}})})},t}(o,i,e,function(c,r,u){"use strict";function a(t){var e=Object.keys(t);return e.length?" "+e.map(function(e){return e+'="'+t[e]+'"'}).join(" "):""}function o(e,t,n){var r,o,i=u.serializeToString(e);return c.validateXHTML(i),(e=(r=t,o=Math.round(r.viewportWidth),e=Math.round(r.viewportHeight),{x:-r.left,y:-r.top,width:o,height:e})).style=(e.style||"")+"float: left;",e.externalResourcesRequired=!0,'<svg xmlns="http://www.w3.org/2000/svg"'+a(function(e,t){t=t||1,e={width:e.width,height:e.height,"font-size":e.rootFontSize};return 1!==t&&(e.style="transform:scale("+t+"); transform-origin: 0 0;"),e}(t,n))+'><style scoped="">html::-webkit-scrollbar { display: none; }</style><foreignObject'+a(e)+">"+i+"</foreignObject></svg>"}var i={};return i.getSvgForDocument=function(e,t,n){return r.rewriteTagNameSelectorsToLowerCase(e),o(e,t,n)},i.drawDocumentAsSvg=function(t,n){return["hover","active","focus","target"].forEach(function(e){n[e]&&r.fakeUserAction(t,n[e],e)}),c.calculateDocumentContentSize(t,n).then(function(e){return i.getSvgForDocument(t,e,n.zoom)})},i}(i,e,t),n,r))});
// the XRWG (XR WordGraph)is mentioned in the spec 
//
// it collects metadata-keys ('foo' e.g.), names and tags across 3D scene-nodes (.userData.foo e.g.) 

let XRWG = xrf.XRWG = []

XRWG.word = (key) => XRWG.find( (w) => w.word == word )

XRWG.cleankey = (word) => String(word).replace(/[^0-9\.a-zA-Z_]/g,'')
                                      .toLowerCase()
                                      .replace(/.*:\/\//,'')
XRWG.get = (v,k) => XRWG.find( (x) => x[ k || 'word'] == v )

XRWG.match = (str,types,level) => {
  if( XRWG.length == 0 ) XRWG.generate(xrf)
  level = level == undefined ? 1000 : level
  types = types || []
  let res = XRWG.filter( (n) => {
    types.map( (type) => n[type] ? n = false : false )
    return n
  })
  str = str.toLowerCase()
           .replace(/[!-\*]/g,'') // remove excludes and wildcards
  if( level  <10   ) res = res.filter( (n) => n.key    == str )
  if( level >=10   ) res = res.filter( (n) => n.word   == str   || n.key == str )
  if( level  >30   ) res = res.filter( (n) => n.word.match(str) || n.key == str )
  if( level  >40   ) res = res.filter( (n) => n.word.match(str) || n.key == str || String(n.value||'').match(str) )
  if( level  >999  ) res = res.filter( (n) => n.word.match(str) != null || n.key.match(str) != null || String(n.value||'').match(str) != null)
  return res
}

XRWG.generate = (opts) => {
  let {scene,model} = opts
  XRWG.slice(0,0) // empty  
    
  // collect words from 3d nodes

  let add = (key, spatialNode, type) => {
    if( !key || key.match(/(^#$|name)/) ) return
    let node = XRWG.get( XRWG.cleankey(key) )
    if( node ){
      node.types.push(type)
      node.nodes.push(spatialNode)
    }else{
      node = { word: XRWG.cleankey(key), key, nodes:[spatialNode], types:[] }
      if( spatialNode.userData[key] ) node.value = spatialNode.userData[key]
      node.types.push(type)
      xrf.emit('XRWGnode',node)
      XRWG.push( node )
    }
  }

  scene.traverse( (o) => {
    add( `#${o.name}`, o, 'name')
    for( let k in o.userData ){
      if( k == 'tag' ){
        let tagArr = o.userData.tag.split(" ")
                      .map(    (t) => t.trim() )
                      .filter( (t) => t )
                      .map(    (w) => add( w, o, 'tag') )
      }else if( k.match(/^(href|src)$/) ) add( o.userData[k], o, k)
      else if( k[0] == '#' ) add( k, o , 'pv')
      else add( k, o , 'query')
    }
  }) 

  // sort by n
  XRWG.sort( (a,b) => a.nodes.length - b.nodes.length )
  XRWG = XRWG.reverse() // the cleankey/get functions e.g. will persist
  xrf.emit('XRWG',XRWG)
}

XRWG.deepApplyMatch = function(match,v,cb){
  match.map( (m) => {
    for( let i in m.types ){
      let type = m.types[i]
      let node = m.nodes[i]
      if (type == 'name' || type == 'tag'){
        cb(match,v,node,type)
        if( v.filter.q.deep ) node.traverse( (c) => cb(match,v,c,t) )
      }
    }
  })
}

// the hashbus (QueryString eventBus) is mentioned in the spec 
//
// it allows metadata-keys ('foo' e.g.) of 3D scene-nodes (.userData.foo e.g.) to 
// react by executing code 

let pub = function( url, node_or_model, flags ){  // evaluate fragments in url
  if( !url ) return 
  if( !url.match(/#/) ) url = `#${url}`
  let { THREE, camera } = xrf
  let frag     = xrf.URI.parse( url, flags )
  let fromNode = node_or_model != xrf.model
  let isNode   = node_or_model && node_or_model.children

  let opts = {
    frag, 
    mesh:  fromNode ? node_or_model : xrf.camera, 
    model: xrf.model,
    camera: xrf.camera, 
    scene: isNode ? node_or_model : xrf.scene, 
    renderer: xrf.renderer,
    THREE: xrf.THREE, 
    hashbus: xrf.hashbus 
  }
  xrf.emit('hashbus',opts)
  .then( () => {
    for ( let k in frag ){
      let nodeAlias       = fromNode && opts.mesh && opts.mesh.userData && opts.mesh.userData[k] && opts.mesh.userData[k][0] == '#'
      if( nodeAlias ) pub(opts.mesh.userData[k], opts.mesh) // evaluate node alias
      else pub.fragment(k,opts)
    }
  })
  return frag
}

pub.fragment = (k, opts ) => { // evaluate one fragment
  let frag = opts.frag[k];

  let isPVorMediaFrag = frag.is( xrf.XRF.PV_EXECUTE ) || frag.is( xrf.XRF.T_MEDIAFRAG)
  if( !opts.skipXRWG && isPVorMediaFrag ) pub.XRWG(k,opts)

  // call native function (xrf/env.js e.g.), or pass it to user decorator
  xrf.emit(k,opts)
  .then( () => {
    let func = xrf.frag[k] || function(){} 
    if( typeof xrf[k] == 'function' ) xrf[k]( func, frag, opts)
    else func( frag, opts)
  })
}

pub.XRWG = (word,opts) => {
  let {frag,scene,model,renderer} = opts 

  // if this query was triggered by an src-value, lets filter it
  const isSRC = opts.embedded && opts.embedded.fragment == 'src'
  if( !isSRC ){                             // spec : https://xrfragment.org/#src

    let triggeredByMesh = opts.model != opts.mesh

    let v      = frag[word]
    let id     = v.is( xrf.XRF.T_DYNAMICKEY ) ? word : v.string || word 

    if( id == '#' || !id ) return
    let match = xrf.XRWG.match(id)

    if( !triggeredByMesh && (v.is( xrf.XRF.PV_EXECUTE ) || v.is( xrf.XRF.T_DYNAMIC)) && !v.is( xrf.XRF.T_DYNAMICKEYVALUE ) ){
      // evaluate global aliases or tag/objectnames
      match.map( (w) => {
        if( w.key == `#${id}` ){
          if(  w.value && w.value[0] == '#' ){
            // if value is alias, execute fragment value 
            xrf.hashbus.pub( w.value, xrf.model, xrf.XRF.METADATA | xrf.XRF.PV_OVERRIDE | xrf.XRF.NAVIGATOR )
          }
        }
      })
      xrf.emit('dynamicKey',{ ...opts,v,frag,id,match,scene })
    }else if( v.string ){
      // evaluate global aliases 
      xrf.emit('dynamicKeyValue',{ ...opts,v,frag,id,match,scene })
    }else{
      xrf.emit('dynamicKey',{ ...opts,v,frag,id,match,scene })
    }
  }
}


xrf.hashbus = { pub }
xrf.frag   = {dynamic:{}}
xrf.model  = {}
xrf.mixers = []

xrf.init = ((init) => function(opts){
  let scene = new opts.THREE.Group()
  opts.scene.add(scene)
  opts.scene = scene
  init(opts)
  //if( opts.loaders ) Object.values(opts.loaders).map( xrf.patchLoader )

  xrf.patchRenderer(opts)
  xrf.navigator.init()
  // return xrfragment lib as 'xrf' query functor (like jquery)
  for ( let i in xrf ) xrf.query[i] = xrf[i] 

  if( xrf.debug ) xrf.stats()

  return xrf.query
})(xrf.init)

xrf.patchRenderer = function(opts){
  let {renderer,camera} = opts
  renderer.xr.addEventListener( 'sessionstart', () => xrf.baseReferenceSpace = renderer.xr.getReferenceSpace() );
  renderer.xr.enabled = true;
  xrf.clock = new xrf.THREE.Clock()
  renderer.render = ((render) => function(scene,camera){
    // update clock
    let time = xrf.clock.delta = xrf.clock.getDelta()
    xrf.emit('render',{scene,camera,time,render}) // allow fragments to do something at renderframe
    render(scene,camera)
    xrf.emit('renderPost',{scene,camera,time,render,renderer}) // allow fragments to do something after renderframe
  })(renderer.render.bind(renderer))

}

xrf.getFile = (url) => url.split("/").pop().replace(/#.*/,'')

// parseModel event is essential for src.js to hook into embedded loaded models
xrf.parseModel = function(model,url){
  let file               = xrf.getFile(url)
  model.file             = file
  model.isXRF            = true
  model.scene.traverse( (n) => n.isXRF = true ) // mark for deletion during reset()

  xrf.emit('parseModel',{model,url,file})
}

xrf.parseModel.metadataInMesh =  (mesh,model) => { 
  if( mesh.userData ){
    let frag = {}
    for( let k in mesh.userData ) xrf.Parser.parse( k, mesh.userData[k], frag )
    for( let k in frag ){
      let opts = {frag, mesh, model, camera: xrf.camera, scene: model.scene, renderer: xrf.renderer, THREE: xrf.THREE, hashbus: xrf.hashbus }
      mesh.userData.XRF = frag // allow fragment impl to access XRF obj already
      xrf.emit('frag2mesh',opts)
      .then( () => {
        xrf.hashbus.pub.fragment(k, {...opts, skipXRWG:true}) 
      })
    }
  }
}


xrf.getLastModel = ()           => xrf.model.last 

xrf.reset = () => {

  // allow others to reset certain events 
  xrf.emit('reset',{})

  const disposeObject = (obj) => {
    if (obj.children.length > 0) obj.children.forEach((child) => disposeObject(child));
    if (obj.geometry) obj.geometry.dispose();
    if (obj.material) {
      if (obj.material.map) obj.material.map.dispose();
      obj.material.dispose();
    }
    obj.clear()
    obj.removeFromParent() 
    return true
  };
  let nodes = []
  xrf.scene.traverse( (child) => child.isXRF && (nodes.push(child)) )
  nodes.map( disposeObject ) // leave non-XRF objects intact
  xrf.interactive = xrf.interactiveGroup( xrf.THREE, xrf.renderer, xrf.camera)
  xrf.add( xrf.interactive )
  xrf.layers = 0
}

xrf.parseUrl = (url) => {
  let urlExHash = url.replace(/#.*/,'')
  let urlObj,file
  let store = {}
  try{
    urlObj = new URL( urlExHash.match(/:\/\//) ? urlExHash : String(`${document.location.origin}/${url}`).replace(/\/\//,'/') )
    file = urlObj.pathname.substring(urlObj.pathname.lastIndexOf('/') + 1);
    let   search = urlObj.search.substr(1).split("&")
    for( let i in search )  store[  (search[i].split("=")[0])  ]  = search[i].split("=")[1] || ''
  }catch(e){ }
  let   hashmap = url.match("#") ? url.replace(/.*#/,'').split("&") : []
  for( let i in hashmap ) store[  (hashmap[i].split("=")[0]) ]  = hashmap[i].split("=")[1] || ''
  let   dir  = url.substring(0, url.lastIndexOf('/') + 1)
  const hash = url.match(/#/) ? url.replace(/.*#/,'') : ''
  const ext  = file.split('.').pop()
  const URN = urlObj.origin+urlObj.pathname
  return {urlObj,dir,file,hash,ext,store, URN }
}

xrf.add = (object) => {
  object.isXRF = true // mark for easy deletion when replacing scene
  xrf.scene.add(object)
}

xrf.hasNoMaterial = (mesh) => {
  const hasTexture        = mesh.material && mesh.material.map 
  const hasMaterialName   = mesh.material && mesh.material.name.length > 0 
  return mesh.geometry && !hasMaterialName && !hasTexture
}
xrf.navigator = {}

xrf.navigator.to = (url,flags,loader,data) => {
  if( !url ) throw 'xrf.navigator.to(..) no url given'
  let oldOrigin        = xrf.navigator.origin
  let origin           = xrf.parseUrl(url)
  let {URN,urlObj,dir,file,hash,ext} = origin 
  if( !URN.match(document.location.origin) ) xrf.navigator.origin = origin // new baseURN

  const hasPos         = String(hash).match(/pos=/)
  const hashbus        = xrf.hashbus
  const newFile        = !oldOrigin || xrf.navigator.origin.URN != oldOrigin.URN
  const hashChangeOnly = ((!newFile || !file) && hash) || (!data && !newFile) && xrf.model.file == file

  return new Promise( (resolve,reject) => {
    xrf
    .emit('navigate', {url,loader,data})
    .then( () => {

      if( ext && !loader ){  
        const Loader = xrf.loaders[ext]
        if( !Loader ) return resolve()
        loader = loader || new Loader().setPath( dir )
      }

      if( !hash && !file && !ext ) return resolve(xrf.model) // nothing we can do here

      if( hashChangeOnly && !hasPos ){
        hashbus.pub( url, xrf.model, flags )     // eval local URI XR fragments 
        xrf.navigator.updateHash(hash)           // which don't require 
        return resolve(xrf.model)                // positional navigation
      }


      xrf
      .emit('navigateLoading', {url,loader,data})
      .then( () => {
        if( hashChangeOnly && hasPos ){         // we're already loaded
          hashbus.pub( url, xrf.model, flags )  // and eval local URI XR fragments 
          xrf.navigator.updateHash(hash)
          xrf.emit('navigateLoaded',{url})
          return resolve(xrf.model) 
        }
          
        // clear xrf objects from scene
        if( xrf.model && xrf.model.scene ) xrf.model.scene.visible = false
        xrf.reset() 

        // force relative path for files which dont include protocol or relative path
        if( dir ) dir = dir[0] == '.' || dir.match("://") ? dir : `.${dir}`
        url = url.replace(dir,"")
        loader = loader || new Loader().setPath( dir )
        const onLoad = (model) => {

          model.file = file
          // only change url when loading *another* file
          if( xrf.model ) xrf.navigator.pushState( `${dir}${file}`, hash )
          xrf.model = model 

          if( !model.isXRF ) xrf.parseModel(model,url) // this marks the model as an XRF model

          if(xrf.debug ) model.animations.map( (a) => console.log("anim: "+a.name) )

          // spec: 1. generate the XRWG
          xrf.XRWG.generate({model,scene:model.scene})

          // spec: 2. init metadata inside model for non-SRC data
          if( !model.isSRC ){ 
            model.scene.traverse( (mesh) => xrf.parseModel.metadataInMesh(mesh,model) )
          }

          // spec: 1. execute the default predefined view '#' (if exist) (https://xrfragment.org/#predefined_view)
          xrf.frag.defaultPredefinedViews({model,scene:model.scene})
          // spec: predefined view(s) & objects-of-interest-in-XRWG from URL (https://xrfragment.org/#predefined_view)
          let frag = xrf.hashbus.pub( url, model) // and eval URI XR fragments 

          xrf.add( model.scene )
          if( hash ) xrf.navigator.updateHash(hash)
          xrf.emit('navigateLoaded',{url,model})
          resolve(model)
        }
  
        if( data ){  // file upload
          loader.parse(data, "", onLoad )
        }else{
          try{
            loader.load(url, onLoad )
          }catch(e){ 
            console.error(e)
            xrf.emit('navigateError',{url})
          }
        }
      })
    })
  })
}

xrf.navigator.init = () => {
  if( xrf.navigator.init.inited ) return

  window.addEventListener('popstate', function (event){
    if( !xrf.navigator.updateHash.active ){ // ignore programmatic hash updates (causes infinite recursion)
      xrf.navigator.to( document.location.search.substr(1) + document.location.hash )
    }
  })
  
  window.addEventListener('hashchange', function (e){
    xrf.emit('hash', {hash: document.location.hash })
  })

  xrf.navigator.setupNavigateFallbacks()

  // this allows selectionlines to be updated according to the camera (renderloop)
  xrf.focusLine = new xrf.THREE.Group()
  xrf.focusLine.material = new xrf.THREE.LineDashedMaterial({color:0xFF00FF,linewidth:3, scale: 1, dashSize: 0.2, gapSize: 0.1,opacity:0.3, transparent:true})
  xrf.focusLine.isXRF = true
  xrf.focusLine.position.set(0,0,-0.5);
  xrf.focusLine.points = []
  xrf.focusLine.lines  = []
  xrf.camera.add(xrf.focusLine)

  xrf.navigator.init.inited = true
}

xrf.navigator.setupNavigateFallbacks = () => {

  xrf.addEventListener('navigate', (opts) => {
    let {url} = opts
    let {urlObj,dir,file,hash,ext} = xrf.parseUrl(url)

    const openNewTab = (url) => {
      let inIframe
      try { inIframe = window.self !== window.top; } catch (e) { inIframe = true; }
      return inIframe ? window.parent.postMessage({ url }, '*') : window.open( url, '_blank')
      // in case you're running in an iframe, then use this in the parent page:
      //
      // window.addEventListener("message", (e) => {
      //   if (e.data && e.data.url){
      //     window.open( e.data.url, '_blank')
      //   }
      // },
      //   false,
      // );
    }

    // handle http links
    if( url.match(/^http/) && !xrf.loaders[ext] ){
      xrf.navigator.pollIndexFallback(url)
      .then( (indexUrl) =>  xrf.navigator.to(indexUrl) )
      .catch( openNewTab )
    }
  })

}

xrf.navigator.pollIndexFallback = (url) => {
  let indexes = {}
  let p       = []

  const pollURL = (url) => new Promise( (resolve,reject) => {
    fetch(url, { method: 'HEAD' })
    .then( (res) => {
      resolve( res.ok ? url : null )
    })
    .catch( () => resolve(null) )
  })

  for( let i in xrf.loaders){
    let index = `${url}${url[ url.length-1 ] == '/' ? `index.${i}` :`/index.${i}`}`
    indexes[ index ] = false
    console.info(`trying ${index}`)
    p.push( pollURL(index) )
  }

  return new Promise( (resolve,reject) => {
    Promise.all(p)
    .then( ( results ) => {
      results = results.filter( (r) => r )
      if( results.length && results[0] ) resolve(results[0]) 
      else reject(url)
    })
    .catch(console.error)
  })
}

xrf.navigator.updateHash = (hash,opts) => {
  if( hash.replace(/^#/,'') == document.location.hash.substr(1) || hash.match(/\|/) ) return  // skip unnecesary pushState triggers
  console.log(`URL: ${document.location.search.substr(1)}#${hash}`)
  xrf.navigator.updateHash.active = true  // important to prevent recursion
  document.location.hash = hash
  xrf.navigator.updateHash.active = false
}

xrf.navigator.pushState = (file,hash) => {
  if( file == document.location.search.substr(1) ) return // page is in its default state
  window.history.pushState({},`${file}#${hash}`, document.location.pathname + `?${file}#${hash}` )
  xrf.emit('pushState', {file, hash} )
}
/**
 * 
 * navigation, portals & mutations
 * 
 * | fragment | type | scope | example value |
 * |`href`| string (uri or predefined view) | 🔒 |`#pos=1,1,0`<br>`#pos=1,1,0&rot=90,0,0`<br>`#pos=pyramid`<br>`#pos=lastvisit|pyramid`<br>`://somefile.gltf#pos=1,1,0`<br> |
 * 
 * [[» example implementation|https://github.com/coderofsalvation/xrfragment/blob/main/src/3rd/three/xrf/href.js]]<br>
 * [[» example 3D asset|https://github.com/coderofsalvation/xrfragment/blob/main/example/assets/href.gltf#L192]]<br>
 * [[» discussion|https://github.com/coderofsalvation/xrfragment/issues/1]]<br>
 *
 * [img[xrfragment.jpg]]
 * 
 * 
 * !!!spec 1.0
 * 
 * 1. an ''external''- or ''file URI'' fully replaces the current scene and assumes `pos=0,0,0&rot=0,0,0` by default (unless specified)
 * 
 * 2. navigation should not happen when queries (`q=`) are present in local url: queries will apply (`pos=`, `rot=` e.g.) to the targeted object(s) instead.
 * 
 * 3. navigation should not happen ''immediately'' when user is more than 2 meter away from the portal/object containing the href (to prevent accidental navigation e.g.)
 * 
 * 4. URL navigation should always be reflected in the client (in case of javascript: see [[here|https://github.com/coderofsalvation/xrfragment/blob/dev/src/3rd/three/navigator.js]] for an example navigator).
 * 
 * 5. In XR mode, the navigator back/forward-buttons should be always visible (using a wearable e.g., see [[here|https://github.com/coderofsalvation/xrfragment/blob/dev/example/aframe/sandbox/index.html#L26-L29]] for an example wearable)
 * 
 * [img[navigation.png]]
 * 
 */

xrf.frag.href = function(v, opts){
  let { frag, mesh, model, camera, scene, renderer, THREE} = opts

  if( mesh.userData.XRF.href.exec ) return // mesh already initialized

  let click = mesh.userData.XRF.href.exec = (e) => {

    if( !mesh.material || !mesh.material.visible ) return // ignore invisible nodes

    // update our values to the latest value (might be edited)
    xrf.Parser.parse( "href", mesh.userData.href, frag )
    const v  = frag.href

    // bubble up!
    mesh.traverseAncestors( (n) => n.userData && n.userData.href && n.dispatchEvent({type:e.type,data:{}}) )

    let lastPos   = `pos=${camera.position.x.toFixed(2)},${camera.position.y.toFixed(2)},${camera.position.z.toFixed(2)}`
    xrf
    .emit('href',{click:true,mesh,xrf:v}) // let all listeners agree
    .then( () => {

      let {urlObj,dir,file,hash,ext} = xrf.parseUrl(v.string)
      const isLocal = v.string[0] == '#'
      const hasPos  = isLocal && v.string.match(/pos=/)
      const flags   = isLocal ? xrf.XRF.PV_OVERRIDE : undefined

      //let toFrag = xrf.URI.parse( v.string, xrf.XRF.NAVIGATOR | xrf.XRF.PV_OVERRIDE | xrf.XRF.METADATA )
      if( v.xrfScheme ){
        xrf.hashbus.pub(v.string)
      } else xrf.navigator.to(v.string)    // let's surf
    }) 
    .catch( console.error )
  }

  let selected = mesh.userData.XRF.href.selected = (state) => () => {
    if( (!mesh.material && !mesh.material.visible) && !mesh.isSRC ) return // ignore invisible nodes
    if( mesh.selected == state ) return // nothing changed 

    xrf.interactive.objects.map( (o) => {
      let newState = o.name == mesh.name ? state : false
      if( o.material ){
        if( o.material.uniforms && o.material.uniforms.selected ) o.material.uniforms.selected.value = newState 
        //if( o.material.emissive ) o.material.emissive.r = o.material.emissive.g = o.material.emissive.b = newState ? 2.0 : 1.0
        if( o.material.emissive ){ 
          if( !o.material.emissive.original ) o.material.emissive.original = o.material.emissive.clone()
          o.material.emissive.r = o.material.emissive.g = o.material.emissive.b = 
            newState ? o.material.emissive.original.r + 0.5 : o.material.emissive.original.r
        }
      }
    })
    // update mouse cursor
    if( !renderer.domElement.lastCursor )
      renderer.domElement.lastCursor = renderer.domElement.style.cursor
    renderer.domElement.style.cursor = state ? 'pointer' : renderer.domElement.lastCursor 

    xrf
    .emit('href',{selected:state,mesh,xrf:v}) // let all listeners agree
    .then( () => mesh.selected = state )
  }

  mesh.addEventListener('click', click )
  mesh.addEventListener('mousemove', selected(true) )
  mesh.addEventListener('mouseenter', selected(true) )
  mesh.addEventListener('mouseleave', selected(false) )

  if( mesh.material ) mesh.material = mesh.material.clone() // clone, so we can individually highlight meshes

  // lazy add mesh (because we're inside a recursive traverse)
  setTimeout( (mesh) => {
    xrf.interactive.add(mesh)
    xrf.emit('interactionReady', {mesh,xrf:v,clickHandler: mesh.userData.XRF.href.exec })
  }, 0, mesh )
}

xrf.addEventListener('audioInited', function(opts){
  let {THREE,listener} = opts
  opts.audio = opts.audio || {}
  opts.audio.click    = opts.audio.click || '/example/assets/audio/click.wav'
  opts.audio.hover    = opts.audio.hover || '/example/assets/audio/hover.wav'
  opts.audio.teleport = opts.audio.teleport || '/example/assets/audio/teleport.wav'

  let audio = xrf.frag.href.audio = {}

  actions = ['click','hover','teleport']
  actions.map( (action) => {
    const audioLoader = new THREE.AudioLoader();
    audio[action] = new THREE.Audio( xrf.camera.listener )
    audioLoader.load( opts.audio[action], function( buffer ) {
      audio[action].setBuffer( buffer );
      audio.loaded = true
    },
    function(){}, // progress
    function(err){
      console.warn(`XR Fragment UI sounds not inited, please host default files or specify your own:

xrf.opts = {
  audio: {
    click: '/example/assets/audio/click.wav'
    hover: '/example/assets/audio/hover.wav'
    teleport: '/example/assets/audio/teleport.wav'
  }
}`
      )
    })
  });

  xrf.addEventListener('href', (opts) => {
    if( !xrf.frag.href.audio.loaded ) return 
    let v = opts.xrf
    if( opts.selected ){
      xrf.frag.href.audio.hover.stop() 
      xrf.frag.href.audio.hover.play() 
      return
    }
    if( opts.click ){
      xrf.frag.href.audio.click.stop() 
      xrf.frag.href.audio.click.play() 
      return
    }
  })

  xrf.addEventListener('navigateLoading', (e) => {
    if( !xrf.frag.href.audio.loaded ) return 
    let v = opts.xrf
    xrf.frag.href.audio.click.stop() 
    xrf.frag.href.audio.teleport.stop() 
    xrf.frag.href.audio.teleport.play() 
  })


})

/**
 * > above solutions were abducted from [[this|https://i.imgur.com/E3En0gJ.png]] and [[this|https://i.imgur.com/lpnTz3A.png]] survey result
 *
 * !!!Demo
 * 
 * <$videojs controls="controls" aspectratio="16:9" preload="auto" poster="" fluid="fluid" class="vjs-big-play-centered">
 *   <source src="https://coderofsalvation.github.io/xrfragment.media/href.mp4" type="video/mp4"/>
 * </$videojs>
 * 
 * > capture of <a href="./example/aframe/sandbox" target="_blank">aframe/sandbox</a>
 */
// this is called by navigator.js rather than by a URL e.g.

xrf.frag.defaultPredefinedViews = (opts) => {
  let {scene,model} = opts;
  scene.traverse( (n) => {
    if( n.userData && n.userData['#'] ){
      if( !n.parent && !document.location.hash ){
        xrf.navigator.to( n.userData['#'] )
      }else xrf.hashbus.pub( n.userData['#'], n )   // evaluate default XR fragments without affecting URL
    }
  })
}
xrf.frag.loop = function(v, opts){
  let { frag, mesh, model, camera, scene, renderer, THREE} = opts

  // handle object media players
  if( mesh && mesh.media ){
    for( let i in mesh.media ) mesh.media[i].set("loop",v)
    return
  }

  // otherwise handle global 3D animations
  xrf.mixers.map ( (mixer) => {
    // update loop
    mixer.loop.enabled  = v.loop 

  })

}
xrf.frag.pos = function(v, opts){
  let { frag, mesh, model, camera, scene, renderer, THREE} = opts

  let pos = v

  // spec: indirect coordinate using objectname: https://xrfragment.org/#navigating%203D
  if( pos.x == undefined ){
    let obj = scene.getObjectByName(v.string)
    if( !obj ) return 
    pos = obj.position.clone()
    obj.getWorldPosition(pos)
    camera.position.copy(pos)
  }else{ 
    // spec: direct coordinate: https://xrfragment.org/#navigating%203D
    camera.position.x = pos.x
    camera.position.y = pos.y
    camera.position.z = pos.z
  }

  if( xrf.debug ) console.log(`#pos.js: setting camera to position ${pos.x},${pos.y},${pos.z}`)

  xrf.frag.pos.last = pos // remember

  camera.updateMatrixWorld()
}

xrf.addEventListener('reset', (opts) => {
  // set the player to position 0,0,0
  xrf.camera.position.set(0,0,0)
})
xrf.frag.rot = function(v, opts){
  let { frag, mesh, model, camera, scene, renderer, THREE} = opts
  if( xrf.debug ) console.log("#rot.js: setting camera rotation to "+v.string)
  if( !model.isSRC ){
    camera.rotation.set( 
      v.x * Math.PI / 180,
      v.y * Math.PI / 180,
      v.z * Math.PI / 180
    )
    camera.rotation.offset = camera.rotation.clone() // remember
    //camera.updateProjectionMatrix()
  }else{
    obj = model.scene.isReparented ? model.scene.children[0] : model.scene
    obj.rotation.set( 
      v.x * Math.PI / 180,
      v.y * Math.PI / 180,
      v.z * Math.PI / 180
    )
  }
}
xrf.frag.s = function(v, opts){
  let { frag, mesh, model, camera, scene, renderer, THREE} = opts

  // handle object media players
  if( mesh && mesh.media ){
    for( let i in mesh.media ) mesh.media[i].set("s",v)
    return
  }

  // otherwise handle global 3D animations
  xrf.mixers.map ( (mixer) => {
    mixer.s = v
    
    // update speed
    mixer.timeScale     = v.x || 1.0 
    mixer.loop.speed    = v.x || 1.0
    mixer.loop.speedAbs = Math.abs( v.x )

  })

}
// *TODO* use webgl instancing

xrf.frag.src = function(v, opts){
  opts.embedded = v // indicate embedded XR fragment
  let { mesh, model, camera, scene, renderer, THREE, hashbus, frag} = opts

  if( mesh.isSRC ) return // only embed src once 

  let url       = xrf.frag.src.expandURI( mesh, v.string )
  let srcFrag   = opts.srcFrag = xrfragment.URI.parse(url)
  opts.isLocal  = v.string[0] == '#'
  opts.isPortal = xrf.frag.src.renderAsPortal(mesh)
  opts.isSRC    = mesh.isSRC = true 

  if(xrf.debug) console.log(`src.js: instancing ${opts.isLocal?'local':'remote'} object ${url}`)

  if( opts.isLocal ){
    xrf.frag.src.localSRC(url,srcFrag,opts)         // local
  }else xrf.frag.src.externalSRC(url,srcFrag,opts)  // external file

  xrf.hashbus.pub( url.replace(/.*#/,''), mesh)     // eval src-url fragments
}

xrf.frag.src.isRelativeURI = function(mesh,uri){
  return uri.match("://") ? false : true
}

xrf.frag.src.expandURI = function(mesh,uri){
  if( uri ) mesh.userData.srcTemplate = uri
  mesh.userData.src = xrf.URI.template( mesh.userData.srcTemplate, xrf.URI.vars.__object )
  if( xrf.frag.src.isRelativeURI(mesh,uri) ){
    mesh.userData.src = ( mesh.userData.src[0] != '/' ? xrf.navigator.origin.dir : xrf.navigator.origin.urlObj.origin ) + mesh.userData.src
  }
  return mesh.userData.src
}

xrf.frag.src.addModel = (model,url,frag,opts) => {
  let {mesh} = opts
  let scene = model.scene
  scene = xrf.frag.src.filterScene(scene,{...opts,frag})         // get filtered scene
  if( mesh.material && mesh.userData.src ) mesh.material.visible = false  // hide placeholder object

  if( opts.isPortal ){
    // only add remote objects, because 
    // local scene-objects are already added to scene
    xrf.portalNonEuclidian({...opts,model,scene:model.scene})
    if( !opts.isLocal ) xrf.scene.add(scene) 
  }else{
    xrf.frag.src.scale( scene, opts, url )           // scale scene
    mesh.add(scene)
  }
  xrf.frag.src.enableSourcePortation({scene,mesh,url,model})
  // flag everything isSRC & isXRF
  mesh.traverse( (n) => { n.isSRC = n.isXRF = n[ opts.isLocal ? 'isSRCLocal' : 'isSRCExternal' ] = true })
  
  xrf.emit('parseModel', {...opts, isSRC:true, mesh, model}) // this will execute all embedded metadata/fragments e.g.
}

xrf.frag.src.renderAsPortal = (mesh) => {
  // *TODO* should support better isFlat(mesh) check
  const isPlane           = mesh.geometry && mesh.geometry.attributes.uv && mesh.geometry.attributes.uv.count == 4 
  return xrf.hasNoMaterial(mesh) && isPlane
}

xrf.frag.src.enableSourcePortation = (opts) => {
  let {scene,mesh,url,model} = opts
  if( url[0] == '#' ) return

  url = url.replace(/(&)?[-][\w-+\.]+(&)?/g,'&') // remove negative selectors to refer to original scene

  if( !mesh.userData.href ){ 
    // show sourceportation clickable sphere for non-portals
    let scale = new THREE.Vector3()
    let size  = new THREE.Vector3()
    scene.getWorldScale(scale)
    new THREE.Box3().setFromObject(scene).getSize(size)
    const geo    = new THREE.SphereGeometry( Math.max(size.x, size.y, size.z) * scale.x * 0.33, 10, 10 )
    const mat    = new THREE.MeshBasicMaterial()
    mat.visible = false // we just use this for collisions
    const sphere = new THREE.Mesh( geo, mat )
    sphere.isXRF = true
    // reparent scene to sphere
    let children = mesh.children
    mesh.children = []
    mesh.add(sphere)
    children.map( (c) => sphere.add(c) )
    // make sphere clickable/hoverable
    let frag = {}
    xrf.Parser.parse("href", url, frag)
    sphere.userData = scene.userData  // allow rich href notifications/hovers
    sphere.userData.href = url.replace(/#.*/,'') // remove fragments to refer to original scene
    sphere.userData.XRF  = frag
    xrf.hashbus.pub.fragment("href", {...opts, mesh:sphere, frag, skipXRWG:true, renderer:xrf.renderer, camera:xrf.camera }) 
  }
  for ( let i in scene.userData ) {
    if( !mesh.userData[i] ) mesh.userData[i] = scene.userData[i] // allow rich href notifications/hovers
  }
}

xrf.frag.src.externalSRC = (url,frag,opts) => {
  fetch(url, { method: 'HEAD' })
  .then( (res) => {
    let mimetype = res.headers.get('Content-type')
    if(xrf.debug != undefined ) console.log("HEAD "+url+" => "+mimetype)
    if( url.replace(/#.*/,'').match(/\.(gltf|glb)$/)     ) mimetype = 'gltf'
    if( url.replace(/#.*/,'').match(/\.(frag|fs|glsl)$/) ) mimetype = 'x-shader/x-fragment'
    if( url.replace(/#.*/,'').match(/\.(vert|vs)$/)      ) mimetype = 'x-shader/x-fragment'
    //if( url.match(/\.(fbx|stl|obj)$/) ) mimetype = 
    opts = { ...opts, frag, mimetype }
    return xrf.frag.src.type[ mimetype ] ? xrf.frag.src.type[ mimetype ](url,opts) : xrf.frag.src.type.unknown(url,opts)
  })
  .then( (model) => {
    if( model && model.scene ) xrf.frag.src.addModel(model, url, frag, opts )
  })
  .finally( () => { })
  .catch( console.error )
  return xrf.frag.src
}

xrf.frag.src.localSRC = (url,frag,opts) => {
  let {model,mesh,scene} = opts
  //setTimeout( (mesh,scene) => {
    if( mesh.material ) mesh.material = mesh.material.clone() // clone, so we can individually highlight meshes
    let _model = {
      animations: model.animations,
      scene: scene.clone()
    }
    _model.scene.traverse( (n) => n.isXRF = true )  // make sure they respond to xrf.reset()
    _model.scenes = [_model.scene]
    xrf.frag.src.addModel(_model,url,frag, opts)    // current file 
  //},1000,mesh,scene )
}

// scale embedded XR fragments https://xrfragment.org/#scaling%20of%20instanced%20objects
xrf.frag.src.scale = function(scene, opts, url){
    let { mesh, model, camera, renderer, THREE} = opts

    // remove invisible objects (hidden by selectors) which might corrupt boundingbox size-detection 
    let cleanScene = scene.clone()
    let remove = []
    const notVisible = (n) => !n.visible || (n.material && !n.material.visible)
    cleanScene.traverse( (n) => notVisible(n) && n.children.length == 0 && (remove.push(n)) )
    remove.map( (n) => n.removeFromParent() )

    let restrictTo3DBoundingBox = mesh.geometry
    if( restrictTo3DBoundingBox ){ 
      // spec 3 of https://xrfragment.org/#src
      // spec 1 of https://xrfragment.org/#scaling%20of%20instanced%20objects  
      // normalize instanced objectsize to boundingbox
      let sizeFrom  = new THREE.Vector3()
      let sizeTo    = new THREE.Vector3()
      let empty = new THREE.Object3D()
      new THREE.Box3().setFromObject(mesh).getSize(sizeTo)
      new THREE.Box3().setFromObject(cleanScene).getSize(sizeFrom)
      let ratio = sizeFrom.divide(sizeTo)
      scene.scale.multiplyScalar( 1.0 / Math.max(ratio.x, ratio.y, ratio.z));
    }else{
      // spec 4 of https://xrfragment.org/#src
      // spec 2 of https://xrfragment.org/#scaling%20of%20instanced%20objects
      scene.scale.multiply( mesh.scale ) 
    }
}

xrf.frag.src.filterScene = (scene,opts) => {
  let { mesh, model, camera, renderer, THREE, hashbus, frag} = opts

  scene = xrf.filter.scene({scene,frag,reparent:true}) //,copyScene: opts.isPortal})

  if( !opts.isLocal ){
    scene.traverse( (m) => {
      if( m.userData && (m.userData.src || m.userData.href) ) return ; // prevent infinite recursion 
      xrf.parseModel.metadataInMesh(m,{scene,recursive:true})
    })
  }
  return scene
}

/*
 * replace the src-mesh with the contents of the src
 */

xrf.frag.src.type = {}

/*
 * mimetype: unknown 
 */

xrf.frag.src.type['unknown'] = function( url, opts ){
  return new Promise( (resolve,reject) => {
    reject(`${url} mimetype '${opts.mimetype}' not found or supported (yet)`)
  })
}
// this ns the global #t mediafragment handler (which affects the 3D animation)

xrf.frag.t = function(v, opts){
  let { frag, mesh, model, camera, scene, renderer, THREE} = opts

  // handle object media players
  if( mesh && mesh.media ){
    for( let i in mesh.media ) mesh.media[i].set("t",v)
    return
  }
 
  // otherwise handle global 3D animations
  if( !model.mixer ) return 
  if( !model.animations || model.animations[0] == undefined ){
    console.warn('no animations found in model')
    return xrf.emit( v.x == 0 ? 'stop' : 'play',{isPlaying: v.x != 0 })
  }
    
  xrf.mixers.map ( (mixer) => {
    
    mixer.t = v
    
    // update speed
    mixer.timeScale     = mixer.loop.speed
    mixer.loop.speedAbs = Math.abs( mixer.timeScale )

    mixer.updateLoop( v )

    // play animations
    mixer.play( v )
  })
}

xrf.frag.t.default = {
  x:0,  // (play from) offset (in seconds)  
  y:0   // optional: (stop at) offset (in seconds)
}

// setup animation mixer for global scene & src scenes
xrf.addEventListener('parseModel', (opts) => {
  let {model} = opts
  let mixer   = model.mixer = new xrf.THREE.AnimationMixer(model.scene)
  mixer.model = model
  mixer.loop      = {timeStart:0,timeStop:0,speed:1.0}
  mixer.i         = xrf.mixers.length
  mixer.actions   = []

  model.animations.map( (anim) => { 
    anim.optimize()
    if( xrf.debug ) console.log("action: "+anim.name)
    mixer.actions.push( mixer.clipAction( anim, model.scene ) )
  })

  mixer.play  = (t) => {
    mixer.isPlaying = t.x !== undefined && t.x != t.y 
    mixer.updateLoop(t)
    xrf.emit( mixer.isPlaying === false ? 'stop' : 'play',{isPlaying: mixer.isPlaying})
  }

  mixer.stop = () => {
    mixer.play(false)
  }

  mixer.updateLoop = (t) => {
    if( t ){
      mixer.loop.timeStart = t.x != undefined ? t.x : mixer.loop.timeStart
      mixer.loop.timeStop  = t.y != undefined ? t.y : mixer.duration
    }
    mixer.actions.map( (action) => { 
      if( mixer.loop.timeStart != undefined ){
        action.time = mixer.loop.timeStart
        action.setLoop( xrf.THREE.LoopOnce, )
        action.timeScale = mixer.timeScale
        action.enabled = true
        if( t && t.x != undefined ) action.play() 
      }
    })
    mixer.setTime(mixer.loop.timeStart)
    mixer.time = Math.abs( mixer.loop.timeStart )
    mixer.update(0)
  }

  // monkeypatch: update loop when needed 
  if( !mixer.update.patched ){

    let update = mixer.update
    mixer.update = function(time){
      mixer.time = Math.abs(mixer.time)
      if( time == 0 ) return update.call(this,time)

      // loop jump
      if( mixer.loop.timeStop > 0 && mixer.time > mixer.loop.timeStop ){ 
        if( mixer.loop.enabled ){
          setTimeout( () => mixer.updateLoop(), 0 ) // prevent recursion
        }else mixer.stop()
      }
      return update.call( this, time )
    }
    mixer.update.patched = true
  }

  // calculate total duration/frame based on longest animation
  mixer.duration  = 0
  if( model.animations.length ){
    model.animations.map( (a) => mixer.duration = ( a.duration > mixer.duration ) ? a.duration : mixer.duration )
  }

  xrf.mixers.push(mixer)
})

if( document.location.hash.match(/t=/) ){
  let url = document.location.href
  let playAfterUserGesture = () => {
    xrf.hashbus.pub(url) // re-post t fragment on the hashbus again
    window.removeEventListener('click',playAfterUserGesture)
    window.removeEventListener('touchstart',playAfterUserGesture)
  }
  window.addEventListener('click', playAfterUserGesture )
  window.addEventListener('touchstart', playAfterUserGesture )
}

xrf.addEventListener('render', (opts) => {
  let model = xrf.model
  let {time} = opts
  if( !model ) return 
  if( xrf.mixers.length ){
    xrf.mixers.map( (m) => m.isPlaying && (m.update( time )) )

    // update active camera in case selected by dynamicKey in URI 
    if( xrf.model.camera && xrf.model.camera.length && model.mixer.isPlaying ){

      let cam = xrf.camera.getCam() 
      // cam.fov = model.cameras[0].fov (why is blender not exporting radians?)
      cam.far = model.cameras[0].far
      cam.near = model.cameras[0].near

      let rig = xrf.camera
      rig.position.copy( model.cameras[0].position )
      rig.position.y -= rig.offsetY // VR/AR compensate camera rig
      //rig.rotation.copy( model.cameras[0].rotation )

      rig.updateProjectionMatrix()
    }
  }
})

// remove mixers and stop mixers when loading another scene
xrf.addEventListener('reset', (opts) => {
  xrf.mixers.map( (m) => m.stop())
  xrf.mixers = []
})
xrf.frag.uv = function(v, opts){
  let { frag, mesh, model, camera, scene, renderer, THREE} = opts

  if( !mesh.geometry ) return // nothing to do here
  if( v.floats.length != 4 ) return console.warn('xrfragment.js: got less than 4 uv values ')

  xrf.frag.uv.init(mesh)
  mesh.uv.u      = v.floats[0]
  mesh.uv.v      = v.floats[1]
  mesh.uv.uspeed = v.floats[2] || 1.0
  mesh.uv.vspeed = v.floats[3] || 1.0
  mesh.uv.ushift = v.shift[0]  || v.floats[0] < 0  // negative u is always relative 
  mesh.uv.vshift = v.shift[1]  || v.floats[1] < 0  // negative v is always relative
  mesh.uv.uloop  = v.shift[2]  || false
  mesh.uv.vloop  = v.shift[3]  || false

  mesh.onBeforeRender = xrf.frag.uv.scroll
}

xrf.frag.uv.init = function(mesh){
  if( !mesh.uv  ) mesh.uv   = {u:0, v:0, uspeed:1, vspeed:1, uloop:false, vloop:false, uv:false}

  let uv    = mesh.geometry.getAttribute("uv")
  if( !uv.old ) uv.old = mesh.geometry.getAttribute("uv").clone()
}

xrf.frag.uv.scroll = function(){

  let diffU  = 0.0 // distance to end-state (non-looping mode)
  let diffV  = 0.0 // distance to end-state (non-looping mode)
  let uv     = this.geometry.getAttribute("uv")

  // translate!
  for( let i = 0; i < uv.count; i++ ){

    if( this.uv.uspeed == 1.0 ) uv.setX(i, this.uv.ushift ? uv.getX(i) + this.uv.u : uv.old.getX(i) + this.uv.u )
    if( this.uv.vspeed == 1.0 ) uv.setY(i, this.uv.vshift ? uv.getY(i) + this.uv.v : uv.old.getY(i) + this.uv.v )

    if( this.uv.uspeed != 1.0 || this.uv.vspeed != 1.0 ){
      let u         = uv.getX(i)     
      let v         = uv.getY(i)     
      let uTarget   = this.uv.ushift ? uv.getX(i) + this.uv.u : uv.old.getX(i) + this.uv.u
      let vTarget   = this.uv.vshift ? uv.getY(i) + this.uv.v : uv.old.getY(i) + this.uv.v

      // scroll U
      if( this.uv.uloop ){
        u += this.uv.uspeed * xrf.clock.delta
      }else{
        // *TODO* tween to offset
        //// recover from super-high uv-values due to looped scrolling
        //if( Math.abs(u-uTarget) > 10.0 ) u = uv.old.getX(i)
        //u = u > uTarget ?  u + (this.uv.uspeed * -xrf.clock.delta)
        //                :  u + (this.uv.uspeed *  xrf.clock.delta) 
        //diffU += Math.abs( u - uTarget )  // are we done yet? (non-looping mode)
      } 

      // scroll V
      if( this.uv.vloop ){
        v += this.uv.vspeed * xrf.clock.delta
      }else{
        // *TODO* tween to offset
        //// recover from super-high uv-values due to looped scrolling
        //// recover from super-high uv-values due to looped scrolling
        //if( Math.abs(v-vTarget) > 10.0 ) v = uv.old.getY(i)
        //v = v > vTarget ?  v + (this.uv.vspeed * -xrf.clock.delta)
        //                :  v + (this.uv.vspeed *  xrf.clock.delta) 
        //diffV += Math.abs( v - vTarget )
      }
      uv.setXY(i,u,v)
    }

  }
  uv.needsUpdate = true

  if( (!this.uv.uloop && diffU < 0.05) &&
      (!this.uv.vloop && diffV < 0.05)
  ){ // stop animating if done
    this.onBeforeRender = function(){}
  }
}
xrf.getCollisionMeshes = () => {
  let meshes = []
  xrf.scene.traverse( (n) => {
    if( n.type == 'Mesh' && !n.userData.href && !n.userData.src && xrf.hasNoMaterial(n) ){
      meshes.push(n)
    }
  })
  return meshes
}
// wrapper to collect interactive raycastable objects 

xrf.interactiveGroup = function(THREE,renderer,camera){

  let {
    Group,
    Matrix4,
    Raycaster,
    Vector2
  } = THREE 

  const _pointer = new Vector2();
  const _event = { type: '', data: _pointer };
  let object   = {selected:false}

  class interactive extends Group {

    constructor( renderer, camera ) {

      super();

      if( !renderer || !camera ) return 

      // extract camera when camera-rig is passed
      camera.traverse( (n) =>  String(n.type).match(/Camera/) ? camera = n : null )

      const scope = this;
      scope.objects = []
      scope.raycastAll = false


      const raycaster = new Raycaster();
      const tempMatrix = new Matrix4();

      // Pointer Events
      const element = renderer.domElement;

      const getAllMeshes = (scene) => {
        let objects = []
        xrf.scene.traverse( (n) => {
          if( !n.material || n.type != 'Mesh' ) return
          objects.push(n)
        })
        return objects
      }

      function onPointerEvent( event ) {

        //event.stopPropagation();

        const rect = renderer.domElement.getBoundingClientRect();

        _pointer.x = ( event.clientX - rect.left ) / rect.width * 2 - 1;
        _pointer.y = - ( event.clientY - rect.top ) / rect.height * 2 + 1;

        raycaster.setFromCamera( _pointer, camera );

        let objects = scope.raycastAll ? getAllMeshes(xrf.scene) : scope.objects 
        const intersects = raycaster.intersectObjects( objects, false )

        if ( intersects.length > 0 ) {

          const intersection = intersects[ 0 ];

          object = intersection.object;
          const uv = intersection.uv;

          _event.type = event.type;
          if( uv ) _event.data.set( uv.x, 1 - uv.y );
          object.dispatchEvent( _event );

        }else{
          if( object.selected ) {
            _event.type = 'mouseleave'
            object.dispatchEvent( _event)
          }
        }

      }

      element.addEventListener( 'pointerdown', onPointerEvent );
      element.addEventListener( 'pointerup', onPointerEvent );
      element.addEventListener( 'pointermove', onPointerEvent );
      element.addEventListener( 'mousedown', onPointerEvent );
      element.addEventListener( 'mousemove', onPointerEvent );
      element.addEventListener( 'click', onPointerEvent );
      element.addEventListener( 'mouseup', onPointerEvent );

      // WebXR Controller Events
      // TODO: Dispatch pointerevents too

      const eventsMapper = {
        'move': 'mousemove',
        'select': 'click',
        'selectstart': 'mousedown',
        'selectend': 'mouseup'
      };

      function onXRControllerEvent( event ) {

        const controller = event.target;

        tempMatrix.identity().extractRotation( controller.matrixWorld );

        raycaster.ray.origin.setFromMatrixPosition( controller.matrixWorld );
        raycaster.ray.direction.set( 0, 0, - 1 ).applyMatrix4( tempMatrix );

        let objects = scope.raycastAll ? getAllMeshes(xrf.scene) : scope.objects 
        const intersects = raycaster.intersectObjects( objects, false )

        if ( intersects.length > 0 ) {

          console.log(object.name)

          const intersection = intersects[ 0 ];

          object = intersection.object;
          const uv = intersection.uv;

          _event.type = eventsMapper[ event.type ];
          if( uv ) _event.data.set( uv.x, 1 - uv.y );

          object.dispatchEvent( _event );

        }else{
          if( object.selected ) {
            _event.type = 'mouseleave'
            object.dispatchEvent(_event)
          }
        }

      }

      const controller1 = renderer.xr.getController( 0 );
      controller1.addEventListener( 'move', onXRControllerEvent );
      controller1.addEventListener( 'select', onXRControllerEvent );
      controller1.addEventListener( 'selectstart', onXRControllerEvent );
      controller1.addEventListener( 'selectend', onXRControllerEvent );

      const controller2 = renderer.xr.getController( 1 );
      controller2.addEventListener( 'move', onXRControllerEvent );
      controller2.addEventListener( 'select', onXRControllerEvent );
      controller2.addEventListener( 'selectstart', onXRControllerEvent );
      controller2.addEventListener( 'selectend', onXRControllerEvent );

    }

    // we create our own add to avoid unnecessary unparenting of buffergeometries from 
    // their 3D model (which breaks animations)
    add(obj, unparent){
      if( unparent ) Group.prototype.add.call( this, obj )
      this.objects.push(obj)
    }

  }

  return new interactive(renderer,camera)
}
xrf.optimize = (opts) => {
  opts.animatedObjects = []

  xrf.optimize
  .checkAnimations(opts)
  .freezeUnAnimatedObjects(opts)
  .disableShadows(opts)
  .disableEmbeddedLights(opts)
  .removeDuplicateLights()
}

  // check unused animations
xrf.optimize.checkAnimations = (opts) => {
  if( xrf.debug ) console.log("TODO: fix freezeUnAnimatedObjects for SRC's")
  return xrf.optimize

  let {model} = opts
  model.animations.map( (anim) => {  
    // collect zombie animations and warn user
    let zombies = anim.tracks.map( (t) => {
      let name = t.name.replace(/\..*/,'')
      let obj  = model.scene.getObjectByName(name)
      if( !model.scene.getObjectByName(name) ) return {anim:anim.name,obj:name}
      else opts.animatedObjects.push(name)
      return undefined
    })
    if( zombies.length > 0 ){ // only warn for zombies in main scene (because src-scenes might be filtered anyways)
      zombies
      .filter( (z) => z ) // filter out undefined
      .map( (z) => console.warn(`gltf: object '${z.obj}' not found (anim: '${z.anim}'`) )
      console.warn(`TIP: remove dots in objectnames in blender (which adds dots when duplicating)`)
    } 
  })
  return xrf.optimize
}

xrf.optimize.freezeUnAnimatedObjects = (opts) => {
  if( xrf.todo ) console.log("TODO: fix freezeUnAnimatedObjects for SRC's")
  return xrf.optimize

  let {model}  = opts
  let scene = model.scene
  // increase performance by freezing all objects
  scene.traverse( (n) => n.matrixAutoUpdate = false )
  // except animated objects and children
  scene.traverse( (n) => {
    if( ~opts.animatedObjects.indexOf(n.name) ){
      n.matrixAutoUpdate = true 
      n.traverse( (m) => m.matrixAutoUpdate = true )
    }
  })
  return xrf.optimize
}

xrf.optimize.disableShadows = (opts) => {
  opts.model.scene.traverse( (n) => {
    if( n.castShadow !== undefined ) n.castShadow = false
  })
  return xrf.optimize
}

xrf.optimize.disableEmbeddedLights = (opts) => {
  if( !opts.isSRC ) return xrf.optimize
  // remove lights from SRC's
  opts.model.scene.traverse( (n) => {
    if( n.type.match(/light/i) ) n.remove()
  })
  return xrf.optimize
}

xrf.optimize.removeDuplicateLights = () => {
  // local/extern src's can cause duplicate lights which tax performance 
  let lights = {}
  xrf.scene.traverse( (n) => {
    if( n.type.match(/light/i) ){
      if( !lights[n.name] ) lights[n.name] = true 
      else n.remove()
    }
  })
  return xrf.optimize
}

xrf.addEventListener('parseModel', (opts) => {
  xrf.optimize(opts)
})
// switch camera when multiple cameras for url #mycameraname

xrf.addEventListener('dynamicKey', (opts) => {
  // select active camera if any
  let {id,match,v} = opts
  match.map( (w) => {
    w.nodes.map( (node) => {
      if( node.isCamera ){ 
        console.log("switching camera to cam: "+node.name)
        xrf.model.camera = node 
      }
    })
  })
})
// switch camera when multiple cameras for url #mycameraname

xrf.addEventListener('navigateLoaded', (opts) => {
  let els       = xrf.getCollisionMeshes()
  let invisible = false
  els.map( (mesh) => {
    if( !invisible ){
      invisible = mesh.material.clone()
      invisible.visible = false
    }
    mesh.material = invisible 
    xrf.emit('collisionMesh', mesh )
  })
})

const doFilter = (opts) => {
  let {scene,id,match,v} = opts
  if( v.filter ){
    let frags = {}
    frags[ v.filter.key ] = v
    xrf.filter.scene({frag:frags,scene})
  }
}

xrf.addEventListener('dynamicKey', doFilter )
xrf.addEventListener('dynamicKeyValue', (opts) => {
  if( xrf.debug ) console.log("*TODO* filter integers only")
  // doFilter(opts)
})

// spec: https://xrfragment.org/#filters
xrf.filter = function(query, cb){
  let result = []
  if( !query     ) return result
  if( query[0] != '#' ) query = '#'+query
  // *TODO* jquery like utility func
  return result
}

xrf.filter.scene = function(opts){
  let {scene,frag} = opts

  scene = xrf.filter 
  .sort(frag)               // get (sorted) filters from XR Fragments
  .process(frag,scene,opts) // show/hide things

  scene.visible = true   // always enable scene

  return scene
}

xrf.filter.sort = function(frag){
  // get all filters from XR Fragments
  frag.filters = Object.values(frag)
                      .filter( (v) => v.filter ? v : null )
                      .sort( (a,b) => a.index > b.index )
  return xrf.filter
}

// opts = {copyScene:true} in case you want a copy of the scene (not filter the current scene inplace)
xrf.filter.process = function(frag,scene,opts){
  const cleanupKey   = (k) => k.replace(/[-\*\/]/g,'')
  let firstFilter    = frag.filters.length ? frag.filters[0].filter.get() : false 
  const hasName      = (m,name,filter)        => m.name == name 
  const hasNameOrTag = (m,name_or_tag,filter) => hasName(m,name_or_tag) || 
                                                 String(m.userData['tag']).match( new RegExp("(^| )"+name_or_tag) )

  // utility functions
  const getOrCloneMaterial = (o) => {
    if( o.material ){
      if( o.material.isXRF ) return o.material
      o.material = o.material.clone()
      o.material.isXRF = true
      return o.material
    }
    return {}
  }
  const setVisible = (n,visible,filter,processed) => {
    if( processed && processed[n.uuid] ) return 
    getOrCloneMaterial(n).visible = visible
    if( filter.deep ) n.traverse( (m) => getOrCloneMaterial(m).visible = visible )
    if( processed ) processed[n.uuid] == true 
  }

  // spec 2: https://xrfragment.org/doc/RFC_XR_Macros.html#embedding-xr-content-using-src
  // reparent scene based on objectname in case it matches a (non-negating) selector 
  if( opts.reparent && firstFilter && !firstFilter.value && firstFilter.show === true ){
    let obj 
    frag.target = firstFilter
    scene.traverse( (n) => hasName(n, firstFilter.key,firstFilter) && (obj = n) )
    if( xrf.debug ) console.log("reparent "+firstFilter.key+" "+((opts.copyScene)?"copy":"inplace"))
    if(obj ){
      obj.position.set(0,0,0)
      if( opts.copyScene ) {
        opts.copyScene = new xrf.THREE.Scene()
        opts.copyScene.children[0] = obj  // we dont use .add() as that reparents it from the original scene
      }else{
        // empty current scene and add obj
        while( scene.children.length > 0 ) scene.children[0].removeFromParent()
        scene.add( obj )
      }
    }else{
      console.warn("could not reparent scene to object "+firstFilter.key+" (not found)")
      opts.copyScene = new xrf.THREE.Scene() // return empty scene
    } 
    if( opts.copyScene ) scene = opts.copyScene
    if( obj ) obj.isReparented = true
  }

  // then show/hide things based on secondary selectors
  // we don't use the XRWG (everything) because we process only the given (sub)scene
  frag.filters.map( (v) => {
    const filter  = v.filter.get()
    const name_or_tag = cleanupKey(v.fragment)
    let processed = {}
    let extembeds = {}

    // hide external objects temporarely (prevent them getting filtered too)
    scene.traverse( (m) => {
      if( m.isSRCExternal ){
        m.traverse( (n) => (extembeds[ n.uuid ] = m) && (m.visible = false) )
      }
    })

    scene.traverseVisible( (m) => {
      // filter on value(expression) #foo=>3 e.g. *TODO* do this in XRWG
      if( filter.value && m.userData[filter.key] ){
        const visible = v.filter.testProperty(filter.key, m.userData[filter.key], filter.show === false )
        setVisible(m,visible,filter,processed)
        return
      }
      if( hasNameOrTag(m,name_or_tag,filter ) ){
        setVisible(m,filter.show,filter)
      }
    })

    // show external objects again 
    for ( let i in extembeds ) extembeds[i].visible = true
  })

  return scene 
}

xrf.frag.dynamic.material = function(v,opts){
  let {match} = opts

  // update material in case of <tag_or_object>[*]=<materialname>
  let material
  xrf.scene.traverse( (n) => n.material && (n.material.name == v.string) && (material = n.material) )
  if( !material && !v.reset ) return // nothing to do

  xrf.XRWG.deepApplyMatch(match, v, (match,v,node,type) => {
    if( node.material ) xrf.frag.dynamic.material.set( node, material, v.reset )
  })
}

xrf.frag.dynamic.material.set = function(mesh,material,reset){
  if( !mesh.materialOriginal ) mesh.materialOriginal = mesh.material
  let visible = mesh.material.visible //remember
  if( reset ){
    mesh.material = mesh.materialOriginal
  }else mesh.material = material
  mesh.material.visible = visible
}

// for reset calls like href: xrf://!myobject e.g.
xrf.addEventListener('dynamicKey', (opts) => {

  let {v,match} = opts

  if( v.reset ){
    xrf.XRWG.deepApplyMatch(match,v, (match,v,node,type) => {
      if( node.material ) xrf.frag.dynamic.material.set( node, null, v.reset )
    })
  } 

})

// this holds all the URI Template variables (https://www.rfc-editor.org/rfc/rfc6570)

xrf.addEventListener('parseModel', (opts) => {
  let {model,url,file} = opts
  if( model.isSRC || opts.isSRC ) return // ignore SRC models

  xrf.URI.vars = new Proxy({},{
    set(me,k,v){ 
      if( k.match(/^(name)$/) ) return
      me[k] = v 
    },
    get(me,k  ){ 
      if( k == '__object' ){
        let obj = {}
        for( let i in xrf.URI.vars ) obj[i] = xrf.URI.vars[i]()
        return obj
      }
      return me[k] 
    },
  })

  model.scene.traverse( (n) => {
    const variables = /{([a-zA-Z0-9-]+)}/g

    if( n.userData ){
      for( let i in n.userData ){
        if( i[0] == '#' || i.match(/^(href|tag)$/) ) continue // ignore XR Fragment aliases
        if( i == 'src' ){
          // lets declare empty variables found in src-values ('https://foo.com/video.mp4#{somevar}') e.g.
          if( n.userData[i].match(variables) ){
            let vars = [].concat( n.userData[i].match(variables) )
            const strip = (v) => v.replace(/[{}]/g,'')
            vars.map( (v) => xrf.URI.vars[ strip(v) ] = () => '' )
          }
        }else xrf.URI.vars[i] = () => n.userData[i] // declare variables with values
      }
    }
  })

})


xrf.addEventListener('dynamicKeyValue', (opts) => {
  // select active camera if any
  let {id,match,v} = opts

  if( !v.is( xrf.XRF.CUSTOMFRAG) ) return // only process custom frags from here
  if( v.string.match(/(<|>)/) )    return // ignore filter values
   
  if( match.length > 0 ){
    xrf.frag.dynamic.material(v,opts) // check if fragment is an objectname
  }
  
  if( !xrf.URI.vars[ v.string ] )           return console.error(`'${v.string}' metadata-key not found in scene`)        
  //if( xrf.URI.vars[ id ] && !match.length ) return console.error(`'${id}'       object/tag/metadata-key not found in scene`)

  if( xrf.debug ) console.log(`URI.vars[${id}]='${v.string}'`)

  if( xrf.URI.vars[id] ){
    xrf.URI.vars[ id ] = xrf.URI.vars[ v.string ]     // update var
    xrf.scene.traverse( (n) => {                      
      // re-expand src-values which use the updated URI Template var 
      if( n.userData && n.userData.src && n.userData.srcTemplate && n.userData.srcTemplate.match(`{${id}}`) ){
        let srcNewFragments = xrf.frag.src.expandURI( n ).replace(/.*#/,'')
        console.log(`URI.vars[${id}] => updating ${n.name} => ${srcNewFragments}`)
        let frag = xrf.hashbus.pub( srcNewFragments, n )
      }
    })
  }else{
    xrf.XRWG.deepApplyMatch(match, v, (match,v,node,type) => {
      console.log(v.string)
      if( node.geometry ) xrf.hashbus.pub( xrf.URI.vars[ v.string ](), node) // apply fragment mesh(es)
    })
  }

})
xrf.addEventListener('dynamicKey', (opts) => {
  let {scene,id,match,v} = opts
  if( !scene ) return 
  let remove = []

  // erase previous lines
  xrf.focusLine.lines.map( (line) => line.parent && (line.parent.remove(line))  )
  xrf.focusLine.points = []
  xrf.focusLine.lines  = []

  // drawlines
  match.map( (w) => {
    w.nodes.map( (mesh) => xrf.drawLineToMesh({ ...opts, mesh}) )
  })
})

xrf.drawLineToMesh = (opts) => {
  let {scene,mesh,frag,id} = opts
  const THREE = xrf.THREE
  let oldSelection
  // Selection of Interest if predefined_view matches object name
  if( mesh.visible && mesh.material){
    xrf.emit('focus',{...opts,frag})
    .then( () => {
      const color    = new THREE.Color();
      const colors   = []
      let from       = new THREE.Vector3()

      let getCenterPoint = (mesh) => {
        var geometry = mesh.geometry;
        geometry.computeBoundingBox();
        var center = new THREE.Vector3();
        geometry.boundingBox.getCenter( center );
        mesh.localToWorld( center );
        return center;
      }         

      let cam = xrf.camera.getCam ? xrf.camera.getCam() : xrf.camera // *FIXME* camerarig/rig are conflicting
      cam.updateMatrixWorld(true); // always keeps me diving into the docs :]
      cam.getWorldPosition(from)
      from.y = 0.5 // originate from the heart chakra! :p
      const points = [from, getCenterPoint(mesh) ]
      const geometry = new THREE.BufferGeometry().setFromPoints( points );
      let line = new THREE.Line( geometry, xrf.focusLine.material );
      line.isXRF = true
      line.computeLineDistances();
      xrf.focusLine.lines.push(line)
      xrf.focusLine.points.push(from)
      xrf.focusLine.opacity = 1
      scene.add(line)
    })
  }
}

xrf.addEventListener('render', (opts) => {
  // update focusline 
  let {time,model} = opts
  if( !xrf.clock ) return 
  xrf.focusLine.material.color.r  = (1.0 + Math.sin( xrf.clock.getElapsedTime()*10  ))/2
  xrf.focusLine.material.dashSize = 0.2 + 0.02*Math.sin( xrf.clock.getElapsedTime()  )
  xrf.focusLine.material.gapSize  = 0.1 + 0.02*Math.sin( xrf.clock.getElapsedTime() *3  )
  xrf.focusLine.material.opacity  = (0.25 + 0.15*Math.sin( xrf.clock.getElapsedTime() * 3 )) * xrf.focusLine.opacity;
  if( xrf.focusLine.opacity > 0.0 ) xrf.focusLine.opacity -= time*0.2
  if( xrf.focusLine.opacity < 0.0 ) xrf.focusLine.opacity = 0
})
/*
 * mimetype: audio/aac 
 * mimetype: audio/mpeg 
 * mimetype: audio/ogg 
 * mimetype: audio/weba 
 * mimetype: audio/wav 
 */

let loadAudio = (mimetype) => function(url,opts){
  let {mesh,src,camera,THREE} = opts
  let {urlObj,dir,file,hash,ext} = xrf.parseUrl(url)
  let frag = xrf.URI.parse( url )

  xrf.init.audio()
  let isPositionalAudio = !(mesh.position.x == 0 && mesh.position.y == 0 && mesh.position.z == 0)
  const audioLoader = new THREE.AudioLoader();
  let sound = isPositionalAudio ? new THREE.PositionalAudio( camera.listener) 
                                : new THREE.Audio( camera.listener )

  mesh.media = mesh.media || {}
  mesh.media.audio = { set: (mediafragment,v) => mesh.media.audio[mediafragment] = v }

  let finalUrl = url.replace(/#.*/,'')
  if( xrf.debug != undefined ) console.log("GET "+finalUrl)
  audioLoader.load( finalUrl, function( buffer ) {

    sound.setBuffer( buffer );
    sound.setLoop(false);
    sound.setVolume( 1.0 )
    if( isPositionalAudio ){
      sound.setRefDistance( mesh.scale.x);
      sound.setRolloffFactor(20.0)
      //sound.setDirectionalCone( 360, 360, 0.01 );
    }else sound.setVolume( mesh.scale.x )

    mesh.add(sound)

    sound.set = (mediafragment,v) => {
      try{
        sound[mediafragment] = v 

        if( mediafragment == 't'){

          if( sound.isPlaying && v.y != undefined && v.x == v.y ){
            sound.offset = v.x * buffer.sampleRate ;
            sound.pause()
            return 
          }else sound.stop()

          // apply embedded audio/video samplerate/fps or global mixer fps
          sound.setLoopStart(v.x);
          sound.setLoopEnd(v.y || buffer.duration);
          sound.offset = v.x;
          sound.play()
        }

        if( mediafragment == 's'){
          // *TODO* https://stackoverflow.com/questions/12484052/how-can-i-reverse-playback-in-web-audio-api-but-keep-a-forward-version-as-well 
          sound.pause()
          sound.setPlaybackRate( Math.abs(v.x) ) // WebAudio does not support negative playback
          sound.play()
        }

        if( mediafragment == 'loop'){
          sound.pause()
          sound.setLoop( v.loop )
          sound.play()
        }
      }catch(e){ console.warn(e) }
    }

    let lazySet = {}
    let mediaFragments = ['loop','s','t']
    mediaFragments.map( (f) => mesh.media.audio[f] && (lazySet[f] = mesh.media.audio[f]) )
    mesh.media.audio = sound

    // autoplay if user already requested play (before the sound was loaded)
    mediaFragments.map( (f) => {
      if( lazySet[f] ) mesh.media.audio.set(f, lazySet[f] )
    })

  });

  // apply Media fragments from URL 
  (['t','loop','s']).map( (f) => { 
    if( frag[f] ){
      mesh.media.audio.set( f, frag[f] )
    }
  })

}

xrf.init.audio = (opts) => {
  let camera = xrf.camera
  /* WebAudio: setup context via THREEjs */
  if( !camera.listener ){
    camera.listener = new THREE.AudioListener();
    // *FIXME* camera vs camerarig conflict
    (camera.getCam ? camera.getCam() : camera).add( camera.listener );
    xrf.emit('audioInited',{listener:camera.listener, ...opts})
  }
}
xrf.addEventListener('init', xrf.init.audio )

// stop playing audio when loading another scene
xrf.addEventListener('reset', () => {
  xrf.scene.traverse( (n)  => {
    if( n.media && n.media.audio ){
      if( n.media.audio.stop ) n.media.audio.stop()
      if( n.media.audio.remove ) n.media.audio.remove()
    }
  })
})



let audioMimeTypes = [
  'audio/x-wav',
  'audio/wav',
  'audio/mpeg',
  'audio/mp3',
  'audio/weba',
  'audio/aac',
  'application/ogg'
]
audioMimeTypes.map( (mimetype) =>  xrf.frag.src.type[ mimetype ] = loadAudio(mimetype) )
/*
 * mimetype: model/gltf+json
 */

xrf.frag.src.type['fbx'] = function( url, opts ){
  return new Promise( async (resolve,reject) => {
    let {mesh,src} = opts
    let {urlObj,dir,file,hash,ext} = xrf.parseUrl(url)
    let loader

    let {THREE}        = await import('https://unpkg.com/three@0.161.0/build/three.module.js')
    let  { FBXLoader } = await import('three/addons/loaders/FBXLoader.js')
    debugger

    //const Loader = xrf.loaders[ext]
    //if( !Loader ) throw 'xrfragment: no loader passed to xrfragment for extension .'+ext 
    //if( !dir.match("://") ){ // force relative path 
    //  dir = dir[0] == './' ? dir : `./${dir}`
    //  loader = new Loader().setPath( dir )
    //}else loader = new Loader()

    //loader.load(url, (model) => {
    //  model.isSRC = true
    //  resolve(model)
    //})
  })
}

/* 
 * extensions: .frag/.fs/.vs/.vert
 */

xrf.frag.src.type['x-shader/x-fragment'] = function(url,opts){
  let {mesh,THREE} = opts

  let isFragmentShader = /\.(fs|frag|glsl)$/
  let isVertexShader   = /\.(vs|vert)$/

  let shaderReqs = []
  let shaderCode = {}
  let shader   = {
    fragment: { code: '', url: url.match( isFragmentShader ) ? url : '' },
    vertex:   { code: '', url: url.match( isVertexShader   ) ? url : '' }
  }
  
  var onShaderLoaded = ((args) => (type, status, code) => {
    shader[type].status = status 
    shader[type].code   = code 
    if( shader.fragment.code && shader.vertex.code ){
      
      let oldMaterial = mesh.material
      mesh.material = new THREE.RawShaderMaterial({
        uniforms: {
          time: { value: 1.0 },
          resolution: { value: new THREE.Vector2(1.0,1.0) }
        },
        // basic shaders include following common vars/funcs: https://github.com/mrdoob/three.js/blob/master/src/renderers/shaders/ShaderChunk/common.glsl.js
        fragmentShader: shader.fragment.status == 200 ? shader.fragment.code : THREE.ShaderChunk.meshbasic_frag,
        vertexShader:   shader.vertex.status   == 200 ? shader.vertex.code   : THREE.ShaderChunk.meshbasic_vert,

      });

      mesh.material.needsUpdate = true 
      mesh.needsUpdate          = true

      mesh.onBeforeRender = () => {
        if( !mesh.material || !mesh.material.uniforms ) return mesh.onBeforeRender = function(){}
        mesh.material.uniforms.time.value = xrf.clock.elapsedTime
      }

    }

  })({})

  // sidecar-load vertex shader file
  if( shader.fragment.url && !shader.vertex.url ){  
    shader.vertex.url = shader.fragment.url.replace(/\.fs$/,   '.vs')
                                           .replace(/\.frag$/, '.vert')
  }
   
  if( shader.fragment.url ){
    fetch(shader.fragment.url)
    .then( (res) => res.text().then( (code) => onShaderLoaded('fragment',res.status,code) ) )
  }

  if( shader.vertex.url ){
    fetch(shader.vertex.url)
    .then( (res) => res.text().then( (code) => onShaderLoaded('vertex',res.status,code) ) )
  }

}

xrf.frag.src.type['x-shader/x-vertex']   = xrf.frag.src.type['x-shader/x-fragment']

/*
 * mimetype: model/gltf+json
 */

xrf.frag.src.type['gltf'] = function( url, opts ){
  return new Promise( (resolve,reject) => {
    let {mesh,src} = opts
    let {urlObj,dir,file,hash,ext} = xrf.parseUrl(url)
    let loader

    const Loader = xrf.loaders[ext]
    if( !Loader ) throw 'xrfragment: no loader passed to xrfragment for extension .'+ext 
    if( !dir.match("://") ){ // force relative path 
      dir = dir.substr(0,2) == './' ? dir : `./${dir}`
      loader = new Loader().setPath( dir )
    }else{
      loader = new Loader() 
    }

    loader.load(url, (model) => {
      model.isSRC = true
      resolve(model)
    })
  })
}


let loadHTML = (mimetype) => function(url,opts){
  let {mesh,src,camera} = opts
  let {urlObj,dir,file,hash,ext} = xrf.parseUrl(url)
  let frag = xrf.URI.parse( url )
  console.warn("todo: html viewer for src not implemented")
}

let htmlMimeTypes = [
  'text/html'
]
htmlMimeTypes.map( (mimetype) =>  xrf.frag.src.type[ mimetype ] = loadHTML(mimetype) )
/*
 * mimetype: image/png 
 * mimetype: image/jpg 
 * mimetype: image/gif 
 */

xrf.frag.src.type['image/png'] = function(url,opts){
  let {mesh,THREE} = opts
  let restrictTo3DBoundingBox = mesh.geometry

  mesh.material = new xrf.THREE.MeshBasicMaterial({ 
    map: null, 
    transparent: url.match(/\.(png|gif)/) ? true : false,
    side: THREE.DoubleSide,
    color: 0xFFFFFF,
    opacity:1
  });

  let renderImage = (texture) => {
    //let   h = texture.source ? texture.source.data.height : texture.image.height 
    //let   w = texture.source ? texture.source.data.width : texture.image.width 

    // stretch image by pinning uv-coordinates to corners 
    if( mesh.geometry ){
      if( mesh.geometry.attributes.uv ){ // buffergeometries 
        let uv = mesh.geometry.attributes.uv;
      }else {
        console.warn("xrfragment: uv's of ${url} might be off for non-buffer-geometries *TODO*")
        //if( geometry.faceVertexUvs ){
        // *TODO* force uv's of dynamically created geometries (in threejs)
        //}
      }
    }
    mesh.material.map = texture
    mesh.material.needsUpdate = true 
    mesh.needsUpdate = true

    //// *TODO* update clones in portals or dont clone scene of portals..
    //xrf.scene.traverse( (n) => {
    //  if( n.userData.src == mesh.userData.src && mesh.uuid != n.uuid ){
    //    n.material = mesh.material
    //    n.material.needsUpdate = true
    //  }
    //})
  } 

  let onLoad = (texture) => {
    texture.colorSpace = THREE.SRGBColorSpace;
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;
    renderImage(texture)
  }

  new THREE.TextureLoader().load( url, onLoad, null, console.error );

}

xrf.frag.src.type['image/gif'] = xrf.frag.src.type['image/png']
xrf.frag.src.type['image/jpeg'] = xrf.frag.src.type['image/png']

// spec 8: https://xrfragment.org/doc/RFC_XR_Macros.html#embedding-xr-content-using-src

xrf.portalNonEuclidian = function(opts){
  let { frag, mesh, model, camera, scene, renderer} = opts

  mesh.portal = {
    pos: mesh.position.clone(),
    posWorld: new xrf.THREE.Vector3(),
    posWorldCamera: new xrf.THREE.Vector3(),
    stencilRef: xrf.portalNonEuclidian.stencilRef,
    needUpdate: false,
    stencilObject: false,
    cameraDirection: new xrf.THREE.Vector3(),
    cameraPosition: new xrf.THREE.Vector3(),
    raycaster: new xrf.THREE.Raycaster(),
    isLocal: opts.isLocal,
    isLens: false,
    isInside: false,
    setStencil:  (stencilRef) => mesh.portal.stencilObjects.traverse( (n) => showPortal(n, stencilRef == 0) && n.stencil && n.stencil(stencilRef) ),
    positionObjectsIfNeeded: (pos,scale)  => !mesh.portal.isLens &&  mesh.portal.stencilObjects.traverse( (n) =>  n.positionAtStencil && (n.positionAtStencil(pos,scale)) )
  }

  // allow objects to flip between original and stencil position (which puts them behind stencilplane)
  const addStencilFeature = (n) => { 
    if( n.stencil ) return n // run once

    n.stencil = (sRef ) => xrf.portalNonEuclidian.selectStencil(n, sRef )
    n.positionAtStencil = (pos,scale) => (newPos,newScale) => {
      n.position.copy( newPos || pos )
      n.scale.copy( scale )
      n.updateMatrixWorld(true)
    }
    // curry function 
    n.positionAtStencil = n.positionAtStencil( n.position.clone(), n.scale.clone() )
    return n
  }

  this.setupStencilObjects = (scene,opts) => {
    // collect related objects to render inside stencilplane
    let stencilObject         = scene 
    if( opts.srcFrag.target ){
      stencilObject = scene.getObjectByName( opts.srcFrag.target.key ) 
      // spec: if src-object is child of portal (then portal is lens, and should include all children )
      mesh.traverse( (n) => n.name == opts.srcFrag.target.key && (stencilObject = n) && (mesh.portal.isLens = true) ) 
    }

    if( !stencilObject ) return console.warn(`no objects were found (src:${mesh.userData.src}) for (portal)object name '${mesh.name}'`)
    mesh.portal.stencilObject = stencilObject 

    // spec: if src points to child, act as lens
    if( !mesh.portal.isLocal || mesh.portal.isLens )  stencilObject.visible = false 

    let stencilObjects = [stencilObject]
    stencilObjects = stencilObjects
                     .filter( (n) => !n.portal ) // filter out (self)references to portals (prevent recursion)
                     .map(addStencilFeature)

    // put it into a scene (without .add() because it reparents objects) so we can render it separately
    mesh.portal.stencilObjects = new xrf.THREE.Scene()
    mesh.portal.stencilObjects.children = stencilObjects 
    mesh.portal.stencilObjects.isXRF = true 

    xrf.portalNonEuclidian.stencilRef += 1 // each portal has unique stencil id
    if( xrf.debug ) console.log(`enabling portal for object '${mesh.name}' (stencilRef:${mesh.portal.stencilRef})`)
  
    return this
  }

  // enable the stencil-material of the stencil objects to prevent stackoverflow (portal in portal rendering)
  const showPortal = (n,show) => {
    if( n.portal ) n.visible = show
    return true
  }

  this.setupListeners = () => {

    // below is a somewhat weird tapdance to render the portals **after** the scene 
    // is rendered (otherwise it messes up occlusion)

    mesh.onAfterRender = function(renderer, scene, camera, geometry, material, group ){
      mesh.portal.needUpdate = true
    }

    xrf.addEventListener('renderPost', (opts) => {
      let {scene,camera,time,render,renderer} = opts

      if( mesh.portal.needUpdate && mesh.portal && mesh.portal.stencilObjects ){  
        let cameraDirection            = mesh.portal.cameraDirection
        let cameraPosition             = mesh.portal.cameraPosition
        let stencilRef                 = mesh.portal.stencilRef
        let newPos                     = mesh.portal.posWorld
        let stencilObject              = mesh.portal.stencilObject
        let newScale                   = mesh.scale 
        let raycaster                  = mesh.portal.raycaster

        let cam = xrf.camera.getCam ? xrf.camera.getCam() : camera
        cam.getWorldPosition(cameraPosition)
        cam.getWorldDirection(cameraDirection)
        if( cameraPosition.distanceTo(newPos) > 15.0 ) return // dont render far portals 

        // init
        if( !mesh.portal.isLocal || mesh.portal.isLens ) stencilObject.visible = true 
        mesh.portal.setStencil(stencilRef)
        renderer.autoClear             = false 
        renderer.autoClearDepth        = false 
        renderer.autoClearColor        = false 
        renderer.autoClearStencil      = false 
        // render
        render( mesh.portal.stencilObjects, camera )
        // de-init 
        renderer.autoClear             = true 
        renderer.autoClearDepth        = true 
        renderer.autoClearColor        = true 
        renderer.autoClearStencil      = true 
        mesh.portal.setStencil(0)
        if( !mesh.portal.isLocal || mesh.portal.isLens ) stencilObject.visible = false 


        // trigger href upon camera collide
        if( mesh.userData.XRF.href ){
          raycaster.far = 0.35
          raycaster.set(cameraPosition, cameraDirection )
          intersects = raycaster.intersectObjects([mesh], false)
          if (intersects.length > 0 && !mesh.portal.teleporting ){
            mesh.portal.teleporting = true
            mesh.userData.XRF.href.exec({nocommit:true})
            setTimeout( () => mesh.portal.teleporting = false, 500) // dont flip back and forth
          }
        }
      }
      mesh.portal.needUpdate = false
    })



    return this
  }

  // turn mesh into stencilplane 
  xrf
  .portalNonEuclidian
  .setMaterial(mesh)
  .getWorldPosition(mesh.portal.posWorld)

  this
  .setupListeners()
  .setupStencilObjects(scene,opts)

  // move portal objects to portalposition
  if( mesh.portal.stencilObjects ) mesh.portal.positionObjectsIfNeeded(mesh.portal.posWorld, mesh.scale)
}

xrf.portalNonEuclidian.selectStencil = (n, stencilRef, nested) => {
  if( n.material ){
    n.material.stencilRef   = stencilRef 
    n.material.stencilWrite = stencilRef > 0 
    n.material.stencilFunc  = xrf.THREE.EqualStencilFunc;
  }
  if( n.children && !nested ) n.traverse( (m) => !m.portal && (xrf.portalNonEuclidian.selectStencil(m,stencilRef,true)) )
}
  
xrf.portalNonEuclidian.setMaterial = function(mesh){
  mesh.material = new xrf.THREE.MeshBasicMaterial({ color: 'orange' });
  mesh.material.depthWrite   = false;
  mesh.material.colorWrite   = false;
  mesh.material.stencilWrite = true;
  mesh.material.stencilRef   = xrf.portalNonEuclidian.stencilRef;
  mesh.material.stencilFunc  = xrf.THREE.AlwaysStencilFunc;
  mesh.material.stencilZPass = xrf.THREE.ReplaceStencilOp;
  mesh.material.stencilZFail = xrf.THREE.ReplaceStencilOp;
  return mesh
}

xrf.addEventListener('parseModel',(opts) => {
  const scene = opts.model.scene
})


// (re)set portalObjects when entering/leaving a portal 
let updatePortals = (opts) => {
  xrf.scene.traverse( (n) => {
    if( !n.portal ) return 
    // move objects back to the portal 
    if( n.portal.isInside ) n.portal.positionObjectsIfNeeded( n.portal.posWorld, n.scale )
    n.portal.isInside = false
  })
  if( opts.mesh && opts.mesh.portal && opts.click ){
    opts.mesh.portal.isInside = true
    opts.mesh.portal.positionObjectsIfNeeded() // move objects back to original pos (since we are teleporting there)
  }
}

xrf.addEventListener('href', (opts) => opts.click && updatePortals(opts) )
xrf.addEventListener('navigate', updatePortals )

xrf.portalNonEuclidian.stencilRef = 1

let loadVideo = (mimetype) => function(url,opts){
  let {mesh,src,camera} = opts
  let {urlObj,dir,file,hash,ext} = xrf.parseUrl(url)
  const THREE = xrf.THREE
  let frag = xrf.URI.parse( url )

  mesh.media = mesh.media || {}

  let video = mesh.media.video = document.createElement('video')
  video.setAttribute("crossOrigin","anonymous")
  video.setAttribute("playsinline",'')
  video.addEventListener('loadedmetadata', function(){
    let texture = new THREE.VideoTexture( video );
    texture.colorSpace = THREE.SRGBColorSpace;
    let mat     = new xrf.THREE.MeshBasicMaterial()
    mat.map = texture
    mesh.material = mat
    // set range
    video.addEventListener('timeupdate', function timeupdate() {
      if (video.t && video.t.y !== undefined && video.t.y > video.t.x && Math.abs(video.currentTime) >= video.t.y ){
        if( video.looping ) video.currentTime = video.t.x // speed means loop
        else video.pause()
      }
    },false)
  })

  video.src = url
  video.speed = 1.0
  video.looping = false
  video.set = (mediafragment,v) => {
    video[mediafragment] = v

    if( mediafragment == 't'){
      video.pause()
      if( v.x !== undefined && v.x == v.y ) return // stop paused
      else{
        video.currentTime = v.x
        video.time = v.x
        video.play()
      }
    }
    if( mediafragment == 's' ){
      video.playbackRate = Math.abs( v.x ) // html5 video does not support reverseplay :/
    }
    if( mediafragment == 'loop' ){
      video.looping = true 
    }
  }
}

// stop playing audio when loading another scene
xrf.addEventListener('reset', () => {
  xrf.scene.traverse( (n)  => n.media && n.media.video && (n.media.video.pause()) && (n.media.video.remove()) )
})

let videoMimeTypes = [
  'video/ogg',
  'video/mp4'
]
videoMimeTypes.map( (mimetype) =>  xrf.frag.src.type[ mimetype ] = loadVideo(mimetype) )
window.AFRAME.registerComponent('xrf', {
  schema: {
    http: { type:'string'},
    https: { type:'string'},
  },
  init: async function () {

    // override this.data when URL has passed (`://....com/?https://foo.com/index.glb` e.g.)
    if( typeof this.data == "string" ){
      let searchIsUri = document.location.search && 
                        !document.location.search.match(/=/) &&
                        document.location.search.match("/")
      if( searchIsUri || document.location.hash.length > 1 ){ // override url
        this.data = `${document.location.search.substr(1)}${document.location.hash}`
      }
    }

    if( !AFRAME.XRF ){

      let aScene = this.el.sceneEl

      // enable XR fragments
      let XRF = AFRAME.XRF = xrf.init({
        THREE,
        camera:    aScene.camera,
        scene:     aScene.object3D,
        renderer:  aScene.renderer,
        loaders: { 
          gltf: THREE.GLTFLoader, // which 3D assets (exts) to check for XR fragments?
          glb:  THREE.GLTFLoader,
          obj:  THREE.OBJLoader,
          fbx:  THREE.FBXLoader,
          usdz: THREE.USDZLoader,
          col:  THREE.ColladaLoader
        }
      })
      aScene.renderer.toneMapping = THREE.ACESFilmicToneMapping;
      aScene.renderer.toneMappingExposure = 1.25;
      if( !XRF.camera ) throw 'xrfragment: no camera detected, please declare <a-entity camera..> ABOVE entities with xrf-attributes'
        
      if( AFRAME.utils.device.isMobile() ){
 //        aScene.setAttribute('webxr',"requiredFeatures: dom-overlay; overlayElement: canvas; referenceSpaceType: local")
      }

      aScene.addEventListener('loaded', () => {
        // this is just for convenience (not part of spec): enforce AR + hide/show stuff based on VR tags in 3D model 
        ARbutton = document.querySelector('.a-enter-ar-button')
        VRbutton = document.querySelector('.a-enter-vr-button')
        if( ARbutton ) ARbutton.addEventListener('click', () => AFRAME.XRF.hashbus.pub( '#-VR' ) )
        if( VRbutton ) VRbutton.addEventListener('click', () => AFRAME.XRF.hashbus.pub( '#VR' ) )
        //if( AFRAME.utils.device.checkARSupport() && VRbutton ){
        //  VRbutton.style.display = 'none'
        //  ARbutton.parentNode.style.right = '20px'
        //}
      })

      let repositionUser = (scale) => () => {
        // sometimes AFRAME resets the user position to 0,0,0 when entering VR (not sure why)
        let pos = xrf.frag.pos.last
        if( pos ){ AFRAME.XRF.camera.position.set(pos.x, pos.y*scale, pos.z) }
      }
      aScene.addEventListener('enter-vr', repositionUser(1) )
      aScene.addEventListener('enter-ar', repositionUser(2) )

      xrf.addEventListener('navigate', (opts) => {
        if( !AFRAME.fade ){
          const camera = $('[camera]')
          camera.setAttribute('xrf-fade','')
          AFRAME.fade = camera.components['xrf-fade']
        }
      })

      xrf.addEventListener('navigateLoaded', (opts) => {
        setTimeout( () => AFRAME.fade.out(),500) 
        let isLocal = opts.url.match(/^#/)
        if( isLocal ) return 

        // make collisionmeshes raycastable via AFRAME
        xrf.addEventListener('collisionMesh', (mesh) => {
          let el = document.createElement("a-entity")
          el.setAttribute("xrf-get", mesh.name )
          el.setAttribute("class","floor")
          $('a-scene').appendChild(el)
        })

        let blinkControls = document.querySelector('[blink-controls]')
        if( blinkControls ){
          let com = blinkControls.components['blink-controls']
          if( com ) com.update({collisionEntities:true})
          else console.warn("xrfragments: blink-controls is not mounted, please run manually: $('[blink-controls]).components['blink-controls'].update({collisionEntities:true})")

          blinkControls.addEventListener('teleported', (e) => {
            if( e.detail.newPosition.z < 0){
              console.warn('teleported to negative Z-value: https://github.com/jure/aframe-blink-controls/issues/30')
            }
          })
        }

        // give headset users way to debug without a cumbersome usb-tapdance
        if( document.location.hostname.match(/^(localhost|[1-9])/) && !aScene.getAttribute("vconsole") ){
          aScene.setAttribute('vconsole','')
        }

      })
      xrf.addEventListener('navigateError', (opts) => {
        AFRAME.fade.out()
      })

      xrf.addEventListener('navigateLoading', (opts) => {
        let p       = opts.promise()
        let url     = opts.url 
        let isLocal = url.match(/^#/)
        let hasPos  = url.match(/pos=/)
        let fastFadeMs = 200
        if( !AFRAME.fade  ) return p.resolve()

        if( isLocal ){
          if( hasPos ){
            // local teleports only
            AFRAME.fade.in(fastFadeMs)
            setTimeout( () => {
              p.resolve()
            }, fastFadeMs)
          }
        }else{
          AFRAME.fade.in(fastFadeMs)
          setTimeout( () => {
            p.resolve()
          }, AFRAME.fade.data.fadetime )
        }
      },{weight:-1000})

      // convert href's to a-entity's so AFRAME
      // raycaster can find & execute it
      AFRAME.XRF.clickableMeshToEntity = (opts) => {
        let {mesh,clickHandler} = opts;
        let createEl            = function(c){
          let el = document.createElement("a-entity")
          el.setAttribute("xrf-get",c.name )     // turn into AFRAME entity
          el.setAttribute("pressable", '' )      // detect click via hand-detection
          el.setAttribute("class","ray")         // expose to raycaster 
          // respond to cursor via laser-controls (https://aframe.io/docs/1.4.0/components/laser-controls.html)
          el.addEventListener("click",          clickHandler )
          el.addEventListener("mouseenter",     mesh.userData.XRF.href.selected(true) )
          el.addEventListener("mouseleave",     mesh.userData.XRF.href.selected(false) )
          $('a-scene').appendChild(el)
        }
        createEl(mesh)
      }
      xrf.addEventListener('interactionReady', AFRAME.XRF.clickableMeshToEntity )

      // cleanup xrf-get objects when resetting scene
      xrf.addEventListener('reset', (opts) => {
        let els = [...document.querySelectorAll('[xrf-get]')]
        els.map( (el) => document.querySelector('a-scene').removeChild(el) )
      })

      if( typeof this.data === 'string' || this.data.http || this.data.https ){
        let url
        if( typeof this.data === 'string' ) url = this.data
        if( this.data.http                ) url = `http:${this.data.http}`
        if( this.data.https               ) url = `https:${this.data.https}`
        AFRAME.XRF.navigator.to( url )
                            .then( (model) => {
                              let gets = [ ...document.querySelectorAll('[xrf-get]') ]
                              gets.map( (g) => g.emit('update') )
                            })
      }

      aScene.emit('XRF',{})
      
      // enable gaze-click on Mobile VR
      aScene.setAttribute('xrf-gaze','')

    }
  },

})
/**
 * Movement Controls
 *
 * @author Don McCurdy <dm@donmccurdy.com>
 */

const COMPONENT_SUFFIX = '-controls';
const MAX_DELTA = 0.2; // ms
const EPS = 10e-6;
const MOVED = 'moved';

AFRAME.registerComponent('movement-controls', {

  /*******************************************************************
   * Schema
   */

  dependencies: ['rotation'],

  schema: {
    enabled:            { default: true },
    controls:           { default: ['gamepad', 'trackpad', 'keyboard', 'touch'] },
    speed:              { default: 0.3, min: 0 },
    fly:                { default: false },
    constrainToNavMesh: { default: false },
    camera:             { default: '[movement-controls] [camera]', type: 'selector' }
  },

  /*******************************************************************
   * Lifecycle
   */

  init: function () {
    const el = this.el;
    if (!this.data.camera) {
      this.data.camera = el.querySelector('[camera]');
    }
    this.velocityCtrl = null;

    this.velocity = new THREE.Vector3();
    this.heading = new THREE.Quaternion();
    this.eventDetail = {};

    // Navigation
    this.navGroup = null;
    this.navNode = null;

    if (el.sceneEl.hasLoaded) {
      this.injectControls();
    } else {
      el.sceneEl.addEventListener('loaded', this.injectControls.bind(this));
    }
  },

  update: function (prevData) {
    const el = this.el;
    const data = this.data;
    const nav = el.sceneEl.systems.nav;
    if (el.sceneEl.hasLoaded) {
      this.injectControls();
    }
    if (nav && data.constrainToNavMesh !== prevData.constrainToNavMesh) {
      data.constrainToNavMesh
        ? nav.addAgent(this)
        : nav.removeAgent(this);
    }
    if (data.enabled !== prevData.enabled) {
      // Propagate the enabled change to all controls
      for (let i = 0; i < data.controls.length; i++) {
        const name = data.controls[i] + COMPONENT_SUFFIX;
        this.el.setAttribute(name, { enabled: this.data.enabled });
      }
    }
  },

  injectControls: function () {
    const data = this.data;

    for (let i = 0; i < data.controls.length; i++) {
      const name = data.controls[i] + COMPONENT_SUFFIX;
      this.el.setAttribute(name, { enabled: this.data.enabled });
    }
  },

  updateNavLocation: function () {
    this.navGroup = null;
    this.navNode = null;
  },

  /*******************************************************************
   * Tick
   */

  tick: (function () {
    const start = new THREE.Vector3();
    const end = new THREE.Vector3();
    const clampedEnd = new THREE.Vector3();

    return function (t, dt) {
      if (!dt) return;

      const el = this.el;
      const data = this.data;

      if (!data.enabled) return;

      this.updateVelocityCtrl();
      const velocityCtrl = this.velocityCtrl;
      const velocity = this.velocity;

      if (!velocityCtrl) return;

      // Update velocity. If FPS is too low, reset.
      if (dt / 1000 > MAX_DELTA) {
        velocity.set(0, 0, 0);
      } else {
        this.updateVelocity(dt);
      }

      if (data.constrainToNavMesh
          && velocityCtrl.isNavMeshConstrained !== false) {

        if (velocity.lengthSq() < EPS) return;

        start.copy(el.object3D.position);
        end
          .copy(velocity)
          .multiplyScalar(dt / 1000)
          .add(start);

        const nav = el.sceneEl.systems.nav;
        this.navGroup = this.navGroup === null ? nav.getGroup(start) : this.navGroup;
        this.navNode = this.navNode || nav.getNode(start, this.navGroup);
        this.navNode = nav.clampStep(start, end, this.navGroup, this.navNode, clampedEnd);
        el.object3D.position.copy(clampedEnd);
      } else if (el.hasAttribute('velocity')) {
        el.setAttribute('velocity', velocity);
      } else {
        el.object3D.position.x += velocity.x * dt / 1000;
        el.object3D.position.y += velocity.y * dt / 1000;
        el.object3D.position.z += velocity.z * dt / 1000;
      }

    };
  }()),

  /*******************************************************************
   * Movement
   */

  updateVelocityCtrl: function () {
    const data = this.data;
    if (data.enabled) {
      for (let i = 0, l = data.controls.length; i < l; i++) {
        const control = this.el.components[data.controls[i] + COMPONENT_SUFFIX];
        if (control && control.isVelocityActive()) {
          this.velocityCtrl = control;
          return;
        }
      }
      this.velocityCtrl = null;
    }
  },

  updateVelocity: (function () {
    const vector2 = new THREE.Vector2();
    const quaternion = new THREE.Quaternion();

    return function (dt) {
      let dVelocity;
      const el = this.el;
      const control = this.velocityCtrl;
      const velocity = this.velocity;
      const data = this.data;

      if (control) {
        if (control.getVelocityDelta) {
          dVelocity = control.getVelocityDelta(dt);
        } else if (control.getVelocity) {
          velocity.copy(control.getVelocity());
          return;
        } else if (control.getPositionDelta) {
          velocity.copy(control.getPositionDelta(dt).multiplyScalar(1000 / dt));
          return;
        } else {
          throw new Error('Incompatible movement controls: ', control);
        }
      }

      if (el.hasAttribute('velocity') && !data.constrainToNavMesh) {
        velocity.copy(this.el.getAttribute('velocity'));
      }

      if (dVelocity && data.enabled) {
        const cameraEl = data.camera;

        // Rotate to heading
        quaternion.copy(cameraEl.object3D.quaternion);
        quaternion.premultiply(el.object3D.quaternion);
        dVelocity.applyQuaternion(quaternion);

        const factor = dVelocity.length();
        if (data.fly) {
          velocity.copy(dVelocity);
          velocity.multiplyScalar(this.data.speed * 16.66667);
        } else {
          vector2.set(dVelocity.x, dVelocity.z);
          vector2.setLength(factor * this.data.speed * 16.66667);
          velocity.x = vector2.x;
          velocity.y = 0;
          velocity.z = vector2.y;
        }
        if (velocity.x !== 0 || velocity.y !== 0 || velocity.z !== 0) {
          this.eventDetail.velocity = velocity;
          this.el.emit(MOVED, this.eventDetail);
        }
      }
    };

  }())
});
// look-controls turns off autoUpdateMatrix (of player) which 
// will break teleporting and other stuff
// overriding this is easier then adding updateMatrixWorld() everywhere else

//AFRAME.components['look-controls'].Component.prototype.onEnterVR = function () {}
//AFRAME.components['look-controls'].Component.prototype.onExitVR = function () {}
AFRAME.components['look-controls'].Component.prototype.onEnterVR = function () {
  var sceneEl = this.el.sceneEl;
  if (!sceneEl.checkHeadsetConnected()) { return; }
  this.saveCameraPose();
  this.el.object3D.position.set(0, 0, 0);
  this.el.object3D.rotation.set(0, 0, 0);
  if (sceneEl.hasWebXR) {
   // this.el.object3D.matrixAutoUpdate = false;
    this.el.object3D.updateMatrix();
  }
}

/**
 * Restore the pose.
 */
AFRAME.components['look-controls'].Component.prototype.onExitVR = function () {
  if (!this.el.sceneEl.checkHeadsetConnected()) { return; }
  this.restoreCameraPose();
  this.previousHMDPosition.set(0, 0, 0);
  this.el.object3D.matrixAutoUpdate = true;
}

// it also needs to apply the offset (in case the #rot was used in URLS)

AFRAME.components['look-controls'].Component.prototype.updateOrientation = function () {
  var object3D = this.el.object3D;
  var pitchObject = this.pitchObject;
  var yawObject = this.yawObject;
  var sceneEl = this.el.sceneEl;

  // In VR or AR mode, THREE is in charge of updating the camera pose.
  if ((sceneEl.is('vr-mode') || sceneEl.is('ar-mode')) && sceneEl.checkHeadsetConnected()) {
    // With WebXR THREE applies headset pose to the object3D internally.
    return;
  }

  this.updateMagicWindowOrientation();

  let offsetX = object3D.rotation.offset ? object3D.rotation.offset.x : 0
  let offsetY = object3D.rotation.offset ? object3D.rotation.offset.y : 0 

  // On mobile, do camera rotation with touch events and sensors.
  object3D.rotation.x = this.magicWindowDeltaEuler.x + offsetX + pitchObject.rotation.x;
  object3D.rotation.y = this.magicWindowDeltaEuler.y + offsetY + yawObject.rotation.y;
  object3D.rotation.z = this.magicWindowDeltaEuler.z;
  object3D.matrixAutoUpdate = true
}
// this makes WebXR hand controls able to click things (by touching it)

AFRAME.registerComponent('pressable', {
    schema: {
        pressDistance: {
            default: 0.01
        }
    },
    init: function() {
        this.worldPosition = new THREE.Vector3();
        this.fingerWorldPosition = new THREE.Vector3();
        this.raycaster = new THREE.Raycaster()
        this.handEls = document.querySelectorAll('[hand-tracking-controls]');
        this.pressed = false;
        this.distance = -1
        // we throttle by distance, to support scenes with loads of clickable objects (far away)
        this.tick = this.throttleByDistance( () => this.detectPress() )
    },
    throttleByDistance: function(f){
        return function(){
           if( this.distance < 0 ) return f() // first call
           if( !f.tid ){
             let x = this.distance
             let y = x*(x*0.05)*1000 // parabolic curve
             f.tid = setTimeout( function(){
               f.tid = null
               f()
             }, y )
           }
        }
    },
    detectPress: function(){
        if( !AFRAME.scenes[0].renderer.xr.isPresenting ) return

        var handEls = this.handEls;
        var handEl;
        let minDistance = 5

        // compensate for xrf-get AFRAME component (which references non-reparented buffergeometries from the 3D model)
        let object3D = this.el.object3D.child || this.el.object3D

        for (var i = 0; i < handEls.length; i++) {
            handEl = handEls[i];
            let indexTipPosition  = handEl.components['hand-tracking-controls'].indexTipPosition
            // Apply the relative position to the parent's world position 
            handEl.object3D.updateMatrixWorld();
            handEl.object3D.getWorldPosition( this.fingerWorldPosition )
            this.fingerWorldPosition.add( indexTipPosition )

            this.raycaster.far = this.data.pressDistance
            // Create a direction vector (doesnt matter because it is supershort for 'touch' purposes)
            const direction = new THREE.Vector3(1.0,0,0);
            this.raycaster.set(this.fingerWorldPosition, direction)
            intersects = this.raycaster.intersectObjects([object3D],true)

            object3D.getWorldPosition(this.worldPosition)
      
            distance    = this.fingerWorldPosition.distanceTo(this.worldPosition)
            minDistance = distance < minDistance ? distance : minDistance 

            if (intersects.length ){
              if( !this.pressed ){
                this.el.emit('pressedstarted');
                this.el.emit('click');
                this.pressed = setTimeout( () => {
                  this.el.emit('pressedended');
                  this.pressed = null 
                },300)
              }
            }
        }
        this.distance = minDistance
    }
});
/**
 * Touch-to-move-forward controls for mobile.
 */
AFRAME.registerComponent('touch-controls', {
  schema: {
    enabled: { default: true },
    reverseEnabled: { default: true }
  },

  init: function () {
    this.dVelocity = new THREE.Vector3();
    this.bindMethods();
    this.direction = 0;
  },

  play: function () {
    this.addEventListeners();
  },

  pause: function () {
    this.removeEventListeners();
    this.dVelocity.set(0, 0, 0);
  },

  remove: function () {
    this.pause();
  },

  addEventListeners: function () {
    const sceneEl = this.el.sceneEl;
    const canvasEl = sceneEl.canvas;

    if (!canvasEl) {
      sceneEl.addEventListener('render-target-loaded', this.addEventListeners.bind(this));
      return;
    }

    canvasEl.addEventListener('touchstart', this.onTouchStart);
    canvasEl.addEventListener('touchend', this.onTouchEnd);
    const vrModeUI = sceneEl.getAttribute('vr-mode-ui');
    if (vrModeUI && vrModeUI.cardboardModeEnabled) {
      sceneEl.addEventListener('enter-vr', this.onEnterVR);
    }
  },

  removeEventListeners: function () {
    const canvasEl = this.el.sceneEl && this.el.sceneEl.canvas;
    if (!canvasEl) { return; }

    canvasEl.removeEventListener('touchstart', this.onTouchStart);
    canvasEl.removeEventListener('touchend', this.onTouchEnd);
    this.el.sceneEl.removeEventListener('enter-vr', this.onEnterVR)
  },

  isVelocityActive: function () {
    return this.data.enabled && !!this.direction;
  },

  getVelocityDelta: function () {
    this.dVelocity.z = this.direction;
    return this.dVelocity.clone();
  },

  bindMethods: function () {
    this.onTouchStart = this.onTouchStart.bind(this);
    this.onTouchEnd = this.onTouchEnd.bind(this);
    this.onEnterVR = this.onEnterVR.bind(this);
  },

  onTouchStart: function (e) {
    this.direction = 0;
    if (this.data.reverseEnabled && e.touches ){
      if( e.touches.length === 3) this.direction = 1;
      if( e.touches.length === 2) this.direction = -1;
    }
    //e.preventDefault();
  },

  onTouchEnd: function (e) {
    this.direction = 0;
    //e.preventDefault();
  },

  onEnterVR: function () {
    // This is to make the Cardboard button on Chrome Android working
    //const xrSession = this.el.sceneEl.xrSession;
    //if (!xrSession) { return; }
    //xrSession.addEventListener('selectstart', this.onTouchStart);
    //xrSession.addEventListener('selectend', this.onTouchEnd);
  }
})
window.AFRAME.registerComponent('xrf-button', {
    schema: {
        label: {
            default: 'label'
        },
        width: {
            default: 0.11
        },
        toggable: {
            default: false
        }, 
        textSize: {
            default: 0.66
        }, 
        color:{
            default: '#111'
        }, 
        textColor:{
            default: '#fff'
        }, 
        hicolor:{
            default: '#555555'
        },
        action:{
            default: ''
        }
    },
    init: function() {
        var el = this.el;
        var labelEl = this.labelEl = document.createElement('a-entity');
        this.color = this.data.color 
        el.setAttribute('geometry', {
            primitive: 'box',
            width: this.data.width,
            height: 0.05,
            depth: 0.005
        });
        el.setAttribute('material', {
            color: this.color, 
            transparent:true,
            opacity:0.3
        });
        el.setAttribute('pressable', '');
        labelEl.setAttribute('position', '0 0 0.01');
        labelEl.setAttribute('text', {
            value: this.data.label,
            color: this.data.textColor, 
            align: 'center'
        });
        labelEl.setAttribute('scale', `${this.data.textSize} ${this.data.textSize} ${this.data.textSize}`);
        this.el.appendChild(labelEl);
        this.bindMethods();
        this.el.addEventListener('stateadded', this.stateChanged);
        this.el.addEventListener('stateremoved', this.stateChanged);
        this.el.addEventListener('pressedstarted', this.onPressedStarted);
        this.el.addEventListener('pressedended', this.onPressedEnded);
        this.el.addEventListener('mouseenter', (e) => this.onMouseEnter(e) );
        this.el.addEventListener('mouseleave', (e) => this.onMouseLeave(e) );

        if( this.data.action ){ 
          this.el.addEventListener('click', new Function(this.data.action) )
        }
    },
    bindMethods: function() {
        this.stateChanged = this.stateChanged.bind(this);
        this.onPressedStarted = this.onPressedStarted.bind(this);
        this.onPressedEnded = this.onPressedEnded.bind(this);
    },
    update: function(oldData) {
        if (oldData.label !== this.data.label) {
            this.labelEl.setAttribute('text', 'value', this.data.label);
        }
    },
    stateChanged: function() {
        var color = this.el.is('pressed') ? this.data.hicolor : this.color;
        this.el.setAttribute('material', {
            color: color
        });
    },
    onMouseEnter: function(){
        this.el.setAttribute('material', { color: this.data.hicolor });
    }, 
    onMouseLeave: function(){
        this.el.setAttribute('material', { color: this.color });
    }, 
    onPressedStarted: function() {
        var el = this.el;
        el.setAttribute('material', {
            color: this.data.hicolor
        });
        el.emit('click');
        if (this.data.togabble) {
            if (el.is('pressed')) {
                el.removeState('pressed');
            } else {
                el.addState('pressed');
            }
        }
    },
    onPressedEnded: function() {
        if (this.el.is('pressed')) {
            return;
        }
        this.el.setAttribute('material', {
            color: this.color
        });
    }
});
AFRAME.registerComponent('vconsole', {
  init: function () {  
      //AFRAME.XRF.navigator.to("https://coderofsalvation.github.io/xrsh-media/assets/background.glb")
    let aScene = AFRAME.scenes[0] 
    return

 //   return
    document.head.innerHTML += `
      <style type="text/css">
        .vc-panel  {
          right:unset !important;
          width:100%;
          max-width:900px;
          z-index:100 !important;
        }
        .vc-mask{ display:none !important; }
      </style>
    `
    let script = document.createElement("script")
    script.src = "https://unpkg.com/vconsole@latest/dist/vconsole.min.js"
    script.setAttribute('async','true')
    script.onload = function(){
      this.vConsole = new window.VConsole() 
      document.querySelector('.vc-switch').style.right = 'unset'
      document.querySelector('.vc-switch').style.left  = '20px'
    }
    document.body.appendChild(script)
  }

});

AFRAME.registerComponent('xrf-fade', {
  schema:{
    fadetime:{type:"number", default: 1000}, 
    color:{type:"color", default:"black"}, 
    opacity:{type:"float",default:1.0}
  },
  init: function(){
    let fb = this.fb = document.createElement("a-box")
    fb.setAttribute("scale", "1 1 1")
    fb.setAttribute("material", `color: ${this.data.color}; transparent: true; side: back; shader: flat; opacity:1`)
    this.el.appendChild(fb)
  }, 
  out: function(fadetime){
    if( fadetime != undefined ) this.data.fadetime = fadetime 
    if( this.data.opacity == 0 ) return 
    this.data.opacity = 0.0
    this.fb.setAttribute("animation", `property: components.material.material.opacity; dur: ${this.data.fadetime}; from: 1; to: ${this.data.opacity}`)
    setTimeout( () => this.fb.object3D.visible = false, this.data.fadetime )
  }, 
  "in": function(fadetime){
    if( fadetime != undefined ) this.data.fadetime = fadetime 
    if( this.data.opacity == 1 ) return 
    this.data.opacity = 1.0
    this.fb.object3D.visible = true 
    this.fb.setAttribute("animation", `property: components.material.material.opacity; dur: ${this.data.fadetime}; from: 0; to: ${this.data.opacity}`)
  }
});
// gaze click on mobile VR

AFRAME.registerComponent('xrf-gaze',{
  schema:{
    spawn:{type:'boolean',default:false}, 
  },
  events:{
    "fusing": function(e){
      if( e.detail.mouseEvent ) return // ignore click event
      console.dir(e)

    }
  },
  setGazer: function(state, fuse){
    if( !AFRAME.utils.device.isMobile() ) return
    let cam = document.querySelector("[camera]") 
    if( state ){
      if( cam.innerHTML.match(/cursor/) ) return; // avoid duplicate calls
      cam.innerHTML = `<a-entity id="cursor" cursor="fuse: ${fuse ? 'true': 'false'}; fuseTimeout: 1500"
        animation__click="property: scale; startEvents: click; easing: easeInCubic; dur: 150; from: 0.1 0.1 0.1; to: 1 1 1"
        animation__fusing="property: scale; startEvents: fusing; easing: easeInCubic; dur: 1500; from: 1 1 1; to: 0.1 0.1 0.1"
        animation__mouseleave="property: scale; startEvents: mouseleave; easing: easeInCubic; dur: 500; to: 1 1 1"
        raycaster="objects: .ray"
        visible="true"
        position="0 0 -1"
        material="color: #BBBBBB; shader: flat">
      </a-entity>`
      cam.querySelector('#cursor').setAttribute("geometry","primitive: ring; radiusInner: 0.02; radiusOuter: 0.03")
    }else{
      cam.querySelector('#cursor').removeAttribute("geometry")
      if( document.querySelector('[cursor]') ) {
        document.querySelector('[cursor]').setAttribute("visible",false)
      }
    }
  }, 
  init:function(data){

    this.setGazer(true);

    document.querySelector("a-scene").addEventListener('exit-vr',  () => this.setGazer(false,false) )
    document.querySelector("a-scene").addEventListener('enter-vr', () => this.setGazer(true,true) )
    document.querySelector("a-scene").addEventListener('enter-ar', () => this.setGazer(true,false) )
    document.querySelector("a-scene").addEventListener('exit-ar',  () => this.setGazer(false,false) )

    let highlightMesh = (state) => (e) => {
      if( !e.target.object3D ) return 
      let obj = e.target.object3D.children[0]
      if( obj && obj.userData && obj.userData.XRF && obj.userData.XRF.href )
        obj.userData.XRF.href.selected( state )()
    }
    this.el.addEventListener("mouseenter", highlightMesh(true) )
    this.el.addEventListener("mouseleave", highlightMesh(false ) )
  }
});
window.AFRAME.registerComponent('xrf-get', {
  schema: {
    name: {type: 'string'},
    clone: {type: 'boolean', default:false},
    reparent: {type: 'boolean', default:false}
  },

  init: function () {

    var el = this.el;
    var meshname = this.data.name || this.data;

    this.el.addEventListener('update', (evt) => {

      setTimeout( () => {

        if( !this.mesh ){
          let scene = AFRAME.XRF.scene 
          let mesh = this.mesh = scene.getObjectByName(meshname);
          if( !this.el.className.match(/ray/) ) this.el.className += " ray"
          if (!mesh){
            console.error("mesh with name '"+meshname+"' not found in model")
            return;
          }
          // we don't want to re-parent gltf-meshes
          mesh.isXRF = true    // mark for deletion by xrf
          if( this.data.reparent ){ 
            const world = { 
              pos: new THREE.Vector3(), 
              scale: new THREE.Vector3(),
              quat: new THREE.Quaternion()
            }
            mesh.getWorldPosition(world.pos)
            mesh.getWorldScale(world.scale)
            mesh.getWorldQuaternion(world.quat);
            mesh.position.copy(world.pos)
            mesh.scale.copy(world.scale)
            mesh.setRotationFromQuaternion(world.quat);
          }else{
            // lets create a dummy add function so that the mesh won't get reparented during setObject3D
            // as this would break animations
            this.el.object3D.add = (a) => a 
          }

          this.el.setObject3D('mesh',mesh) // (doing this.el.object3D = mesh causes AFRAME to crash when resetting scene)
          this.el.object3D.child = mesh    // keep reference (because .children will be empty)

          if( !this.el.id ) this.el.setAttribute("id",`xrf-${mesh.name}`)
        }else console.warn("xrf-get ignore: "+JSON.stringify(this.data))
      }, evt && evt.timeout ? evt.timeout: 500)

    })

    this.el.emit("update",{timeout:0})

    AFRAME.XRF.addEventListener('reset', () => {
      try{
        if( this.el ){
          while ( this.el.object3D.children.length ){
            this.el.object3D.children[0].remove()
          }
          this.el.remove()
        }
      }catch(e){}
    })

  }

});

// poor man's way to move forward using hand gesture pinch

window.AFRAME.registerComponent('xrf-pinchmove', {
  schema:{
    rig: {type: "selector"}
  }, 
  init: function(){

    this.el.addEventListener("pinchended", () => {
      // get the cameras world direction
      let direction = new THREE.Vector3()
      this.el.sceneEl.camera.getWorldDirection(direction);
      // multiply the direction by a "speed" factor
      direction.multiplyScalar(0.4)
      // get the current position
      var pos = player.getAttribute("position")
      // add the direction vector
      pos.x += direction.x 
      pos.z += direction.z
      // set the new position
      this.data.rig.setAttribute("position", pos);
      // !!! NOTE - it would be more efficient to do the
      // position change on the players THREE.Object:
      // `player.object3D.position.add(direction)`
      // but it would break "getAttribute("position")
    })
  }, 
})
window.AFRAME.registerComponent('xrf-wear', {
  schema:{
    el: {type:"selector"}, 
    position: {type:"vec3"}, 
    rotation: {type:"vec3"} 
  }, 
  init: function(){
    $('a-scene').addEventListener('enter-vr', (e) => this.wear(e) )
    $('a-scene').addEventListener('exit-vr',  (e) => this.unwear(e) )
  }, 
  wear: function(){
    if( !this.wearable ){
      let d = this.data
      this.wearable = new THREE.Group()
      this.el.object3D.children.map( (c) => this.wearable.add(c) )
      this.wearable.position.set( d.position.x,  d.position.y,  d.position.z)
      this.wearable.rotation.set( d.rotation.x,  d.rotation.y,  d.rotation.z)
    }
    this.data.el.object3D.add(this.wearable)
  }, 
  unwear: function(){
    this.data.el.remove(this.wearable)
    this.wearable.children.map( (c) => this.el.object3D.add(c) )
    delete this.wearable
  }
})
// https://github.com/yyx990803/QR.js
//
window.QR = (function () {

    // alignment pattern
    adelta = [
      0, 11, 15, 19, 23, 27, 31, // force 1 pat
      16, 18, 20, 22, 24, 26, 28, 20, 22, 24, 24, 26, 28, 28, 22, 24, 24,
      26, 26, 28, 28, 24, 24, 26, 26, 26, 28, 28, 24, 26, 26, 26, 28, 28
      ];

    // version block
    vpat = [
        0xc94, 0x5bc, 0xa99, 0x4d3, 0xbf6, 0x762, 0x847, 0x60d,
        0x928, 0xb78, 0x45d, 0xa17, 0x532, 0x9a6, 0x683, 0x8c9,
        0x7ec, 0xec4, 0x1e1, 0xfab, 0x08e, 0xc1a, 0x33f, 0xd75,
        0x250, 0x9d5, 0x6f0, 0x8ba, 0x79f, 0xb0b, 0x42e, 0xa64,
        0x541, 0xc69
    ];

    // final format bits with mask: level << 3 | mask
    fmtword = [
        0x77c4, 0x72f3, 0x7daa, 0x789d, 0x662f, 0x6318, 0x6c41, 0x6976,    //L
        0x5412, 0x5125, 0x5e7c, 0x5b4b, 0x45f9, 0x40ce, 0x4f97, 0x4aa0,    //M
        0x355f, 0x3068, 0x3f31, 0x3a06, 0x24b4, 0x2183, 0x2eda, 0x2bed,    //Q
        0x1689, 0x13be, 0x1ce7, 0x19d0, 0x0762, 0x0255, 0x0d0c, 0x083b    //H
    ];

    // 4 per version: number of blocks 1,2; data width; ecc width
    eccblocks = [
        1, 0, 19, 7, 1, 0, 16, 10, 1, 0, 13, 13, 1, 0, 9, 17,
        1, 0, 34, 10, 1, 0, 28, 16, 1, 0, 22, 22, 1, 0, 16, 28,
        1, 0, 55, 15, 1, 0, 44, 26, 2, 0, 17, 18, 2, 0, 13, 22,
        1, 0, 80, 20, 2, 0, 32, 18, 2, 0, 24, 26, 4, 0, 9, 16,
        1, 0, 108, 26, 2, 0, 43, 24, 2, 2, 15, 18, 2, 2, 11, 22,
        2, 0, 68, 18, 4, 0, 27, 16, 4, 0, 19, 24, 4, 0, 15, 28,
        2, 0, 78, 20, 4, 0, 31, 18, 2, 4, 14, 18, 4, 1, 13, 26,
        2, 0, 97, 24, 2, 2, 38, 22, 4, 2, 18, 22, 4, 2, 14, 26,
        2, 0, 116, 30, 3, 2, 36, 22, 4, 4, 16, 20, 4, 4, 12, 24,
        2, 2, 68, 18, 4, 1, 43, 26, 6, 2, 19, 24, 6, 2, 15, 28,
        4, 0, 81, 20, 1, 4, 50, 30, 4, 4, 22, 28, 3, 8, 12, 24,
        2, 2, 92, 24, 6, 2, 36, 22, 4, 6, 20, 26, 7, 4, 14, 28,
        4, 0, 107, 26, 8, 1, 37, 22, 8, 4, 20, 24, 12, 4, 11, 22,
        3, 1, 115, 30, 4, 5, 40, 24, 11, 5, 16, 20, 11, 5, 12, 24,
        5, 1, 87, 22, 5, 5, 41, 24, 5, 7, 24, 30, 11, 7, 12, 24,
        5, 1, 98, 24, 7, 3, 45, 28, 15, 2, 19, 24, 3, 13, 15, 30,
        1, 5, 107, 28, 10, 1, 46, 28, 1, 15, 22, 28, 2, 17, 14, 28,
        5, 1, 120, 30, 9, 4, 43, 26, 17, 1, 22, 28, 2, 19, 14, 28,
        3, 4, 113, 28, 3, 11, 44, 26, 17, 4, 21, 26, 9, 16, 13, 26,
        3, 5, 107, 28, 3, 13, 41, 26, 15, 5, 24, 30, 15, 10, 15, 28,
        4, 4, 116, 28, 17, 0, 42, 26, 17, 6, 22, 28, 19, 6, 16, 30,
        2, 7, 111, 28, 17, 0, 46, 28, 7, 16, 24, 30, 34, 0, 13, 24,
        4, 5, 121, 30, 4, 14, 47, 28, 11, 14, 24, 30, 16, 14, 15, 30,
        6, 4, 117, 30, 6, 14, 45, 28, 11, 16, 24, 30, 30, 2, 16, 30,
        8, 4, 106, 26, 8, 13, 47, 28, 7, 22, 24, 30, 22, 13, 15, 30,
        10, 2, 114, 28, 19, 4, 46, 28, 28, 6, 22, 28, 33, 4, 16, 30,
        8, 4, 122, 30, 22, 3, 45, 28, 8, 26, 23, 30, 12, 28, 15, 30,
        3, 10, 117, 30, 3, 23, 45, 28, 4, 31, 24, 30, 11, 31, 15, 30,
        7, 7, 116, 30, 21, 7, 45, 28, 1, 37, 23, 30, 19, 26, 15, 30,
        5, 10, 115, 30, 19, 10, 47, 28, 15, 25, 24, 30, 23, 25, 15, 30,
        13, 3, 115, 30, 2, 29, 46, 28, 42, 1, 24, 30, 23, 28, 15, 30,
        17, 0, 115, 30, 10, 23, 46, 28, 10, 35, 24, 30, 19, 35, 15, 30,
        17, 1, 115, 30, 14, 21, 46, 28, 29, 19, 24, 30, 11, 46, 15, 30,
        13, 6, 115, 30, 14, 23, 46, 28, 44, 7, 24, 30, 59, 1, 16, 30,
        12, 7, 121, 30, 12, 26, 47, 28, 39, 14, 24, 30, 22, 41, 15, 30,
        6, 14, 121, 30, 6, 34, 47, 28, 46, 10, 24, 30, 2, 64, 15, 30,
        17, 4, 122, 30, 29, 14, 46, 28, 49, 10, 24, 30, 24, 46, 15, 30,
        4, 18, 122, 30, 13, 32, 46, 28, 48, 14, 24, 30, 42, 32, 15, 30,
        20, 4, 117, 30, 40, 7, 47, 28, 43, 22, 24, 30, 10, 67, 15, 30,
        19, 6, 118, 30, 18, 31, 47, 28, 34, 34, 24, 30, 20, 61, 15, 30
    ];

    // Galois field log table
    glog = [
        0xff, 0x00, 0x01, 0x19, 0x02, 0x32, 0x1a, 0xc6, 0x03, 0xdf, 0x33, 0xee, 0x1b, 0x68, 0xc7, 0x4b,
        0x04, 0x64, 0xe0, 0x0e, 0x34, 0x8d, 0xef, 0x81, 0x1c, 0xc1, 0x69, 0xf8, 0xc8, 0x08, 0x4c, 0x71,
        0x05, 0x8a, 0x65, 0x2f, 0xe1, 0x24, 0x0f, 0x21, 0x35, 0x93, 0x8e, 0xda, 0xf0, 0x12, 0x82, 0x45,
        0x1d, 0xb5, 0xc2, 0x7d, 0x6a, 0x27, 0xf9, 0xb9, 0xc9, 0x9a, 0x09, 0x78, 0x4d, 0xe4, 0x72, 0xa6,
        0x06, 0xbf, 0x8b, 0x62, 0x66, 0xdd, 0x30, 0xfd, 0xe2, 0x98, 0x25, 0xb3, 0x10, 0x91, 0x22, 0x88,
        0x36, 0xd0, 0x94, 0xce, 0x8f, 0x96, 0xdb, 0xbd, 0xf1, 0xd2, 0x13, 0x5c, 0x83, 0x38, 0x46, 0x40,
        0x1e, 0x42, 0xb6, 0xa3, 0xc3, 0x48, 0x7e, 0x6e, 0x6b, 0x3a, 0x28, 0x54, 0xfa, 0x85, 0xba, 0x3d,
        0xca, 0x5e, 0x9b, 0x9f, 0x0a, 0x15, 0x79, 0x2b, 0x4e, 0xd4, 0xe5, 0xac, 0x73, 0xf3, 0xa7, 0x57,
        0x07, 0x70, 0xc0, 0xf7, 0x8c, 0x80, 0x63, 0x0d, 0x67, 0x4a, 0xde, 0xed, 0x31, 0xc5, 0xfe, 0x18,
        0xe3, 0xa5, 0x99, 0x77, 0x26, 0xb8, 0xb4, 0x7c, 0x11, 0x44, 0x92, 0xd9, 0x23, 0x20, 0x89, 0x2e,
        0x37, 0x3f, 0xd1, 0x5b, 0x95, 0xbc, 0xcf, 0xcd, 0x90, 0x87, 0x97, 0xb2, 0xdc, 0xfc, 0xbe, 0x61,
        0xf2, 0x56, 0xd3, 0xab, 0x14, 0x2a, 0x5d, 0x9e, 0x84, 0x3c, 0x39, 0x53, 0x47, 0x6d, 0x41, 0xa2,
        0x1f, 0x2d, 0x43, 0xd8, 0xb7, 0x7b, 0xa4, 0x76, 0xc4, 0x17, 0x49, 0xec, 0x7f, 0x0c, 0x6f, 0xf6,
        0x6c, 0xa1, 0x3b, 0x52, 0x29, 0x9d, 0x55, 0xaa, 0xfb, 0x60, 0x86, 0xb1, 0xbb, 0xcc, 0x3e, 0x5a,
        0xcb, 0x59, 0x5f, 0xb0, 0x9c, 0xa9, 0xa0, 0x51, 0x0b, 0xf5, 0x16, 0xeb, 0x7a, 0x75, 0x2c, 0xd7,
        0x4f, 0xae, 0xd5, 0xe9, 0xe6, 0xe7, 0xad, 0xe8, 0x74, 0xd6, 0xf4, 0xea, 0xa8, 0x50, 0x58, 0xaf
    ];

    // Galios field exponent table
    gexp = [
        0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1d, 0x3a, 0x74, 0xe8, 0xcd, 0x87, 0x13, 0x26,
        0x4c, 0x98, 0x2d, 0x5a, 0xb4, 0x75, 0xea, 0xc9, 0x8f, 0x03, 0x06, 0x0c, 0x18, 0x30, 0x60, 0xc0,
        0x9d, 0x27, 0x4e, 0x9c, 0x25, 0x4a, 0x94, 0x35, 0x6a, 0xd4, 0xb5, 0x77, 0xee, 0xc1, 0x9f, 0x23,
        0x46, 0x8c, 0x05, 0x0a, 0x14, 0x28, 0x50, 0xa0, 0x5d, 0xba, 0x69, 0xd2, 0xb9, 0x6f, 0xde, 0xa1,
        0x5f, 0xbe, 0x61, 0xc2, 0x99, 0x2f, 0x5e, 0xbc, 0x65, 0xca, 0x89, 0x0f, 0x1e, 0x3c, 0x78, 0xf0,
        0xfd, 0xe7, 0xd3, 0xbb, 0x6b, 0xd6, 0xb1, 0x7f, 0xfe, 0xe1, 0xdf, 0xa3, 0x5b, 0xb6, 0x71, 0xe2,
        0xd9, 0xaf, 0x43, 0x86, 0x11, 0x22, 0x44, 0x88, 0x0d, 0x1a, 0x34, 0x68, 0xd0, 0xbd, 0x67, 0xce,
        0x81, 0x1f, 0x3e, 0x7c, 0xf8, 0xed, 0xc7, 0x93, 0x3b, 0x76, 0xec, 0xc5, 0x97, 0x33, 0x66, 0xcc,
        0x85, 0x17, 0x2e, 0x5c, 0xb8, 0x6d, 0xda, 0xa9, 0x4f, 0x9e, 0x21, 0x42, 0x84, 0x15, 0x2a, 0x54,
        0xa8, 0x4d, 0x9a, 0x29, 0x52, 0xa4, 0x55, 0xaa, 0x49, 0x92, 0x39, 0x72, 0xe4, 0xd5, 0xb7, 0x73,
        0xe6, 0xd1, 0xbf, 0x63, 0xc6, 0x91, 0x3f, 0x7e, 0xfc, 0xe5, 0xd7, 0xb3, 0x7b, 0xf6, 0xf1, 0xff,
        0xe3, 0xdb, 0xab, 0x4b, 0x96, 0x31, 0x62, 0xc4, 0x95, 0x37, 0x6e, 0xdc, 0xa5, 0x57, 0xae, 0x41,
        0x82, 0x19, 0x32, 0x64, 0xc8, 0x8d, 0x07, 0x0e, 0x1c, 0x38, 0x70, 0xe0, 0xdd, 0xa7, 0x53, 0xa6,
        0x51, 0xa2, 0x59, 0xb2, 0x79, 0xf2, 0xf9, 0xef, 0xc3, 0x9b, 0x2b, 0x56, 0xac, 0x45, 0x8a, 0x09,
        0x12, 0x24, 0x48, 0x90, 0x3d, 0x7a, 0xf4, 0xf5, 0xf7, 0xf3, 0xfb, 0xeb, 0xcb, 0x8b, 0x0b, 0x16,
        0x2c, 0x58, 0xb0, 0x7d, 0xfa, 0xe9, 0xcf, 0x83, 0x1b, 0x36, 0x6c, 0xd8, 0xad, 0x47, 0x8e, 0x00
    ];

    // Working buffers:
    // data input and ecc append, image working buffer, fixed part of image, run lengths for badness
    var strinbuf=[], eccbuf=[], qrframe=[], framask=[], rlens=[]; 
    // Control values - width is based on version, last 4 are from table.
    var version, width, neccblk1, neccblk2, datablkw, eccblkwid;
    var ecclevel = 2;
    // set bit to indicate cell in qrframe is immutable.  symmetric around diagonal
    function setmask(x, y)
    {
        var bt;
        if (x > y) {
            bt = x;
            x = y;
            y = bt;
        }
        // y*y = 1+3+5...
        bt = y;
        bt *= y;
        bt += y;
        bt >>= 1;
        bt += x;
        framask[bt] = 1;
    }

    // enter alignment pattern - black to qrframe, white to mask (later black frame merged to mask)
    function putalign(x, y)
    {
        var j;

        qrframe[x + width * y] = 1;
        for (j = -2; j < 2; j++) {
            qrframe[(x + j) + width * (y - 2)] = 1;
            qrframe[(x - 2) + width * (y + j + 1)] = 1;
            qrframe[(x + 2) + width * (y + j)] = 1;
            qrframe[(x + j + 1) + width * (y + 2)] = 1;
        }
        for (j = 0; j < 2; j++) {
            setmask(x - 1, y + j);
            setmask(x + 1, y - j);
            setmask(x - j, y - 1);
            setmask(x + j, y + 1);
        }
    }

    //========================================================================
    // Reed Solomon error correction
    // exponentiation mod N
    function modnn(x)
    {
        while (x >= 255) {
            x -= 255;
            x = (x >> 8) + (x & 255);
        }
        return x;
    }

    var genpoly = [];

    // Calculate and append ECC data to data block.  Block is in strinbuf, indexes to buffers given.
    function appendrs(data, dlen, ecbuf, eclen)
    {
        var i, j, fb;

        for (i = 0; i < eclen; i++)
            strinbuf[ecbuf + i] = 0;
        for (i = 0; i < dlen; i++) {
            fb = glog[strinbuf[data + i] ^ strinbuf[ecbuf]];
            if (fb != 255)     /* fb term is non-zero */
                for (j = 1; j < eclen; j++)
                    strinbuf[ecbuf + j - 1] = strinbuf[ecbuf + j] ^ gexp[modnn(fb + genpoly[eclen - j])];
            else
                for( j = ecbuf ; j < ecbuf + eclen; j++ )
                    strinbuf[j] = strinbuf[j + 1];
            strinbuf[ ecbuf + eclen - 1] = fb == 255 ? 0 : gexp[modnn(fb + genpoly[0])];
        }
    }

    //========================================================================
    // Frame data insert following the path rules

    // check mask - since symmetrical use half.
    function ismasked(x, y)
    {
        var bt;
        if (x > y) {
            bt = x;
            x = y;
            y = bt;
        }
        bt = y;
        bt += y * y;
        bt >>= 1;
        bt += x;
        return framask[bt];
    }

    //========================================================================
    //  Apply the selected mask out of the 8.
    function  applymask(m)
    {
        var x, y, r3x, r3y;

        switch (m) {
        case 0:
            for (y = 0; y < width; y++)
                for (x = 0; x < width; x++)
                    if (!((x + y) & 1) && !ismasked(x, y))
                        qrframe[x + y * width] ^= 1;
            break;
        case 1:
            for (y = 0; y < width; y++)
                for (x = 0; x < width; x++)
                    if (!(y & 1) && !ismasked(x, y))
                        qrframe[x + y * width] ^= 1;
            break;
        case 2:
            for (y = 0; y < width; y++)
                for (r3x = 0, x = 0; x < width; x++, r3x++) {
                    if (r3x == 3)
                        r3x = 0;
                    if (!r3x && !ismasked(x, y))
                        qrframe[x + y * width] ^= 1;
                }
            break;
        case 3:
            for (r3y = 0, y = 0; y < width; y++, r3y++) {
                if (r3y == 3)
                    r3y = 0;
                for (r3x = r3y, x = 0; x < width; x++, r3x++) {
                    if (r3x == 3)
                        r3x = 0;
                    if (!r3x && !ismasked(x, y))
                        qrframe[x + y * width] ^= 1;
                }
            }
            break;
        case 4:
            for (y = 0; y < width; y++)
                for (r3x = 0, r3y = ((y >> 1) & 1), x = 0; x < width; x++, r3x++) {
                    if (r3x == 3) {
                        r3x = 0;
                        r3y = !r3y;
                    }
                    if (!r3y && !ismasked(x, y))
                        qrframe[x + y * width] ^= 1;
                }
            break;
        case 5:
            for (r3y = 0, y = 0; y < width; y++, r3y++) {
                if (r3y == 3)
                    r3y = 0;
                for (r3x = 0, x = 0; x < width; x++, r3x++) {
                    if (r3x == 3)
                        r3x = 0;
                    if (!((x & y & 1) + !(!r3x | !r3y)) && !ismasked(x, y))
                        qrframe[x + y * width] ^= 1;
                }
            }
            break;
        case 6:
            for (r3y = 0, y = 0; y < width; y++, r3y++) {
                if (r3y == 3)
                    r3y = 0;
                for (r3x = 0, x = 0; x < width; x++, r3x++) {
                    if (r3x == 3)
                        r3x = 0;
                    if (!(((x & y & 1) + (r3x && (r3x == r3y))) & 1) && !ismasked(x, y))
                        qrframe[x + y * width] ^= 1;
                }
            }
            break;
        case 7:
            for (r3y = 0, y = 0; y < width; y++, r3y++) {
                if (r3y == 3)
                    r3y = 0;
                for (r3x = 0, x = 0; x < width; x++, r3x++) {
                    if (r3x == 3)
                        r3x = 0;
                    if (!(((r3x && (r3x == r3y)) + ((x + y) & 1)) & 1) && !ismasked(x, y))
                        qrframe[x + y * width] ^= 1;
                }
            }
            break;
        }
        return;
    }

    // Badness coefficients.
    var N1 = 3, N2 = 3, N3 = 40, N4 = 10;

    // Using the table of the length of each run, calculate the amount of bad image 
    // - long runs or those that look like finders; called twice, once each for X and Y
    function badruns(length)
    {
        var i;
        var runsbad = 0;
        for (i = 0; i <= length; i++)
            if (rlens[i] >= 5)
                runsbad += N1 + rlens[i] - 5;
        // BwBBBwB as in finder
        for (i = 3; i < length - 1; i += 2)
            if (rlens[i - 2] == rlens[i + 2]
                && rlens[i + 2] == rlens[i - 1]
                && rlens[i - 1] == rlens[i + 1]
                && rlens[i - 1] * 3 == rlens[i]
                // white around the black pattern? Not part of spec
                && (rlens[i - 3] == 0 // beginning
                    || i + 3 > length  // end
                    || rlens[i - 3] * 3 >= rlens[i] * 4 || rlens[i + 3] * 3 >= rlens[i] * 4)
               )
                runsbad += N3;
        return runsbad;
    }

    // Calculate how bad the masked image is - blocks, imbalance, runs, or finders.
    function badcheck()
    {
        var x, y, h, b, b1;
        var thisbad = 0;
        var bw = 0;

        // blocks of same color.
        for (y = 0; y < width - 1; y++)
            for (x = 0; x < width - 1; x++)
                if ((qrframe[x + width * y] && qrframe[(x + 1) + width * y]
                     && qrframe[x + width * (y + 1)] && qrframe[(x + 1) + width * (y + 1)]) // all black
                    || !(qrframe[x + width * y] || qrframe[(x + 1) + width * y]
                         || qrframe[x + width * (y + 1)] || qrframe[(x + 1) + width * (y + 1)])) // all white
                    thisbad += N2;

        // X runs
        for (y = 0; y < width; y++) {
            rlens[0] = 0;
            for (h = b = x = 0; x < width; x++) {
                if ((b1 = qrframe[x + width * y]) == b)
                    rlens[h]++;
                else
                    rlens[++h] = 1;
                b = b1;
                bw += b ? 1 : -1;
            }
            thisbad += badruns(h);
        }

        // black/white imbalance
        if (bw < 0)
            bw = -bw;

        var big = bw;
        count = 0;
        big += big << 2;
        big <<= 1;
        while (big > width * width)
            big -= width * width, count++;
        thisbad += count * N4;

        // Y runs
        for (x = 0; x < width; x++) {
            rlens[0] = 0;
            for (h = b = y = 0; y < width; y++) {
                if ((b1 = qrframe[x + width * y]) == b)
                    rlens[h]++;
                else
                    rlens[++h] = 1;
                b = b1;
            }
            thisbad += badruns(h);
        }
        return thisbad;
    }

    function genframe(instring)
    {
        var x, y, k, t, v, i, j, m;

        instring = instring || ''

    // find the smallest version that fits the string
        t = instring.length;
        version = 0;
        do {
            version++;
            k = (ecclevel - 1) * 4 + (version - 1) * 16;
            neccblk1 = eccblocks[k++];
            neccblk2 = eccblocks[k++];
            datablkw = eccblocks[k++];
            eccblkwid = eccblocks[k];
            k = datablkw * (neccblk1 + neccblk2) + neccblk2 - 3 + (version <= 9);
            if (t <= k)
                break;
        } while (version < 40);

    // FIXME - insure that it fits insted of being truncated
        width = 17 + 4 * version;

    // allocate, clear and setup data structures
        v = datablkw + (datablkw + eccblkwid) * (neccblk1 + neccblk2) + neccblk2;
        for( t = 0; t < v; t++ )
            eccbuf[t] = 0;
        strinbuf = instring.slice(0);

        for( t = 0; t < width * width; t++ )
            qrframe[t] = 0;

        for( t = 0 ; t < (width * (width + 1) + 1) / 2; t++)
            framask[t] = 0;

    // insert finders - black to frame, white to mask
        for (t = 0; t < 3; t++) {
            k = 0;
            y = 0;
            if (t == 1)
                k = (width - 7);
            if (t == 2)
                y = (width - 7);
            qrframe[(y + 3) + width * (k + 3)] = 1;
            for (x = 0; x < 6; x++) {
                qrframe[(y + x) + width * k] = 1;
                qrframe[y + width * (k + x + 1)] = 1;
                qrframe[(y + 6) + width * (k + x)] = 1;
                qrframe[(y + x + 1) + width * (k + 6)] = 1;
            }
            for (x = 1; x < 5; x++) {
                setmask(y + x, k + 1);
                setmask(y + 1, k + x + 1);
                setmask(y + 5, k + x);
                setmask(y + x + 1, k + 5);
            }
            for (x = 2; x < 4; x++) {
                qrframe[(y + x) + width * (k + 2)] = 1;
                qrframe[(y + 2) + width * (k + x + 1)] = 1;
                qrframe[(y + 4) + width * (k + x)] = 1;
                qrframe[(y + x + 1) + width * (k + 4)] = 1;
            }
        }

    // alignment blocks
        if (version > 1) {
            t = adelta[version];
            y = width - 7;
            for (;;) {
                x = width - 7;
                while (x > t - 3) {
                    putalign(x, y);
                    if (x < t)
                        break;
                    x -= t;
                }
                if (y <= t + 9)
                    break;
                y -= t;
                putalign(6, y);
                putalign(y, 6);
            }
        }

    // single black
        qrframe[8 + width * (width - 8)] = 1;

    // timing gap - mask only
        for (y = 0; y < 7; y++) {
            setmask(7, y);
            setmask(width - 8, y);
            setmask(7, y + width - 7);
        }
        for (x = 0; x < 8; x++) {
            setmask(x, 7);
            setmask(x + width - 8, 7);
            setmask(x, width - 8);
        }

    // reserve mask-format area
        for (x = 0; x < 9; x++)
            setmask(x, 8);
        for (x = 0; x < 8; x++) {
            setmask(x + width - 8, 8);
            setmask(8, x);
        }
        for (y = 0; y < 7; y++)
            setmask(8, y + width - 7);

    // timing row/col
        for (x = 0; x < width - 14; x++)
            if (x & 1) {
                setmask(8 + x, 6);
                setmask(6, 8 + x);
            }
            else {
                qrframe[(8 + x) + width * 6] = 1;
                qrframe[6 + width * (8 + x)] = 1;
            }

    // version block
        if (version > 6) {
            t = vpat[version - 7];
            k = 17;
            for (x = 0; x < 6; x++)
                for (y = 0; y < 3; y++, k--)
                    if (1 & (k > 11 ? version >> (k - 12) : t >> k)) {
                        qrframe[(5 - x) + width * (2 - y + width - 11)] = 1;
                        qrframe[(2 - y + width - 11) + width * (5 - x)] = 1;
                    }
            else {
                setmask(5 - x, 2 - y + width - 11);
                setmask(2 - y + width - 11, 5 - x);
            }
        }

    // sync mask bits - only set above for white spaces, so add in black bits
        for (y = 0; y < width; y++)
            for (x = 0; x <= y; x++)
                if (qrframe[x + width * y])
                    setmask(x, y);

    // convert string to bitstream
    // 8 bit data to QR-coded 8 bit data (numeric or alphanum, or kanji not supported)
        v = strinbuf.length;

    // string to array
        for( i = 0 ; i < v; i++ )
            eccbuf[i] = strinbuf.charCodeAt(i);
        strinbuf = eccbuf.slice(0);

    // calculate max string length
        x = datablkw * (neccblk1 + neccblk2) + neccblk2;
        if (v >= x - 2) {
            v = x - 2;
            if (version > 9)
                v--;
        }

    // shift and repack to insert length prefix
        i = v;
        if (version > 9) {
            strinbuf[i + 2] = 0;
            strinbuf[i + 3] = 0;
            while (i--) {
                t = strinbuf[i];
                strinbuf[i + 3] |= 255 & (t << 4);
                strinbuf[i + 2] = t >> 4;
            }
            strinbuf[2] |= 255 & (v << 4);
            strinbuf[1] = v >> 4;
            strinbuf[0] = 0x40 | (v >> 12);
        }
        else {
            strinbuf[i + 1] = 0;
            strinbuf[i + 2] = 0;
            while (i--) {
                t = strinbuf[i];
                strinbuf[i + 2] |= 255 & (t << 4);
                strinbuf[i + 1] = t >> 4;
            }
            strinbuf[1] |= 255 & (v << 4);
            strinbuf[0] = 0x40 | (v >> 4);
        }
    // fill to end with pad pattern
        i = v + 3 - (version < 10);
        while (i < x) {
            strinbuf[i++] = 0xec;
            // buffer has room    if (i == x)      break;
            strinbuf[i++] = 0x11;
        }

    // calculate and append ECC

    // calculate generator polynomial
        genpoly[0] = 1;
        for (i = 0; i < eccblkwid; i++) {
            genpoly[i + 1] = 1;
            for (j = i; j > 0; j--)
                genpoly[j] = genpoly[j]
                ? genpoly[j - 1] ^ gexp[modnn(glog[genpoly[j]] + i)] : genpoly[j - 1];
            genpoly[0] = gexp[modnn(glog[genpoly[0]] + i)];
        }
        for (i = 0; i <= eccblkwid; i++)
            genpoly[i] = glog[genpoly[i]]; // use logs for genpoly[] to save calc step

    // append ecc to data buffer
        k = x;
        y = 0;
        for (i = 0; i < neccblk1; i++) {
            appendrs(y, datablkw, k, eccblkwid);
            y += datablkw;
            k += eccblkwid;
        }
        for (i = 0; i < neccblk2; i++) {
            appendrs(y, datablkw + 1, k, eccblkwid);
            y += datablkw + 1;
            k += eccblkwid;
        }
    // interleave blocks
        y = 0;
        for (i = 0; i < datablkw; i++) {
            for (j = 0; j < neccblk1; j++)
                eccbuf[y++] = strinbuf[i + j * datablkw];
            for (j = 0; j < neccblk2; j++)
                eccbuf[y++] = strinbuf[(neccblk1 * datablkw) + i + (j * (datablkw + 1))];
        }
        for (j = 0; j < neccblk2; j++)
            eccbuf[y++] = strinbuf[(neccblk1 * datablkw) + i + (j * (datablkw + 1))];
        for (i = 0; i < eccblkwid; i++)
            for (j = 0; j < neccblk1 + neccblk2; j++)
                eccbuf[y++] = strinbuf[x + i + j * eccblkwid];
        strinbuf = eccbuf;

    // pack bits into frame avoiding masked area.
        x = y = width - 1;
        k = v = 1;         // up, minus
        /* inteleaved data and ecc codes */
        m = (datablkw + eccblkwid) * (neccblk1 + neccblk2) + neccblk2;
        for (i = 0; i < m; i++) {
            t = strinbuf[i];
            for (j = 0; j < 8; j++, t <<= 1) {
                if (0x80 & t)
                    qrframe[x + width * y] = 1;
                do {        // find next fill position
                    if (v)
                        x--;
                    else {
                        x++;
                        if (k) {
                            if (y != 0)
                                y--;
                            else {
                                x -= 2;
                                k = !k;
                                if (x == 6) {
                                    x--;
                                    y = 9;
                                }
                            }
                        }
                        else {
                            if (y != width - 1)
                                y++;
                            else {
                                x -= 2;
                                k = !k;
                                if (x == 6) {
                                    x--;
                                    y -= 8;
                                }
                            }
                        }
                    }
                    v = !v;
                } while (ismasked(x, y));
            }
        }

    // save pre-mask copy of frame
        strinbuf = qrframe.slice(0);
        t = 0;           // best
        y = 30000;         // demerit
    // for instead of while since in original arduino code
    // if an early mask was "good enough" it wouldn't try for a better one
    // since they get more complex and take longer.
        for (k = 0; k < 8; k++) {
            applymask(k);      // returns black-white imbalance
            x = badcheck();
            if (x < y) { // current mask better than previous best?
                y = x;
                t = k;
            }
            if (t == 7)
                break;       // don't increment i to a void redoing mask
            qrframe = strinbuf.slice(0); // reset for next pass
        }
        if (t != k)         // redo best mask - none good enough, last wasn't t
            applymask(t);

    // add in final mask/ecclevel bytes
        y = fmtword[t + ((ecclevel - 1) << 3)];
        // low byte
        for (k = 0; k < 8; k++, y >>= 1)
            if (y & 1) {
                qrframe[(width - 1 - k) + width * 8] = 1;
                if (k < 6)
                    qrframe[8 + width * k] = 1;
                else
                    qrframe[8 + width * (k + 1)] = 1;
            }
        // high byte
        for (k = 0; k < 7; k++, y >>= 1)
            if (y & 1) {
                qrframe[8 + width * (width - 7 + k)] = 1;
                if (k)
                    qrframe[(6 - k) + width * 8] = 1;
                else
                    qrframe[7 + width * 8] = 1;
            }

    // return image
        return qrframe;
    }

    var _canvas = null,
        _size = null;

    var api = {

        get ecclevel () {
            return ecclevel;
        },

        set ecclevel (val) {
            ecclevel = val;
        },

        get size () {
            return _size;
        },

        set size (val) {
            _size = val
        },

        get canvas () {
            return _canvas;
        },

        set canvas (el) {
            _canvas = el;
        },

        getFrame: function (string) {
            return genframe(string);
        },

        draw: function (string, canvas, size, ecc) {
            
            ecclevel = ecc || ecclevel;
            canvas = canvas || _canvas;

            if (!canvas) {
                console.warn('No canvas provided to draw QR code in!')
                return;
            }

            size = size || _size || Math.min(canvas.width, canvas.height);

            var frame = genframe(string),
                ctx = canvas.getContext('2d'),
                px = Math.round(size / (width + 8));

            var roundedSize = px * (width + 8),
                offset = Math.floor((size - roundedSize) / 2);

            size = roundedSize;

            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.fillStyle = '#fff';
            ctx.fillRect(0, 0, size, size);
            ctx.fillStyle = '#000';
            for (var i = 0; i < width; i++) {
                for (var j = 0; j < width; j++) {
                    if (frame[j * width + i]) {
                        ctx.fillRect(px * (4 + i) + offset, px * (4 + j) + offset, px, px);
                    }
                }
            }

        },

        toDataURL: function (string, size, ecc) {
            var canvas = document.createElement('canvas');
            canvas.width = size || _size || 300;
            canvas.height = canvas.width;
            api.draw(string, canvas, canvas.width, ecc);
            return canvas.toDataURL();
        },

        makeImage: function (string, size, ecc) {
            var img = new Image();
            img.src = api.toDataURL(string, size, ecc);

            return img;
        }
    }

    return api;

})()
"use strict";

AFRAME.registerComponent("xyinput", {
    dependencies: [ "xylabel" ],
    schema: {
        value: {
            default: ""
        },
        type: {
            default: ""
        },
        placeholder: {
            default: ""
        },
        caretColor: {
            default: "#0088ff"
        },
        bgColor: {
            default: "white"
        }
    },
    init() {
        let data = this.data, el = this.el, xyrect = el.components.xyrect;
        let insertString = v => {
            let pos = this.cursor, s = el.value;
            this.cursor += v.length;
            el.value = s.slice(0, pos) + v + s.slice(pos);
        };
        Object.defineProperty(el, "value", {
            get: () => data.value,
            set: v => el.setAttribute("xyinput", "value", "" + v)
        });
        this._caretObj = new THREE.Mesh(new THREE.PlaneGeometry(.04, xyrect.height * .9));
        el.setObject3D("caret", this._caretObj);
        el.classList.add("collidable");
        let updateGeometory = () => {
            el.setAttribute("geometry", {
                primitive: "xy-rounded-rect",
                width: xyrect.width,
                height: xyrect.height
            });
        };
        updateGeometory();
        el.setAttribute("tabindex", 0);
        el.addEventListener("xyresize", updateGeometory);
        let oncopy = ev => {
            ev.clipboardData.setData("text/plain", el.value);
            ev.preventDefault();
        };
        let onpaste = ev => {
            insertString(ev.clipboardData.getData("text/plain"));
            ev.preventDefault();
        };
        let self = this;
        this.events = {
            click(ev) {
                el.focus();
                el.emit("xykeyboard-request", data.type);
                let intersection = ev.detail.intersection;
                if (intersection) {
                    let v = intersection.uv.x;
                    let min = 0, max = el.value.length, p = 0;
                    while (max > min) {
                        p = min + ((max - min + 1) / 2 | 0);
                        if (self._caretpos(p) < v) {
                            min = p;
                        } else {
                            max = p - 1;
                        }
                    }
                    self._updateCursor(min);
                }
            },
            focus(ev) {
                self._updateCursor(self.cursor);
                window.addEventListener("copy", oncopy);
                window.addEventListener("paste", onpaste);
            },
            blur(ev) {
                self._updateCursor(self.cursor);
                window.removeEventListener("copy", oncopy);
                window.removeEventListener("paste", onpaste);
            },
            keypress(ev) {
                if (ev.code != "Enter") {
                    insertString(ev.key);
                }
            },
            keydown(ev) {
                let pos = self.cursor, s = el.value;
                if (ev.code == "ArrowLeft") {
                    if (pos > 0) {
                        self._updateCursor(pos - 1);
                    }
                } else if (ev.code == "ArrowRight") {
                    if (pos < s.length) {
                        self._updateCursor(pos + 1);
                    }
                } else if (ev.code == "Backspace") {
                    if (pos > 0) {
                        self.cursor--;
                        el.value = s.slice(0, pos - 1) + s.slice(pos);
                    }
                }
            }
        };
    },
    update(oldData) {
        let el = this.el, data = this.data;
        let s = el.value, cursor = this.cursor, len = s.length;
        if (cursor > len || oldData.value == null) {
            cursor = len;
        }
        el.setAttribute("xylabel", {
            color: s ? "black" : "#aaa",
            value: (data.type == "password" ? "*".repeat(len) : s) || data.placeholder
        });
        el.setAttribute("material", {
            color: data.bgColor
        });
        this._caretObj.material.color = new THREE.Color(data.caretColor);
        this._updateCursor(cursor);
    },
    _updateCursor(p) {
        let caretObj = this._caretObj;
        this.cursor = p;
        caretObj.visible = false;
        if (document.activeElement == this.el) {
            setTimeout(() => {
                caretObj.position.set(this._caretpos(p), 0, .02);
                caretObj.visible = true;
            }, 0);
        }
    },
    _caretpos(cursorPos) {
        return this.el.components.xylabel.getPos(cursorPos) + .04;
    }
});

AFRAME.registerComponent("xyime", {
    schema: {
        label: {
            default: null,
            type: "selector"
        }
    },
    table: {
        a: "あ",
        i: "い",
        u: "う",
        e: "え",
        o: "お",
        ka: "か",
        ki: "き",
        ku: "く",
        ke: "け",
        ko: "こ",
        ga: "が",
        gi: "ぎ",
        gu: "ぐ",
        ge: "げ",
        go: "ご",
        sa: "さ",
        si: "し",
        su: "す",
        se: "せ",
        so: "そ",
        za: "ざ",
        zi: "じ",
        zu: "ず",
        ze: "ぜ",
        zo: "ぞ",
        ta: "た",
        ti: "ち",
        tu: "つ",
        te: "て",
        to: "と",
        da: "だ",
        di: "ぢ",
        du: "づ",
        de: "で",
        do: "ど",
        na: "な",
        ni: "に",
        nu: "ぬ",
        ne: "ね",
        no: "の",
        ha: "は",
        hi: "ひ",
        hu: "ふ",
        he: "へ",
        ho: "ほ",
        pa: "ぱ",
        pi: "ぴ",
        pu: "ぷ",
        pe: "ぺ",
        po: "ぽ",
        ba: "ば",
        bi: "び",
        bu: "ぶ",
        be: "べ",
        bo: "ぼ",
        ma: "ま",
        mi: "み",
        mu: "む",
        me: "め",
        mo: "も",
        ya: "や",
        yi: "い",
        yu: "ゆ",
        ye: "いぇ",
        yo: "よ",
        ra: "ら",
        ri: "り",
        ru: "る",
        re: "れ",
        ro: "ろ",
        wa: "わ",
        wi: "うぃ",
        wu: "う",
        we: "うぇ",
        wo: "を",
        xa: "ぁ",
        xi: "ぃ",
        xu: "ぅ",
        xe: "ぇ",
        xo: "ぉ",
        xya: "ゃ",
        xyi: "ぃ",
        xyu: "ゅ",
        xye: "ぇ",
        xyo: "ょ",
        xtu: "っ",
        xka: "ヵ",
        xke: "ヶ",
        nn: "ん",
        wyi: "ゐ",
        wye: "ゑ",
        fu: "ふ",
        vu: "ヴ",
        tsu: "つ",
        chi: "ち",
        ji: "じ",
        shi: "し",
        "-": "ー"
    },
    init() {
        this.enable = false;
        this._kana = "";
        this._suggestions = [];
        this._suggestionIdx = 0;
        this._onkeydown = this._onkeydown.bind(this);
        document.body.addEventListener("keydown", this._onkeydown, true);
        document.body.addEventListener("keypress", this._onkeydown, true);
    },
    remove() {
        document.body.removeEventListener("keydown", this._onkeydown, true);
        document.body.removeEventListener("keypress", this._onkeydown, true);
    },
    async convert(text, suggest) {
        let response = await fetch(`https://www.google.com/transliterate?langpair=ja-Hira|ja&text=${text},`);
        let result = await response.json();
        suggest(result[0][1]);
    },
    _onkeydown(ev) {
        if (ev.code == "CapsLock" && ev.shiftKey || ev.key == "HiraganaKatakana") {
            this.enable = !this.enable;
            this._confirm(ev.target);
            return;
        }
        if (!this.enable || !ev.code) {
            return;
        }
        if (ev.type == "keypress") {
            if (this._suggestions.length > 0) {
                if (ev.code == "Space") {
                    this._suggestionIdx = (this._suggestionIdx + 1) % this._suggestions.length;
                    this._updateStr(this._suggestions[this._suggestionIdx]);
                    ev.stopPropagation();
                    return;
                }
                this._confirm(ev.target);
            }
            if (ev.key.match(/^[^\s]$/)) {
                let temp = [ [ /n([^aiueoyn])/g, "nn$1" ], [ /([ksthmyrwgzbpdjfv])\1/g, "xtu$1" ], [ /([kstnhmrgzbpdj])(y[aiueo])/g, "$1ix$2" ], [ /(j|ch|sh)([aueo])/g, "$1ixy$2" ], [ /(f|v|ts)(y?[aieo])/g, "$1ux$2" ], [ /(t|d)h([aiueo])/g, "$1exy$2" ] ].reduce((acc, [ptn, r]) => acc.replace(ptn, r), this._kana + ev.key);
                for (let p = 0; p < temp.length; p++) {
                    for (let l = 3; l >= 0; l--) {
                        let t = this.table[temp.slice(p, p + l)];
                        if (t) {
                            temp = temp.slice(0, p) + t + temp.slice(p + l);
                            break;
                        }
                    }
                }
                this._updateStr(temp);
            } else if (ev.code == "Space" && this._kana) {
                this.convert(this._kana, ret => {
                    this._suggestions = ret;
                    this._suggestionIdx = 0;
                    this._updateStr(ret[0]);
                });
                this._updateStr("");
            } else if (this._kana) {
                this._updateStr(this._kana + ev.key);
            } else {
                return;
            }
        } else if (this._kana) {
            if (ev.code == "Enter") {
                this._confirm(ev.target);
            } else if (ev.code == "Backspace") {
                this._updateStr(this._kana.slice(0, -1));
            }
        } else {
            return;
        }
        ev.stopPropagation();
    },
    _updateStr(s) {
        this._kana = s;
        (this.data.label || this.el).setAttribute("value", s);
    },
    _confirm(target) {
        if (this._kana) {
            target.dispatchEvent(new KeyboardEvent("keypress", {
                key: this._kana
            }));
            this._updateStr("");
        }
        this._suggestions = [];
    }
});

AFRAME.registerComponent("xykeyboard", {
    schema: {
        distance: {
            default: .7
        },
        ime: {
            default: false
        }
    },
    blocks: {
        main: {
            size: [ 11, 4 ],
            rows: [ {
                pos: [ 0, 3 ],
                keys: [ "qQ!", "wW@", "eE#", "rR$", "tT%", "yY^", "uU&", "iI*", "oO(", "pP)", "-_=" ]
            }, {
                pos: [ 0, 2 ],
                keys: [ "aA1", "sS2", "dD3", "fF4", "gG5", "hH`", "jJ~", "kK+", "lL[", ":;]" ]
            }, {
                pos: [ 0, 1 ],
                keys: [ {
                    code: "Shift",
                    symbols: "⇧⬆"
                }, "zZ6", "xX7", "cC8", "vV9", "bB0", "nN{", "mM}", ",'<", '.">', "/?\\" ]
            }, {
                pos: [ 0, 0 ],
                keys: [ {
                    code: "Space",
                    key: " ",
                    label: "_",
                    size: 4
                } ]
            }, {
                pos: [ -4.5, 0 ],
                keys: [ {
                    code: "_Fn",
                    label: "#!"
                }, {
                    code: "HiraganaKatakana",
                    label: "🌐"
                } ]
            } ]
        },
        num: {
            size: [ 4, 4 ],
            rows: [ {
                pos: [ 0, 3 ],
                keys: [ "7", "8", "9", "/" ]
            }, {
                pos: [ 0, 2 ],
                keys: [ "4", "5", "6", "*" ]
            }, {
                pos: [ 0, 1 ],
                keys: [ "1", "2", "3", "-" ]
            }, {
                pos: [ 0, 0 ],
                keys: [ "0", ":", ".", "+" ]
            } ]
        },
        ctrl: {
            size: [ 2, 4 ],
            rows: [ {
                pos: [ 0, 3 ],
                keys: [ {
                    code: "Backspace",
                    label: "⌫",
                    size: 2
                } ]
            }, {
                pos: [ 0, 2 ],
                keys: [ {
                    code: "Space",
                    key: " ",
                    label: "SP",
                    size: 2
                } ]
            }, {
                pos: [ 0, 1 ],
                keys: [ {
                    code: "Enter",
                    label: "⏎",
                    size: 2
                } ]
            }, {
                pos: [ 1.3, 3.5 ],
                keys: [ {
                    code: "_Close",
                    label: "x",
                    size: .8
                } ]
            }, {
                pos: [ 0, 0 ],
                keys: [ {
                    code: "ArrowLeft",
                    label: "⇦"
                }, {
                    code: "ArrowRight",
                    label: "⇨"
                } ]
            } ]
        }
    },
    init() {
        this.el.sceneEl.addEventListener("xykeyboard-request", ev => this.show(ev.detail));
    },
    show(type) {
        this.hide();
        let el = this.el;
        let data = this.data;
        let keySize = .2;
        let excludes = data.ime ? [] : [ "HiraganaKatakana" ];
        let blocks = this.blocks;
        let createKeys = (block, excludes = []) => {
            let pane = document.createElement("a-entity");
            let padding = keySize * .3;
            let size = block.size;
            pane.setAttribute("geometry", {
                primitive: "xy-rounded-rect",
                width: size[0] * keySize + padding,
                height: size[1] * keySize + padding
            });
            pane.setAttribute("material", {
                color: "#222233"
            });
            for (let row of block.rows) {
                let keyrow = pane.appendChild(document.createElement("a-xycontainer"));
                keyrow.setAttribute("xycontainer", {
                    direction: "row"
                });
                keyrow.setAttribute("position", {
                    x: row.pos[0] * keySize,
                    y: row.pos[1] * keySize - (size[1] - 1) * keySize / 2,
                    z: .02
                });
                for (let key of row.keys) {
                    if (excludes.includes(key.code)) {
                        continue;
                    }
                    let keyEl = keyrow.appendChild(document.createElement("a-xybutton"));
                    keyEl.setAttribute("material", "visible", false);
                    keyEl.setAttribute("xylabel", {
                        value: key.label || "",
                        align: "center"
                    });
                    keyEl.setAttribute("xyrect", {
                        width: (key.size || 1) * keySize,
                        height: keySize
                    });
                    keyEl.addEventListener("mouseenter", ev => keyEl.setAttribute("material", "visible", true));
                    keyEl.addEventListener("mouseleave", ev => keyEl.setAttribute("material", "visible", false));
                    if (key.symbols || typeof key === "string") {
                        keyEl.classList.add("xyinput-key");
                        keyEl.dataset.keySymbols = key.symbols || key;
                    }
                    if (key.code == "_Close") {
                        keyEl.classList.add("xyinput-close");
                        keyEl.addEventListener("click", ev => this.hide());
                    }
                    keyEl.addEventListener("mousedown", ev => {
                        if (document.activeElement == document.body && this._target) {
                            this._target.focus();
                        }
                        this._target = document.activeElement;
                        setTimeout(() => this._target.focus(), 0);
                        ev.preventDefault();
                        if (key.code == "_Fn") {
                            this._updateSymbols(this._keyidx == 2 ? 0 : 2);
                            return;
                        }
                        if (key.code == "Shift") {
                            this._updateSymbols((this._keyidx + 1) % 2);
                        }
                        let ks = key.code ? key.key : key;
                        let eventdata = {
                            key: ks ? ks[this._keyidx] || ks[0] : key.code,
                            code: key.code || key[0].toUpperCase()
                        };
                        let emit = (name, eventdata) => {
                            this._target.dispatchEvent(new KeyboardEvent(name, eventdata));
                        };
                        emit("keydown", eventdata);
                        emit("keyup", eventdata);
                        if (ks) {
                            emit("keypress", eventdata);
                        }
                    });
                }
            }
            return el.appendChild(pane);
        };
        if (type == "number") {
            let w = blocks.num.size[0] + blocks.ctrl.size[0];
            createKeys(blocks.num);
            createKeys(blocks.ctrl).setAttribute("position", "x", (w / 2 + .4) * keySize);
        } else if (type == "full") {
            let w = blocks.main.size[0] + blocks.ctrl.size[0];
            createKeys(blocks.main, excludes);
            createKeys(blocks.ctrl, [ "Space" ]).setAttribute("position", "x", (w / 2 + .4) * keySize);
            w += blocks.ctrl.size[0] + blocks.num.size[0];
            createKeys(blocks.num).setAttribute("position", "x", (w / 2 + .8) * keySize);
        } else {
            let w = blocks.main.size[0] + blocks.ctrl.size[0];
            createKeys(blocks.main, excludes);
            createKeys(blocks.ctrl, [ "Space" ]).setAttribute("position", "x", (w / 2 + .4) * keySize);
        }
        if (data.ime) {
            let convText = el.appendChild(document.createElement("a-xylabel"));
            convText.setAttribute("xylabel", {
                color: "yellow",
                renderingMode: "canvas"
            });
            convText.setAttribute("position", {
                x: 0,
                y: 2 * keySize * .95,
                z: .03
            });
            convText.setAttribute("xyrect", {
                width: 8 * keySize,
                height: keySize * .6
            });
            convText.setAttribute("xyime", "");
        }
        el.setAttribute("xy-drag-control", "draggable", ".xyinput-close");
        this._updateSymbols(0);
        let obj = el.object3D, position = obj.position;
        let tr = obj.parent.matrixWorld.clone().invert().multiply(el.sceneEl.camera.matrixWorld);
        let orgY = position.y;
        position.set(0, 0, -data.distance).applyMatrix4(tr);
        position.y = orgY;
        obj.rotation.y = new THREE.Euler().setFromRotationMatrix(tr.extractRotation(tr), "YXZ").y;
    },
    hide() {
        let el = this.el;
        this._target = null;
        el.removeAttribute("xy-drag-control");
        while (el.firstChild) {
            el.removeChild(el.firstChild);
        }
    },
    _updateSymbols(keyidx) {
        this._keyidx = keyidx;
        for (let keyEl of this.el.querySelectorAll(".xyinput-key")) {
            let s = keyEl.dataset.keySymbols;
            keyEl.setAttribute("xylabel", "value", s[keyidx] || s[0]);
        }
    }
});

AFRAME.registerPrimitive("a-xykeyboard", {
    defaultComponents: {
        xykeyboard: {}
    },
    mappings: {
        ime: "xykeyboard.ime",
        distance: "xykeyboard.distance"
    }
});

AFRAME.registerPrimitive("a-xyinput", {
    defaultComponents: {
        xyrect: {
            width: 2,
            height: .5
        },
        xyinput: {}
    },
    mappings: {
        width: "xyrect.width",
        height: "xyrect.height",
        value: "xyinput.value",
        type: "xyinput.type",
        placeholder: "xyinput.placeholder",
        "caret-color": "xyinput.caretColor",
        "background-color": "xyinput.bgColor"
    }
});

"use strict";

AFRAME.registerComponent("xycontainer", {
    dependencies: [ "xyrect" ],
    schema: {
        spacing: {
            default: 0
        },
        padding: {
            default: 0
        },
        reverse: {
            default: false
        },
        wrap: {
            default: "nowrap",
            oneOf: [ "wrap", "nowrap" ]
        },
        direction: {
            default: "column",
            oneOf: [ "none", "row", "column", "vertical", "horizontal" ]
        },
        alignItems: {
            default: "none",
            oneOf: [ "none", "center", "start", "end", "baseline", "stretch" ]
        },
        justifyItems: {
            default: "start",
            oneOf: [ "center", "start", "end", "space-between", "space-around", "stretch" ]
        },
        alignContent: {
            default: "",
            oneOf: [ "", "none", "start", "end", "center", "stretch" ]
        }
    },
    init() {
        for (let e of [ "child-attached", "child-detached", "xyresize" ]) {
            this.el.addEventListener(e, ev => ev.target == this.el && setTimeout(() => this.update()));
        }
    },
    update() {
        let data = this.data;
        let direction = data.direction;
        if (direction == "none") {
            return;
        }
        let containerRect = this.el.components.xyrect;
        let children = this.el.children;
        let isVertical = direction == "vertical" || direction == "column";
        let padding = data.padding;
        let spacing = data.spacing;
        let mainDir = data.reverse != isVertical ? -1 : 1;
        let toXY = (m, c) => isVertical ? [ c, m * mainDir ] : [ m * mainDir, -c ];
        let xyToMainCross = (x, y) => isVertical ? [ y, x ] : [ x, y ];
        let [containerSizeM, containerSizeC] = xyToMainCross(containerRect.width - padding * 2, containerRect.height - padding * 2);
        let [attrNameM, attrNameC] = xyToMainCross("width", "height");
        let mainSize = 0;
        let crossSizeSum = 0;
        let targets = [];
        let lines = [];
        let sizeSum = 0;
        let growSum = 0;
        let shrinkSum = 0;
        let crossSize = 0;
        let newLine = () => {
            mainSize = Math.max(mainSize, sizeSum + spacing * (targets.length - 1));
            crossSizeSum += crossSize;
            lines.push([ targets, sizeSum, growSum, shrinkSum, crossSize ]);
            targets = [];
            sizeSum = 0;
            growSum = 0;
            shrinkSum = 0;
            crossSize = 0;
        };
        for (let el of children) {
            let xyitem = el.getAttribute("xyitem");
            if (xyitem && xyitem.fixed) {
                continue;
            }
            let rect = el.components.xyrect || el.getAttribute("geometry") || {
                width: +(el.getAttribute("width") || NaN),
                height: +(el.getAttribute("height") || NaN)
            };
            let childScale = el.getAttribute("scale") || {
                x: 1,
                y: 1
            };
            let size = xyToMainCross(rect.width * childScale.x, rect.height * childScale.y);
            let [sizeM, sizeC] = size;
            if (sizeM == null || isNaN(sizeM)) {
                continue;
            }
            let pivot = rect.data ? rect.data.pivot : {
                x: .5,
                y: .5
            };
            let contentSize = sizeSum + sizeM + spacing * targets.length;
            if (data.wrap == "wrap" && sizeSum > 0 && contentSize > containerSizeM) {
                newLine();
            }
            targets.push([ el, xyitem, size, xyToMainCross(pivot.x, pivot.y), xyToMainCross(childScale.x, childScale.y) ]);
            sizeSum += sizeM;
            growSum += xyitem ? xyitem.grow : 1;
            shrinkSum += xyitem ? xyitem.shrink : 1;
            crossSize = sizeC > crossSize ? sizeC : crossSize;
        }
        if (targets.length > 0) {
            newLine();
        }
        crossSizeSum += spacing * (lines.length - 1);
        if (containerRect.data[attrNameM] == -1) {
            containerSizeM = mainSize;
            containerRect[attrNameM] = mainSize + padding * 2;
        }
        if (containerRect.data[attrNameC] == -1) {
            containerSizeC = crossSizeSum;
            containerRect[attrNameC] = crossSizeSum + padding * 2;
        }
        let crossOffset = -containerSizeC / 2;
        let crossStretch = 0;
        let alignContent = data.alignContent || data.alignItems;
        if (alignContent == "end") {
            crossOffset += containerSizeC - crossSizeSum;
        } else if (alignContent == "center") {
            crossOffset += (containerSizeC - crossSizeSum) / 2;
        } else if (alignContent == "stretch" || alignContent == "none") {
            crossStretch = (containerSizeC - crossSizeSum) / lines.length;
        }
        for (let [targets, sizeSum, growSum, shrinkSum, crossSize] of lines) {
            this._layoutLine(targets, sizeSum, growSum, shrinkSum, -containerSizeM / 2, crossOffset, containerSizeM, crossSize + crossStretch, attrNameM, attrNameC, toXY);
            crossOffset += crossSize + crossStretch + spacing;
        }
    },
    _layoutLine(targets, sizeSum, growSum, shrinkSum, offset0, offset1, containerSize0, containerSize1, attrName0, attrName1, toXY) {
        let {justifyItems: justifyItems, alignItems: alignItems, spacing: spacing, wrap: wrap} = this.data;
        let stretchFactor = 0;
        let numTarget = targets.length;
        if (justifyItems === "center") {
            offset0 += (containerSize0 - sizeSum - spacing * numTarget) / 2;
        } else if (justifyItems === "end") {
            offset0 += containerSize0 - sizeSum - spacing * numTarget;
        } else if (justifyItems === "stretch") {
            stretchFactor = containerSize0 - sizeSum - spacing * (numTarget - 1);
            if (stretchFactor > 0) {
                stretchFactor = growSum > 0 ? stretchFactor / growSum : 0;
            } else {
                stretchFactor = shrinkSum > 0 ? stretchFactor / shrinkSum : 0;
            }
        } else if (justifyItems === "space-between") {
            spacing = (containerSize0 - sizeSum) / (numTarget - 1);
        } else if (justifyItems === "space-around") {
            spacing = (containerSize0 - sizeSum) / numTarget;
            offset0 += spacing / 2;
        }
        for (let [el, xyitem, [size0, size1], [pivot0, pivot1], [scale0, scale1]] of targets) {
            let align = xyitem && xyitem.align || alignItems;
            let stretch = (xyitem ? stretchFactor > 0 ? xyitem.grow : xyitem.shrink : 1) * stretchFactor;
            let posCross = offset1 + containerSize1 / 2;
            let pos = el.getAttribute("position") || {
                x: 0,
                y: 0
            };
            if (scale0 > 0 && stretch != 0) {
                size0 += stretch;
                el.setAttribute(attrName0, size0 / scale0);
            }
            if (scale1 > 0 && align === "stretch") {
                size1 = containerSize1;
                el.setAttribute(attrName1, size1 / scale1);
            }
            if (align === "start" || align === "stretch") {
                posCross = offset1 + pivot1 * size1;
            } else if (align === "end") {
                posCross = offset1 + containerSize1 - (1 - pivot1) * size1;
            } else if (align === "center") {
                posCross += (pivot1 - .5) * size1;
            } else if (align === "none" && wrap != "wrap") {
                posCross = attrName1 == "width" ? pos.x : -pos.y;
            }
            [pos.x, pos.y] = toXY(offset0 + size0 * pivot0, posCross);
            el.setAttribute("position", pos);
            offset0 += size0 + spacing;
        }
    }
});

AFRAME.registerComponent("xyitem", {
    schema: {
        align: {
            default: "none",
            oneOf: [ "none", "center", "start", "end", "baseline", "stretch" ]
        },
        grow: {
            default: 1
        },
        shrink: {
            default: 1
        },
        fixed: {
            default: false
        }
    },
    update(oldData) {
        if (oldData.align) {
            let xycontainer = this.el.parentNode.components.xycontainer;
            if (xycontainer) {
                xycontainer.update();
            }
        }
    }
});

AFRAME.registerComponent("xyrect", {
    schema: {
        width: {
            default: -1
        },
        height: {
            default: -1
        },
        pivot: {
            type: "vec2",
            default: {
                x: .5,
                y: .5
            }
        }
    },
    update(oldData) {
        let el = this.el;
        let {width: width, height: height} = this.data;
        let geometry = el.getAttribute("geometry") || {};
        this.width = width < 0 ? +(el.getAttribute("width") || geometry.width || 0) : width;
        this.height = height < 0 ? +(el.getAttribute("height") || geometry.height || 0) : height;
        if (oldData.width !== undefined) {
            el.emit("xyresize", {
                xyrect: this
            }, false);
        }
    }
});

AFRAME.registerPrimitive("a-xycontainer", {
    defaultComponents: {
        xyrect: {},
        xycontainer: {}
    },
    mappings: {
        width: "xyrect.width",
        height: "xyrect.height",
        direction: "xycontainer.direction",
        spacing: "xycontainer.spacing",
        padding: "xycontainer.padding",
        reverse: "xycontainer.reverse",
        wrap: "xycontainer.wrap",
        "align-items": "xycontainer.alignItems",
        "justify-items": "xycontainer.justifyItems",
        "align-content": "xycontainer.alignContent"
    }
});

"use strict";

const XYTheme = {
    get(el) {
        return this.defaultTheme;
    },
    defaultTheme: {
        button: {
            color: "#222",
            labelColor: "#fff",
            hoverColor: "#333",
            geometry: "xy-rounded-rect",
            hoverHaptic: .3,
            hoverHapticMs: 10
        },
        window: {
            closeButton: {
                color: "#111",
                hoverColor: "#f00"
            },
            titleBar: {
                color: "#111"
            },
            background: {
                color: "#111"
            }
        },
        collidableClass: "collidable",
        createButton(width, height, parentEl, params, hasLabel, buttonEl) {
            let getParam = p => params && params[p] || this.button[p];
            buttonEl = buttonEl || document.createElement("a-entity");
            if (!buttonEl.hasAttribute("geometry")) {
                buttonEl.setAttribute("geometry", {
                    primitive: getParam("geometry"),
                    width: width,
                    height: height
                });
            }
            buttonEl.classList.add(this.collidableClass);
            buttonEl.addEventListener("mouseenter", ev => {
                buttonEl.setAttribute("material", {
                    color: getParam("hoverColor")
                });
                let intensity = getParam("hoverHaptic");
                if (intensity) {
                    let trackedControls = ev.detail.cursorEl.components["tracked-controls"];
                    let gamepad = trackedControls && trackedControls.controller;
                    let hapticActuators = gamepad && gamepad.hapticActuators;
                    if (hapticActuators && hapticActuators[0]) {
                        hapticActuators[0].pulse(intensity, getParam("hoverHapticMs"));
                    } else {}
                }
            });
            buttonEl.addEventListener("xyresize", ev => {
                let r = ev.detail.xyrect;
                buttonEl.setAttribute("geometry", {
                    width: r.width,
                    height: r.height
                });
            });
            buttonEl.addEventListener("mouseleave", ev => {
                buttonEl.setAttribute("material", {
                    color: getParam("color")
                });
            });
            buttonEl.setAttribute("material", {
                color: getParam("color")
            });
            if (hasLabel) {
                buttonEl.setAttribute("xylabel", {
                    color: getParam("labelColor")
                });
            }
            return parentEl ? parentEl.appendChild(buttonEl) : buttonEl;
        }
    }
};

AFRAME.registerGeometry("xy-rounded-rect", {
    schema: {
        height: {
            default: 1,
            min: 0
        },
        width: {
            default: 1,
            min: 0
        },
        radius: {
            default: .05,
            min: 0
        }
    },
    init(data) {
        let shape = new THREE.Shape();
        let radius = data.radius;
        let w = (data.width || .01) / 2, h = (data.height || .01) / 2;
        shape.moveTo(-w, -h + radius);
        shape.lineTo(-w, h - radius);
        shape.quadraticCurveTo(-w, h, -w + radius, h);
        shape.lineTo(w - radius, h);
        shape.quadraticCurveTo(w, h, w, h - radius);
        shape.lineTo(w, -h + radius);
        shape.quadraticCurveTo(w, -h, w - radius, -h);
        shape.lineTo(-w + radius, -h);
        shape.quadraticCurveTo(-w, -h, -w, -h + radius);
        this.geometry = new THREE.ShapeGeometry(shape);
    }
});

AFRAME.registerComponent("xylabel", {
    dependencies: [ "xyrect" ],
    schema: {
        value: {
            default: ""
        },
        color: {
            default: "white"
        },
        align: {
            default: "left"
        },
        wrapCount: {
            default: 0
        },
        xOffset: {
            default: 0
        },
        zOffset: {
            default: .01
        },
        resolution: {
            default: 32
        },
        renderingMode: {
            default: "auto",
            oneOf: [ "auto", "canvas" ]
        }
    },
    init() {
        this.el.addEventListener("xyresize", ev => this.update());
    },
    update() {
        let data = this.data;
        let el = this.el;
        let value = data.value;
        let widthFactor = .65;
        let {width: w, height: h} = el.components.xyrect;
        let wrapCount = data.wrapCount;
        if (wrapCount == 0 && h > 0) {
            wrapCount = Math.max(w / h / widthFactor, value.length) + 1;
        }
        if (value == "") {
            this.remove();
            return;
        }
        if (data.renderingMode == "auto" && !/[\u0100-\uDFFF]/.test(value)) {
            let textData = Object.assign({}, data, {
                wrapCount: wrapCount,
                width: w,
                height: h
            });
            delete textData["resolution"];
            delete textData["renderingMode"];
            el.setAttribute("text", textData);
            setTimeout(() => {
                let textObj = el.getObject3D("text");
                if (textObj) {
                    textObj.raycast = (() => {});
                }
            }, 0);
            this._removeObject3d();
            return;
        }
        let lineHeight = data.resolution;
        let textWidth = Math.floor(lineHeight * wrapCount * widthFactor);
        let canvas = this._canvas || document.createElement("canvas");
        let font = "" + lineHeight * .9 + "px bold sans-serif";
        let ctx = canvas.getContext("2d");
        ctx.font = font;
        let lines = [ "" ], ln = 0;
        for (let char of value) {
            if (char == "\n" || ctx.measureText(lines[ln] + char).width > textWidth) {
                lines.push("");
                ln++;
            }
            if (char != "\n") {
                lines[ln] += char;
            }
        }
        let canvasHeight = lineHeight * lines.length;
        if (!this._canvas || this.textWidth != textWidth || canvas.height != canvasHeight) {
            let canvasWidth = 8;
            while (canvasWidth < textWidth) canvasWidth *= 2;
            this.remove();
            this._canvas = canvas;
            canvas.height = canvasHeight;
            canvas.width = canvasWidth;
            this._textWidth = textWidth;
            let texture = this._texture = new THREE.CanvasTexture(canvas);
            texture.anisotropy = 4;
            texture.repeat.x = textWidth / canvasWidth;
            let meshH = Math.min(w / textWidth * canvasHeight, h);
            let mesh = new THREE.Mesh(new THREE.PlaneGeometry(w, meshH), new THREE.MeshBasicMaterial({
                map: texture,
                transparent: true
            }));
            mesh.position.set(data.xOffset, 0, data.zOffset);
            mesh.raycast = (() => {});
            el.setObject3D("xylabel", mesh);
        }
        ctx.clearRect(0, 0, textWidth, canvasHeight);
        ctx.font = font;
        ctx.textBaseline = "top";
        ctx.textAlign = data.align;
        ctx.fillStyle = data.color;
        let x = data.align === "center" ? textWidth / 2 : 0;
        let y = lineHeight * .1;
        for (let line of lines) {
            ctx.fillText(line, x, y);
            y += lineHeight;
        }
        this._texture.needsUpdate = true;
    },
    remove() {
        this._removeObject3d();
        if (this.el.hasAttribute("text")) {
            this.el.removeAttribute("text");
        }
    },
    _removeObject3d() {
        let el = this.el;
        let labelObj = el.getObject3D("xylabel");
        if (labelObj) {
            labelObj.material.map.dispose();
            labelObj.material.dispose();
            labelObj.geometry.dispose();
            el.removeObject3D("xylabel");
            this._canvas = null;
        }
    },
    getPos(cursorPos) {
        let {text: text, xyrect: xyrect} = this.el.components;
        let s = this.data.value;
        let pos = 0;
        if (this._canvas) {
            let ctx = this._canvas.getContext("2d");
            pos = ctx.measureText(s.slice(0, cursorPos)).width / this._textWidth;
        } else if (text) {
            let textLayout = text.geometry.layout;
            let glyphs = textLayout.glyphs;
            let numGlyph = glyphs.length;
            let p = Math.max(0, numGlyph + cursorPos - s.length);
            let g = glyphs[Math.min(p, numGlyph - 1)];
            pos = g ? (g.position[0] + g.data.width * (p >= numGlyph ? 1 : .1)) / textLayout.width : 0;
        }
        return (pos - .5) * xyrect.width;
    }
});

AFRAME.registerComponent("xybutton", {
    dependencies: [ "xyrect" ],
    schema: {
        color: {
            default: ""
        },
        hoverColor: {
            default: ""
        },
        labelColor: {
            default: ""
        }
    },
    init() {
        let el = this.el;
        let xyrect = el.components.xyrect;
        XYTheme.get(el).createButton(xyrect.width, xyrect.height, null, this.data, true, el);
    }
});

AFRAME.registerComponent("xytoggle", {
    dependencies: [ "xyrect" ],
    schema: {
        value: {
            default: false
        }
    },
    events: {
        click(ev) {
            let el = this.el;
            el.value = !el.value;
            el.emit("change", {
                value: el.value
            }, false);
        }
    },
    init() {
        let el = this.el;
        Object.defineProperty(el, "value", {
            get: () => this.data.value,
            set: v => el.setAttribute("xytoggle", "value", v)
        });
        this._thumb = el.appendChild(document.createElement("a-circle"));
        el.addEventListener("xyresize", ev => this.update());
    },
    update() {
        let el = this.el;
        let xyrect = el.components.xyrect;
        let r = xyrect.height / 2;
        let v = el.value;
        let params = {
            color: v ? "#0066ff" : XYTheme.get(el).button.color,
            hoverColor: v ? "#4499ff" : ""
        };
        el.setAttribute("xybutton", params);
        el.setAttribute("material", "color", params.color);
        this._thumb.setAttribute("geometry", "radius", r * .8);
        this._thumb.object3D.position.set((xyrect.width / 2 - r) * (v ? 1 : -1), 0, .05);
        el.setAttribute("geometry", {
            primitive: "xy-rounded-rect",
            width: xyrect.width,
            height: r * 2,
            radius: r
        });
    }
});

AFRAME.registerComponent("xyselect", {
    dependencies: [ "xyrect", "xybutton" ],
    schema: {
        values: {
            default: []
        },
        label: {
            default: ""
        },
        toggle: {
            default: false
        },
        select: {
            default: 0
        }
    },
    events: {
        click(ev) {
            let data = this.data;
            if (data.toggle) {
                this.select((data.select + 1) % data.values.length);
            } else {
                this._listEl ? this.hide() : this.show();
            }
        }
    },
    init() {
        let el = this.el;
        if (this.data.toggle) {
            el.setAttribute("xylabel", "align", "center");
        } else {
            let marker = this._marker = el.appendChild(document.createElement("a-triangle"));
            marker.setAttribute("geometry", {
                vertexA: {
                    x: .1,
                    y: .03,
                    z: 0
                },
                vertexB: {
                    x: -.1,
                    y: .03,
                    z: 0
                },
                vertexC: {
                    x: 0,
                    y: -.12,
                    z: 0
                }
            });
            el.addEventListener("xyresize", ev => this.update());
        }
    },
    update() {
        let data = this.data, el = this.el;
        el.setAttribute("xylabel", {
            value: data.label || data.values[data.select]
        });
        if (this._marker) {
            this._marker.object3D.position.set(el.components.xyrect.width / 2 - .2, 0, .05);
        }
    },
    show() {
        if (this._listEl) return;
        let values = this.data.values;
        let height = this.el.components.xyrect.height;
        let listY = (height + values.length * height) / 2;
        let listEl = this._listEl = document.createElement("a-xycontainer");
        values.forEach((v, i) => {
            let itemEl = listEl.appendChild(document.createElement("a-xybutton"));
            itemEl.setAttribute("height", height);
            itemEl.setAttribute("label", v);
            itemEl.addEventListener("click", ev => {
                ev.stopPropagation();
                this.select(i);
                this.hide();
            });
        });
        listEl.object3D.position.set(0, listY, .1);
        this.el.appendChild(listEl);
    },
    select(idx) {
        this.el.setAttribute("xyselect", "select", idx);
        this.el.emit("change", {
            value: this.data.values[idx],
            index: idx
        }, false);
    },
    hide() {
        if (!this._listEl) return;
        this.el.removeChild(this._listEl);
        this._listEl.destroy();
        this._listEl = null;
    }
});

AFRAME.registerComponent("xydraggable", {
    schema: {
        dragThreshold: {
            default: .02
        },
        base: {
            type: "selector",
            default: null
        }
    },
    init() {
        let el = this.el;
        el.classList.add(XYTheme.get(el).collidableClass);
        this._onmousedown = this._onmousedown.bind(this);
        el.addEventListener("mousedown", this._onmousedown);
        this._dragFun = null;
    },
    remove() {
        this.el.removeEventListener("mousedown", this._onmousedown);
    },
    tick() {
        if (this._dragFun) {
            this._dragFun("xy-drag");
        } else {
            this.pause();
        }
    },
    _onmousedown(ev) {
        if (!ev.detail.cursorEl || !ev.detail.cursorEl.components.raycaster) {
            return;
        }
        let baseObj = (this.data.base || this.el).object3D;
        let cursorEl = ev.detail.cursorEl;
        let draggingRaycaster = cursorEl.components.raycaster.raycaster;
        let point = new THREE.Vector3();
        let startDirection = draggingRaycaster.ray.direction.clone();
        let dragPlane = new THREE.Plane(new THREE.Vector3(0, 0, -1), 0).applyMatrix4(baseObj.matrixWorld);
        let intersection = ev.detail.intersection;
        if (intersection) {
            dragPlane.setFromNormalAndCoplanarPoint(baseObj.getWorldDirection(point), intersection.point);
        }
        if (draggingRaycaster.ray.intersectPlane(dragPlane, point) === null) {
            baseObj.worldToLocal(point);
        }
        let prevRay = draggingRaycaster.ray.clone();
        let prevPoint = point.clone();
        let _this = this;
        let dragging = false;
        ev.stopPropagation();
        let dragFun = _this._dragFun = (event => {
            if (!dragging) {
                let d = startDirection.manhattanDistanceTo(draggingRaycaster.ray.direction);
                if (d < _this.data.dragThreshold) return;
                event = "xy-dragstart";
                _this.dragging = dragging = true;
            }
            prevPoint.copy(point);
            if (draggingRaycaster.ray.intersectPlane(dragPlane, point) !== null) {
                baseObj.worldToLocal(point);
            }
            _this.el.emit(event, {
                raycaster: draggingRaycaster,
                point: point,
                pointDelta: prevPoint.sub(point),
                prevRay: prevRay,
                cursorEl: cursorEl
            }, false);
            prevRay.copy(draggingRaycaster.ray);
        });
        _this.play();
        let cancelEvelt = ev1 => ev1.target != ev.target && ev1.stopPropagation();
        window.addEventListener("mouseenter", cancelEvelt, true);
        window.addEventListener("mouseleave", cancelEvelt, true);
        let mouseup = ev => {
            if (ev.detail.cursorEl != cursorEl) return;
            window.removeEventListener("mouseup", mouseup);
            window.removeEventListener("mouseenter", cancelEvelt, true);
            window.removeEventListener("mouseleave", cancelEvelt, true);
            _this._dragFun = null;
            if (dragging) {
                _this.dragging = false;
                let cancelClick = ev => ev.stopPropagation();
                window.addEventListener("click", cancelClick, true);
                setTimeout(() => window.removeEventListener("click", cancelClick, true), 0);
                dragFun("xy-dragend");
            }
        };
        window.addEventListener("mouseup", mouseup);
    }
});

AFRAME.registerComponent("xy-drag-control", {
    schema: {
        target: {
            type: "selector",
            default: null
        },
        draggable: {
            default: ""
        },
        autoRotate: {
            default: false
        }
    },
    init() {
        this._ondrag = this._ondrag.bind(this);
        this._draggable = [];
        this._prevQ = new THREE.Quaternion();
    },
    update(oldData) {
        let draggable = this.data.draggable;
        if (draggable !== oldData.draggable) {
            this.remove();
            this._draggable = Array.isArray(draggable) ? draggable : draggable ? this.el.querySelectorAll(draggable) : [ this.el ];
            this._draggable.forEach(el => {
                el.setAttribute("xydraggable", {});
                el.addEventListener("xy-dragstart", this._ondrag);
                el.addEventListener("xy-drag", this._ondrag);
            });
        }
    },
    remove() {
        this._draggable.forEach(el => {
            el.removeAttribute("xydraggable");
            el.removeEventListener("xy-dragstart", this._ondrag);
            el.removeEventListener("xy-drag", this._ondrag);
        });
    },
    _ondrag(ev) {
        let el = this.el;
        let data = this.data;
        let evDetail = ev.detail;
        let {origin: origin, direction: direction} = evDetail.raycaster.ray;
        let {origin: origin0, direction: direction0} = evDetail.prevRay;
        let cursorEl = evDetail.cursorEl;
        let targetObj = (data.target || el).object3D;
        let rot = new THREE.Quaternion();
        if (cursorEl.components["tracked-controls"]) {
            if (ev.type != "xy-dragstart") {
                rot.copy(this._prevQ).invert().premultiply(cursorEl.object3D.getWorldQuaternion(this._prevQ));
            } else {
                cursorEl.object3D.getWorldQuaternion(this._prevQ);
            }
        } else {
            rot.setFromUnitVectors(direction0, direction);
        }
        let pm = targetObj.parent.matrixWorld;
        let tr = new THREE.Matrix4();
        let mat = new THREE.Matrix4().makeRotationFromQuaternion(rot).multiply(tr.setPosition(origin0.clone().negate())).premultiply(tr.setPosition(origin)).premultiply(pm.clone().invert()).multiply(pm);
        targetObj.applyMatrix4 ? targetObj.applyMatrix4(mat) : targetObj.applyMatrix(mat);
        if (this.postProcess) {
            this.postProcess(targetObj, ev);
        }
        if (data.autoRotate) {
            let targetPosition = targetObj.getWorldPosition(new THREE.Vector3());
            let d = origin.clone().sub(targetPosition).normalize();
            let t = .8 - d.y * d.y;
            if (t > 0) {
                let intersection = cursorEl.components.raycaster.getIntersection(ev.target);
                let intersectPoint = intersection ? intersection.point : targetPosition;
                let c = targetObj.parent.worldToLocal(intersectPoint);
                let tq = targetObj.quaternion.clone();
                mat.lookAt(origin, targetPosition, new THREE.Vector3(0, 1, 0));
                targetObj.quaternion.slerp(rot.setFromRotationMatrix(mat.premultiply(pm.clone().invert())), t * .1);
                targetObj.position.sub(c).applyQuaternion(tq.invert().premultiply(targetObj.quaternion)).add(c);
            }
        }
    }
});

AFRAME.registerComponent("xywindow", {
    schema: {
        title: {
            default: ""
        },
        closable: {
            default: true
        },
        background: {
            default: false
        }
    },
    init() {
        let el = this.el;
        let theme = XYTheme.get(el);
        let windowStyle = theme.window;
        let controls = this.controls = el.appendChild(document.createElement("a-entity"));
        let controlsObj = controls.object3D;
        controls.setAttribute("xyitem", {
            fixed: true
        });
        controlsObj.position.set(0, 0, .02);
        if (this.data.background) {
            let background = this._background = controls.appendChild(document.createElement("a-plane"));
            background.setAttribute("material", {
                color: windowStyle.background.color,
                side: "double",
                transparent: true,
                opacity: .8
            });
            background.object3D.position.set(0, .25, -.04);
            el.addEventListener("object3dset", ev => {
                let children = el.object3D.children;
                let i = children.indexOf(controlsObj);
                if (i > 0) {
                    children.unshift(...children.splice(i, 1));
                }
            });
        }
        let titleBar = this._titleBar = theme.createButton(1, .5, controls, windowStyle.titleBar, true);
        titleBar.setAttribute("xy-drag-control", {
            target: el,
            autoRotate: true
        });
        this._buttons = [];
        if (this.data.closable) {
            let closeButton = theme.createButton(.5, .5, controls, windowStyle.closeButton, true);
            closeButton.setAttribute("xylabel", {
                value: "X",
                align: "center"
            });
            closeButton.addEventListener("click", ev => el.parentNode.removeChild(el));
            this._buttons.push(closeButton);
        }
        el.addEventListener("xyresize", ev => {
            this.update({});
        });
    },
    update(oldData) {
        let el = this.el;
        let data = this.data;
        let {width: width, height: height} = el.components.xyrect;
        let titleBar = this._titleBar;
        let background = this._background;
        let buttonsWidth = 0;
        let tiyleY = height / 2 + .3;
        for (let b of this._buttons) {
            b.object3D.position.set(width / 2 - .25 - buttonsWidth, tiyleY, 0);
            buttonsWidth += .52;
        }
        if (data.title != oldData.title) {
            let titleW = width - buttonsWidth - .1;
            titleBar.setAttribute("xyrect", {
                width: titleW,
                height: .45
            });
            titleBar.setAttribute("xylabel", {
                value: data.title,
                wrapCount: Math.max(10, titleW / .2),
                xOffset: .1
            });
        }
        titleBar.setAttribute("geometry", {
            width: width - buttonsWidth
        });
        titleBar.object3D.position.set(-buttonsWidth / 2, tiyleY, 0);
        if (background) {
            background.object3D.scale.set(width + .1, height + .7, 1);
        }
    }
});

AFRAME.registerComponent("xyrange", {
    dependencies: [ "xyrect" ],
    schema: {
        min: {
            default: 0
        },
        max: {
            default: 100
        },
        step: {
            default: 0
        },
        value: {
            default: 0
        },
        color0: {
            default: "white"
        },
        color1: {
            default: "#06f"
        },
        thumbSize: {
            default: .4
        },
        barHeight: {
            default: .08
        }
    },
    init() {
        let data = this.data;
        let el = this.el;
        let thumb = this._thumb = XYTheme.get(el).createButton(0, 0, el, {
            geometry: "circle"
        });
        let plane = new THREE.PlaneGeometry(1, 1);
        let bar = this._bar = new THREE.Mesh(plane);
        let prog = this._prog = new THREE.Mesh(plane);
        el.setObject3D("xyrange", new THREE.Group().add(bar, prog));
        thumb.setAttribute("xydraggable", {
            base: el,
            dragThreshold: 0
        });
        thumb.addEventListener("xy-drag", ev => {
            let r = el.components.xyrect.width - data.thumbSize;
            let p = (ev.detail.point.x + r / 2) / r * (data.max - data.min);
            if (data.step > 0) {
                p = Math.round(p / data.step) * data.step;
            }
            this.setValue(p + data.min, true);
        });
        Object.defineProperty(el, "value", {
            get: () => data.value,
            set: v => this.setValue(v, false)
        });
    },
    update() {
        let data = this.data;
        let barHeight = data.barHeight;
        let barWidth = this.el.components.xyrect.width - data.thumbSize;
        let len = data.max - data.min;
        let pos = len > 0 ? barWidth * (data.value - data.min) / len : 0;
        let prog = this._prog, bar = this._bar, thumb = this._thumb;
        bar.scale.set(barWidth, barHeight, 1);
        bar.material.color = new THREE.Color(data.color0);
        prog.scale.set(pos, barHeight, 1);
        prog.position.set((pos - barWidth) / 2, 0, .02);
        prog.material.color = new THREE.Color(data.color1);
        thumb.setAttribute("geometry", "radius", data.thumbSize / 2);
        thumb.object3D.position.set(pos - barWidth / 2, 0, .04);
    },
    setValue(value, emitEvent) {
        if (!this._thumb.components.xydraggable.dragging || emitEvent) {
            let data = this.data;
            let v = Math.max(Math.min(value, data.max), data.min);
            if (v != data.value && emitEvent) {
                this.el.emit("change", {
                    value: v
                }, false);
            }
            this.el.setAttribute("xyrange", "value", v);
        }
    }
});

AFRAME.registerComponent("xyclipping", {
    dependencies: [ "xyrect" ],
    schema: {
        exclude: {
            type: "selector",
            default: null
        },
        clipTop: {
            default: true
        },
        clipBottom: {
            default: true
        },
        clipLeft: {
            default: false
        },
        clipRight: {
            default: false
        }
    },
    init() {
        this.el.sceneEl.renderer.localClippingEnabled = true;
        this._clippingPlanesLocal = [];
        this._clippingPlanes = [];
        this._currentMatrix = null;
        this._raycastOverrides = {};
        this.update = this.update.bind(this);
        this.el.addEventListener("xyresize", this.update);
    },
    update() {
        let data = this.data;
        let rect = this.el.components.xyrect;
        let planes = this._clippingPlanesLocal = [];
        if (data.clipBottom) planes.push(new THREE.Plane(new THREE.Vector3(0, 1, 0), 0));
        if (data.clipTop) planes.push(new THREE.Plane(new THREE.Vector3(0, -1, 0), rect.height));
        if (data.clipLeft) planes.push(new THREE.Plane(new THREE.Vector3(1, 0, 0), 0));
        if (data.clipRight) planes.push(new THREE.Plane(new THREE.Vector3(-1, 0, 0), rect.width));
        this._clippingPlanes = planes.map(p => p.clone());
        this._updateMatrix();
    },
    remove() {
        this.el.removeEventListener("xyresize", this.update);
        this._clippingPlanes.splice(0);
        for (let [obj, raycast] of Object.values(this._raycastOverrides)) {
            obj.raycast = raycast;
        }
    },
    tick() {
        if (!this.el.object3D.matrixWorld.equals(this._currentMatrix)) {
            this._updateMatrix();
        }
    },
    _updateMatrix() {
        this._currentMatrix = this.el.object3D.matrixWorld.clone();
        this._clippingPlanesLocal.forEach((plane, i) => {
            this._clippingPlanes[i].copy(plane).applyMatrix4(this._currentMatrix);
        });
        this.applyClippings();
    },
    applyClippings() {
        let excludeObj = this.data.exclude && this.data.exclude.object3D;
        let setCliping = obj => {
            if (obj === excludeObj) return;
            if (obj.material && obj.material.clippingPlanes !== undefined) {
                obj.material.clippingPlanes = this._clippingPlanes;
                if (!this._raycastOverrides[obj.uuid]) {
                    let raycastFn = obj.raycast;
                    this._raycastOverrides[obj.uuid] = [ obj, raycastFn ];
                    obj.raycast = ((r, intersects) => {
                        let len = intersects.length;
                        raycastFn.apply(obj, [ r, intersects ]);
                        let added = intersects[len];
                        if (added && this._clippingPlanes.some(plane => plane.distanceToPoint(added.point) < 0)) {
                            intersects.pop();
                        }
                    });
                }
            }
            for (let child of obj.children) {
                setCliping(child);
            }
        };
        setCliping(this.el.object3D);
    }
});

AFRAME.registerComponent("xyscroll", {
    dependencies: [ "xyrect" ],
    schema: {
        scrollbar: {
            default: true
        }
    },
    init() {
        this._scrollX = this._scrollY = this._speedY = 0;
        this._contentHeight = 0;
        this._thumbLen = 0;
        let el = this.el;
        let scrollBar = this._scrollBar = this._initScrollBar(el, .3);
        el.setAttribute("xyclipping", {
            exclude: scrollBar
        });
        el.setAttribute("xydraggable", {});
        el.addEventListener("xy-drag", ev => {
            let d = ev.detail.pointDelta;
            this._speedY = -d.y;
            this._scrollOffset(d.x, -d.y);
        });
        el.addEventListener("xy-dragstart", ev => this.pause());
        el.addEventListener("xy-dragend", ev => this.play());
        el.addEventListener("xyresize", ev => this.update());
        let item = this._getContentEl();
        if (item) {
            item.addEventListener("xyresize", ev => this.update());
        }
    },
    _getContentEl() {
        for (let item of this.el.children) {
            if (item === this._scrollBar || (item.getAttribute("xyitem") || {}).fixed) {
                continue;
            }
            return item;
        }
    },
    _initScrollBar(el, w) {
        let theme = XYTheme.get(el);
        let scrollBar = el.appendChild(document.createElement("a-entity"));
        this._upButton = theme.createButton(w, w, scrollBar);
        this._upButton.addEventListener("click", ev => {
            this._speedY = -this._scrollDelta;
            this.play();
        });
        this._downButton = theme.createButton(w, w, scrollBar);
        this._downButton.addEventListener("click", ev => {
            this._speedY = this._scrollDelta;
            this.play();
        });
        this._scrollThumb = theme.createButton(w * .7, 1, scrollBar);
        this._scrollThumb.setAttribute("xydraggable", {
            base: scrollBar
        });
        this._scrollThumb.addEventListener("xy-drag", ev => {
            let xyrect = this.el.components.xyrect;
            let dy = ev.detail.pointDelta.y * (this._contentHeight - xyrect.height) / (this._scrollLength - this._thumbLen || 1);
            this._scrollOffset(0, dy);
        });
        return scrollBar;
    },
    update() {
        let xyrect = this.el.components.xyrect;
        let scrollBarHeight = xyrect.height;
        this._scrollBar.setAttribute("visible", this.data.scrollbar);
        this._scrollBar.setAttribute("position", {
            x: xyrect.width + .1,
            y: 0,
            z: .05
        });
        this._upButton.setAttribute("position", {
            x: 0,
            y: scrollBarHeight - .15,
            z: 0
        });
        this._downButton.setAttribute("position", {
            x: 0,
            y: .15,
            z: 0
        });
        this._scrollDelta = Math.max(scrollBarHeight / 2, .5) * .3;
        this._scrollStart = scrollBarHeight - .3;
        this._scrollLength = scrollBarHeight - .6;
        this._scrollOffset(0, 0);
    },
    tick() {
        if (Math.abs(this._speedY) > .001) {
            this._speedY *= .8;
            this._scrollOffset(0, this._speedY);
        } else {
            this.pause();
        }
    },
    _scrollOffset(dx, dy) {
        this.setScroll(this._scrollX + dx, this._scrollY + dy);
    },
    setScroll(x, y) {
        let item = this._getContentEl();
        if (!item) {
            return;
        }
        let el = this.el;
        let {width: scrollWidth, height: scrollHeight} = el.components.xyrect;
        let itemRect = item.components.xyrect;
        let contentHeight = itemRect.height;
        let contentWidth = itemRect.width;
        if (!item.components.xyrec) {
            item.setAttribute("xyrect", {});
        }
        this._scrollX = Math.max(0, Math.min(x, contentWidth - scrollWidth));
        this._scrollY = Math.max(0, Math.min(y, contentHeight - scrollHeight));
        this._contentHeight = contentHeight;
        let thumbLen = this._thumbLen = Math.max(.2, Math.min(this._scrollLength * scrollHeight / contentHeight, this._scrollLength));
        let thumbY = this._scrollStart - thumbLen / 2 - (this._scrollLength - thumbLen) * this._scrollY / (contentHeight - scrollHeight || 1);
        this._scrollThumb.setAttribute("geometry", "height", thumbLen);
        this._scrollThumb.setAttribute("position", "y", thumbY);
        let itemPivot = itemRect.data.pivot;
        let vx = itemPivot.x * itemRect.width - this._scrollX;
        let vy = (1 - itemPivot.y) * itemRect.height - this._scrollY;
        item.setAttribute("position", {
            x: vx,
            y: scrollHeight - vy
        });
        item.emit("xyviewport", [ vy, vy - scrollHeight, -vx, scrollWidth - vx ], false);
        let clippling = el.components.xyclipping;
        if (clippling) {
            clippling.applyClippings();
        }
    }
});

AFRAME.registerComponent("xylist", {
    dependencies: [ "xyrect" ],
    schema: {
        itemWidth: {
            default: -1
        },
        itemHeight: {
            default: -1
        }
    },
    events: {
        click(ev) {
            for (let p of ev.path || ev.composedPath()) {
                let index = p.dataset.listPosition;
                if (index != null && index >= 0) {
                    this.el.emit("clickitem", {
                        index: index,
                        ev: ev
                    }, false);
                    break;
                }
            }
        }
    },
    init() {
        let el = this.el;
        let data = this.data;
        this._adapter = null;
        this._elements = {};
        this._cache = [];
        this._userData = null;
        this._itemCount = 0;
        this._layout = {
            size(itemCount, list) {
                if (data.itemHeight <= 0) {
                    let el = list._adapter.create();
                    data.itemHeight = +el.getAttribute("height");
                    data.itemWidth = +el.getAttribute("width");
                }
                return {
                    width: data.itemWidth,
                    height: data.itemHeight * itemCount
                };
            },
            * targets(viewport) {
                let itemHeight = data.itemHeight;
                let position = Math.floor(-viewport[0] / itemHeight);
                let end = Math.ceil(-viewport[1] / itemHeight);
                while (position < end) {
                    yield position++;
                }
            },
            layout(el, position) {
                let x = 0, y = -position * data.itemHeight;
                let xyrect = el.components.xyrect;
                let pivot = xyrect ? xyrect.data.pivot : {
                    x: .5,
                    y: .5
                };
                el.setAttribute("position", {
                    x: x + pivot.x * xyrect.width,
                    y: y - pivot.y * xyrect.height,
                    z: 0
                });
            }
        };
        el.setAttribute("xyrect", "pivot", {
            x: 0,
            y: 1
        });
        el.addEventListener("xyviewport", ev => this.setViewport(ev.detail));
        this.setViewport([ 0, 0 ]);
    },
    setLayout(layout) {
        this._layout = layout;
    },
    setAdapter(adapter) {
        this._adapter = adapter;
    },
    setContents(data, count, invalidateView = true) {
        if (invalidateView) {
            for (let el of Object.values(this._elements)) {
                el.dataset.listPosition = -1;
                el.setAttribute("position", "y", 999);
            }
        }
        this._userData = data;
        count = count != null ? count : data.length;
        this._itemCount = count;
        this.el.setAttribute("xyrect", this._layout.size(count, this));
        this._refresh();
    },
    setViewport(vp) {
        this._viewport = vp;
        this._refresh();
    },
    _refresh() {
        let el = this.el;
        let adapter = this._adapter, layout = this._layout, elements = this._elements;
        let visibleItems = {};
        if (!adapter) return;
        for (let position of layout.targets(this._viewport)) {
            if (position >= 0 && position < this._itemCount) {
                let itemEl = elements[position];
                if (!itemEl) {
                    itemEl = elements[position] = this._cache.pop() || el.appendChild(adapter.create(el));
                    itemEl.classList.add(XYTheme.get(el).collidableClass);
                }
                visibleItems[position] = itemEl;
                let dataset = itemEl.dataset;
                if (dataset.listPosition != position) {
                    dataset.listPosition = position;
                    let update = () => {
                        if (dataset.listPosition == position) {
                            layout.layout(itemEl, position);
                            adapter.bind(position, itemEl, this._userData);
                        }
                    };
                    if (itemEl.hasLoaded) {
                        update();
                    } else {
                        itemEl.addEventListener("loaded", update, {
                            once: true
                        });
                    }
                }
            }
        }
        for (let [position, el] of Object.entries(elements)) {
            el.setAttribute("visible", visibleItems[position] != null);
            if (!visibleItems[position]) {
                this._cache.push(el);
            }
        }
        this._elements = visibleItems;
    }
});

AFRAME.registerPrimitive("a-xylabel", {
    defaultComponents: {
        xyrect: {},
        xylabel: {}
    },
    mappings: {
        width: "xyrect.width",
        height: "xyrect.height",
        value: "xylabel.value",
        color: "xylabel.color",
        align: "xylabel.align",
        "wrap-count": "xylabel.wrapCount"
    }
});

AFRAME.registerPrimitive("a-xybutton", {
    defaultComponents: {
        xyrect: {
            width: 2,
            height: .5
        },
        xylabel: {
            align: "center"
        },
        xybutton: {}
    },
    mappings: {
        width: "xyrect.width",
        height: "xyrect.height",
        label: "xylabel.value",
        align: "xylabel.align",
        color: "xybutton.color",
        "hover-color": "xybutton.hoverColor",
        "label-color": "xybutton.labelColor"
    }
});

AFRAME.registerPrimitive("a-xytoggle", {
    defaultComponents: {
        xyrect: {
            width: .8,
            height: .4
        },
        xytoggle: {}
    },
    mappings: {
        width: "xyrect.width",
        height: "xyrect.height",
        value: "xytoggle.value"
    }
});

AFRAME.registerPrimitive("a-xyselect", {
    defaultComponents: {
        xyrect: {
            width: 2,
            height: .5
        },
        xyselect: {}
    },
    mappings: {
        width: "xyrect.width",
        height: "xyrect.height",
        values: "xyselect.values",
        label: "xyselect.label",
        toggle: "xyselect.toggle",
        select: "xyselect.select",
        color: "xybutton.color",
        "hover-color": "xybutton.hoverColor",
        "label-color": "xybutton.labelColor"
    }
});

AFRAME.registerPrimitive("a-xywindow", {
    defaultComponents: {
        xycontainer: {
            alignItems: "center"
        },
        xywindow: {}
    },
    mappings: {
        width: "xyrect.width",
        height: "xyrect.height",
        title: "xywindow.title"
    }
});

AFRAME.registerPrimitive("a-xyscroll", {
    defaultComponents: {
        xyrect: {
            pivot: {
                x: 0,
                y: 1
            }
        },
        xyscroll: {}
    },
    mappings: {
        width: "xyrect.width",
        height: "xyrect.height",
        scrollbar: "xyscroll.scrollbar"
    }
});

AFRAME.registerPrimitive("a-xyrange", {
    defaultComponents: {
        xyrect: {},
        xyrange: {}
    },
    mappings: {
        width: "xyrect.width",
        height: "xyrect.height",
        min: "xyrange.min",
        max: "xyrange.max",
        step: "xyrange.step",
        value: "xyrange.value",
        "bar-height": "xyrange.barHeight"
    }
});AFRAME.registerComponent('xylist-grid-layout', {
    dependencies: ['xyrect', 'xylist'],
    schema: {
        itemWidth: { default: 1 },
        itemHeight: { default: 1 },
        stretch: { default: true },
    },
    init() {
        this.update = this.update.bind(this);
        this.el.addEventListener('xyresize', this.update);
    },
    remove() {
        this.el.removeEventListener('xyresize', this.update);
    },
    update() {
        let el = this.el, data = this.data;
        let { xylist, xyrect } = el.components;
        let containerWidth = xyrect.width;
        if (containerWidth <= 0) {
            containerWidth = el.parentElement.getAttribute("width");
        }

        let itemWidth = data.itemWidth;
        let itemHeight = data.itemHeight;
        let cols = Math.max(containerWidth / itemWidth | 0, 1);
        if (data.stretch) {
            itemWidth = containerWidth / cols;
            itemHeight *= itemWidth / data.itemWidth;
        }

        xylist.setLayout({
            size(itemCount) {
                return { width: itemWidth * cols, height: itemHeight * Math.ceil(itemCount / cols) };
            },
            *targets(viewport) {
                let position = Math.floor((-viewport[0]) / itemHeight) * cols;
                let end = Math.ceil((-viewport[1]) / itemHeight) * cols;
                while (position < end) {
                    yield position++;
                }
            },
            layout(el, position) {
                el.setAttribute("xyrect", { width: itemWidth, height: itemHeight });
                let x = (position % cols) * itemWidth, y = - (position / cols | 0) * itemHeight;
                let pivot = el.components.xyrect.data.pivot;
                el.setAttribute("position", { x: x + pivot.x * itemWidth, y: y - pivot.y * itemHeight, z: 0 });
            }
        });
    }
});



AFRAME.registerComponent('xylist-spriral-layout', {
    dependencies: ['xyrect', 'xylist'],
    schema: {
        itemWidth: { default: 1 },
        itemHeight: { default: 1 },
        radius: { default: 5 },
        stretch: { default: true },
    },
    init() {
        this.update = this.update.bind(this);
        this.el.addEventListener('xyresize', this.update);
    },
    remove() {
        this.el.removeEventListener('xyresize', this.update);
    },
    update() {
        let el = this.el, data = this.data;
        let { xylist } = el.components;
        let radius = this.data.radius;
        let containerWidth = this.data.radius * Math.PI * 2;

        let itemWidth = data.itemWidth;
        let itemHeight = data.itemHeight;
        let cols = Math.max(containerWidth / itemWidth | 0, 1);
        if (data.stretch) {
            itemWidth = containerWidth / cols;
            itemHeight *= itemWidth / data.itemWidth;
        }

        xylist.setLayout({
            size(itemCount) {
                return { width: 0.01, height: itemHeight * Math.ceil(itemCount / cols) };
            },
            *targets(viewport) {
                let position = Math.floor((-viewport[0]) / itemHeight - 1) * cols;
                let end = Math.ceil((-viewport[1]) / itemHeight) * cols;
                while (position < end) {
                    yield position++;
                }
            },
            layout(el, position) {
                el.setAttribute("xyrect", { width: itemWidth, height: itemHeight });
                let t = (position % cols) / cols * Math.PI * 2, y = - (position / cols) * itemHeight;
                let x = Math.sin(t) * radius;
                let z = Math.cos(t) * radius;
                let xyrect = el.components.xyrect;
                let pivot = xyrect.data.pivot;
                el.setAttribute("position", { x: x, y: y - pivot.y * itemHeight, z: z });
                el.setAttribute("rotation", { x: 0, y: t * 180 / Math.PI, z: 0 });
            }
        });
    }
});
(()=>{var We=!1,Ge=!1,B=[];function $t(e){an(e)}function an(e){B.includes(e)||B.push(e),cn()}function he(e){let t=B.indexOf(e);t!==-1&&B.splice(t,1)}function cn(){!Ge&&!We&&(We=!0,queueMicrotask(ln))}function ln(){We=!1,Ge=!0;for(let e=0;e<B.length;e++)B[e]();B.length=0,Ge=!1}var A,K,Y,Ye,Je=!0;function Lt(e){Je=!1,e(),Je=!0}function jt(e){A=e.reactive,Y=e.release,K=t=>e.effect(t,{scheduler:r=>{Je?$t(r):r()}}),Ye=e.raw}function Ze(e){K=e}function Ft(e){let t=()=>{};return[n=>{let i=K(n);return e._x_effects||(e._x_effects=new Set,e._x_runEffects=()=>{e._x_effects.forEach(o=>o())}),e._x_effects.add(i),t=()=>{i!==void 0&&(e._x_effects.delete(i),Y(i))},i},()=>{t()}]}var Bt=[],Kt=[],zt=[];function Vt(e){zt.push(e)}function _e(e,t){typeof t=="function"?(e._x_cleanups||(e._x_cleanups=[]),e._x_cleanups.push(t)):(t=e,Kt.push(t))}function Ht(e){Bt.push(e)}function qt(e,t,r){e._x_attributeCleanups||(e._x_attributeCleanups={}),e._x_attributeCleanups[t]||(e._x_attributeCleanups[t]=[]),e._x_attributeCleanups[t].push(r)}function Qe(e,t){!e._x_attributeCleanups||Object.entries(e._x_attributeCleanups).forEach(([r,n])=>{(t===void 0||t.includes(r))&&(n.forEach(i=>i()),delete e._x_attributeCleanups[r])})}var et=new MutationObserver(Xe),tt=!1;function rt(){et.observe(document,{subtree:!0,childList:!0,attributes:!0,attributeOldValue:!0}),tt=!0}function fn(){un(),et.disconnect(),tt=!1}var te=[],nt=!1;function un(){te=te.concat(et.takeRecords()),te.length&&!nt&&(nt=!0,queueMicrotask(()=>{dn(),nt=!1}))}function dn(){Xe(te),te.length=0}function m(e){if(!tt)return e();fn();let t=e();return rt(),t}var it=!1,ge=[];function Ut(){it=!0}function Wt(){it=!1,Xe(ge),ge=[]}function Xe(e){if(it){ge=ge.concat(e);return}let t=[],r=[],n=new Map,i=new Map;for(let o=0;o<e.length;o++)if(!e[o].target._x_ignoreMutationObserver&&(e[o].type==="childList"&&(e[o].addedNodes.forEach(s=>s.nodeType===1&&t.push(s)),e[o].removedNodes.forEach(s=>s.nodeType===1&&r.push(s))),e[o].type==="attributes")){let s=e[o].target,a=e[o].attributeName,c=e[o].oldValue,l=()=>{n.has(s)||n.set(s,[]),n.get(s).push({name:a,value:s.getAttribute(a)})},u=()=>{i.has(s)||i.set(s,[]),i.get(s).push(a)};s.hasAttribute(a)&&c===null?l():s.hasAttribute(a)?(u(),l()):u()}i.forEach((o,s)=>{Qe(s,o)}),n.forEach((o,s)=>{Bt.forEach(a=>a(s,o))});for(let o of r)if(!t.includes(o)&&(Kt.forEach(s=>s(o)),o._x_cleanups))for(;o._x_cleanups.length;)o._x_cleanups.pop()();t.forEach(o=>{o._x_ignoreSelf=!0,o._x_ignore=!0});for(let o of t)r.includes(o)||!o.isConnected||(delete o._x_ignoreSelf,delete o._x_ignore,zt.forEach(s=>s(o)),o._x_ignore=!0,o._x_ignoreSelf=!0);t.forEach(o=>{delete o._x_ignoreSelf,delete o._x_ignore}),t=null,r=null,n=null,i=null}function xe(e){return D(k(e))}function C(e,t,r){return e._x_dataStack=[t,...k(r||e)],()=>{e._x_dataStack=e._x_dataStack.filter(n=>n!==t)}}function ot(e,t){let r=e._x_dataStack[0];Object.entries(t).forEach(([n,i])=>{r[n]=i})}function k(e){return e._x_dataStack?e._x_dataStack:typeof ShadowRoot=="function"&&e instanceof ShadowRoot?k(e.host):e.parentNode?k(e.parentNode):[]}function D(e){let t=new Proxy({},{ownKeys:()=>Array.from(new Set(e.flatMap(r=>Object.keys(r)))),has:(r,n)=>e.some(i=>i.hasOwnProperty(n)),get:(r,n)=>(e.find(i=>{if(i.hasOwnProperty(n)){let o=Object.getOwnPropertyDescriptor(i,n);if(o.get&&o.get._x_alreadyBound||o.set&&o.set._x_alreadyBound)return!0;if((o.get||o.set)&&o.enumerable){let s=o.get,a=o.set,c=o;s=s&&s.bind(t),a=a&&a.bind(t),s&&(s._x_alreadyBound=!0),a&&(a._x_alreadyBound=!0),Object.defineProperty(i,n,{...c,get:s,set:a})}return!0}return!1})||{})[n],set:(r,n,i)=>{let o=e.find(s=>s.hasOwnProperty(n));return o?o[n]=i:e[e.length-1][n]=i,!0}});return t}function ye(e){let t=n=>typeof n=="object"&&!Array.isArray(n)&&n!==null,r=(n,i="")=>{Object.entries(Object.getOwnPropertyDescriptors(n)).forEach(([o,{value:s,enumerable:a}])=>{if(a===!1||s===void 0)return;let c=i===""?o:`${i}.${o}`;typeof s=="object"&&s!==null&&s._x_interceptor?n[o]=s.initialize(e,c,o):t(s)&&s!==n&&!(s instanceof Element)&&r(s,c)})};return r(e)}function be(e,t=()=>{}){let r={initialValue:void 0,_x_interceptor:!0,initialize(n,i,o){return e(this.initialValue,()=>pn(n,i),s=>st(n,i,s),i,o)}};return t(r),n=>{if(typeof n=="object"&&n!==null&&n._x_interceptor){let i=r.initialize.bind(r);r.initialize=(o,s,a)=>{let c=n.initialize(o,s,a);return r.initialValue=c,i(o,s,a)}}else r.initialValue=n;return r}}function pn(e,t){return t.split(".").reduce((r,n)=>r[n],e)}function st(e,t,r){if(typeof t=="string"&&(t=t.split(".")),t.length===1)e[t[0]]=r;else{if(t.length===0)throw error;return e[t[0]]||(e[t[0]]={}),st(e[t[0]],t.slice(1),r)}}var Gt={};function x(e,t){Gt[e]=t}function re(e,t){return Object.entries(Gt).forEach(([r,n])=>{Object.defineProperty(e,`$${r}`,{get(){let[i,o]=at(t);return i={interceptor:be,...i},_e(t,o),n(t,i)},enumerable:!1})}),e}function Yt(e,t,r,...n){try{return r(...n)}catch(i){J(i,e,t)}}function J(e,t,r=void 0){Object.assign(e,{el:t,expression:r}),console.warn(`Alpine Expression Error: ${e.message}

${r?'Expression: "'+r+`"

`:""}`,t),setTimeout(()=>{throw e},0)}var ve=!0;function Jt(e){let t=ve;ve=!1,e(),ve=t}function P(e,t,r={}){let n;return g(e,t)(i=>n=i,r),n}function g(...e){return Zt(...e)}var Zt=ct;function Qt(e){Zt=e}function ct(e,t){let r={};re(r,e);let n=[r,...k(e)];if(typeof t=="function")return mn(n,t);let i=hn(n,t,e);return Yt.bind(null,e,t,i)}function mn(e,t){return(r=()=>{},{scope:n={},params:i=[]}={})=>{let o=t.apply(D([n,...e]),i);we(r,o)}}var lt={};function _n(e,t){if(lt[e])return lt[e];let r=Object.getPrototypeOf(async function(){}).constructor,n=/^[\n\s]*if.*\(.*\)/.test(e)||/^(let|const)\s/.test(e)?`(() => { ${e} })()`:e,o=(()=>{try{return new r(["__self","scope"],`with (scope) { __self.result = ${n} }; __self.finished = true; return __self.result;`)}catch(s){return J(s,t,e),Promise.resolve()}})();return lt[e]=o,o}function hn(e,t,r){let n=_n(t,r);return(i=()=>{},{scope:o={},params:s=[]}={})=>{n.result=void 0,n.finished=!1;let a=D([o,...e]);if(typeof n=="function"){let c=n(n,a).catch(l=>J(l,r,t));n.finished?(we(i,n.result,a,s,r),n.result=void 0):c.then(l=>{we(i,l,a,s,r)}).catch(l=>J(l,r,t)).finally(()=>n.result=void 0)}}}function we(e,t,r,n,i){if(ve&&typeof t=="function"){let o=t.apply(r,n);o instanceof Promise?o.then(s=>we(e,s,r,n)).catch(s=>J(s,i,t)):e(o)}else e(t)}var ut="x-";function E(e=""){return ut+e}function Xt(e){ut=e}var er={};function d(e,t){er[e]=t}function ne(e,t,r){if(t=Array.from(t),e._x_virtualDirectives){let o=Object.entries(e._x_virtualDirectives).map(([a,c])=>({name:a,value:c})),s=ft(o);o=o.map(a=>s.find(c=>c.name===a.name)?{name:`x-bind:${a.name}`,value:`"${a.value}"`}:a),t=t.concat(o)}let n={};return t.map(tr((o,s)=>n[o]=s)).filter(rr).map(xn(n,r)).sort(yn).map(o=>gn(e,o))}function ft(e){return Array.from(e).map(tr()).filter(t=>!rr(t))}var dt=!1,ie=new Map,nr=Symbol();function ir(e){dt=!0;let t=Symbol();nr=t,ie.set(t,[]);let r=()=>{for(;ie.get(t).length;)ie.get(t).shift()();ie.delete(t)},n=()=>{dt=!1,r()};e(r),n()}function at(e){let t=[],r=a=>t.push(a),[n,i]=Ft(e);return t.push(i),[{Alpine:I,effect:n,cleanup:r,evaluateLater:g.bind(g,e),evaluate:P.bind(P,e)},()=>t.forEach(a=>a())]}function gn(e,t){let r=()=>{},n=er[t.type]||r,[i,o]=at(e);qt(e,t.original,o);let s=()=>{e._x_ignore||e._x_ignoreSelf||(n.inline&&n.inline(e,t,i),n=n.bind(n,e,t,i),dt?ie.get(nr).push(n):n())};return s.runCleanups=o,s}var Ee=(e,t)=>({name:r,value:n})=>(r.startsWith(e)&&(r=r.replace(e,t)),{name:r,value:n}),Se=e=>e;function tr(e=()=>{}){return({name:t,value:r})=>{let{name:n,value:i}=or.reduce((o,s)=>s(o),{name:t,value:r});return n!==t&&e(n,t),{name:n,value:i}}}var or=[];function Z(e){or.push(e)}function rr({name:e}){return sr().test(e)}var sr=()=>new RegExp(`^${ut}([^:^.]+)\\b`);function xn(e,t){return({name:r,value:n})=>{let i=r.match(sr()),o=r.match(/:([a-zA-Z0-9\-:]+)/),s=r.match(/\.[^.\]]+(?=[^\]]*$)/g)||[],a=t||e[r]||r;return{type:i?i[1]:null,value:o?o[1]:null,modifiers:s.map(c=>c.replace(".","")),expression:n,original:a}}}var pt="DEFAULT",Ae=["ignore","ref","data","id","radio","tabs","switch","disclosure","menu","listbox","list","item","combobox","bind","init","for","mask","model","modelable","transition","show","if",pt,"teleport"];function yn(e,t){let r=Ae.indexOf(e.type)===-1?pt:e.type,n=Ae.indexOf(t.type)===-1?pt:t.type;return Ae.indexOf(r)-Ae.indexOf(n)}function z(e,t,r={}){e.dispatchEvent(new CustomEvent(t,{detail:r,bubbles:!0,composed:!0,cancelable:!0}))}var mt=[],ht=!1;function Te(e=()=>{}){return queueMicrotask(()=>{ht||setTimeout(()=>{Oe()})}),new Promise(t=>{mt.push(()=>{e(),t()})})}function Oe(){for(ht=!1;mt.length;)mt.shift()()}function ar(){ht=!0}function R(e,t){if(typeof ShadowRoot=="function"&&e instanceof ShadowRoot){Array.from(e.children).forEach(i=>R(i,t));return}let r=!1;if(t(e,()=>r=!0),r)return;let n=e.firstElementChild;for(;n;)R(n,t,!1),n=n.nextElementSibling}function O(e,...t){console.warn(`Alpine Warning: ${e}`,...t)}function lr(){document.body||O("Unable to initialize. Trying to load Alpine before `<body>` is available. Did you forget to add `defer` in Alpine's `<script>` tag?"),z(document,"alpine:init"),z(document,"alpine:initializing"),rt(),Vt(t=>w(t,R)),_e(t=>bn(t)),Ht((t,r)=>{ne(t,r).forEach(n=>n())});let e=t=>!V(t.parentElement,!0);Array.from(document.querySelectorAll(cr())).filter(e).forEach(t=>{w(t)}),z(document,"alpine:initialized")}var _t=[],ur=[];function fr(){return _t.map(e=>e())}function cr(){return _t.concat(ur).map(e=>e())}function Ce(e){_t.push(e)}function Re(e){ur.push(e)}function V(e,t=!1){return Q(e,r=>{if((t?cr():fr()).some(i=>r.matches(i)))return!0})}function Q(e,t){if(!!e){if(t(e))return e;if(e._x_teleportBack&&(e=e._x_teleportBack),!!e.parentElement)return Q(e.parentElement,t)}}function dr(e){return fr().some(t=>e.matches(t))}function w(e,t=R){ir(()=>{t(e,(r,n)=>{ne(r,r.attributes).forEach(i=>i()),r._x_ignore&&n()})})}function bn(e){R(e,t=>Qe(t))}function oe(e,t){return Array.isArray(t)?pr(e,t.join(" ")):typeof t=="object"&&t!==null?vn(e,t):typeof t=="function"?oe(e,t()):pr(e,t)}function pr(e,t){let r=o=>o.split(" ").filter(Boolean),n=o=>o.split(" ").filter(s=>!e.classList.contains(s)).filter(Boolean),i=o=>(e.classList.add(...o),()=>{e.classList.remove(...o)});return t=t===!0?t="":t||"",i(n(t))}function vn(e,t){let r=a=>a.split(" ").filter(Boolean),n=Object.entries(t).flatMap(([a,c])=>c?r(a):!1).filter(Boolean),i=Object.entries(t).flatMap(([a,c])=>c?!1:r(a)).filter(Boolean),o=[],s=[];return i.forEach(a=>{e.classList.contains(a)&&(e.classList.remove(a),s.push(a))}),n.forEach(a=>{e.classList.contains(a)||(e.classList.add(a),o.push(a))}),()=>{s.forEach(a=>e.classList.add(a)),o.forEach(a=>e.classList.remove(a))}}function H(e,t){return typeof t=="object"&&t!==null?wn(e,t):En(e,t)}function wn(e,t){let r={};return Object.entries(t).forEach(([n,i])=>{r[n]=e.style[n],n.startsWith("--")||(n=Sn(n)),e.style.setProperty(n,i)}),setTimeout(()=>{e.style.length===0&&e.removeAttribute("style")}),()=>{H(e,r)}}function En(e,t){let r=e.getAttribute("style",t);return e.setAttribute("style",t),()=>{e.setAttribute("style",r||"")}}function Sn(e){return e.replace(/([a-z])([A-Z])/g,"$1-$2").toLowerCase()}function se(e,t=()=>{}){let r=!1;return function(){r?t.apply(this,arguments):(r=!0,e.apply(this,arguments))}}d("transition",(e,{value:t,modifiers:r,expression:n},{evaluate:i})=>{typeof n=="function"&&(n=i(n)),n?An(e,n,t):On(e,r,t)});function An(e,t,r){mr(e,oe,""),{enter:i=>{e._x_transition.enter.during=i},"enter-start":i=>{e._x_transition.enter.start=i},"enter-end":i=>{e._x_transition.enter.end=i},leave:i=>{e._x_transition.leave.during=i},"leave-start":i=>{e._x_transition.leave.start=i},"leave-end":i=>{e._x_transition.leave.end=i}}[r](t)}function On(e,t,r){mr(e,H);let n=!t.includes("in")&&!t.includes("out")&&!r,i=n||t.includes("in")||["enter"].includes(r),o=n||t.includes("out")||["leave"].includes(r);t.includes("in")&&!n&&(t=t.filter((h,b)=>b<t.indexOf("out"))),t.includes("out")&&!n&&(t=t.filter((h,b)=>b>t.indexOf("out")));let s=!t.includes("opacity")&&!t.includes("scale"),a=s||t.includes("opacity"),c=s||t.includes("scale"),l=a?0:1,u=c?ae(t,"scale",95)/100:1,p=ae(t,"delay",0),y=ae(t,"origin","center"),N="opacity, transform",W=ae(t,"duration",150)/1e3,pe=ae(t,"duration",75)/1e3,f="cubic-bezier(0.4, 0.0, 0.2, 1)";i&&(e._x_transition.enter.during={transformOrigin:y,transitionDelay:p,transitionProperty:N,transitionDuration:`${W}s`,transitionTimingFunction:f},e._x_transition.enter.start={opacity:l,transform:`scale(${u})`},e._x_transition.enter.end={opacity:1,transform:"scale(1)"}),o&&(e._x_transition.leave.during={transformOrigin:y,transitionDelay:p,transitionProperty:N,transitionDuration:`${pe}s`,transitionTimingFunction:f},e._x_transition.leave.start={opacity:1,transform:"scale(1)"},e._x_transition.leave.end={opacity:l,transform:`scale(${u})`})}function mr(e,t,r={}){e._x_transition||(e._x_transition={enter:{during:r,start:r,end:r},leave:{during:r,start:r,end:r},in(n=()=>{},i=()=>{}){Me(e,t,{during:this.enter.during,start:this.enter.start,end:this.enter.end},n,i)},out(n=()=>{},i=()=>{}){Me(e,t,{during:this.leave.during,start:this.leave.start,end:this.leave.end},n,i)}})}window.Element.prototype._x_toggleAndCascadeWithTransitions=function(e,t,r,n){let i=document.visibilityState==="visible"?requestAnimationFrame:setTimeout,o=()=>i(r);if(t){e._x_transition&&(e._x_transition.enter||e._x_transition.leave)?e._x_transition.enter&&(Object.entries(e._x_transition.enter.during).length||Object.entries(e._x_transition.enter.start).length||Object.entries(e._x_transition.enter.end).length)?e._x_transition.in(r):o():e._x_transition?e._x_transition.in(r):o();return}e._x_hidePromise=e._x_transition?new Promise((s,a)=>{e._x_transition.out(()=>{},()=>s(n)),e._x_transitioning.beforeCancel(()=>a({isFromCancelledTransition:!0}))}):Promise.resolve(n),queueMicrotask(()=>{let s=hr(e);s?(s._x_hideChildren||(s._x_hideChildren=[]),s._x_hideChildren.push(e)):i(()=>{let a=c=>{let l=Promise.all([c._x_hidePromise,...(c._x_hideChildren||[]).map(a)]).then(([u])=>u());return delete c._x_hidePromise,delete c._x_hideChildren,l};a(e).catch(c=>{if(!c.isFromCancelledTransition)throw c})})})};function hr(e){let t=e.parentNode;if(!!t)return t._x_hidePromise?t:hr(t)}function Me(e,t,{during:r,start:n,end:i}={},o=()=>{},s=()=>{}){if(e._x_transitioning&&e._x_transitioning.cancel(),Object.keys(r).length===0&&Object.keys(n).length===0&&Object.keys(i).length===0){o(),s();return}let a,c,l;Tn(e,{start(){a=t(e,n)},during(){c=t(e,r)},before:o,end(){a(),l=t(e,i)},after:s,cleanup(){c(),l()}})}function Tn(e,t){let r,n,i,o=se(()=>{m(()=>{r=!0,n||t.before(),i||(t.end(),Oe()),t.after(),e.isConnected&&t.cleanup(),delete e._x_transitioning})});e._x_transitioning={beforeCancels:[],beforeCancel(s){this.beforeCancels.push(s)},cancel:se(function(){for(;this.beforeCancels.length;)this.beforeCancels.shift()();o()}),finish:o},m(()=>{t.start(),t.during()}),ar(),requestAnimationFrame(()=>{if(r)return;let s=Number(getComputedStyle(e).transitionDuration.replace(/,.*/,"").replace("s",""))*1e3,a=Number(getComputedStyle(e).transitionDelay.replace(/,.*/,"").replace("s",""))*1e3;s===0&&(s=Number(getComputedStyle(e).animationDuration.replace("s",""))*1e3),m(()=>{t.before()}),n=!0,requestAnimationFrame(()=>{r||(m(()=>{t.end()}),Oe(),setTimeout(e._x_transitioning.finish,s+a),i=!0)})})}function ae(e,t,r){if(e.indexOf(t)===-1)return r;let n=e[e.indexOf(t)+1];if(!n||t==="scale"&&isNaN(n))return r;if(t==="duration"){let i=n.match(/([0-9]+)ms/);if(i)return i[1]}return t==="origin"&&["top","right","left","center","bottom"].includes(e[e.indexOf(t)+2])?[n,e[e.indexOf(t)+2]].join(" "):n}var gt=!1;function $(e,t=()=>{}){return(...r)=>gt?t(...r):e(...r)}function _r(e,t){t._x_dataStack||(t._x_dataStack=e._x_dataStack),gt=!0,Rn(()=>{Cn(t)}),gt=!1}function Cn(e){let t=!1;w(e,(n,i)=>{R(n,(o,s)=>{if(t&&dr(o))return s();t=!0,i(o,s)})})}function Rn(e){let t=K;Ze((r,n)=>{let i=t(r);return Y(i),()=>{}}),e(),Ze(t)}function ce(e,t,r,n=[]){switch(e._x_bindings||(e._x_bindings=A({})),e._x_bindings[t]=r,t=n.includes("camel")?Dn(t):t,t){case"value":Mn(e,r);break;case"style":Pn(e,r);break;case"class":Nn(e,r);break;default:kn(e,t,r);break}}function Mn(e,t){if(e.type==="radio")e.attributes.value===void 0&&(e.value=t),window.fromModel&&(e.checked=gr(e.value,t));else if(e.type==="checkbox")Number.isInteger(t)?e.value=t:!Number.isInteger(t)&&!Array.isArray(t)&&typeof t!="boolean"&&![null,void 0].includes(t)?e.value=String(t):Array.isArray(t)?e.checked=t.some(r=>gr(r,e.value)):e.checked=!!t;else if(e.tagName==="SELECT")In(e,t);else{if(e.value===t)return;e.value=t}}function Nn(e,t){e._x_undoAddedClasses&&e._x_undoAddedClasses(),e._x_undoAddedClasses=oe(e,t)}function Pn(e,t){e._x_undoAddedStyles&&e._x_undoAddedStyles(),e._x_undoAddedStyles=H(e,t)}function kn(e,t,r){[null,void 0,!1].includes(r)&&Ln(t)?e.removeAttribute(t):(xr(t)&&(r=t),$n(e,t,r))}function $n(e,t,r){e.getAttribute(t)!=r&&e.setAttribute(t,r)}function In(e,t){let r=[].concat(t).map(n=>n+"");Array.from(e.options).forEach(n=>{n.selected=r.includes(n.value)})}function Dn(e){return e.toLowerCase().replace(/-(\w)/g,(t,r)=>r.toUpperCase())}function gr(e,t){return e==t}function xr(e){return["disabled","checked","required","readonly","hidden","open","selected","autofocus","itemscope","multiple","novalidate","allowfullscreen","allowpaymentrequest","formnovalidate","autoplay","controls","loop","muted","playsinline","default","ismap","reversed","async","defer","nomodule"].includes(e)}function Ln(e){return!["aria-pressed","aria-checked","aria-expanded","aria-selected"].includes(e)}function yr(e,t,r){if(e._x_bindings&&e._x_bindings[t]!==void 0)return e._x_bindings[t];let n=e.getAttribute(t);return n===null?typeof r=="function"?r():r:n===""?!0:xr(t)?!![t,"true"].includes(n):n}function Ne(e,t){var r;return function(){var n=this,i=arguments,o=function(){r=null,e.apply(n,i)};clearTimeout(r),r=setTimeout(o,t)}}function Pe(e,t){let r;return function(){let n=this,i=arguments;r||(e.apply(n,i),r=!0,setTimeout(()=>r=!1,t))}}function br(e){e(I)}var q={},vr=!1;function wr(e,t){if(vr||(q=A(q),vr=!0),t===void 0)return q[e];q[e]=t,typeof t=="object"&&t!==null&&t.hasOwnProperty("init")&&typeof t.init=="function"&&q[e].init(),ye(q[e])}function Er(){return q}var Sr={};function Ar(e,t){let r=typeof t!="function"?()=>t:t;e instanceof Element?xt(e,r()):Sr[e]=r}function Or(e){return Object.entries(Sr).forEach(([t,r])=>{Object.defineProperty(e,t,{get(){return(...n)=>r(...n)}})}),e}function xt(e,t,r){let n=[];for(;n.length;)n.pop()();let i=Object.entries(t).map(([s,a])=>({name:s,value:a})),o=ft(i);i=i.map(s=>o.find(a=>a.name===s.name)?{name:`x-bind:${s.name}`,value:`"${s.value}"`}:s),ne(e,i,r).map(s=>{n.push(s.runCleanups),s()})}var Tr={};function Cr(e,t){Tr[e]=t}function Rr(e,t){return Object.entries(Tr).forEach(([r,n])=>{Object.defineProperty(e,r,{get(){return(...i)=>n.bind(t)(...i)},enumerable:!1})}),e}var jn={get reactive(){return A},get release(){return Y},get effect(){return K},get raw(){return Ye},version:"3.10.5",flushAndStopDeferringMutations:Wt,dontAutoEvaluateFunctions:Jt,disableEffectScheduling:Lt,setReactivityEngine:jt,closestDataStack:k,skipDuringClone:$,addRootSelector:Ce,addInitSelector:Re,addScopeToNode:C,deferMutations:Ut,mapAttributes:Z,evaluateLater:g,setEvaluator:Qt,mergeProxies:D,findClosest:Q,closestRoot:V,interceptor:be,transition:Me,setStyles:H,mutateDom:m,directive:d,throttle:Pe,debounce:Ne,evaluate:P,initTree:w,nextTick:Te,prefixed:E,prefix:Xt,plugin:br,magic:x,store:wr,start:lr,clone:_r,bound:yr,$data:xe,data:Cr,bind:Ar},I=jn;function yt(e,t){let r=Object.create(null),n=e.split(",");for(let i=0;i<n.length;i++)r[n[i]]=!0;return t?i=>!!r[i.toLowerCase()]:i=>!!r[i]}var ns={[1]:"TEXT",[2]:"CLASS",[4]:"STYLE",[8]:"PROPS",[16]:"FULL_PROPS",[32]:"HYDRATE_EVENTS",[64]:"STABLE_FRAGMENT",[128]:"KEYED_FRAGMENT",[256]:"UNKEYED_FRAGMENT",[512]:"NEED_PATCH",[1024]:"DYNAMIC_SLOTS",[2048]:"DEV_ROOT_FRAGMENT",[-1]:"HOISTED",[-2]:"BAIL"},is={[1]:"STABLE",[2]:"DYNAMIC",[3]:"FORWARDED"};var Fn="itemscope,allowfullscreen,formnovalidate,ismap,nomodule,novalidate,readonly";var os=yt(Fn+",async,autofocus,autoplay,controls,default,defer,disabled,hidden,loop,open,required,reversed,scoped,seamless,checked,muted,multiple,selected");var Mr=Object.freeze({}),ss=Object.freeze([]);var bt=Object.assign;var Bn=Object.prototype.hasOwnProperty,le=(e,t)=>Bn.call(e,t),L=Array.isArray,X=e=>Nr(e)==="[object Map]";var Kn=e=>typeof e=="string",ke=e=>typeof e=="symbol",ue=e=>e!==null&&typeof e=="object";var zn=Object.prototype.toString,Nr=e=>zn.call(e),vt=e=>Nr(e).slice(8,-1);var De=e=>Kn(e)&&e!=="NaN"&&e[0]!=="-"&&""+parseInt(e,10)===e;var Ie=e=>{let t=Object.create(null);return r=>t[r]||(t[r]=e(r))},Vn=/-(\w)/g,as=Ie(e=>e.replace(Vn,(t,r)=>r?r.toUpperCase():"")),Hn=/\B([A-Z])/g,cs=Ie(e=>e.replace(Hn,"-$1").toLowerCase()),wt=Ie(e=>e.charAt(0).toUpperCase()+e.slice(1)),ls=Ie(e=>e?`on${wt(e)}`:""),Et=(e,t)=>e!==t&&(e===e||t===t);var St=new WeakMap,fe=[],M,U=Symbol("iterate"),At=Symbol("Map key iterate");function qn(e){return e&&e._isEffect===!0}function Pr(e,t=Mr){qn(e)&&(e=e.raw);let r=Un(e,t);return t.lazy||r(),r}function Dr(e){e.active&&(kr(e),e.options.onStop&&e.options.onStop(),e.active=!1)}var Wn=0;function Un(e,t){let r=function(){if(!r.active)return e();if(!fe.includes(r)){kr(r);try{return Gn(),fe.push(r),M=r,e()}finally{fe.pop(),Ir(),M=fe[fe.length-1]}}};return r.id=Wn++,r.allowRecurse=!!t.allowRecurse,r._isEffect=!0,r.active=!0,r.raw=e,r.deps=[],r.options=t,r}function kr(e){let{deps:t}=e;if(t.length){for(let r=0;r<t.length;r++)t[r].delete(e);t.length=0}}var ee=!0,Ot=[];function Yn(){Ot.push(ee),ee=!1}function Gn(){Ot.push(ee),ee=!0}function Ir(){let e=Ot.pop();ee=e===void 0?!0:e}function T(e,t,r){if(!ee||M===void 0)return;let n=St.get(e);n||St.set(e,n=new Map);let i=n.get(r);i||n.set(r,i=new Set),i.has(M)||(i.add(M),M.deps.push(i),M.options.onTrack&&M.options.onTrack({effect:M,target:e,type:t,key:r}))}function j(e,t,r,n,i,o){let s=St.get(e);if(!s)return;let a=new Set,c=u=>{u&&u.forEach(p=>{(p!==M||p.allowRecurse)&&a.add(p)})};if(t==="clear")s.forEach(c);else if(r==="length"&&L(e))s.forEach((u,p)=>{(p==="length"||p>=n)&&c(u)});else switch(r!==void 0&&c(s.get(r)),t){case"add":L(e)?De(r)&&c(s.get("length")):(c(s.get(U)),X(e)&&c(s.get(At)));break;case"delete":L(e)||(c(s.get(U)),X(e)&&c(s.get(At)));break;case"set":X(e)&&c(s.get(U));break}let l=u=>{u.options.onTrigger&&u.options.onTrigger({effect:u,target:e,key:r,type:t,newValue:n,oldValue:i,oldTarget:o}),u.options.scheduler?u.options.scheduler(u):u()};a.forEach(l)}var Jn=yt("__proto__,__v_isRef,__isVue"),$r=new Set(Object.getOwnPropertyNames(Symbol).map(e=>Symbol[e]).filter(ke)),Zn=$e(),Qn=$e(!1,!0),Xn=$e(!0),ei=$e(!0,!0),Le={};["includes","indexOf","lastIndexOf"].forEach(e=>{let t=Array.prototype[e];Le[e]=function(...r){let n=_(this);for(let o=0,s=this.length;o<s;o++)T(n,"get",o+"");let i=t.apply(n,r);return i===-1||i===!1?t.apply(n,r.map(_)):i}});["push","pop","shift","unshift","splice"].forEach(e=>{let t=Array.prototype[e];Le[e]=function(...r){Yn();let n=t.apply(this,r);return Ir(),n}});function $e(e=!1,t=!1){return function(n,i,o){if(i==="__v_isReactive")return!e;if(i==="__v_isReadonly")return e;if(i==="__v_raw"&&o===(e?t?ri:jr:t?ti:Lr).get(n))return n;let s=L(n);if(!e&&s&&le(Le,i))return Reflect.get(Le,i,o);let a=Reflect.get(n,i,o);return(ke(i)?$r.has(i):Jn(i))||(e||T(n,"get",i),t)?a:Tt(a)?!s||!De(i)?a.value:a:ue(a)?e?Fr(a):je(a):a}}var ni=Br(),ii=Br(!0);function Br(e=!1){return function(r,n,i,o){let s=r[n];if(!e&&(i=_(i),s=_(s),!L(r)&&Tt(s)&&!Tt(i)))return s.value=i,!0;let a=L(r)&&De(n)?Number(n)<r.length:le(r,n),c=Reflect.set(r,n,i,o);return r===_(o)&&(a?Et(i,s)&&j(r,"set",n,i,s):j(r,"add",n,i)),c}}function oi(e,t){let r=le(e,t),n=e[t],i=Reflect.deleteProperty(e,t);return i&&r&&j(e,"delete",t,void 0,n),i}function si(e,t){let r=Reflect.has(e,t);return(!ke(t)||!$r.has(t))&&T(e,"has",t),r}function ai(e){return T(e,"iterate",L(e)?"length":U),Reflect.ownKeys(e)}var Kr={get:Zn,set:ni,deleteProperty:oi,has:si,ownKeys:ai},zr={get:Xn,set(e,t){return console.warn(`Set operation on key "${String(t)}" failed: target is readonly.`,e),!0},deleteProperty(e,t){return console.warn(`Delete operation on key "${String(t)}" failed: target is readonly.`,e),!0}},hs=bt({},Kr,{get:Qn,set:ii}),_s=bt({},zr,{get:ei}),Ct=e=>ue(e)?je(e):e,Rt=e=>ue(e)?Fr(e):e,Mt=e=>e,Fe=e=>Reflect.getPrototypeOf(e);function Be(e,t,r=!1,n=!1){e=e.__v_raw;let i=_(e),o=_(t);t!==o&&!r&&T(i,"get",t),!r&&T(i,"get",o);let{has:s}=Fe(i),a=n?Mt:r?Rt:Ct;if(s.call(i,t))return a(e.get(t));if(s.call(i,o))return a(e.get(o));e!==i&&e.get(t)}function Ke(e,t=!1){let r=this.__v_raw,n=_(r),i=_(e);return e!==i&&!t&&T(n,"has",e),!t&&T(n,"has",i),e===i?r.has(e):r.has(e)||r.has(i)}function ze(e,t=!1){return e=e.__v_raw,!t&&T(_(e),"iterate",U),Reflect.get(e,"size",e)}function Vr(e){e=_(e);let t=_(this);return Fe(t).has.call(t,e)||(t.add(e),j(t,"add",e,e)),this}function qr(e,t){t=_(t);let r=_(this),{has:n,get:i}=Fe(r),o=n.call(r,e);o?Hr(r,n,e):(e=_(e),o=n.call(r,e));let s=i.call(r,e);return r.set(e,t),o?Et(t,s)&&j(r,"set",e,t,s):j(r,"add",e,t),this}function Ur(e){let t=_(this),{has:r,get:n}=Fe(t),i=r.call(t,e);i?Hr(t,r,e):(e=_(e),i=r.call(t,e));let o=n?n.call(t,e):void 0,s=t.delete(e);return i&&j(t,"delete",e,void 0,o),s}function Wr(){let e=_(this),t=e.size!==0,r=X(e)?new Map(e):new Set(e),n=e.clear();return t&&j(e,"clear",void 0,void 0,r),n}function Ve(e,t){return function(n,i){let o=this,s=o.__v_raw,a=_(s),c=t?Mt:e?Rt:Ct;return!e&&T(a,"iterate",U),s.forEach((l,u)=>n.call(i,c(l),c(u),o))}}function He(e,t,r){return function(...n){let i=this.__v_raw,o=_(i),s=X(o),a=e==="entries"||e===Symbol.iterator&&s,c=e==="keys"&&s,l=i[e](...n),u=r?Mt:t?Rt:Ct;return!t&&T(o,"iterate",c?At:U),{next(){let{value:p,done:y}=l.next();return y?{value:p,done:y}:{value:a?[u(p[0]),u(p[1])]:u(p),done:y}},[Symbol.iterator](){return this}}}}function F(e){return function(...t){{let r=t[0]?`on key "${t[0]}" `:"";console.warn(`${wt(e)} operation ${r}failed: target is readonly.`,_(this))}return e==="delete"?!1:this}}var Gr={get(e){return Be(this,e)},get size(){return ze(this)},has:Ke,add:Vr,set:qr,delete:Ur,clear:Wr,forEach:Ve(!1,!1)},Yr={get(e){return Be(this,e,!1,!0)},get size(){return ze(this)},has:Ke,add:Vr,set:qr,delete:Ur,clear:Wr,forEach:Ve(!1,!0)},Jr={get(e){return Be(this,e,!0)},get size(){return ze(this,!0)},has(e){return Ke.call(this,e,!0)},add:F("add"),set:F("set"),delete:F("delete"),clear:F("clear"),forEach:Ve(!0,!1)},Zr={get(e){return Be(this,e,!0,!0)},get size(){return ze(this,!0)},has(e){return Ke.call(this,e,!0)},add:F("add"),set:F("set"),delete:F("delete"),clear:F("clear"),forEach:Ve(!0,!0)},ci=["keys","values","entries",Symbol.iterator];ci.forEach(e=>{Gr[e]=He(e,!1,!1),Jr[e]=He(e,!0,!1),Yr[e]=He(e,!1,!0),Zr[e]=He(e,!0,!0)});function qe(e,t){let r=t?e?Zr:Yr:e?Jr:Gr;return(n,i,o)=>i==="__v_isReactive"?!e:i==="__v_isReadonly"?e:i==="__v_raw"?n:Reflect.get(le(r,i)&&i in n?r:n,i,o)}var li={get:qe(!1,!1)},gs={get:qe(!1,!0)},ui={get:qe(!0,!1)},xs={get:qe(!0,!0)};function Hr(e,t,r){let n=_(r);if(n!==r&&t.call(e,n)){let i=vt(e);console.warn(`Reactive ${i} contains both the raw and reactive versions of the same object${i==="Map"?" as keys":""}, which can lead to inconsistencies. Avoid differentiating between the raw and reactive versions of an object and only use the reactive version if possible.`)}}var Lr=new WeakMap,ti=new WeakMap,jr=new WeakMap,ri=new WeakMap;function fi(e){switch(e){case"Object":case"Array":return 1;case"Map":case"Set":case"WeakMap":case"WeakSet":return 2;default:return 0}}function di(e){return e.__v_skip||!Object.isExtensible(e)?0:fi(vt(e))}function je(e){return e&&e.__v_isReadonly?e:Qr(e,!1,Kr,li,Lr)}function Fr(e){return Qr(e,!0,zr,ui,jr)}function Qr(e,t,r,n,i){if(!ue(e))return console.warn(`value cannot be made reactive: ${String(e)}`),e;if(e.__v_raw&&!(t&&e.__v_isReactive))return e;let o=i.get(e);if(o)return o;let s=di(e);if(s===0)return e;let a=new Proxy(e,s===2?n:r);return i.set(e,a),a}function _(e){return e&&_(e.__v_raw)||e}function Tt(e){return Boolean(e&&e.__v_isRef===!0)}x("nextTick",()=>Te);x("dispatch",e=>z.bind(z,e));x("watch",(e,{evaluateLater:t,effect:r})=>(n,i)=>{let o=t(n),s=!0,a,c=r(()=>o(l=>{JSON.stringify(l),s?a=l:queueMicrotask(()=>{i(l,a),a=l}),s=!1}));e._x_effects.delete(c)});x("store",Er);x("data",e=>xe(e));x("root",e=>V(e));x("refs",e=>(e._x_refs_proxy||(e._x_refs_proxy=D(pi(e))),e._x_refs_proxy));function pi(e){let t=[],r=e;for(;r;)r._x_refs&&t.push(r._x_refs),r=r.parentNode;return t}var Nt={};function Pt(e){return Nt[e]||(Nt[e]=0),++Nt[e]}function Xr(e,t){return Q(e,r=>{if(r._x_ids&&r._x_ids[t])return!0})}function en(e,t){e._x_ids||(e._x_ids={}),e._x_ids[t]||(e._x_ids[t]=Pt(t))}x("id",e=>(t,r=null)=>{let n=Xr(e,t),i=n?n._x_ids[t]:Pt(t);return r?`${t}-${i}-${r}`:`${t}-${i}`});x("el",e=>e);tn("Focus","focus","focus");tn("Persist","persist","persist");function tn(e,t,r){x(t,n=>O(`You can't use [$${directiveName}] without first installing the "${e}" plugin here: https://alpinejs.dev/plugins/${r}`,n))}d("modelable",(e,{expression:t},{effect:r,evaluateLater:n})=>{let i=n(t),o=()=>{let l;return i(u=>l=u),l},s=n(`${t} = __placeholder`),a=l=>s(()=>{},{scope:{__placeholder:l}}),c=o();a(c),queueMicrotask(()=>{if(!e._x_model)return;e._x_removeModelListeners.default();let l=e._x_model.get,u=e._x_model.set;r(()=>a(l())),r(()=>u(o()))})});d("teleport",(e,{expression:t},{cleanup:r})=>{e.tagName.toLowerCase()!=="template"&&O("x-teleport can only be used on a <template> tag",e);let n=document.querySelector(t);n||O(`Cannot find x-teleport element for selector: "${t}"`);let i=e.content.cloneNode(!0).firstElementChild;e._x_teleport=i,i._x_teleportBack=e,e._x_forwardEvents&&e._x_forwardEvents.forEach(o=>{i.addEventListener(o,s=>{s.stopPropagation(),e.dispatchEvent(new s.constructor(s.type,s))})}),C(i,{},e),m(()=>{n.appendChild(i),w(i),i._x_ignore=!0}),r(()=>i.remove())});var rn=()=>{};rn.inline=(e,{modifiers:t},{cleanup:r})=>{t.includes("self")?e._x_ignoreSelf=!0:e._x_ignore=!0,r(()=>{t.includes("self")?delete e._x_ignoreSelf:delete e._x_ignore})};d("ignore",rn);d("effect",(e,{expression:t},{effect:r})=>r(g(e,t)));function de(e,t,r,n){let i=e,o=c=>n(c),s={},a=(c,l)=>u=>l(c,u);if(r.includes("dot")&&(t=mi(t)),r.includes("camel")&&(t=hi(t)),r.includes("passive")&&(s.passive=!0),r.includes("capture")&&(s.capture=!0),r.includes("window")&&(i=window),r.includes("document")&&(i=document),r.includes("prevent")&&(o=a(o,(c,l)=>{l.preventDefault(),c(l)})),r.includes("stop")&&(o=a(o,(c,l)=>{l.stopPropagation(),c(l)})),r.includes("self")&&(o=a(o,(c,l)=>{l.target===e&&c(l)})),(r.includes("away")||r.includes("outside"))&&(i=document,o=a(o,(c,l)=>{e.contains(l.target)||l.target.isConnected!==!1&&(e.offsetWidth<1&&e.offsetHeight<1||e._x_isShown!==!1&&c(l))})),r.includes("once")&&(o=a(o,(c,l)=>{c(l),i.removeEventListener(t,o,s)})),o=a(o,(c,l)=>{_i(t)&&gi(l,r)||c(l)}),r.includes("debounce")){let c=r[r.indexOf("debounce")+1]||"invalid-wait",l=kt(c.split("ms")[0])?Number(c.split("ms")[0]):250;o=Ne(o,l)}if(r.includes("throttle")){let c=r[r.indexOf("throttle")+1]||"invalid-wait",l=kt(c.split("ms")[0])?Number(c.split("ms")[0]):250;o=Pe(o,l)}return i.addEventListener(t,o,s),()=>{i.removeEventListener(t,o,s)}}function mi(e){return e.replace(/-/g,".")}function hi(e){return e.toLowerCase().replace(/-(\w)/g,(t,r)=>r.toUpperCase())}function kt(e){return!Array.isArray(e)&&!isNaN(e)}function xi(e){return e.replace(/([a-z])([A-Z])/g,"$1-$2").replace(/[_\s]/,"-").toLowerCase()}function _i(e){return["keydown","keyup"].includes(e)}function gi(e,t){let r=t.filter(o=>!["window","document","prevent","stop","once"].includes(o));if(r.includes("debounce")){let o=r.indexOf("debounce");r.splice(o,kt((r[o+1]||"invalid-wait").split("ms")[0])?2:1)}if(r.length===0||r.length===1&&nn(e.key).includes(r[0]))return!1;let i=["ctrl","shift","alt","meta","cmd","super"].filter(o=>r.includes(o));return r=r.filter(o=>!i.includes(o)),!(i.length>0&&i.filter(s=>((s==="cmd"||s==="super")&&(s="meta"),e[`${s}Key`])).length===i.length&&nn(e.key).includes(r[0]))}function nn(e){if(!e)return[];e=xi(e);let t={ctrl:"control",slash:"/",space:"-",spacebar:"-",cmd:"meta",esc:"escape",up:"arrow-up",down:"arrow-down",left:"arrow-left",right:"arrow-right",period:".",equal:"="};return t[e]=e,Object.keys(t).map(r=>{if(t[r]===e)return r}).filter(r=>r)}d("model",(e,{modifiers:t,expression:r},{effect:n,cleanup:i})=>{let o=g(e,r),s=`${r} = rightSideOfExpression($event, ${r})`,a=g(e,s);var c=e.tagName.toLowerCase()==="select"||["checkbox","radio"].includes(e.type)||t.includes("lazy")?"change":"input";let l=yi(e,t,r),u=de(e,c,t,y=>{a(()=>{},{scope:{$event:y,rightSideOfExpression:l}})});e._x_removeModelListeners||(e._x_removeModelListeners={}),e._x_removeModelListeners.default=u,i(()=>e._x_removeModelListeners.default());let p=g(e,`${r} = __placeholder`);e._x_model={get(){let y;return o(N=>y=N),y},set(y){p(()=>{},{scope:{__placeholder:y}})}},e._x_forceModelUpdate=()=>{o(y=>{y===void 0&&r.match(/\./)&&(y=""),window.fromModel=!0,m(()=>ce(e,"value",y)),delete window.fromModel})},n(()=>{t.includes("unintrusive")&&document.activeElement.isSameNode(e)||e._x_forceModelUpdate()})});function yi(e,t,r){return e.type==="radio"&&m(()=>{e.hasAttribute("name")||e.setAttribute("name",r)}),(n,i)=>m(()=>{if(n instanceof CustomEvent&&n.detail!==void 0)return n.detail||n.target.value;if(e.type==="checkbox")if(Array.isArray(i)){let o=t.includes("number")?Dt(n.target.value):n.target.value;return n.target.checked?i.concat([o]):i.filter(s=>!bi(s,o))}else return n.target.checked;else{if(e.tagName.toLowerCase()==="select"&&e.multiple)return t.includes("number")?Array.from(n.target.selectedOptions).map(o=>{let s=o.value||o.text;return Dt(s)}):Array.from(n.target.selectedOptions).map(o=>o.value||o.text);{let o=n.target.value;return t.includes("number")?Dt(o):t.includes("trim")?o.trim():o}}})}function Dt(e){let t=e?parseFloat(e):null;return vi(t)?t:e}function bi(e,t){return e==t}function vi(e){return!Array.isArray(e)&&!isNaN(e)}d("cloak",e=>queueMicrotask(()=>m(()=>e.removeAttribute(E("cloak")))));Re(()=>`[${E("init")}]`);d("init",$((e,{expression:t},{evaluate:r})=>typeof t=="string"?!!t.trim()&&r(t,{},!1):r(t,{},!1)));d("text",(e,{expression:t},{effect:r,evaluateLater:n})=>{let i=n(t);r(()=>{i(o=>{m(()=>{e.textContent=o})})})});d("html",(e,{expression:t},{effect:r,evaluateLater:n})=>{let i=n(t);r(()=>{i(o=>{m(()=>{e.innerHTML=o,e._x_ignoreSelf=!0,w(e),delete e._x_ignoreSelf})})})});Z(Ee(":",Se(E("bind:"))));d("bind",(e,{value:t,modifiers:r,expression:n,original:i},{effect:o})=>{if(!t){let a={};Or(a),g(e,n)(l=>{xt(e,l,i)},{scope:a});return}if(t==="key")return wi(e,n);let s=g(e,n);o(()=>s(a=>{a===void 0&&typeof n=="string"&&n.match(/\./)&&(a=""),m(()=>ce(e,t,a,r))}))});function wi(e,t){e._x_keyExpression=t}Ce(()=>`[${E("data")}]`);d("data",$((e,{expression:t},{cleanup:r})=>{t=t===""?"{}":t;let n={};re(n,e);let i={};Rr(i,n);let o=P(e,t,{scope:i});o===void 0&&(o={}),re(o,e);let s=A(o);ye(s);let a=C(e,s);s.init&&P(e,s.init),r(()=>{s.destroy&&P(e,s.destroy),a()})}));d("show",(e,{modifiers:t,expression:r},{effect:n})=>{let i=g(e,r);e._x_doHide||(e._x_doHide=()=>{m(()=>{e.style.setProperty("display","none",t.includes("important")?"important":void 0)})}),e._x_doShow||(e._x_doShow=()=>{m(()=>{e.style.length===1&&e.style.display==="none"?e.removeAttribute("style"):e.style.removeProperty("display")})});let o=()=>{e._x_doHide(),e._x_isShown=!1},s=()=>{e._x_doShow(),e._x_isShown=!0},a=()=>setTimeout(s),c=se(p=>p?s():o(),p=>{typeof e._x_toggleAndCascadeWithTransitions=="function"?e._x_toggleAndCascadeWithTransitions(e,p,s,o):p?a():o()}),l,u=!0;n(()=>i(p=>{!u&&p===l||(t.includes("immediate")&&(p?a():o()),c(p),l=p,u=!1)}))});d("for",(e,{expression:t},{effect:r,cleanup:n})=>{let i=Si(t),o=g(e,i.items),s=g(e,e._x_keyExpression||"index");e._x_prevKeys=[],e._x_lookup={},r(()=>Ei(e,i,o,s)),n(()=>{Object.values(e._x_lookup).forEach(a=>a.remove()),delete e._x_prevKeys,delete e._x_lookup})});function Ei(e,t,r,n){let i=s=>typeof s=="object"&&!Array.isArray(s),o=e;r(s=>{Ai(s)&&s>=0&&(s=Array.from(Array(s).keys(),f=>f+1)),s===void 0&&(s=[]);let a=e._x_lookup,c=e._x_prevKeys,l=[],u=[];if(i(s))s=Object.entries(s).map(([f,h])=>{let b=on(t,h,f,s);n(v=>u.push(v),{scope:{index:f,...b}}),l.push(b)});else for(let f=0;f<s.length;f++){let h=on(t,s[f],f,s);n(b=>u.push(b),{scope:{index:f,...h}}),l.push(h)}let p=[],y=[],N=[],W=[];for(let f=0;f<c.length;f++){let h=c[f];u.indexOf(h)===-1&&N.push(h)}c=c.filter(f=>!N.includes(f));let pe="template";for(let f=0;f<u.length;f++){let h=u[f],b=c.indexOf(h);if(b===-1)c.splice(f,0,h),p.push([pe,f]);else if(b!==f){let v=c.splice(f,1)[0],S=c.splice(b-1,1)[0];c.splice(f,0,S),c.splice(b,0,v),y.push([v,S])}else W.push(h);pe=h}for(let f=0;f<N.length;f++){let h=N[f];a[h]._x_effects&&a[h]._x_effects.forEach(he),a[h].remove(),a[h]=null,delete a[h]}for(let f=0;f<y.length;f++){let[h,b]=y[f],v=a[h],S=a[b],G=document.createElement("div");m(()=>{S.after(G),v.after(S),S._x_currentIfEl&&S.after(S._x_currentIfEl),G.before(v),v._x_currentIfEl&&v.after(v._x_currentIfEl),G.remove()}),ot(S,l[u.indexOf(b)])}for(let f=0;f<p.length;f++){let[h,b]=p[f],v=h==="template"?o:a[h];v._x_currentIfEl&&(v=v._x_currentIfEl);let S=l[b],G=u[b],me=document.importNode(o.content,!0).firstElementChild;C(me,A(S),o),m(()=>{v.after(me),w(me)}),typeof G=="object"&&O("x-for key cannot be an object, it must be a string or an integer",o),a[G]=me}for(let f=0;f<W.length;f++)ot(a[W[f]],l[u.indexOf(W[f])]);o._x_prevKeys=u})}function Si(e){let t=/,([^,\}\]]*)(?:,([^,\}\]]*))?$/,r=/^\s*\(|\)\s*$/g,n=/([\s\S]*?)\s+(?:in|of)\s+([\s\S]*)/,i=e.match(n);if(!i)return;let o={};o.items=i[2].trim();let s=i[1].replace(r,"").trim(),a=s.match(t);return a?(o.item=s.replace(t,"").trim(),o.index=a[1].trim(),a[2]&&(o.collection=a[2].trim())):o.item=s,o}function on(e,t,r,n){let i={};return/^\[.*\]$/.test(e.item)&&Array.isArray(t)?e.item.replace("[","").replace("]","").split(",").map(s=>s.trim()).forEach((s,a)=>{i[s]=t[a]}):/^\{.*\}$/.test(e.item)&&!Array.isArray(t)&&typeof t=="object"?e.item.replace("{","").replace("}","").split(",").map(s=>s.trim()).forEach(s=>{i[s]=t[s]}):i[e.item]=t,e.index&&(i[e.index]=r),e.collection&&(i[e.collection]=n),i}function Ai(e){return!Array.isArray(e)&&!isNaN(e)}function sn(){}sn.inline=(e,{expression:t},{cleanup:r})=>{let n=V(e);n._x_refs||(n._x_refs={}),n._x_refs[t]=e,r(()=>delete n._x_refs[t])};d("ref",sn);d("if",(e,{expression:t},{effect:r,cleanup:n})=>{let i=g(e,t),o=()=>{if(e._x_currentIfEl)return e._x_currentIfEl;let a=e.content.cloneNode(!0).firstElementChild;return C(a,{},e),m(()=>{e.after(a),w(a)}),e._x_currentIfEl=a,e._x_undoIf=()=>{R(a,c=>{c._x_effects&&c._x_effects.forEach(he)}),a.remove(),delete e._x_currentIfEl},a},s=()=>{!e._x_undoIf||(e._x_undoIf(),delete e._x_undoIf)};r(()=>i(a=>{a?o():s()})),n(()=>e._x_undoIf&&e._x_undoIf())});d("id",(e,{expression:t},{evaluate:r})=>{r(t).forEach(i=>en(e,i))});Z(Ee("@",Se(E("on:"))));d("on",$((e,{value:t,modifiers:r,expression:n},{cleanup:i})=>{let o=n?g(e,n):()=>{};e.tagName.toLowerCase()==="template"&&(e._x_forwardEvents||(e._x_forwardEvents=[]),e._x_forwardEvents.includes(t)||e._x_forwardEvents.push(t));let s=de(e,t,r,a=>{o(()=>{},{scope:{$event:a},params:[a]})});i(()=>s())}));Ue("Collapse","collapse","collapse");Ue("Intersect","intersect","intersect");Ue("Focus","trap","focus");Ue("Mask","mask","mask");function Ue(e,t,r){d(t,n=>O(`You can't use [x-${t}] without first installing the "${e}" plugin here: https://alpinejs.dev/plugins/${r}`,n))}I.setEvaluator(ct);I.setReactivityEngine({reactive:je,effect:Pr,release:Dr,raw:_});var It=I;window.Alpine=It;queueMicrotask(()=>{It.start()});})();
