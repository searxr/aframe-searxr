  localStorage.setItem("theme", this.opts.theme) // TODO store centrally and elsewhere 
## SEARXR.init()
 
initialize-phase (called during startup)
 
example:
```
// optional: SEARXR.on('init', (init) => init() )
SEARXR.init()
```
 
## SEARXR.destroy()
 
initialize-phase (called during startup)
 
example:
```
// optional: SEARXR.on('destroy', (destroy) => destroy() )
SEARXR.destroy()
```
 
## SEARXR.search(query, result)
 
called when search query is entered by user
 
example:
```
// optional: SEARXR.on('destroy', (destroy) => destroy() )
SEARXR.on('search', (search, query, result) => search(query, result) )
```
 
