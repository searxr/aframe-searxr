the code uses the following functional [KISSJS](https://gist.github.com/coderofsalvation/4933c95edfc97d3756bc159c28eff9b5) paradigm:

```
function SEARXR(){
	_.decorate(this) // add util functions

	return this
}

SEARXR.prototype.init          = function(a){}
SEARXR.prototype._private_func = function(){}

SEARXR.on('init', (init,a) => {
	// before
	init(a)
	//after
})

```
